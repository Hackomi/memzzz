﻿#include "filatovgui.h"
#include "TextureGenerator.h"
enum MenuState : int {
	Close = 0,
	OpenAnimate,
	Open,
	CloseAnimate
};
template<typename T>
void CElement<T>::DrawToolTip()
{
	if (Tooltip && Tooltip != "") {
		int Value = pSetting ? *pSetting : 0, Width, Height;
		static char buffer[1024];
		sprintf(buffer, Tooltip, Data[0].c_str(), Data.size() - 1 <= Value ? std::to_string(Value).c_str() : Data[Value + 1].c_str());
		static wchar_t wbuffer[1024];
		MultiByteToWideChar(CP_UTF8, 0, buffer, 256, wbuffer, 256);
		g_pSurface->GetTextSize(c_ayala::CourierNew, wbuffer, Width, Height);
		g_pSurface->DrawRoundedBox(CMenu::Get().iX + iX - 5, CMenu::Get().iY + iY + 28 - Height / 2, Width + 10, Height + 4, 3, Color(49, 49, 49));
		g_pSurface->DrawT(CMenu::Get().iX + iX, CMenu::Get().iY + iY + 32, Color().White(), c_ayala::CourierNew, false, buffer);
	}
}
template<typename T>
void CElement<T>::Update(char* data, int x, int y, T setting, bool * render, bool * update, int min, int max, int tab, char* tooltip)
{
	iX = x;
	iY = y;
	pSetting = setting;
	pDraw = render;
	pControl = update;
	iMin = min;
	iMax = max;
	iTab = tab;
	Tooltip = tooltip;
	char bufferArray[100];
	strcpy(bufferArray, data);
	char* buffer = strtok(bufferArray, "|");
	while (buffer != NULL) {
		Data.push_back(buffer);
		buffer = strtok(NULL, "|");
	}
}
template<typename T>
void CCheckbox<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		g_pSurface->DrawOutlinedRect(iPosX, iPosY, 19, 19, Color(120, 120, 120));
		g_pSurface->FilledRect(iPosX + 2, iPosY + 2, 15, 15, Color(69, 69, 69)/*, Color(0, 0, 0), false*/);
		if (pSetting && *pSetting)
			g_pSurface->FilledRect(iPosX + 2, iPosY + 2, 15, 15, Color(235, 125, 35)/*, Color(0, 0, 0)*//*Color(abs(255 * sin(100 * *pSetting / iMax)), abs(255 * cos(50 * *pSetting / iMax)), abs(255 * sin(200 * *pSetting / iMax)))*/);
		g_pSurface->DrawT(22 + iPosX, iPosY + 10, Color(255, 255, 255), c_ayala::CourierNew, false, Data[0].c_str());
		if (CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 21 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19)
			DrawToolTip();
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab)
		if (CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 21 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19 && !CMenu::Get().bPress && CMenu::Get().bClick && pSetting && !CMenu::Get().bUse)
			*pSetting = *pSetting == iMax ? iMin : *pSetting + 1;
}


template<typename T>
void CSlider<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;

	int iDeltaX = iMax - iMin;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		if (bChangeValue || ((CMenu::Get().MousePos.x > iPosX - 5 && CMenu::Get().MousePos.x < iPosX + 205 && CMenu::Get().MousePos.y > iPosY - 5 && CMenu::Get().MousePos.y < iPosY + 20) && !CMenu::Get().bUse)) {
			g_pSurface->DrawOutlinedRect(iPosX, iPosY - 2, 200, 14, Color(120, 120, 120));
			g_pSurface->FilledRect(iPosX + 2, iPosY, 196, 10, Color(69, 69, 69)/*, Color(0, 0, 0), false*/);
			if (pSetting) {
				g_pSurface->FilledRect(iPosX + 2, iPosY, 196 * *pSetting / iDeltaX, 10, Color(235, 125, 35));
				if (bChangeValue)
					g_pSurface->DrawT(iPosX + 196 * *pSetting / iDeltaX, iPosY + 15, Color(255, 255, 255), c_ayala::CourierNew, false, std::to_string(*pSetting).c_str());
				DrawToolTip();
			}
		}
		else
			g_pSurface->DrawT(iPosX, iPosY + 5, Color(255, 255, 255), c_ayala::CourierNew, false, Data[0].c_str());
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		bAccess = bChangeValue;
		bChangeValue = bChangeValue && CMenu::Get().bPress ? true : CMenu::Get().MousePos.x > iPosX + 2 && CMenu::Get().MousePos.x < iPosX + 202 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 10 && CMenu::Get().bPress && !CMenu::Get().bUse;
		if (bChangeValue && pSetting) {
			*pSetting = iDeltaX * (CMenu::Get().MousePos.x - iPosX) / 196;
			if (*pSetting > iMax)
				*pSetting = iMax;
			if (*pSetting < iMin)
				*pSetting = iMin;
			CMenu::Get().bUse = true;
		}
		if (bAccess && !bChangeValue)
			CMenu::Get().bUse = false;
	}
}
template<typename T>
void CDropbox<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;
	int Delta = iMax - iMin;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		g_pSurface->DrawOutlinedRect(iPosX, iPosY - 2, 200, 14, Color(120, 120, 120));
		g_pSurface->FilledRect(iPosX + 2, iPosY, 196, 10, Color(69, 69, 69)/*, Color(0, 0, 0), false*/);
		g_pSurface->DrawT(iPosX + 4, iPosY + 5, Color(255, 255, 255), c_ayala::CourierNew, false, Data[0].c_str());
		if (bChangeValue) {
			g_pSurface->FilledRect(iPosX, iPosY + 12, 200, 14 * (Data.size() - 1) - Data.size(), Color(39, 39, 39));
			for (int i = 1; i < Data.size(); i++) {
				g_pSurface->DrawOutlinedRect(iPosX, iPosY - 2 + 14 * i - i, 200, 14, Color(120, 120, 120));
				g_pSurface->FilledRect(iPosX + 2, iPosY + 14 * i - i, 196, 10, Color(69, 69, 69)/*, Color(0, 0, 0), false*/);
				g_pSurface->DrawT(iPosX + 4, iPosY + 5 + 14 * i - i, Color(255, 255, 255), c_ayala::CourierNew, false, Data[i].c_str());
			}
		}
		if (CMenu::Get().MousePos.x > iPosX + 2 && CMenu::Get().MousePos.x < iPosX + 202 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14 && !bChangeValue)
			DrawToolTip();
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		bAccess = bChangeValue;
		if (CMenu::Get().MousePos.x > iPosX + 2 && CMenu::Get().MousePos.x < iPosX + 202 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14 && !CMenu::Get().bPress && CMenu::Get().bClick && !CMenu::Get().bUse)
			bChangeValue = !bChangeValue;
		if (bChangeValue && pSetting) {
			CMenu::Get().bUse = true;
			for (int i = 1; i < Data.size(); i++) {
				if (CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 202 && CMenu::Get().MousePos.y > iPosY + 14 * i && CMenu::Get().MousePos.y < iPosY + 14 * (i + 1) && !CMenu::Get().bPress && CMenu::Get().bClick && pSetting) {
					*pSetting = i - 1;
					bChangeValue = false;
				}
			}
		}
		if (!(CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 202 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14 * Data.size()) && !CMenu::Get().bPress && CMenu::Get().bClick && bChangeValue)
			bChangeValue = false;
		if (bAccess && !bChangeValue)
			CMenu::Get().bUse = false;
	}
}
template<typename T>
void CColorPicker<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		if (pSetting) {
			g_pSurface->DrawOutlinedRect(iPosX + 176, iPosY, 24, 19, Color(120, 120, 120));
			g_pSurface->FilledRect(iPosX + 178, iPosY + 2, 20, 15, Color(*pSetting, *(pSetting + 1), *(pSetting + 2)));
		}
		if (bChangeValue) {
			iPosY += 20;
			g_pSurface->FilledRect(iPosX + 48, iPosY, 154, 154, Color(29, 29, 29));
			g_pSurface->DrawOutlinedRect(iPosX + 48, iPosY, 154, 154, Color(120, 120, 120));
			static CColorPickerTexture ColorPicker(200, 200);
			
			g_pSurface->SetDrawColor(Color(255, 255, 255, 255));
			g_pSurface->DrawSetTexture(ColorPicker.GetTextureId());
			g_pSurface->DrawTexturedRect(iPosX + 50, iPosY + 2, iPosX + 200, iPosY + 150);
			
			g_pSurface->DrawOutlinedRect(iPosX + 49 + iDeltaX, iPosY + iDeltaY + 1, 3, 3, Color(255, 255, 255));
		}

		if (CMenu::Get().MousePos.x > iPosX + 180 && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14 && !bChangeValue)
			DrawToolTip();
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		bAccess = bChangeValue;
		if (CMenu::Get().MousePos.x > iPosX + 180 && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19 && !CMenu::Get().bPress && CMenu::Get().bClick && !CMenu::Get().bUse)
			bChangeValue = !bChangeValue;
		if (bChangeValue)
			CMenu::Get().bUse = true;
		if (bChangeValue && pSetting && (CMenu::Get().MousePos.x > iPosX + 50 && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 150) && CMenu::Get().bPress) {
			iDeltaX = CMenu::Get().MousePos.x - (iPosX + 50);
			iDeltaY = CMenu::Get().MousePos.y - (iPosY + 2);
			if (iDeltaX > 150)
				iDeltaX = 150;
			if (iDeltaX < 0)
				iDeltaX = 0;
			if (iDeltaY > 150)
				iDeltaY = 150;
			if (iDeltaY < 0)
				iDeltaY = 0;
			Color buffer = Color::FromHSB((float)iDeltaX / 150, iDeltaY < 75 ? (float)iDeltaY / 75 : 1.f, iDeltaY >= 75 ? 1.f - ((float)iDeltaY - 75) / 75 : 1.f);
			*pSetting = buffer.r();
			*(pSetting + 1) = buffer.g();
			*(pSetting + 2) = buffer.b();
		}
		if (!(CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 150) && !CMenu::Get().bPress && CMenu::Get().bClick && bChangeValue)
			bChangeValue = false;
		if (bAccess && !bChangeValue)
			CMenu::Get().bUse = false;
	}

}
char* KeyDigits[254] = { nullptr, "Mouse 1", "Mouse 2", "Control+Break", "Mouse 3", "Mouse 4", "Mouse 5",
nullptr, "Backspace", "TAB", nullptr, nullptr, nullptr, "Enter", nullptr, nullptr, "SHIFT", "CTRL", "ALT", "PAUSE",
"CAPS LOCK", nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, "ESC", nullptr, nullptr, nullptr, nullptr, "Space",
"PG UP", "PG DOWN", "End", "Home", "Left", "Up", "Right", "Down", nullptr, "Print", nullptr, "PRNTSCR", "Insert",
"Delete", nullptr, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
nullptr, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
"Y", "Z", "Left Windows", "Right Windows", nullptr, nullptr, nullptr, "0", "1", "2", "3", "4", "5", "6",
"7", "8", "9", "*", "+", "_", "-", ".", "/", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12",
"F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21", "F22", "F23", "F24", nullptr, nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, "NUM LOCK", "SCROLL LOCK", nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, "LShift", "RShift", "LControl", "RControl", "LALT", "RALT", nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, "Next Track", "Previous Track", "Stop", "Play/Pause", nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, ";", "+", ",", "-", ".", "/?", "~", nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, "[{", "\\|", "}]", "'\"", nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
template<typename T>
void CTextInput<T>::CaptureInput() // © Guzey
{
	if (GetAsyncKeyState(VK_BACK) & 1 && buffer.size() > 0)
		buffer = buffer.substr(0, buffer.size() - 1);

	if (GetAsyncKeyState(VK_BACK) && GetAsyncKeyState(VK_CONTROL))
		buffer = "";

	if (buffer.size() > 40)
		return;

	int iKey;
	for (int i = 0; i < 255; i++)
	{
		if (GetAsyncKeyState(i) & 1)
		{
			iKey = i;
		}
	}

	if (iKey >= 0x30 && iKey <= 0x39) // 0 - 9 KEYS
		buffer += KeyDigits[iKey];

	if ((GetKeyState(VK_CAPITAL) & 0x0001) != 0) //CAPSLOCK => CAPITAL => ONLY A-Z
	{
		if (iKey >= 0x41 && iKey <= 0x5A)
			buffer += KeyDigits[iKey];
	}
	else
	{
		if (GetAsyncKeyState(VK_SHIFT)) //SHIFT => CAPITAL => ONLY A-Z
		{
			if (iKey >= 0x41 && iKey <= 0x5A)
				buffer += KeyDigits[iKey];
		}
		else
		{
			if (iKey >= 0x41 && iKey <= 0x5A)
			{
				std::string key = KeyDigits[iKey];
				for (auto &c : key)
					buffer += tolower(c);
			}
		}
	}
}
template<typename T>
void CTextInput<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		g_pSurface->DrawOutlinedRect(iPosX, iPosY, 200, 14, Color(120, 120, 120));
		g_pSurface->FilledRect(iPosX + 2, iPosY + 2, 196, 10, Color(69, 69, 69));
		g_pSurface->DrawT(iPosX + 3, iPosY + 8, Color(255, 255, 255), c_ayala::CourierNew, false, buffer.c_str());
		if (bChangeValue)
			g_pSurface->DrawOutlinedRect(iPosX, iPosY, 200, 14, Color(120, 0, 0));
		if (CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19)
			DrawToolTip();
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		if (!bChangeValue && CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14 && CMenu::Get().bClick && !CMenu::Get().bPress)
			bChangeValue = true;
		if (bChangeValue && !(CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 14) && CMenu::Get().bClick && !CMenu::Get().bPress)
			bChangeValue = false;
		if (bChangeValue)
			CaptureInput();
		if (pSetting) {
			std::stringstream ss(buffer);
			ss >> *pSetting;
		}
	}
}
template<typename T>
void CKeyBind<T>::Action()
{
	int iPosX = CMenu::Get().iX + iX;
	int iPosY = CMenu::Get().iY + iY;
	if ((!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		g_pSurface->DrawOutlinedRect(iPosX + 158, iPosY, 44, 14, Color(120, 120, 120));
		g_pSurface->FilledRect(iPosX + 160, iPosY + 2, 40, 10, Color(69, 69, 69));
		if (pSetting)
			g_pSurface->DrawT(iPosX + 180, iPosY + 8, Color(255, 255, 255), c_ayala::CourierNew, true, KeyDigits[*pSetting] ? KeyDigits[*pSetting] : std::to_string(*pSetting).c_str());
		if (bChangeValue)
			g_pSurface->DrawOutlinedRect(iPosX + 158, iPosY, 44, 14, Color(120, 0, 0));
		if (CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19)
			DrawToolTip();
	}

	if ((!pControl || *pControl) && (!pDraw || *pDraw) && iTab == CMenu::Get().iSelectedTab) {
		if (!bChangeValue && CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19 && CMenu::Get().bClick && !CMenu::Get().bPress)
			bChangeValue = true;
		if (bChangeValue && !(CMenu::Get().MousePos.x > iPosX && CMenu::Get().MousePos.x < iPosX + 200 && CMenu::Get().MousePos.y > iPosY && CMenu::Get().MousePos.y < iPosY + 19) && CMenu::Get().bClick && !CMenu::Get().bPress)
			bChangeValue = false;
		if (bChangeValue && pSetting)
			for (int i = 0; i < 255; i++)
				if (GetAsyncKeyState(i) & 1)
				{
					*pSetting = i;
					bChangeValue = false;
				}
	}
}
void CMenu::RegisterTab(char * name)
{
	iTabCount++;
	Tabs.push_back(СTab(5, 0, 30, 30, iTabCount, name));
}

void CMenu::Render()
{
	BackGround();
	static bool Done;
	if (!Done) {
		SetupMenu();
		Done = true;
	}
	UpdateControl();
	MainControl();
	DrawMouse();
}

void CMenu::BackGround()
{
	g_pSurface->FilledRect(iX, iY, 800, 450, Color(39, 39, 39));
	g_pSurface->FilledRect(iX, iY, 800, 15, Color(69, 69, 69));
}

void CMenu::MainControl()
{
	float flDist = Vector(iX - MousePos.x, iY + 30 - MousePos.y, 0).Length();
	static float Time, ResetTime;
	static int OldSelectedTab;
	int iDeltaX, iDeltaY;
	iDeltaX = MousePos.x - iX;
	iDeltaY = MousePos.y - iY;
	MousePos = g_pSurface->GetMousePosition();
	g_pSurface->SetMouse(nullptr);
	bClick = bPress;
	bPress = GetAsyncKeyState(VK_LBUTTON) & 0x8000;

	if (iMenuState == Close && flDist <= 30 && bPress) {
		iMenuState = OpenAnimate;
		ResetTime = g_globals->realtime + 15;
		Time = g_globals->realtime;
	}
	if ((iMenuState == Open || iMenuState == OpenAnimate) && (ResetTime <= g_globals->realtime || (!(MousePos.x > iX && MousePos.x < iX + 200 && MousePos.y > iY && MousePos.y < iY + 50 * iTabCount) && bClick && !bPress))) {
		iMenuState = CloseAnimate;
		Time = g_globals->realtime;
	}
	if (iSelectedTab != OldSelectedTab) {
		OldSelectedTab = iSelectedTab;
		ResetTime = g_globals->realtime + 1;
	}
	if (iMenuState == Close)
		if (flDist <= 30)
			g_pSurface->DrawAngleCircle(iX, iY + 30, -90, 90, 30, Color(235, 125, 35));
	if (iMenuState == OpenAnimate)
		if (OpenTabAnimate(Time))
			iMenuState = Open;
	if (iMenuState == CloseAnimate)
		if (CloseTabAnimation(Time))
			iMenuState = Close;
	if (iMenuState != Close) {
		g_pSurface->FilledRect(iX, iY, 800, 450, Color(39, 39, 39, 200));
		DrawTab(iSelectedTab);
	}
	bMenuMove = bMenuMove && bPress ? true : MousePos.x > iX && MousePos.x < iX + 800 && MousePos.y > iY && MousePos.y < iY + 15 && bPress;

	if (bMenuMove) {
		iX = MousePos.x - iDeltaX;
		iY = MousePos.y - iDeltaY;
	}
}
void CMenu::UpdateControl()
{
	std::reverse(Objects.begin(), Objects.end());
	for (int i = 0; i < Objects.size(); i++)
		Objects[i]->Action();
	std::reverse(Objects.begin(), Objects.end());
}
void CMenu::SetupMenu()
{
	RegisterTab("Test");
	RegisterTab("Test");
	RegisterTab("Test");
	AddElement<CCheckbox<int*>, int>("Checkbox|Disable|Enable", 1, 20, 60, &Menu.Ragebot.ResolverOverride, 0, 1, "Setting %s, Value |%s|");
	AddElement<CDropbox<int*>, int>("Dropbox|Element|NotElement|Nothing|hz", 1, 20, 90, &Menu.Ragebot.ResolverOverride, 0, 9, "Setting %s, Value |%s|");
	AddElement<CSlider<int*>, int>("Slider", 1, 20, 120, &Menu.Ragebot.ResolverOverride, 0, 100, "Setting %s, Value |%s|");
	AddElement<CColorPicker<float*>, float>("ColorPicker", 1, 20, 150, Menu.Colors.BoundingBox, 0, 100, "Setting %s");
	AddElement<CTextInput<char*>, char>("Input", 1, 20, 180, Menu.Misc.configname, 0, 100, "Setting %s, Value |%s|");
	AddElement<CKeyBind<int*>, int>("Input", 1, 20, 210, &Menu.Antiaim.AntiaimMode, 0, 100, "Setting %s, Value |%s|");
}
bool CMenu::OpenTabAnimate(float Time)
{
	for (int i = 0; i < iTabCount; i++) {
		if (Animate<int>(Tabs[i].iY, 50 * i + 10, Time + 0.1 * (i + 1), 0.3))
			if (Animate<int>(Tabs[i].iWidth, 200, Time + 0.2 * (i + 1), 0.3))
				if (Animate<int>(Tabs[i].iAlpha, 255, Time + 0.3 * (i + 1), 0.3) && i == iTabCount - 1)
					return true;
	}
	return false;
}
void CMenu::DrawTab(int& Tab)
{
	for (int i = 0; i < iTabCount; i++) {
		g_pSurface->DrawRoundedBox(iX + Tabs[i].iX, iY + Tabs[i].iY, Tabs[i].iWidth, Tabs[i].iHeight, i == iSelectedTab - 1 ? Color(89, 89, 89) : Color(255, 255, 255));
		g_pSurface->DrawT(iX + (Tabs[i].iX + Tabs[i].iX + Tabs[i].iWidth) / 2, iY + (Tabs[i].iY + Tabs[i].iY + Tabs[i].iHeight) / 2, Color(255, 255, 255, Tabs[i].iAlpha), c_ayala::CourierNew, true, Tabs[i].Name);
		if (MousePos.x > iX + Tabs[i].iX && MousePos.x < iX + Tabs[i].iX + Tabs[i].iWidth && MousePos.y > iY + Tabs[i].iY && MousePos.y < iY + Tabs[i].iY + Tabs[i].iHeight && bPress && iMenuState == Open)
			Tab = Tabs[i].iN;
	}
}
bool CMenu::CloseTabAnimation(float Time)
{
	for (int i = 0; i < iTabCount; i++) {
		if (Animate<int>(Tabs[iTabCount - i - 1].iAlpha, 0, Time + 0.1 * (i + 1), 0.3))
			if (Animate<int>(Tabs[iTabCount - i - 1].iWidth, 30, Time + 0.2 * (i + 1), 0.3))
				if (Animate<int>(Tabs[iTabCount - i - 1].iY, 0, Time + 0.3 * (i + 1), 0.3) && i == iTabCount - 1)
					return true;
	}
	return false;
}
void CMenu::DrawMouse()
{
	static float Multiplier;
	Multiplier += 0.005;
	std::vector<Vertex_t> buffer;
	for (float a = -180; a <= -90; a++)
		buffer.push_back(Vertex_t(Vector2D(MousePos.x + 20 + (18 * cosf(DEG2RAD(a))) + 9, MousePos.y + 20 + (18 * sinf(DEG2RAD(a)) + 9))));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x, MousePos.y)));
	std::reverse(buffer.begin(), buffer.end());
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(150, 150, 150));
	buffer.clear();
	for (float a = -180; a <= -90; a++)
		buffer.push_back(Vertex_t(Vector2D(MousePos.x + 25 + (18 * cosf(DEG2RAD(a))) + 9, MousePos.y + 25 + (18 * sinf(DEG2RAD(a)) + 9))));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 5, MousePos.y + 5)));
	std::reverse(buffer.begin(), buffer.end());
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(59, 59, 59));
	buffer.clear();
	for (float a = -147; a <= -123; a++)
		buffer.push_back(Vertex_t(Vector2D(MousePos.x + 25 + (18 * cosf(DEG2RAD(a))) + 9, MousePos.y + 25 + (18 * sinf(DEG2RAD(a)) + 9))));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 21, MousePos.y + 17)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 17, MousePos.y + 21)));
	std::reverse(buffer.begin(), buffer.end());
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(230 * abs(cos(Multiplier)), 125 * abs(cos(Multiplier)), 35 * abs(cos(Multiplier))));
	buffer.clear();
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 16, MousePos.y + 13)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 20, MousePos.y + 16)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 16, MousePos.y + 20)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 13, MousePos.y + 16)));
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(230 * abs(cos(Multiplier + .25)), 125 * abs(cos(Multiplier + .25)), 35 * abs(cos(Multiplier + .25))));
	buffer.clear();
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 11, MousePos.y + 9)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 15, MousePos.y + 12)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 12, MousePos.y + 15)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 9, MousePos.y + 11)));
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(230 * abs(cos(Multiplier + .5)), 125 * abs(cos(Multiplier + .5)), 35 * abs(cos(Multiplier + .5))));
	buffer.clear();
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 5, MousePos.y + 5)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 10, MousePos.y + 8)));
	buffer.push_back(Vertex_t(Vector2D(MousePos.x + 8, MousePos.y + 10)));
	g_pSurface->DrawPolygon(buffer.size(), &buffer[0], Color(230 * abs(cos(Multiplier + .75)), 125 * abs(cos(Multiplier + .75)), 35 * abs(cos(Multiplier + .75))));
}
template<class C, typename T>
void CMenu::AddElement(char* name, int tab, int x, int y, T* setting, int min, int max, char* tooltip, bool* render, bool* update)
{
	CElement<void*>* Object = (CElement<void*>*)new C;
	if (!Object)
		return;
	Object->Update(name, x, y, setting, render, update, min, max, tab, tooltip);
	Objects.push_back(Object);
}
template<typename T>
bool CMenu::Animate(T & In, T Need, float StartTime, float AnimateTime)
{
	if (g_globals->realtime < StartTime)
		return false;
	float flDeltaTime = StartTime + AnimateTime - g_globals->realtime;
	//T StartValue = Need - (Need - In) / (flDeltaTime / AnimateTime); 礤 噤尻忄蝽?疣犷蜞弪
	T Delta = In - Need;
	In = Need + (Delta * (flDeltaTime / AnimateTime));
	if (flDeltaTime < 0)
		return true;
	return false;
}
