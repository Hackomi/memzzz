#pragma once
#include "Singleton.h"

#define DISABLE_INTERPOLATION 0
#define ENABLE_INTERPOLATION 1

enum EHitboxGroup
{
	HEADGROUP = 0,
	NECKGROUP,
	CHESTGROUP,
	STOMACHGROUP,
	PELVISGROUP,
	ARMSGROUP,
	LEGSGROUP
};

struct HitboxData_t 
{
	int	iBone = -1;
	int iGroup = -1;
	Vector vecBBmin = Vector();
	Vector vecBBmax = Vector();
	float flRadius = -1;
	int iHitbox = -1;

	int GetPriority();
	Vector GetMPSize();
	EHitboxGroup NumToGroup();
};

enum ETickFlags
{
	TICK_Invalid = -1,
	TICK_Rebuild = 1 << 0,
	TICK_Break = 1 << 1,
	TICK_Normal = 1 << 2,
	TICK_Resolved = 1 << 3,
	TICK_Unlag = 1 << 4,
	TICK_Moving = 1 << 5,
	TICK_InAir = 1 << 6,
};

struct RecordTick_t
{
	int GetTickPriority()
	{
		int iTickPriority = nTickFlags & TICK_Break ? nTickFlags & TICK_Rebuild ? 1 : 0 : nTickFlags & TICK_Normal ? 2 : 0;
		if (nTickFlags & TICK_Unlag)
			iTickPriority += 1;
		if (nTickFlags & TICK_Resolved)
			iTickPriority += 2;
		if (nTickFlags & TICK_InAir)
			iTickPriority /= 2;
		else if (nTickFlags & TICK_Moving)
			iTickPriority *= 2;
		return iTickPriority;
	}
	int iIndex = -1;
	float flSimulationTime = -1.f;
	Vector vecOrigin = Vector();
	Vector vecAbsOrigin = Vector();
	QAngle angEyeAngle = QAngle();
	QAngle angAbsAngle = QAngle();
	float flPoseParameter[24];
	VMatrix boneMatrix[128];
	Vector vecVelocity = Vector();
	int iFlags = -1;
	float flLowerBodyYaw = -1.f;
	int iLayerCount = -1;
	AnimationLayer cAnimationLayer[15];
	RecordTick_t(C_BaseEntity* pEntity);
	RecordTick_t() {};
	Vector vecMin = Vector(), vecMax = Vector();
	std::vector<HitboxData_t> HitboxData;
	int nTickFlags = TICK_Invalid;
	float flDuckAmount = -1, flFeetCycle = -1, flFeetYawRate = -1;
	float flEyePitch = -1;
	float flEyeYaw = -1;
	float flCurrentFeetYaw = -1;
	float flCurrentTorsoYaw = -1;
	float flGoalFeetYaw = -1;
	float flVelocityLean = -1;
	float flLeanAmount = -1;
};

enum ECheckFlag
{
	Corners = 1 << 0,
	Center = 1 << 1,
	Cross = 1 << 2
};

class CBackTrackManager : public Singleton<CBackTrackManager>
{
private:
	std::vector<RecordTick_t> RecordedTicks[64];
	int GetEstimatedServerTickCount(float latency);
public:
	bool DataIsClear();
	float GetLerpTime();
	float GetTickOffset(RecordTick_t tick);
	Vector PerformExtrapolation(C_BaseEntity * player, Vector Position);
	bool IsVisibleTick(RecordTick_t tick, bool smokecheck, bool bodycheck);
	bool CanHitTick(RecordTick_t tick, int flag);
	void SetRecord(C_BaseEntity* pEntity, RecordTick_t tick);
	bool getSmthMatrix(int ent_index, matrix3x4 * out);
	void getSmthMatrix1(int ent_index, matrix3x4 * out);
	void SetAnimState(C_BaseEntity* pEntity, RecordTick_t tick);
	void UpdateTicks();
	bool IsValidTick(RecordTick_t tick);
	void SimulateMovement(Vector &velocity, Vector &origin, C_BaseEntity* player, int &flags, bool was_in_air);
	void AnimationFix1();
	void AnimationFix(C_BaseEntity * pEntity);
	void set_interpolation_flags(C_BaseEntity * entity, int flag);
	std::vector<RecordTick_t> GetRecords(C_BaseEntity* pEntity) { return RecordedTicks[pEntity->GetIndex()]; }
	RecordTick_t GetLastValidRecord(C_BaseEntity* pEntity);
	std::vector<RecordTick_t> GetScanTicks(C_BaseEntity* pEntity);
	std::vector<RecordTick_t> GetLastValidRecords(); //LastRecord All entity
	void ClearData();
};