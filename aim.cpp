﻿#include "sdk.h"
#include "c_track-manager.hpp"
#include "aim.h"
#include "global.h"
#include "Menu.h"
#include "Math.h"
#include "GameUtils.h"
#include "Autowall.h"
#include "no-spread.h"
#include "fake_latency.hpp"
#include "resolve.h"
#include "simple_auto_wall.h"
#include "simplehpb_auto_wall.h"
#include "auto_wall.h"

#define TICK_INTERVAL			( g_globals->interval_per_tick )
#define TIME_TO_TICKS( dt )		( (int)( 0.5f + (float)(dt) / TICK_INTERVAL ) )
#define TICKS_TO_TIME( t )		( TICK_INTERVAL *( t ) )
template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
void AutoRevolver()
{
	if (!c_ayala::c_weapon->WeaponID() == ItemDefinitionIndex::WEAPON_REVOLVER)
		return;

	float shit = 0.2421874;
	static bool pushing = true;
	static float last_shittime = 0;
	static int ticks_pushed = 0;
	float curtime = GameUtils::GetCurTime();
	float flPostponeFireReady = c_ayala::c_weapon->GetPostponeFireReadyTime();
	if (ticks_pushed < TIME_TO_TICKS(shit) && pushing || ticks_pushed == 0)
	{
		c_ayala::user_cmd->buttons |= IN_ATTACK;
		ticks_pushed++;
	}
	else
	{
		last_shittime = curtime;
		c_ayala::user_cmd->buttons &= ~IN_ATTACK;
		ticks_pushed = -1;
		pushing = false;
	}
	if (ticks_pushed == -1)
	{
		static int ticks = 0;
		if (ticks > 1)
		{
			pushing = true;
			ticks_pushed = 0;
			ticks = 0;
		}
		ticks++;

	}
	if (Menu.Ragebot.NewAutomaticRevolver && flPostponeFireReady + g_globals->interval_per_tick * Menu.Ragebot.NewAutomaticRevolverFactor > GameUtils::GetCurTime())
		c_ayala::user_cmd->buttons |= IN_ATTACK2;
}
void CRageBot::Run()
{
	if (!g_pEngine->IsConnected() || !g_pEngine->IsInGame())
	{
		if (!CBackTrackManager::Get().DataIsClear())
		{
			CBackTrackManager::Get().ClearData();
			g_BacktrackHelper->ClearIncomingSequences();
		}
	}
	if (!c_ayala::l_player
		|| !c_ayala::c_weapon
		|| !c_ayala::l_player->isAlive()
	/*	|| c_ayala::c_weapon->IsMiscWeapon()*/
		/*|| c_ayala::c_weapon->Clip1() < 1*/
		/*|| !GameUtils::IsAbleToShoot()*/)
		return;
	if (Menu.Ragebot.AutomaticRevolver)
	{
		if (c_ayala::c_weapon->WeaponID() == ItemDefinitionIndex::WEAPON_REVOLVER)
		{
		        AutoRevolver();
		}
	}
	for (int i = 0; i < g_globals->max_сlients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (!pEntity
			|| !pEntity->isAlive()
			|| !pEntity->IsPlayer()
			|| !pEntity->IsEnemy()
			|| pEntity == c_ayala::l_player || pEntity->IsDormant())
			continue;
		TargetEntity.push_back(pEntity);
	}

	auto bSortHP = [](C_BaseEntity* ent1, C_BaseEntity* ent2) { return ent1->GetHealth() < ent2->GetHealth(); }; //Nada bol'she

	bool(*pEntSortFunc)(C_BaseEntity*, C_BaseEntity*) = [](C_BaseEntity* ent1, C_BaseEntity* ent2) { return ent1->GetIndex() < ent2->GetIndex(); };

	auto bHitboxSort = [](HitboxData_t hitbox1, HitboxData_t hitbox2)
	{
		int FirstTickPriority = hitbox1.GetPriority();
		int TwiceTickPriority = hitbox2.GetPriority();
		if (FirstTickPriority == TwiceTickPriority)
			return hitbox1.iHitbox < hitbox2.iHitbox;
		return FirstTickPriority > TwiceTickPriority;
	};

	auto bTickSort = [](RecordTick_t tick1, RecordTick_t tick2)
	{
		int FirstTickPriority = tick1.GetTickPriority();
		int TwiceTickPriority = tick2.GetTickPriority();
		if (FirstTickPriority == TwiceTickPriority)
			return CBackTrackManager::Get().GetTickOffset(tick1) < CBackTrackManager::Get().GetTickOffset(tick2);
		return FirstTickPriority > TwiceTickPriority;
	};

	switch (Setting.iTargetingType)
	{
	case 0:
		pEntSortFunc = bSortHP;
		break;
	case 1:
		pEntSortFunc = bSortHP;
		break;
	}
	std::sort(TargetEntity.begin(), TargetEntity.end(), pEntSortFunc);
	for (C_BaseEntity* pTarget : TargetEntity)
	{
		if (bFindGoodTarget)
			break;

		if (!pTarget
			|| !pTarget->isAlive()
			|| !pTarget->IsPlayer()
			|| !pTarget->IsEnemy() || pTarget->IsDormant())
			continue;
		*(int*)((uintptr_t)pTarget + 0xA30) = g_globals->framecount;
		*(int*)((uintptr_t)pTarget + 0xA28) = 0;
		RecordTick_t BackupRecord(pTarget);
		auto TargetTicks = CBackTrackManager::Get().GetScanTicks(pTarget);
		std::sort(TargetTicks.begin(), TargetTicks.end(), bTickSort);
	
		for (auto Tick : TargetTicks)
		{
			if (bFindGoodTarget)
				break;

			if (CBackTrackManager::Get().IsValidTick(Tick))
			{
				//CBackTrackManager::Get().SetRecord(pTarget, Tick);
			/*	CBackTrackManager::Get().SetAnimState(pTarget, Tick);
				VMatrix boneMatrix[128];*/
				/*if (!pTarget->SetupBones(Tick.boneMatrix, 128, BONE_USED_BY_HITBOX, g_globals->curtime))
					continue;*/
		/*		if(CBackTrackManager::Get().CanHitTick(Tick, Center | Corners | Cross))
				{*/
				std::sort(Tick.HitboxData.begin(), Tick.HitboxData.end(), bHitboxSort);

				for (auto Hitbox : Tick.HitboxData)
				{
					if (bFindGoodTarget)
						break;
					
					auto AimPoints = Multipoint(pTarget, Hitbox, Tick.boneMatrix[Hitbox.iBone]);
					for (auto AimPoint : AimPoints)
					{
						if (bFindGoodTarget)
							break;
						//if (/*CBulletHandler::Get().CanHit(c_ayala::l_player, AimPoint) > Menu.Ragebot.Mindamage*//*CBackTrackManager::Get().IsVisibleTick(Tick, false, true)*//*c_autowall.CanHitFloatingPoint(AimPoint, c_ayala::l_player->GetEyePosition())*//* && c_autowall.Damage(AimPoint) > Menu.Ragebot.Mindamage*//* > Menu.Ragebot.Mindamage*/)
						if(c_autowall.CanHitFloatingPoint(AimPoint, c_ayala::l_player->GetEyePosition()))
						{
							if (Menu.Ragebot.AutomaticScope && c_ayala::c_weapon->IsScopeable() && !c_ayala::l_player->IsScoped())
								c_ayala::user_cmd->buttons |= IN_ATTACK2;
							
							if (HitChance(pTarget, AimPoint))
							{
								c_ayala::user_cmd->buttons |= IN_ATTACK;

								if (c_ayala::user_cmd->buttons & IN_ATTACK)
								{
									c_ayala::user_cmd->viewangles = GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), AimPoint);
									c_ayala::user_cmd->tick_count = TIME_TO_TICKS(Tick.flSimulationTime + CBackTrackManager::Get().GetLerpTime());
									CompensateInaccuracies();
							/*	for (auto Hitbox : Tick.HitboxData)
									g_pDebugOverlay->AddCapsuleOverlay(Math::VectorTransform(Hitbox.vecBBmin, boneMatrix[Hitbox.iBone]), Math::VectorTransform(Hitbox.vecBBmax, boneMatrix[Hitbox.iBone]), Hitbox.flRadius, 255, 255, 255, 255, 4);*/
								}

							}
							else
							{
								//c_ayala::user_cmd->forwardmove = c_ayala::user_cmd->sidemove = 0;
							}
							bFindGoodTarget = true;
						}
					}
				}
			}
		}
		/*}*/
		//CBackTrackManager::Get().SetRecord(pTarget, BackupRecord);
		//CBackTrackManager::Get().SetAnimState(pTarget, BackupRecord);
	}
	bFindGoodTarget = false;
	TargetEntity.clear();
}
std::vector<Vector> CRageBot::Multipoint(C_BaseEntity* player, HitboxData_t hitbox, VMatrix matrix)
{
	if (!player)
		return std::vector<Vector>();
	std::vector<Vector> multipoints;
	if (hitbox.iHitbox < 11 || hitbox.iHitbox > 14) //CapsuleHitbox
	{
		Vector density = hitbox.GetMPSize();
		Vector lCenter, lPoint, dir, head2D;

		lCenter = (hitbox.vecBBmin + hitbox.vecBBmax) * .5f;
		multipoints.push_back(Math::VectorTransform(lCenter, matrix));

		QAngle dAngle, angle;

		if (density.x > 0 || density.y > 0)
		{
			Math::VectorAngles((hitbox.vecBBmin - hitbox.vecBBmax).Normalized(), dAngle);
			switch (hitbox.iHitbox)
			{
			case 2:
				dAngle += QAngle(0, 0, 0);
				break;
			case 3:
				dAngle += QAngle(180, 0, 0);
				break;
			case 4:
				dAngle += QAngle(-90, 0, 0);
				break;
			case 5:
			case 6:
				dAngle += QAngle(0, 0, 0);
				break;
			default:
				dAngle += QAngle(90, 90, 0);
				break;
			}
			lCenter = hitbox.vecBBmin - (hitbox.vecBBmax - hitbox.vecBBmin).Normalized() * .8f * density.z;
			multipoints.push_back(Math::VectorTransform(lCenter, matrix));
			for (float i = density.x > 1 ? 0 : .5f; i <= density.x; i += 1)
			{
				for (float k = density.y > 1 ? 0 : .5f; k <= density.y; k += 1)
				{
					lPoint = lCenter;
					angle = dAngle + QAngle(180.0f / density.x * i, 180.0f / density.y * k, 0);
					Math::AngleVectors(angle, dir);
					lPoint += dir.Normalized() * hitbox.flRadius * .8f * density.z;
					multipoints.push_back(Math::VectorTransform(lPoint, matrix));
				}
			}
			Math::VectorAngles((hitbox.vecBBmin - hitbox.vecBBmax).Normalized(), dAngle);
			switch (hitbox.iHitbox)
			{
			case 2:
				dAngle += QAngle(180, 0, 0);
				break;
			case 3:
				dAngle += QAngle(0, 0, 0);
				break;
			case 4:
				dAngle += QAngle(90, 0, 0);
				break;
			case 5:
			case 6:
				dAngle += QAngle(180, 0, 0);
				break;
			default:
				dAngle += QAngle(-90, 90, 0);
				break;
			}
			lCenter = hitbox.vecBBmax + (hitbox.vecBBmax - hitbox.vecBBmin).Normalized() * .8f * density.z;
			multipoints.push_back(Math::VectorTransform(lCenter, matrix));
			for (float i = density.x > 1 ? 0 : .5f; i <= density.x; i += 1)
			{
				for (float k = density.y > 1 ? 0 : .5f; k <= density.y; k += 1)
				{
					lPoint = lCenter;
					angle = dAngle + QAngle(180.0f / density.x * i, 180.0f / density.y * k, 0);
					Math::AngleVectors(angle, dir);
					lPoint += dir.Normalized() * hitbox.flRadius * .8f * density.z;
					multipoints.push_back(Math::VectorTransform(lPoint, matrix));
				}
			}
		}

		Math::VectorAngles((hitbox.vecBBmax - hitbox.vecBBmin).Normalized(), dAngle);

		switch (hitbox.iHitbox)
		{
		case 2:
		case 3:
			dAngle += QAngle(0, 0, 0);
			break;
		case 4:
			dAngle += QAngle(90, 0, 0);
			break;
		case 5:
		case 6:
			dAngle += QAngle(0, 0, 0);
			break;
		default:
			dAngle += QAngle(0, 90, 0);
			break;
		}

		if (density.x > 0 && density.y > 1)
		{
			for (float p = density.x > 1 ? 0 : .5f; p <= 1.0f; p += density.x > 1 ? 1.0f / (density.x - 1) : 2) {
				lCenter = hitbox.vecBBmin + (hitbox.vecBBmax - hitbox.vecBBmin) * p;
				for (int o = 0; o < density.y; o++) {
					lPoint = lCenter;
					angle = dAngle + (hitbox.iHitbox < 2 || hitbox.iHitbox > 6 ? QAngle(360.0f / density.y, 0, 0) * o : QAngle(0, 360.0f / density.y, 0) * o);
					Math::AngleVectors(angle, dir);
					lPoint += dir.Normalized() * hitbox.flRadius * .8f * density.z;
					multipoints.push_back(Math::VectorTransform(lPoint, matrix));
				}
			}
		}
	}
	else //Box hitbox
	{

	}
	return multipoints;
}
bool CRageBot::HitChance(C_BaseEntity* Target,Vector AimPoint)
{
	float Velocity = c_ayala::l_player->GetVelocity().Length();

	if (Velocity <= (c_ayala::c_weapon->GetCSWpnData()->max_speed_alt * .34f))
		Velocity = 0.0f;

	float SpreadCone = c_ayala::c_weapon->GetAccuracyPenalty() * 256.0f / M_PI + c_ayala::c_weapon->GetCSWpnData()->max_speed * Velocity / 3000.0f; // kmeth https://github.com/DankPaster/kmethdude
	float a = (AimPoint - c_ayala::l_player->GetEyePosition()).Length();
	float b = sqrt(tan(SpreadCone * M_PI / 180.0f) * a);
	if (2.2f > b) return true;
	return (Menu.Ragebot.Minhitchance <= ((2.2f / fmax(b, 2.2f)) * 100.0f));
	/*if (!Menu.Ragebot.Minhitchance)
		return true;
	int iHit = 0;
	int iHitsNeed = Menu.Ragebot.Minhitchance;
	bool bHitchance = false;

	if (c_ayala::c_weapon->WeaponID() == ItemDefinitionIndex::WEAPON_TASER)
		iHitsNeed = 0;

	Vector forward, right, up;
	Math::AngleVectors(GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), AimPoint), &forward, &right, &up);

	c_ayala::c_weapon->UpdateAccuracyPenalty();

	for (auto i = 0; i < 100; ++i) {

		float RandomA = Math::RandomFloat(0.0f, 1.0f);
		float RandomB = 1.0f - RandomA * RandomA;

		RandomA = Math::RandomFloat(0.0f, 3.14 * 2.0f);
		RandomB *= c_ayala::c_weapon->GetInaccuracy();

		float SpreadX1 = (cos(RandomA) * RandomB);
		float SpreadY1 = (sin(RandomA) * RandomB);

		float RandomC = Math::RandomFloat(0.0f, 1.0f);
		float RandomF = RandomF = 1.0f - RandomC * RandomC;

		RandomC = Math::RandomFloat(0.0f, 3.14 * 2.0f);
		RandomF *= c_ayala::c_weapon->GetSpread();

		float SpreadX2 = (cos(RandomC) * RandomF);
		float SpreadY2 = (sin(RandomC) * RandomF);

		float fSpreadX = SpreadX1 + SpreadX2;
		float fSpreadY = SpreadY1 + SpreadY2;

		Vector vSpreadForward;
		vSpreadForward[0] = forward[0] + (fSpreadX * right[0]) + (fSpreadY * up[0]);
		vSpreadForward[1] = forward[1] + (fSpreadX * right[1]) + (fSpreadY * up[1]);
		vSpreadForward[2] = forward[2] + (fSpreadX * right[2]) + (fSpreadY * up[2]);
		vSpreadForward.NormalizeInPlace();

		Vector qaNewAngle;
		Math::VectorAngles(vSpreadForward, qaNewAngle);
		Math::NormalizeAngle(qaNewAngle);

		Vector vEnd;
		Math::AngleVectors(qaNewAngle, &vEnd);

		vEnd = c_ayala::l_player->GetEyePosition() + (vEnd * 8192.f);


		if (c_ayala::l_player->canHit(vEnd, Target)) {
			iHit++;
		}

		if (iHit >= Menu.Ragebot.Minhitchance) {
			bHitchance = true;
			break;
		}

		if ((100 - 1 - i + iHit) < iHitsNeed)
			break;
	}

	return bHitchance;*/
}
void CRageBot::CompensateInaccuracies()
{
	if (c_ayala::user_cmd->buttons & IN_ATTACK)
	{
		ConVar* recoilscale = g_pCvar->FindVar(XorStr("weapon_recoil_scale"));
		if (recoilscale)
		{
			QAngle qPunchAngles = c_ayala::l_player->GetPunchAngle();
			QAngle qAimAngles = c_ayala::user_cmd->viewangles;
			qAimAngles -= qPunchAngles * recoilscale->GetFloat();
			c_ayala::user_cmd->viewangles = qAimAngles;
		}
	}
  /*  if (Menu.Ragebot.NoSpread)
		c_ayala::user_cmd->viewangles = g_NoSpread->SpreadFactor(c_ayala::user_cmd->random_seed);*/
}
