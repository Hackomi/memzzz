#include "sdk.h"
#include "Autowall.h"
#include "GameUtils.h"
#include "Math.h"
#include "Menu.h"
#include "global.h"
//#include "simple_auto_wall.h"

#define enc_str(s) std::string(s)
#define enc_char(s) enc_str(s).c_str()
#define enc_wstr(s) std::wstring(enc_str(s).begin(), enc_str(s).end())
#define enc_wchar(s) enc_wstr(s).c_str()


Autowall autowall;

void Autowall::ScaleDamage(C_BaseEntity* entity, CSWeaponInfo* weapon_info, int hitgroup, float& current_damage)
{
	//Cred. to N0xius for reversing this.
	//TODO: _xAE^; look into reversing this yourself sometime

	bool hasHeavyArmor = false;
	int armorValue = entity->GetArmor();

	//Fuck making a new function, lambda beste. ~ Does the person have armor on for the hitbox checked?
	auto IsArmored = [&entity, &hitgroup]()-> bool
	{
		C_BaseEntity* targetEntity = entity;
		switch (hitgroup)
		{
		case HITGROUP_HEAD:
			return targetEntity->HasHelmet();
		case HITGROUP_GENERIC:
		case HITGROUP_CHEST:
		case HITGROUP_STOMACH:
		case HITGROUP_LEFTARM:
		case HITGROUP_RIGHTARM:
			return true;
		default:
			return false;
		}
	};

	switch (hitgroup)
	{
	case HITGROUP_HEAD:
		current_damage *= hasHeavyArmor ? 2.f : 4.f; //Heavy Armor does 1/2 damage
		break;
	case HITGROUP_STOMACH:
		current_damage *= 1.25f;
		break;
	case HITGROUP_LEFTLEG:
	case HITGROUP_RIGHTLEG:
		current_damage *= 0.75f;
		break;
	default:
		break;
	}

	if (armorValue > 0 && IsArmored())
	{
		float bonusValue = 1.f, armorBonusRatio = 0.5f, armorRatio = weapon_info->armor_ratio / 2.f;

		//Damage gets modified for heavy armor users
		if (hasHeavyArmor)
		{
			armorBonusRatio = 0.33f;
			armorRatio *= 0.5f;
			bonusValue = 0.33f;
		}

		auto NewDamage = current_damage * armorRatio;

		if (hasHeavyArmor)
			NewDamage *= 0.85f;

		if (((current_damage - (current_damage * armorRatio)) * (bonusValue * armorBonusRatio)) > armorValue)
			NewDamage = current_damage - (armorValue / armorBonusRatio);

		current_damage = NewDamage;
	}
}
bool Autowall::CanPenetrate(C_BaseEntity* attacker, Autowall_Info& info, CSWeaponInfo* weapon_data)
{
	if (!weapon_data || !attacker)
		return false;

	if (this)
		return false;

	//typedef bool(__thiscall* HandleBulletPenetrationFn)(SDK::C_BaseEntity*, float&, int&, int*, SDK::trace_t*, Vector*, float, float, float, int, int, float, int*, Vector*, float, float, float*);
	//C_BaseEntity *pl_player, float *flPenetration, int *SurfaceMaterial, char *IsSolid, trace_t *ray, Vector *vecDir, int unused, float flPenetrationModifier, float flDamageModifier, int unused2, int weaponmask, float flPenetration2, int *hitsleft, Vector *ResultPos, int unused3, int unused4, float *damage)
	typedef bool(__thiscall* HandleBulletPenetrationFn)(C_BaseEntity*, float*, int*, char*, trace_t*, Vector*, int, float, float, int, int, float, int*, Vector*, int, int, float*);

	static HandleBulletPenetrationFn HBP = reinterpret_cast<HandleBulletPenetrationFn>(Utilities::Memory::FindPattern(c_ayala::Panorama ? "client_panorama.dll" : "client.dll",
		(PBYTE)"\x53\x8B\xDC\x83\xEC\x08\x83\xE4\xF8\x83\xC4\x04\x55\x8B\x6B\x04\x89\x6C\x24\x04\x8B\xEC\x83\xEC\x78\x56\x8B\x73\x2C",
		"xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));

	if (!HBP)
	{
		std::cout << "JOPA" << std::endl;
		return false;
	}

	auto enter_surface_data = g_pPhysics->GetSurfaceData(info.enter_trace.surface.surfaceProps);
	if (!enter_surface_data)
		return true;

	char is_solid = 0;
	int material = enter_surface_data->game.material;
	int mask = 0x1002;
	ClientClass * pClass = (ClientClass*)info.enter_trace.m_pEnt->GetClientClass();

	// glass and shit gg
	/*if (info.enter_trace.m_pEnt && !strcmp("CBreakableSurface", pClass->m_pNetworkName))
		*reinterpret_cast<byte*>(uintptr_t(info.enter_trace.m_pEnt + 0x27C)) = 2;*/
	Vector rem = info.end;
	FireBulletData data;

	data.src = attacker->GetEyePosition();
	data.filter.pSkip1 = attacker;

	QAngle angle = GameUtils::CalculateAngle(data.src, rem);
	Math::AngleVectors(angle, data.direction);

	data.direction.Normalized();
	data.current_damage = info.current_damage;
	data.penetrate_count = info.penetration_count;
	data.enter_trace = info.enter_trace;
	data.trace_length = info.trace_length;
	data.trace_length_remaining = info.trace_length_remaining;
	
	is_autowalling = true;
	bool return_value = false; /*!CAutowallSimple::Get().HandleBulletPenetration(weapon_data, data);*///!HBP(attacker, &material, &is_solid, &info.enter_trace, &info.direction, 0, enter_surface_data->game.flPenetrationModifier, enter_surface_data->game.flDamageModifier, 0, mask, weapon_data->GetPen(), &info.penetration_count, &info.current_position, 0, 0, &info.current_damage, 0);
	is_autowalling = false;

	
	info.current_damage = data.current_damage;
	info.penetration_count = data.penetrate_count;
	info.enter_trace = data.enter_trace;
	info.trace_length = data.trace_length;
	info.trace_length_remaining = data.trace_length_remaining;

	return return_value;
}
Autowall::Autowall_Return_Info Autowall::CalculateDamage(Vector start, Vector end, C_BaseEntity* from_entity, C_BaseEntity* to_entity, int specific_hitgroup)
{
	// default values for return info, in case we need to return abruptly
	Autowall_Return_Info return_info;
	return_info.damage = -1;
	return_info.hitgroup = -1;
	return_info.hit_entity = nullptr;
	return_info.penetration_count = 4;
	return_info.thickness = 0.f;
	return_info.did_penetrate_wall = false;

	Autowall_Info autowall_info;
	autowall_info.penetration_count = 4;
	autowall_info.start = start;
	autowall_info.end = end;
	autowall_info.current_position = start;
	autowall_info.thickness = 0.f;

	// direction 
	Math::AngleVectors(GameUtils::CalculateAngle(start, end), &autowall_info.direction);

	// attacking entity
	if (!from_entity)
		from_entity = c_ayala::l_player;
	if (!from_entity)
		return return_info;

	auto filter_player = CTraceFilterOneEntity();
	filter_player.pEntity = to_entity;
	auto filter_local = CTraceFilter();
	filter_local.pSkip1 = from_entity;

	// setup filters
	if (to_entity)
		autowall_info.filter = &filter_player;
	else
		autowall_info.filter = &filter_player;

	// weapon
	auto weapon = reinterpret_cast<CBaseCombatWeapon*>(g_pEntitylist->GetClientEntity(from_entity->GetActiveWeaponIndex()));
	if (!weapon)
		return return_info;

	// weapon data
	auto weapon_info = weapon->GetWeaponInfo();
	if (!weapon_info)
		return return_info;

	// client class
	auto weapon_client_class = reinterpret_cast<C_BaseEntity*>(weapon)->GetClientClass();
	if (!weapon_client_class)
		return return_info;

	// weapon range
	float range = min(weapon_info->range, (start - end).Length());
	end = start + (autowall_info.direction * range);
	autowall_info.current_damage = weapon_info->damage;

	while (autowall_info.current_damage > 0 && autowall_info.penetration_count > 0)
	{
		return_info.penetration_count = autowall_info.penetration_count;

		GameUtils::TraceLine(autowall_info.current_position, end, MASK_SHOT | CONTENTS_GRATE, from_entity, &autowall_info.enter_trace);
		GameUtils::UTIL_ClipTraceToPlayers(autowall_info.current_position, autowall_info.current_position + autowall_info.direction * 40.f, MASK_SHOT | CONTENTS_GRATE, autowall_info.filter, &autowall_info.enter_trace);

		const float distance_traced = (autowall_info.enter_trace.endpos - start).Length();
		autowall_info.current_damage *= pow(weapon_info->range_modifier, (distance_traced / 500.f));

		/// if reached the end
		if (autowall_info.enter_trace.fraction == 1.f)
		{
			if (to_entity && specific_hitgroup != -1)
			{
				ScaleDamage(to_entity, weapon_info, specific_hitgroup, autowall_info.current_damage);

				return_info.damage = autowall_info.current_damage;
				return_info.hitgroup = specific_hitgroup;
				return_info.end = autowall_info.enter_trace.endpos;
				return_info.hit_entity = to_entity;
			}
			else
			{
				return_info.damage = autowall_info.current_damage;
				return_info.hitgroup = -1;
				return_info.end = autowall_info.enter_trace.endpos;
				return_info.hit_entity = nullptr;
			}

			break;
		}
		// if hit an entity
		if (autowall_info.enter_trace.hitgroup > 0 && autowall_info.enter_trace.hitgroup <= 7 && autowall_info.enter_trace.m_pEnt)
		{
			// checkles gg
			if ((to_entity && autowall_info.enter_trace.m_pEnt != to_entity) ||
				(autowall_info.enter_trace.m_pEnt->GetTeamNum() == from_entity->GetTeamNum()))
			{
				return_info.damage = -1;
				return return_info;
			}

			if (specific_hitgroup != -1)
				ScaleDamage(autowall_info.enter_trace.m_pEnt, weapon_info, specific_hitgroup, autowall_info.current_damage);
			else
				ScaleDamage(autowall_info.enter_trace.m_pEnt, weapon_info, autowall_info.enter_trace.hitgroup, autowall_info.current_damage);

			// fill the return info
			return_info.damage = autowall_info.current_damage;
			return_info.hitgroup = autowall_info.enter_trace.hitgroup;
			return_info.end = autowall_info.enter_trace.endpos;
			return_info.hit_entity = autowall_info.enter_trace.m_pEnt;

			break;
		}

		// break out of the loop retard
		if (!CanPenetrate(from_entity, autowall_info, weapon_info))
			break;

		return_info.did_penetrate_wall = true;
	}

	return_info.penetration_count = autowall_info.penetration_count;
	return_info.percent = (float)return_info.damage / (float)weapon_info->damage * 100;
	Utilities::Log(std::to_string(return_info.percent).c_str());
	return return_info;
}