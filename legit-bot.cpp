#include "sdk.h"
#include "c_track-manager.hpp"
#include "legit-bot.h"
#include "global.h"
#include "GameUtils.h"
#include "Math.h"

bool CLegitBot::SettingProxy()
{
	if (!c_ayala::c_weapon)
		return false;
	if (c_ayala::c_weapon->IsMiscWeapon())
		return false;
	if (c_ayala::c_weapon->GetWeaponType() == WEPCLASS_KNIFE || c_ayala::c_weapon->GetWeaponType() == WEPCLASS_INVALID)
		return false;
	int WeaponID = c_ayala::c_weapon->GetWeaponNum();

	if (WeaponID < 0)
		return false;
	nBackupButtons = c_ayala::user_cmd->buttons;
	Setting.flFov = Menu.LegitBot.flFov[WeaponID];
	Setting.flPrediction = Menu.LegitBot.flPrediction[WeaponID];
	Setting.flRecoilControl = Menu.LegitBot.flRecoil[WeaponID];
	Setting.bUseSpecificAim = Menu.LegitBot.bUseSpecificAim[WeaponID];
	Setting.flSwitchTargetDelay = Menu.LegitBot.flSwitchTargetDelay[WeaponID];
	Setting.iAimType = Menu.LegitBot.iAimType[WeaponID];
	Setting.iBackTrackType = Menu.LegitBot.iBackTrackType[WeaponID];
	Setting.iDelay = Menu.LegitBot.iDelay[WeaponID];
	Setting.iFovType = Menu.LegitBot.iFovType[WeaponID];
	Setting.iSmoothType = Menu.LegitBot.iSmoothType[WeaponID];
	Setting.flSmooth = Menu.LegitBot.flSmooth[WeaponID];
	Setting.iSpecificAimType = Menu.LegitBot.iSpecificAimType[WeaponID];
	return true;
}

QAngle CLegitBot::Smooth(QAngle delta)
{
	if (delta.IsZero())
		return QAngle();
	if (delta.Length() < Statictic.flAverageMovementSpeed)
		return delta;
	return delta *  (1 / (delta.Length() / Statictic.flAverageMovementSpeed));
}

void CLegitBot::Run()
{
	if (!c_ayala::l_player)
		return;
	if (!c_ayala::l_player->isAlive())
	{
		ReTargetTime = 0.f;
		LastTargetIndex = -1;
	}
	CBaseCombatWeapon* pWeapon = c_ayala::l_player->GetWeapon();
	if (!pWeapon || pWeapon->IsMiscWeapon())
		return;
	float flNearestTick = FLT_MAX;
	float flNearestEntity = FLT_MAX;
	if (!SettingProxy())
		return;
	for (int i = 0; i <= g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (!pEntity || !pEntity->isAlive() || !pEntity->IsPlayer() || !pEntity->IsEnemy())
			continue;

		if (pEntity->GetVelocity().Length() < .1f)
			AimData[i].flStandTime += g_globals->interval_per_tick;
		else
			AimData[i].flStandTime = 0;

		if (Setting.iBackTrackType > 0)
		{
			auto Ticks = CBackTrackManager::Get().GetRecords(pEntity);

			if (Ticks.empty())
				continue;

			for (auto Tick : Ticks)
			{
				if (AimData[i].flStandTime > .2f)
					continue;

				if (!CBackTrackManager::Get().IsValidTick(Tick))
					continue;

				if (!CBackTrackManager::Get().IsVisibleTick(Tick, false, true))
					continue;

				Vector HeadPos = pEntity->GetBonePos(8, Tick.boneMatrix);
				float Fov = Get2DFov(HeadPos);

				if (Fov > 0 && Fov < flNearestTick)
				{
					flNearestTick = Fov;
					NearestTick = Tick;
				}
			}
		}
		RecordTick_t CurentTick(pEntity);
		if (CBackTrackManager::Get().IsVisibleTick(CurentTick, false, true))
		{
			Vector HeadPos = pEntity->GetBonePos(8);
			float Fov = Get2DFov(HeadPos);

			if (Fov > 0 && Fov < flNearestEntity)
			{
				flNearestEntity = Fov;
				NearestEntity = CurentTick;
			}
		}
	}

	if (Setting.bRecordStatictic)
	{
		QAngle MyAngle;
		g_pEngine->GetViewAngles(MyAngle);
		if (Statictic.angOldEyeAngle != QAngle())
		{
			QAngle Movement = Statictic.angOldEyeAngle - MyAngle;
			Movement.Normalized();
			float flMovement = Movement.Length();
			if (flMovement > 1.2f)
			{
				if (Statictic.iRecordCount > 50000) //������ ��� ��������� ����� �� �������, �������� ���� ����� ������� ��� ������
					Statictic.iRecordCount = 50000;
				float flAllMovement = Statictic.flAverageMovementSpeed * Statictic.iRecordCount++;
				flAllMovement += flMovement;
				Statictic.flAverageMovementSpeed = flAllMovement / Statictic.iRecordCount;
			}
		}
		Statictic.angOldEyeAngle = MyAngle;
	}
	bool ValidEnt = CBackTrackManager::Get().IsValidTick(NearestEntity);
	bool ValidTick = CBackTrackManager::Get().IsValidTick(NearestTick);
	RecordTick_t Target;
	if (Setting.iBackTrackType < 2 && ValidEnt)
		Target = NearestEntity;
	if (Setting.iBackTrackType == 2 && ValidTick)
		Target = NearestTick;
	else if (Setting.iBackTrackType == 2 && ValidEnt)
		Target = NearestEntity;
	if (bReadyToNextTarget && CBackTrackManager::Get().IsValidTick(Target) && CBackTrackManager::Get().IsVisibleTick(Target, true, true) && c_ayala::user_cmd->buttons & IN_ATTACK)
		CreateAimPath(Target);
	bReadyToNextTarget = Aimbot();
	bReadyToNextTarget = bReadyToNextTarget && (ReTargetTime + Setting.flSwitchTargetDelay < g_globals->curtime || Target.iIndex == LastTargetIndex);

	if (c_ayala::user_cmd->buttons & IN_ATTACK && ValidTick && Setting.iBackTrackType > 0)
	{
		c_ayala::user_cmd->tick_count = TIME_TO_TICKS(NearestTick.flSimulationTime);
	}
	g_pEngine->GetViewAngles(UserMoveDetect);
}

float CLegitBot::Get2DFov(Vector pos)
{
	Vector vecScreen;
	int iWidth, iHeight;
	QAngle punchAngle = c_ayala::l_player->GetPunchAngle() * 2;
	g_pEngine->GetScreenSize(iWidth, iHeight);
	int x = iWidth / 2;
	int y = iHeight / 2;
	int dy = iHeight / 90;
	int dx = iWidth / 90;
	x -= (dx * punchAngle.y);
	y += (dy * punchAngle.x);
	if (GameUtils::WorldToScreen(pos, vecScreen))
		return (Vector(x - vecScreen.x, y - vecScreen.y, 0).Length2D()) / (iHeight / 2) * 90;
	return -1.f;
}

float CLegitBot::GetAngFov(Vector pos)
{
	QAngle angDelta = GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), pos) - UserMoveDetect /*- c_ayala::l_player->GetPunchAngle() **/;
	return Vector(Math::NormalizeYaw(angDelta.x), Math::NormalizeYaw(angDelta.y), 0).Length2D();
}

float CLegitBot::Get3DFov(Vector pos) 
{
	float flDist = (c_ayala::l_player->GetEyePosition() - pos).Length();
	Vector forward, endtrace;

	Math::AngleVectors(c_ayala::l_player->GetEyeAngles(), &forward);
	endtrace = c_ayala::l_player->GetEyePosition() + (forward * flDist);

	return (pos - endtrace).Length();
}

bool CLegitBot::Aimbot()
{
	if (AimPath.empty())
		return true;
	QAngle MyAngle;
	g_pEngine->GetViewAngles(MyAngle);
	if ((MyAngle - UserMoveDetect).Length() > 0.3) 
	{
		c_ayala::user_cmd->buttons = nBackupButtons;
		AimPath.clear();
		return true;
	}
	if (AimPath[0].bNoShot/* && c_ayala::user_cmd->buttons & IN_ATTACK*/) //Perfect way for delay
		c_ayala::user_cmd->buttons &= ~IN_ATTACK;
	if (AimPath[0].bSilentHit && !AimPath[0].bNoShot)
	{
		c_ayala::user_cmd->viewangles.y = AimPath[AimPath.size() - 1].angAimAngle.y - c_ayala::l_player->GetPunchAngle().y * Setting.flRecoilControl / 50;
		c_ayala::c_SendPacket = false;
		if (!c_ayala::user_cmd->buttons & IN_ATTACK)
			c_ayala::user_cmd->buttons |= IN_ATTACK;
	}
	if (AimPath[0].bForceShot)
		c_ayala::user_cmd->buttons |= IN_ATTACK;
	g_pEngine->SetViewAngles(AimPath[0].angAimAngle - c_ayala::l_player->GetPunchAngle() * Setting.flRecoilControl / 50/* - Smooth(c_ayala::l_player->GetPunchAngle() *)*/);
	AimPath.erase(AimPath.begin());
	if (AimPath.empty())
		ReTargetTime = g_globals->curtime;
	return false;
}

void CLegitBot::CreateAimPath(RecordTick_t target)
{
	auto VectorTransform_Wrapper = [](const Vector& in1, const VMatrix &in2, Vector &out)
	{
		auto VectorTransform = [](const float *in1, const VMatrix& in2, float *out)
		{
			auto DotProducts = [](const float *v1, const float *v2)
			{
				return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
			};
			out[0] = DotProducts(in1, in2[0]) + in2[0][3];
			out[1] = DotProducts(in1, in2[1]) + in2[1][3];
			out[2] = DotProducts(in1, in2[2]) + in2[2][3];
		};
		VectorTransform(&in1.x, in2, &out.x);
	};
	C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(target.iIndex);
	if (!pEntity || !pEntity->isAlive() || !pEntity->IsPlayer())
		return;
	studiohdr_t* pStudioModel = g_pModelInfo->GetStudioModel(pEntity->GetModel());
	if (!pStudioModel)
		return;
	mstudiohitboxset_t* set = pStudioModel->pHitboxSet(0);
	if (!set)
		return;
	float Fov = FLT_MAX;
	Vector Target;
	for (int i = 0; i < set->numhitboxes; i++) {
		mstudiobbox_t *hitbox = set->pHitbox(i);
		if (!hitbox)
			continue;

		Vector max;
		Vector min;

		VectorTransform_Wrapper(hitbox->bbmax, target.boneMatrix[hitbox->bone], max);
		VectorTransform_Wrapper(hitbox->bbmin, target.boneMatrix[hitbox->bone], min);

		auto center = (min + max) * 0.5f;
		float fov = Get2DFov(center);
		if (fov < Fov)
		{
			Ray_t ray;
			ray.Init(c_ayala::l_player->GetEyePosition(), center);
			CTraceFilter filter;
			filter.pSkip1 = c_ayala::l_player;

			trace_t trace;
			g_pEngineTrace->TraceRay(ray, MASK_SHOT | CONTENTS_GRATE, &filter, &trace);

			if (trace.fraction > .95f) 
			{
				Fov = fov;
				Target = center;
			}
		}
	}
	if (Fov > Setting.flFov) //FovCheck
		return;
	if (!Target.IsValid() || Target.IsZero())
		return;
	Vector Velocity = pEntity->GetVelocity();
	QAngle AimAngle = Math::NormalizeAngle2(GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), Target));
	QAngle PredictableAimPath = Math::NormalizeAngle2(GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), Target + Velocity) - AimAngle);
	//g_pDebugOverlay->AddBoxOverlay(Target, Vector(2, 2, 2), Vector(-2, -2, -2), QAngle(0, 0, 0), 255, 255, 255, 255, 2);
	//g_pDebugOverlay->AddBoxOverlay(Target + Velocity, Vector(2, 2, 2), Vector(-2, -2, -2), QAngle(0, 0, 0), 255, 255, 255, 255, 0.1);
	QAngle MyAngle;
	g_pEngine->GetViewAngles(MyAngle);
	MyAngle += c_ayala::l_player->GetPunchAngle() * Setting.flRecoilControl / 50;
	if (Math::NormalizeAngle2(AimAngle - MyAngle).Length2D() < 0.7)
		return;
	int AimSteps = 1;
	if (Setting.iSmoothType == 0)
	{
		AimSteps = AimAngle.Length2D() / Statictic.flAverageMovementSpeed;
		if (AimAngle.Length2D() < Statictic.flAverageMovementSpeed)
			AimSteps = 1;
	}
	else if (Setting.iSmoothType == 1)
	{
		AimSteps = Setting.flSmooth > g_globals->interval_per_tick ? Setting.flSmooth / g_globals->interval_per_tick : 1;
	}
	if (Setting.bUseSpecificAim == false) 
	{
		if (Setting.iAimType == 0) 
		{
			for (int i = 1; i <= AimSteps; i++)
			{
				AimPath_t Path;
				Path.angAimAngle = Math::NormalizeAngle2(MyAngle + (Math::NormalizeAngle2(AimAngle + Math::NormalizeAngle2(PredictableAimPath / 1 * g_globals->interval_per_tick * AimSteps * Setting.flPrediction / 100) - MyAngle) / AimSteps * i));
				Path.bNoShot = Setting.iDelay > 0;
				Path.bSilentHit = false;
				Path.bForceShot = false;
				AimPath.push_back(Path);
			}
		}
		if (Setting.iAimType == 1)
		{
			for (int i = 1; i <= AimSteps; i++)
			{
				AimPath_t Path;
				Path.angAimAngle = Math::NormalizeAngle2(MyAngle + (Math::NormalizeAngle2(QAngle((AimAngle.x + Math::NormalizeYaw(PredictableAimPath.x / 1 * AimSteps * g_globals->interval_per_tick * Setting.flPrediction / 100) - MyAngle.x) * pow(sin((M_PI / 2) / AimSteps * i),2), (AimAngle.y + Math::NormalizeYaw(PredictableAimPath.y / 1 * g_globals->interval_per_tick * AimSteps * Setting.flPrediction / 100) - MyAngle.y) * pow(sin((M_PI / 2) / AimSteps * i),4)))));
				Path.bNoShot = Setting.iDelay > 0;
				Path.bSilentHit = false;
				Path.bForceShot = false;
				AimPath.push_back(Path);
			}
		}
		if (Setting.iDelay == 2)
		{
			AimPath_t Path;
			Path.angAimAngle = Math::NormalizeAngle2(MyAngle + (Math::NormalizeAngle2(AimAngle + Math::NormalizeAngle2(PredictableAimPath / 1 * g_globals->interval_per_tick * AimSteps * Setting.flPrediction / 100) - MyAngle)));
			Path.bNoShot = false;
			Path.bSilentHit = false;
			Path.bForceShot = true;
			AimPath.push_back(Path);
			AimPath.push_back(Path);
		}
	}
	else
	{

	}
	LastTargetIndex = target.iIndex;
	if (Setting.iDelay > 0)
		c_ayala::user_cmd->buttons &= ~IN_ATTACK;
}

