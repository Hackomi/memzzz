﻿#include "sdk.h"
#include "hooks.h"
#include "Menu.h"
#include "MaterialHelper.h"
#include "fake_latency.hpp"
#include "security.h"
#include "Math.h"
#include "global.h"
#include <dbghelp.h>
#include <tchar.h>
#include "Config.h"
#include "ScreenEffects.h"
#include "AW_hitmarker.h"
#include "Serversounds.h"
#pragma once

#include <map>

typedef DWORD** PPDWORD;
class VFTableHook {
	VFTableHook(const VFTableHook&) = delete;
public:
	VFTableHook(PPDWORD ppClass, bool bReplace) {
		m_ppClassBase = ppClass;
		m_bReplace = bReplace;
		if (bReplace) {
			m_pOriginalVMTable = *ppClass;
			uint32_t dwLength = CalculateLength();

			m_pNewVMTable = new DWORD[dwLength];
			memcpy(m_pNewVMTable, m_pOriginalVMTable, dwLength * sizeof(DWORD));

			DWORD old;
			VirtualProtect(m_ppClassBase, sizeof(DWORD), PAGE_EXECUTE_READWRITE, &old);
			*m_ppClassBase = m_pNewVMTable;
			VirtualProtect(m_ppClassBase, sizeof(DWORD), old, &old);

		}
		else {
			m_pOriginalVMTable = *ppClass;
			m_pNewVMTable = *ppClass;
		}
	}
	~VFTableHook() {
		RestoreTable();
		if (m_bReplace && m_pNewVMTable) delete[] m_pNewVMTable;
	}

	void RestoreTable() {
		for (auto& pair : m_vecHookedIndexes) {
			Unhook(pair.first);
		}
	}

	template<class Type>
	static Type HookManual(uintptr_t* vftable, uint32_t index, Type fnNew) {
		DWORD Dummy;
		Type fnOld = (Type)vftable[index];
		VirtualProtect((void*)(vftable + index * 0x4), 0x4, PAGE_EXECUTE_READWRITE, &Dummy);
		vftable[index] = (uintptr_t)fnNew;
		VirtualProtect((void*)(vftable + index * 0x4), 0x4, Dummy, &Dummy);
		return fnOld;
	}

	template<class Type>
	static void UnHookManual(uintptr_t* vftable, uint32_t index, Type fnNew) {
		DWORD Dummy;
		VirtualProtect((void*)(vftable + index * 0x4), 0x4, PAGE_EXECUTE_READWRITE, &Dummy);
		vftable[index] = (uintptr_t)fnNew;
		VirtualProtect((void*)(vftable + index * 0x4), 0x4, Dummy, &Dummy);
	}

	template<class Type>
	Type Hook(uint32_t index, Type fnNew) {
		DWORD dwOld = (DWORD)m_pOriginalVMTable[index];
		m_pNewVMTable[index] = (DWORD)fnNew;
		m_vecHookedIndexes.insert(std::make_pair(index, (DWORD)dwOld));
		return (Type)dwOld;
	}

	void Unhook(uint32_t index) {
		auto it = m_vecHookedIndexes.find(index);
		if (it != m_vecHookedIndexes.end()) {
			m_pNewVMTable[index] = (DWORD)it->second;
			m_vecHookedIndexes.erase(it);
		}
	}

	template<class Type>
	Type GetOriginal(uint32_t index) {
		return (Type)m_pOriginalVMTable[index];
	}

private:
	uint32_t CalculateLength() {
		uint32_t dwIndex = 0;
		if (!m_pOriginalVMTable) return 0;
		for (dwIndex = 0; m_pOriginalVMTable[dwIndex]; dwIndex++) {
			if (IsBadCodePtr((FARPROC)m_pOriginalVMTable[dwIndex])) {
				break;
			}
		}
		return dwIndex;
	}

private:
	std::map<uint32_t, DWORD> m_vecHookedIndexes;

	PPDWORD m_ppClassBase;
	PDWORD m_pOriginalVMTable;
	PDWORD m_pNewVMTable;
	bool m_bReplace;
};
HINSTANCE hAppInstance;
CClient* g_pClient;
CClientEntityList* g_pEntitylist;
CEngine* g_pEngine;
CInput* g_pInput;
CModelInfo* g_pModelInfo;
CInputSystem* g_pInputSystem;
CPanel* g_pPanel;
IStudioRender* g_pStudioRender;
CSurface* g_pSurface;
CEngineTrace* g_pEngineTrace;
CDebugOverlay* g_pDebugOverlay;
IPhysicsSurfaceProps* g_pPhysics;
CRenderView* g_pRenderView;
CUtlVectorSimple *g_ClientSideAnimationList;
CClientModeShared* g_pClientMode;
CGlobalVarsBase* g_globals;
CModelRender* g_pModelRender;
CGlowObjectManager* g_GlowObjManager;
CMaterialSystem* g_pMaterialSystem;
IMoveHelper* g_pMoveHelper;
IDirect3DDevice9* g_pDevice;
IEngineSound* g_pEngineSound;
CPrediction* g_pPrediction;
CGameMovement* g_pGameMovement;
IGameEventManager* g_pGameEventManager;
IEngineVGui* g_pEngineVGUI;
ICvar* g_pCvar;
CInterfaces Interfaces;
CCRC gCRC;
CClientState* g_pClientState;
C_TEFireBullets* g_pFireBullet;
IViewRenderBeams* g_pViewRenderBeams;
ISteamClient* g_pSteamClient;
ISteamHTTP* g_pSteamHTTP;

VMT* panelVMT;
VMT* beginFrame;
VMT* clientmodeVMT;
VMT* enginevguiVMT;
VMT* modelrenderVMT;
VMT* clientVMT;
VMT* HNetchan;
VMT* firebulletVMT;
VMT* d3d9VMT;
VMT* netChan;
VMT* renderviewVMT;
VMT* TraceVMT;
VMT* engineVMT;
VMT* SurfaceVMT;

//
namespace FakeLatency
{
	extern char *clientdllstr;
	extern char *enginedllstr;
	extern char *cam_tothirdperson_sig;
	extern char *cam_tofirstperson_sig;
	extern char *clientstatestr;
	extern char *updateclientsideanimfnsigstr;
	extern void DecStr(char *adr, const unsigned len);
	extern void EncStr(char *adr, const unsigned len);
	extern void DecStr(char *adr);
	extern void EncStr(char *adr);
	extern HANDLE FindHandle(std::string name);
	extern uintptr_t FindMemoryPattern(HANDLE ModuleHandle, char* strpattern, int length);
	extern HANDLE EngineHandle;
	extern HANDLE ClientHandle;
	extern DWORD ClientState;
}
HMODULE h_ThisModule;
namespace Offsets
{
	DWORD invalidateBoneCache = 0x00;
	DWORD smokeCount = 0x00;
	DWORD playerResource = 0x00;
	DWORD bOverridePostProcessingDisable = 0x00;
	DWORD getSequenceActivity = 0x00;
	DWORD lgtSmoke = 0x00;
	DWORD dwCCSPlayerRenderablevftable = 0x00;
}

void Init()
{
	SetupOffsets();
	g_Event.RegisterSelf();
	Offsets::dwCCSPlayerRenderablevftable = *(DWORD*)(FindPatternIDA("client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 89 7C 24 0C") + 0x4E);

	GUI_Init();

	Hooks::g_pOldWindowProc = (WNDPROC)SetWindowLongPtr(c_ayala::Window, GWLP_WNDPROC, (LONG_PTR)Hooks::WndProc);
	DWORD adr = FindPatternIDA(XorStr("engine.dll"), XorStr("8B 3D ? ? ? ? 8A F9"));
	FakeLatency::ClientState = *(DWORD*)(adr + 2);

	panelVMT = new VMT(g_pPanel);
	panelVMT->HookVM((void*)Hooks::PaintTraverse, 41);
	panelVMT->ApplyVMT();
	clientmodeVMT = new VMT(g_pClientMode);
	clientmodeVMT->HookVM((void*)hkDoPostScreenSpaceEffects, 44);
	clientmodeVMT->HookVM((void*)GGetViewModelFOV, 35);
	clientmodeVMT->HookVM((void*)Hooks::CreateMove, 24);
	clientmodeVMT->HookVM((void*)Hooks::OverrideView, 18);
	clientmodeVMT->ApplyVMT();
	renderviewVMT = new VMT(g_pRenderView);
	renderviewVMT->HookVM((void*)Hooks::scene_end, 9);
	renderviewVMT->ApplyVMT();
	modelrenderVMT = new VMT(g_pModelRender);
	modelrenderVMT->HookVM((void*)Hooks::DrawModelExecute, 21);
	modelrenderVMT->ApplyVMT();
	clientVMT = new VMT(g_pClient);
	clientVMT->HookVM((void*)Hooks::FrameStageNotify, 37);
	clientVMT->ApplyVMT();
	TraceVMT = new VMT(g_pEngineTrace);
	TraceVMT->HookVM(Hooks::HookedTraceRay, 5);
	TraceVMT->ApplyVMT();
	//oSetupBones = VFTableHook::HookManual<SetupBones_t>(*(uintptr_t**)Offsets::dwCCSPlayerRenderablevftable, 13, (SetupBones_t)Hooks::SetupBones_h);
	//engineVMT = new VMT(g_pEngine);
	//engineVMT->ApplyVMT();
	//engineVMT->HookVM((void*)Hooks::hooked_timedemo, 84);
	//engineVMT->ApplyVMT();
	SurfaceVMT = new VMT(g_pSurface);
	SurfaceVMT->HookVM(Hooks::LockCursor, 67);
	SurfaceVMT->ApplyVMT();
	beginFrame = new VMT(g_pStudioRender);
	beginFrame->HookVM((void*)Hooks::DrawModel, 29);
	beginFrame->HookVM((void*)Hooks::g_hkBeginFrame, 9);
	beginFrame->ApplyVMT();
	auto dwDevice = **(uint32_t**)(FindPatternIDA(XorStr("shaderapidx9.dll"), XorStr("A1 ? ? ? ? 50 8B 08 FF 51 0C")) + 1);
	d3d9VMT = new VMT((void*)dwDevice);
	d3d9VMT->HookVM((void*)(Hooks::hkdReset), 16);
	d3d9VMT->HookVM((void*)Hooks::D3D9_EndScene, 42);
	d3d9VMT->ApplyVMT();
	g_GlowObjManager = *(CGlowObjectManager**)(FindPatternIDA(XorStr("client_panorama.dll"), XorStr("0F 11 05 ? ? ? ? 83 C8 01 C7 05 ? ? ? ? 00 00 00 00")) + 3);
	c_ayala::CourierNew = g_pSurface->SCreateFont();
	g_pSurface->SetFontGlyphSet(c_ayala::CourierNew, XorStr("Courier New"), 14, 300, 0, 0, FONTFLAG_OUTLINE);
}

void StartCheat()
{
	//Utilities::OpenConsole("Debug");
	c_ayala::Panorama = GetModuleHandleA(XorStr("client_panorama.dll"));
	if (Interfaces.GetInterfaces(h_ThisModule) && g_pPanel && g_pClientMode)
	{
		Sleep(500);
		Init();
	}	
	while (!GetAsyncKeyState(VK_END))
	{
		Sleep(100);
	}
	SetWindowLongPtr(c_ayala::Window, GWLP_WNDPROC, (LONG_PTR)Hooks::g_pOldWindowProc);
	panelVMT->ReleaseVMT();
	beginFrame->ReleaseVMT();
	clientmodeVMT->ReleaseVMT();
	modelrenderVMT->ReleaseVMT();
	clientVMT->ReleaseVMT();
	d3d9VMT->ReleaseVMT();
	renderviewVMT->ReleaseVMT();
	TraceVMT->ReleaseVMT();
	//engineVMT->ReleaseVMT();
	if (HNetchan)
		HNetchan->ReleaseVMT();
	SurfaceVMT->ReleaseVMT();
	Sleep(2000);
	FreeConsole();
	FreeLibraryAndExitThread(h_ThisModule, 0);
}

BOOL WINAPI DllMain(HINSTANCE Instance, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
	//	Utilities::OpenConsole("Debug");
		DisableThreadLibraryCalls(Instance);
		c_ayala::Window = FindWindowA((XorStr("Valve001")), 0);
		h_ThisModule = Instance;
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)StartCheat, 0, 0, 0);
		break;
	}
	return true;
}

MsgFn oMsg;

void __cdecl Msg(char const* msg, ...)
{
	//DOES NOT CRASH
	if (!oMsg)
		oMsg = (MsgFn)GetProcAddress(GetModuleHandle(XorStr("tier0.dll")), XorStr("Msg"));

	char buffer[989];
	va_list list;
	va_start(list, msg);
	vsprintf_s(buffer, msg, list);
	va_end(list);
	oMsg(buffer, list);
}