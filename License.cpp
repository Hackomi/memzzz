#include "sdk.h"

__forceinline bool CLicense::SendOffset(HMODULE inst)
{
	auto Request = g_pSteamHTTP->CreateHTTPRequest(HTTP_METHOD_GET, XorStr("https://ayala.club/forums/offets.php"));
	SteamAPICall_t api;

	if (Request)
	{
		DWORD proc;
		GetVolumeInformation(XorStr("C:/"), 0, 0, &proc, 0, 0, 0, NULL);
		if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("hwid"), md5(std::to_string(proc)).c_str()))
		{
			if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("set"), XorStr("1")))
			{
				if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("hwid_value"), std::to_string((int)szHwid /*- (int)inst*/).c_str()))
				{
					if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("token_value"), std::to_string((int)szToken /*- (int)inst*/).c_str()))
					{
						if (g_pSteamHTTP->SendHTTPRequest(Request, &api))
						{
							/*uint32_t iSize = 0;

							while (iSize < 1)
							{
								Sleep(200);
								g_pSteamHTTP->GetHTTPResponseBodySize(Request, &iSize);
							}*/
							//Utilities::Log("Send %#X %#X base adress %#X", (int)szHwid /*- (int)inst*/, (int)szToken /*- (int)inst*/, inst);
							if (g_pSteamHTTP->ReleaseHTTPRequest(Request))
								return true;
						}
					}
				}
			}
		}
	}

	return false;
}

__forceinline bool CLicense::SendAuthReq()
{
	auto Request = g_pSteamHTTP->CreateHTTPRequest(HTTP_METHOD_GET, XorStr("https://ayala.club/forums/zxcvbn.php"));
	SteamAPICall_t api;

	if (Request)
	{
		DWORD proc;
		GetVolumeInformation(XorStr("C:/"), 0, 0, &proc, 0, 0, 0, NULL);
		if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("hwid"), md5(std::to_string(proc)).c_str()))
		{
			if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("stage"), XorStr("0")))
			{
				if (g_pSteamHTTP->SendHTTPRequest(Request, &api))
				{
					uint32_t iSize = 0;

					while (iSize < 1)
					{
						Sleep(200);
						g_pSteamHTTP->GetHTTPResponseBodySize(Request, &iSize);
					}

					if (g_pSteamHTTP->GetHTTPResponseBodyData(Request, (uint8_t*)szServerToken, iSize))
					{
						Utilities::Log(XorStr("Auth token %s"), std::string(szServerToken).c_str());
						if (g_pSteamHTTP->ReleaseHTTPRequest(Request))
							return true;
					}
				}
			}
		}
	}
	return false;
}

__forceinline bool CLicense::GetNickName()
{
	auto Request = g_pSteamHTTP->CreateHTTPRequest(HTTP_METHOD_GET, XorStr("https://ayala.club/forums/zxcvbn.php"));
	SteamAPICall_t api;

	if (Request)
	{
		if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("hwid"), std::string(szHwid).c_str()))
		{
			if (g_pSteamHTTP->SetHTTPRequestGetOrPostParameter(Request, XorStr("stage"), XorStr("1")))
			{
				if (g_pSteamHTTP->SendHTTPRequest(Request, &api))
				{
					uint32_t iSize = 0;

						while (iSize < 1)
						{
							Sleep(200);
								g_pSteamHTTP->GetHTTPResponseBodySize(Request, &iSize);
						}

					if (g_pSteamHTTP->GetHTTPResponseBodyData(Request, (uint8_t*)szNickName.c_str(), iSize))
					{
						Utilities::Log(XorStr("NickName %s"), szNickName.c_str());
						if (g_pSteamHTTP->ReleaseHTTPRequest(Request))
							return true;
					}
				}
			}
		}
	}
	return false;
}

bool CLicense::Request(HMODULE inst)
{
	if (SendOffset(inst))
	{
		while (!IsDataReady())
			Sleep(200);

		if (SendAuthReq())
		{
			for (int i = 0; i < 32; ++i) {
				if (szToken[i] != szServerToken[i])
					return false;
			}
			return GetNickName();
		}
	}
	else
	{
		Utilities::Log(XorStr("Error send"));
		Sleep(100);
		Request(inst);
	}
	return false;
}

__forceinline bool CLicense::IsDataReady()
{
	Utilities::Log(XorStr("Check hwid = %s token = %s"), szHwid, szToken);
	return szHwid[0] != 0 && szHwid[31] != 0 && szToken[0] != 0 && szToken[31] != 0;
}


