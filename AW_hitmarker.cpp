#pragma once
#include "GameUtils.h"
#include "AW_hitmarker.h"
#include <chrono>
#include "render.h"
CAW_hitmaker* hitmarker_2 = new CAW_hitmaker();

void CAW_hitmaker::paint()
{
	if (!Menu.Visuals.Hitmarker)
		return;
	float time = GameUtils::GetCurTime();

	for (int i = 0; i < hitmarkers.size(); i++)
	{   
		c_ayala::expired = time >= hitmarkers.at(i).impact.time + 1.4f;
		if (c_ayala::expired)
			hitmarkers.at(i).alpha -= 1;
		if (c_ayala::expired && hitmarkers.at(i).alpha <= 0)
		{
			hitmarkers.erase(hitmarkers.begin() + i);
			continue;
		}

		Vector pos3D = Vector(hitmarkers.at(i).impact.x, hitmarkers.at(i).impact.y, hitmarkers.at(i).impact.z), pos2D;
		if (!GameUtils::WorldToScreen(pos3D, pos2D))
			continue;

		int linesize = 2;

		g_pSurface->SetDrawColor(240, 240, 240, hitmarkers.at(i).alpha);
		g_pSurface->DrawLine(pos2D.x - linesize * 2, pos2D.y - linesize * 2, pos2D.x - (1), pos2D.y - (1));
		g_pSurface->DrawLine(pos2D.x - linesize * 2, pos2D.y + linesize * 2, pos2D.x - (1), pos2D.y + (1));
		g_pSurface->DrawLine(pos2D.x + linesize * 2, pos2D.y + linesize * 2, pos2D.x + (2), pos2D.y + (2));
		g_pSurface->DrawLine(pos2D.x + linesize * 2, pos2D.y - linesize * 2, pos2D.x + (2), pos2D.y - (2));
		g_pSurface->SetDrawColor(0, 0, 204, hitmarkers.at(i).alpha);
	}
}