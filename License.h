#pragma once
#include "Singleton.h"

class CLicense : public Singleton<CLicense>
{
private:
	char szHwid[33] = { 0 };
	char szToken[33] = { 0 };
	char szServerToken[33] = { 0 };
	std::string szNickName = XorStr("Error");
	__forceinline bool SendOffset(HMODULE inst);
	__forceinline bool SendAuthReq();
	__forceinline bool GetNickName();
	__forceinline bool IsDataReady();
public:
	bool Request(HMODULE inst);
	/*bool FastCheck(int num, int offset);
	bool DeepCheck();*/
};