#pragma once
#include "sdk.h"
#include "hooks.h"
#include <vector>
#include "Singleton.h"

template <typename T>
class CElement {
public:
	int iX, iY, iMin, iMax, iTab;
	T pSetting;
	bool* pDraw, *pControl;
	char* Tooltip;
	std::vector<std::string> Data;
	void DrawToolTip();
	virtual void Action() = 0;
	void Update(char* data, int x, int y, T setting, bool * render, bool * update, int min, int max, int tab, char* tooltip);
};

template <typename T>
class CCheckbox : public CElement<T>
{
public:
	void Action();
};
template <typename T>
class CSlider : public CElement<T>
{
private:
	bool bChangeValue = false, bAccess = false;
public:
	void Action();
};
template <typename T>
class CDropbox : public CElement<T>
{
private:
	bool bChangeValue = false, bAccess = false;
public:
	void Action();
};
template <typename T>
class CColorPicker : public CElement<T>
{
private:
	int iDeltaX = 0, iDeltaY = 0;
	bool bChangeValue = false, bAccess = false;
public:
	void Action();
};
template <typename T>
class CTextInput : public CElement<T>
{
private:
	bool bChangeValue = false;
	std::string buffer;
public:
	void CaptureInput();
	void Action();
};
template <typename T>
class CKeyBind : public CElement<T>
{
private:
	bool bChangeValue = false, bAccess = false;
public:
	void Action();
};
class CGroup
{
public:
	std::vector<CElement<void*>*> Objects;
	std::vector<CGroup*> Groups;
	void Init(int x, int y, bool * render, bool * update);
	void Draw();
	void Update();
	void AddElement(int x = 0, int y = 0, void* setting = nullptr, bool* render = nullptr, bool* update = nullptr);
	void RecreateObjectPosition();
};

struct �Tab {
	�Tab(int x, int y, int w, int h, int n, char* name) : iX(x), iY(y), iWidth(w), iHeight(h), iN(n), Name(name) { iAlpha = 0; }
	int iX, iY, iN, iWidth, iHeight, iAlpha;
	char* Name;
};

class CMenu : public Singleton<CMenu>, public CGroup {
private:
	std::vector<�Tab> Tabs;
	void BackGround();
	void MainControl();
	void UpdateControl();
	void SetupMenu();
	template <typename T>
	bool Animate(T & In, T Need, float StartTime, float AnimateTime);
	bool OpenTabAnimate(float Time);
	void DrawTab(int& Tab);
	bool CloseTabAnimation(float Time);
	void DrawMouse();
public:
	void RegisterTab(char* name);
	template <typename C, typename T>
	void AddElement(char* name, int tab, int x = 0, int y = 0, T* setting = nullptr, int min = 0, int max = 1, char* tooltip = nullptr, bool* render = nullptr, bool* update = nullptr);
	void Render();

	Vector2D MousePos;
	int iX, iY, iMouseX, iMouseY, iTabCount, iSelectedTab, iMenuState;
	bool bPress, bClick, bMenuMove, bUse;
};
