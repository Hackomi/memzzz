#pragma once
#include "sdk.h"
#include "global.h"
#include "vmt.h"



bool _fastcall hkDoPostScreenSpaceEffects(void* ecx, void* edx, CViewSetup* pSetup)
{
	static auto ofunc = clientmodeVMT->GetOriginalMethod<do_post_screen_space_effects_t>(44);
	if (!c_ayala::l_player || !g_pEngine->IsConnected() || !g_pEngine->IsInGame())
		return ofunc(ecx, pSetup);
	IMaterial *pMatGlowColor = g_pMaterialSystem->FindMaterial("dev/glow_color", TEXTURE_GROUP_OTHER, true);

	g_pModelRender->ForcedMaterialOverride(pMatGlowColor);

	float Red = Menu.Colors.Glow[0];
	float Green = Menu.Colors.Glow[1];
	float Blue = Menu.Colors.Glow[2];

	if (Menu.Visuals.Glow && g_GlowObjManager && g_pEngine->IsConnected())
	{
		if (c_ayala::l_player)
		{
			for (int i = 0; i <= g_GlowObjManager->m_GlowObjectDefinitions.Count(); i++)
			{
				if (g_GlowObjManager->m_GlowObjectDefinitions[i].IsUnused() || !g_GlowObjManager->m_GlowObjectDefinitions[i].getEnt())
					continue;

				CGlowObjectManager::GlowObjectDefinition_t* glowEnt = &g_GlowObjManager->m_GlowObjectDefinitions[i];

				if (glowEnt != nullptr && glowEnt->getEnt()->IsValidRenderable() && glowEnt->getEnt()->IsPlayer())
				{
					ClientClass* pClass = (ClientClass*)glowEnt->getEnt()->GetClientClass();

					switch (pClass->m_ClassID)
					{
					case 38:
						glowEnt->set(Red, Green, Blue, Menu.Colors.Glow[3]);
						break;
					}
				}
			}
		}
		return ofunc(ecx, pSetup);
	}
}