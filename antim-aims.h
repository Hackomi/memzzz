#pragma once

class CAntiaim
{
private:
	
	void DormantAntiAims();
	void Pitch();
	void FakeDuck();
	void RealYaw();
	void FakeYaw();
	float get_max_desync_delta();
	bool Fakewalk(c_user_cmd * user_cmd);
	void UpdateLowerBodyYawTime(bool ignoremove);
	float GetRightYaw();
	void UpdateLowerBodyYawTime1(bool ignoremove);
	std::vector<float> GetYaw();
	float GetClosetYaw();
	float GetWorstYaw();
	bool no_active;
	//float GetBestPitch();
	enum
	{
		back,
		right,
		left
	};
public:
	float flLowerBodyYawUpdateTime = -1;
	float flUpdateTime = -1;
	int iChocked = 0;
	void Run();
	bool meme;
	
}; extern CAntiaim* g_Antiaim;
