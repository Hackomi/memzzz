#include "sdk.h"
#include "Misc.h"
#include "global.h"
#include <chrono>
#include "GameUtils.h"
#include "Math.h"
#include "xor.h"
#include "Draw.h"
#include <chrono>
CMisc* g_Misc = new CMisc;
typedef void(*RevealAllFn)(int);
RevealAllFn fnReveal;
//void CMisc::RankReveal()
//{
//	if (!Menu.Visuals.Rank)
//		return;
//
//	if(!fnReveal)
//		fnReveal = (RevealAllFn)Utilities::Memory::FindPattern(XorStr(c_ayala::Panorama ? "client_panorama.dll" : "client.dll"), (PBYTE)XorStr("\x55\x8B\xEC\x8B\x0D\x00\x00\x00\x00\x68\x00\x00\x00\x00"), XorStr("xxxxx????x????"));
//
//	int iBuffer[1];
//
//	if (c_ayala::user_cmd->buttons & IN_SCORE)
//		fnReveal(iBuffer[1]);
//}

float RightMovement;
bool IsActive;
float StrafeAngle;

template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
void RotateMovement(c_user_cmd* cmd, float yaw)
{
	Vector viewangles;
	g_pEngine->GetViewAngles(viewangles);

	float rotation = DEG2RAD(viewangles.y - yaw);

	float cos_rot = cos(rotation);
	float sin_rot = sin(rotation);

	float new_forwardmove = (cos_rot * cmd->forwardmove) - (sin_rot * cmd->sidemove);
	float new_sidemove = (sin_rot * cmd->forwardmove) + (cos_rot * cmd->sidemove);

	cmd->forwardmove = new_forwardmove;
	cmd->sidemove = new_sidemove;
}
#define CheckIfNonValidNumber(x) (fpclassify(x) == FP_INFINITE || fpclassify(x) == FP_NAN || fpclassify(x) == FP_SUBNORMAL)
template <class T>
constexpr const T& Clamp(const T& v, const T& lo, const T& hi)
{
	return (v >= lo && v <= hi) ? v : (v < lo ? lo : hi);
}
static inline float GetIdealRotation(float speed)
{
	return Clamp<float>(RAD2DEG(std::atan2(15.f, speed)), 0.f, 45.f) * g_globals->interval_per_tick;
} // per tick

void CMisc::AutoStrafe()
{
	bool CircleStrafe = GetAsyncKeyState(Menu.Misc.PrespeedKey);
	auto local_player = c_ayala::l_player;
	auto cmd = c_ayala::user_cmd;
	if (!Menu.Misc.AutoStrafe || c_ayala::l_player->GetMoveType() == MOVETYPE_LADDER)
		return;
	if (!local_player || local_player->GetHealth() <= 0 || (*local_player->GetFlags() & FL_ONGROUND && !(cmd->buttons & IN_JUMP)))
		return;
	static bool side_switch = false;
	side_switch = !side_switch;
	Vector viewangles;
    g_pEngine->GetViewAngles(viewangles);
	cmd->forwardmove = 0.f;
	cmd->sidemove = side_switch ? 450.f : -450.f;

	const float velocity_yaw = GameUtils::CalculateAngle(Vector(0, 0, 0), local_player->GetVelocity()).y;
	const float ideal_rotation = GetIdealRotation(local_player->GetVelocity().Length2D()) / g_globals->interval_per_tick;
	float ideal_yaw_rotation = (side_switch ? ideal_rotation : -ideal_rotation) + (fabs(Math::NormalizeYaw(velocity_yaw - viewangles.y)) < 5.f ? velocity_yaw : viewangles.y);
	if (GetAsyncKeyState(0x41))
		ideal_yaw_rotation += 90;
	if (GetAsyncKeyState(0x44))
		ideal_yaw_rotation -= 90;
	if (GetAsyncKeyState(0x53))
		ideal_yaw_rotation -= 180;
	RotateMovement(cmd, ideal_yaw_rotation);

}

void CMisc::FixCmd()
{
	if (Menu.Misc.AntiUT)
	{
		c_ayala::user_cmd->viewangles.y = Math::NormalizeYaw(c_ayala::user_cmd->viewangles.y);
		Math::ClampAngles(c_ayala::user_cmd->viewangles);

		if (c_ayala::user_cmd->forwardmove > 450)
			c_ayala::user_cmd->forwardmove = 450;
		if (c_ayala::user_cmd->forwardmove < -450)
			c_ayala::user_cmd->forwardmove = -450;

		if (c_ayala::user_cmd->sidemove > 450)
			c_ayala::user_cmd->sidemove = 450;
		if (c_ayala::user_cmd->sidemove < -450)
			c_ayala::user_cmd->sidemove = -450;
	}
}

void CMisc::Bunnyhop()
{
	if (Menu.Misc.AutoJump)
	{
		static auto bJumped = false;
		static auto bFake = false;
		if (!bJumped && bFake)
		{
			bFake = false;
			c_ayala::user_cmd->buttons |= IN_JUMP;
		}
		else if (c_ayala::user_cmd->buttons & IN_JUMP)
		{
			if (*c_ayala::l_player->GetFlags() & FL_ONGROUND)
			{
				bJumped = true;
				bFake = true;
			}
			else
			{
				c_ayala::user_cmd->buttons &= ~IN_JUMP;
				bJumped = false;
			}
		}
		else
		{
			bJumped = false;
			bFake = false;
		}
	}
}

void CMisc::FixMovement()
{
	auto cmd = c_ayala::user_cmd;
	Vector real_viewangles;
	g_pEngine->GetViewAngles(real_viewangles);

	Vector vecMove(cmd->forwardmove, cmd->sidemove, cmd->upmove);
	float speed = sqrt(vecMove.x * vecMove.x + vecMove.y * vecMove.y);

	Vector angMove;
	Math::VectorAngles(vecMove, angMove);

	float yaw = DEG2RAD(cmd->viewangles.y - real_viewangles.y + angMove.y);

	cmd->forwardmove = cos(yaw) * speed;
	cmd->sidemove = sin(yaw) * speed;

	cmd->buttons &= ~IN_RIGHT;
	cmd->buttons &= ~IN_MOVERIGHT;
	cmd->buttons &= ~IN_LEFT;
	cmd->buttons &= ~IN_MOVELEFT;
	cmd->buttons &= ~IN_FORWARD;
	cmd->buttons &= ~IN_BACK;

	if (cmd->forwardmove > 0.f)
		cmd->buttons |= IN_FORWARD;
	else if (cmd->forwardmove < 0.f)
		cmd->buttons |= IN_BACK;

	if (cmd->sidemove > 0.f)
	{
		cmd->buttons |= IN_RIGHT;
		cmd->buttons |= IN_MOVERIGHT;
	}
	else if (cmd->sidemove < 0.f)
	{
		cmd->buttons |= IN_LEFT;
		cmd->buttons |= IN_MOVELEFT;
	}
}
