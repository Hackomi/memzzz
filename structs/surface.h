#include "ImGUI\imconfig.h"
#include "ImGUI\imgui.h"
#include "ImGUI\imgui_internal.h"
#include "ImGUI\stb_rect_pack.h"
#include "ImGUI\stb_textedit.h"
#include "ImGUI\stb_truetype.h"
#include "ImGUI\imgui_impl_dx9.h"

struct Vertex_t
{
	Vector2D	m_Position;
	Vector2D	m_TexCoord;

	Vertex_t() {}
	Vertex_t(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
	{
		m_Position = pos;
		m_TexCoord = coord;
	}
	void Init(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
	{
		m_Position = pos;
		m_TexCoord = coord;
	}
};

typedef  Vertex_t FontVertex_t;
class Color
{
public:
	Color()
	{
		*((int *)this) = 0;
	}
	Color(int color32)
	{
		*((int *)this) = color32;
	}
	Color(int _r, int _g, int _b)
	{
		SetColor(_r, _g, _b, 255);
	}
	Color(int _r, int _g, int _b, int _a)
	{
		SetColor(_r, _g, _b, _a);
	}

	void SetColor(int _r, int _g, int _b, int _a = 255)
	{
		_color[0] = (unsigned char)_r;
		_color[1] = (unsigned char)_g;
		_color[2] = (unsigned char)_b;
		_color[3] = (unsigned char)_a;
	}

	ImU32 GetU32()
	{
		return ((_color[3] & 0xff) << 24) + ((_color[2] & 0xff) << 16) + ((_color[1] & 0xff) << 8)
			+ (_color[0] & 0xff);
		//return (ImU32)(((_color[3] & 0xff) << 24) + ((_color[0] & 0xff) << 16) + ((_color[1] & 0xff) << 8) + (_color[2] & 0xff));
	}

	void GetColor(int &_r, int &_g, int &_b, int &_a) const
	{
		_r = _color[0];
		_g = _color[1];
		_b = _color[2];
		_a = _color[3];
	}
	void SetAlpha(int a)
	{
		_color[3] = (unsigned char)a;
	}
	void SetRawColor(int color32)
	{
		*((int*)this) = color32;
	}

	int GetRawColor() const
	{
		return *((int*)this);
	}

	int GetD3DColor() const
	{
		return ((int)((((_color[3]) & 0xff) << 24) | (((_color[0]) & 0xff) << 16) | (((_color[1]) & 0xff) << 8) | ((_color[2]) & 0xff)));
	}

	inline int r() const	{ return _color[0]; }
	inline int g() const	{ return _color[1]; }
	inline int b() const	{ return _color[2]; }
	inline int a() const	{ return _color[3]; }

	unsigned char &operator[](int index)
	{
		return _color[index];
	}

	const unsigned char &operator[](int index) const
	{
		return _color[index];
	}

	bool operator == (const Color &rhs) const
	{
		return (*((int *)this) == *((int *)&rhs));
	}

	bool operator != (const Color &rhs) const
	{
		return !(operator==(rhs));
	}

	Color &operator=(const Color &rhs)
	{
		SetRawColor(rhs.GetRawColor());
		return *this;
	}

	float* Base()
	{
		float clr[3];

		clr[0] = _color[0] / 255.0f;
		clr[1] = _color[1] / 255.0f;
		clr[2] = _color[2] / 255.0f;

		return &clr[0];
	}

	float* BaseAlpha()
	{
		float clr[4];

		clr[0] = _color[0] / 255.0f;
		clr[1] = _color[1] / 255.0f;
		clr[2] = _color[2] / 255.0f;
		clr[3] = _color[3] / 255.0f;

		return &clr[0];
	}

	float Hue() const
	{
		if (_color[0] == _color[1] && _color[1] == _color[2])
		{
			return 0.0f;
		}

		float r = _color[0] / 255.0f;
		float g = _color[1] / 255.0f;
		float b = _color[2] / 255.0f;

		float max = r > g ? r : g > b ? g : b,
			min = r < g ? r : g < b ? g : b;
		float delta = max - min;
		float hue = 0.0f;

		if (r == max)
		{
			hue = (g - b) / delta;
		}
		else if (g == max)
		{
			hue = 2 + (b - r) / delta;
		}
		else if (b == max)
		{
			hue = 4 + (r - g) / delta;
		}
		hue *= 60;

		if (hue < 0.0f)
		{
			hue += 360.0f;
		}
		return hue;
	}

	float Saturation() const
	{
		float r = _color[0] / 255.0f;
		float g = _color[1] / 255.0f;
		float b = _color[2] / 255.0f;

		float max = r > g ? r : g > b ? g : b,
			min = r < g ? r : g < b ? g : b;
		float l, s = 0;

		if (max != min)
		{
			l = (max + min) / 2;
			if (l <= 0.5f)
				s = (max - min) / (max + min);
			else
				s = (max - min) / (2 - max - min);
		}
		return s;
	}

	float Brightness() const
	{
		float r = _color[0] / 255.0f;
		float g = _color[1] / 255.0f;
		float b = _color[2] / 255.0f;

		float max = r > g ? r : g > b ? g : b,
			min = r < g ? r : g < b ? g : b;
		return (max + min) / 2;
	}

	constexpr Color& FromHSV(float h, float s, float v)
	{
		float red = _color[0] / 255.0f;
		float green = _color[1] / 255.0f;
		float blue = _color[2] / 255.0f;

		float colOut[3]{ };
		if (s == 0.0f)
		{
			red = green = blue = static_cast<int>(v * 255);
			return *this;
		}

		h = std::fmodf(h, 1.0f) / (60.0f / 360.0f);
		int   i = static_cast<int>(h);
		float f = h - static_cast<float>(i);
		float p = v * (1.0f - s);
		float q = v * (1.0f - s * f);
		float t = v * (1.0f - s * (1.0f - f));

		switch (i)
		{
		case 0:
			colOut[0] = v;
			colOut[1] = t;
			colOut[2] = p;
			break;
		case 1:
			colOut[0] = q;
			colOut[1] = v;
			colOut[2] = p;
			break;
		case 2:
			colOut[0] = p;
			colOut[1] = v;
			colOut[2] = t;
			break;
		case 3:
			colOut[0] = p;
			colOut[1] = q;
			colOut[2] = v;
			break;
		case 4:
			colOut[0] = t;
			colOut[1] = p;
			colOut[2] = v;
			break;
		case 5: default:
			colOut[0] = v;
			colOut[1] = p;
			colOut[2] = q;
			break;
		}

		red = static_cast<int>(colOut[0] * 255);
		green = static_cast<int>(colOut[1] * 255);
		blue = static_cast<int>(colOut[2] * 255);
		return *this;
	}
	static Color FromHSB(float hue, float saturation, float brightness)
	{
		float h = hue == 1.0f ? 0 : hue * 6.0f;
		float f = h - (int)h;
		float p = brightness * (1.0f - saturation);
		float q = brightness * (1.0f - saturation * f);
		float t = brightness * (1.0f - (saturation * (1.0f - f)));

		if (h < 1)
		{
			return Color(
				(unsigned char)(brightness * 255),
				(unsigned char)(t * 255),
				(unsigned char)(p * 255)
				);
		}
		else if (h < 2)
		{
			return Color(
				(unsigned char)(q * 255),
				(unsigned char)(brightness * 255),
				(unsigned char)(p * 255)
				);
		}
		else if (h < 3)
		{
			return Color(
				(unsigned char)(p * 255),
				(unsigned char)(brightness * 255),
				(unsigned char)(t * 255)
				);
		}
		else if (h < 4)
		{
			return Color(
				(unsigned char)(p * 255),
				(unsigned char)(q * 255),
				(unsigned char)(brightness * 255)
				);
		}
		else if (h < 5)
		{
			return Color(
				(unsigned char)(t * 255),
				(unsigned char)(p * 255),
				(unsigned char)(brightness * 255)
				);
		}
		else
		{
			return Color(
				(unsigned char)(brightness * 255),
				(unsigned char)(p * 255),
				(unsigned char)(q * 255)
				);
		}
	}

	static Color Red()		{ return Color(255, 0, 0); }
	static Color Green()	{ return Color(0, 255, 0); }
	static Color Blue()		{ return Color(0, 0, 255); }
	static Color LightBlue(){ return Color(100, 100, 255); }
	static Color Grey()		{ return Color(128, 128, 128); }
	static Color DarkGrey()	{ return Color(45, 45, 45); }
	static Color Black()	{ return Color(0, 0, 0); }
	static Color White()	{ return Color(255, 255, 255); }
	static Color Purple()	{ return Color(220, 0, 220); }

private:
	unsigned char _color[4];
};
class CSurface
{
public:

	void SetDrawColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 15)(this, r, g, b, a);
	}
	void SetDrawColor(Color color)
	{
		//typedef void(__thiscall* Fn)(PVOID, Color); // int, int , int, int
		//return CallVFunction<Fn>(this, 14)(this, color); // r, g, b, a
		SetDrawColor(color.r(), color.g(), color.b(), color.a());
	}
	void DrawColoredCircle(int centerx, int centery, float radius, int r, int g, int b, int a)
	{
		typedef void(__thiscall* Fn)(void*, int, int, float, int, int, int, int);
		CallVFunction< Fn >(this, 162)(this, centerx, centery, radius, r, g, b, a);
	}
	void DrawFilledRect( int x, int y, int x2, int y2 )
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 16)(this, x, y, x2, y2);
	}
	void FilledRect(int x, int y, int w, int h, Color color)
	{
		SetDrawColor(color.r(), color.g(), color.b(), color.a());
		DrawFilledRect(x, y, x + w, y + h);
	}
	void DrawOutlinedRect(int x, int y, int x2, int y2, Color color)
	{
		SetDrawColor(color);
		DrawOutlinedRect(x, y, x + x2, y + y2);
	}
	void DrawOutlinedRect( int x, int y, int x2, int y2 )
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 18)(this, x, y, x2, y2);
	}

	void DrawLine( int x, int y, int x2, int y2 )
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 19)(this, x, y, x2, y2);
	}

	unsigned int SCreateFont()
	{
		typedef unsigned int(__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 71)(this);
	}
	void DrawPolyLine(int *px, int *py, int numPoints)
	{
		typedef void(__thiscall* Fn)(void*, int*, int*, int);
		return CallVFunction<Fn>(this, 20)(this, px, py, numPoints);
	}
	bool SetFontGlyphSet(unsigned int font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags, int nRangeMin = 0, int nRangeMax = 0)
	{
		typedef bool(__thiscall* Fn)(void*, unsigned int, const char*, int, int, int, int, int, int, int);
		return CallVFunction<Fn>(this, 72)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, nRangeMin, nRangeMax);
	}
	void DrawSetTextFont(unsigned int Font)
	{
		typedef void(__thiscall* Fn)(void*, unsigned int);
		return CallVFunction<Fn>(this, 23)(this, Font);
	}

	void DrawSetTextColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 25)(this, r, g, b, a);
	}

	void DrawSetTextColor(Color col)
	{
		typedef void(__thiscall* oDrawSetTextColor)(PVOID, Color);
		return CallVFunction< oDrawSetTextColor >(this, 24)(this, col);
	}


	void DrawSetTextPos( int x, int y )
	{
		typedef void(__thiscall* Fn)(void*, int, int);
		return CallVFunction<Fn>(this, 26)(this, x, y);
	}

	void DrawPrintText(const wchar_t* Text, int Len, int DrawType = 0)
	{
		typedef void(__thiscall* Fn)(void*, wchar_t const*, int, int);
		return CallVFunction<Fn>(this, 28)(this, Text, Len, DrawType);
	}
	void       DrawUnicodeChar(wchar_t wch, int DrawType = 0)
	{
		typedef void(__thiscall* Fn)(void*, wchar_t, int);
		return CallVFunction<Fn>(this, 29)(this, wch, DrawType);
	}
	void GetTextSize(unsigned long font, const wchar_t *text, int &wide, int &tall)
	{
		typedef void(__thiscall* Fn)(void*, unsigned long font, const wchar_t *text, int &wide, int &tall);
		return CallVFunction<Fn>(this, 79)(this, font, text, wide, tall);
	}

	//void DrawColoredCircle(int centerx, int centery, float radius, int r, int g, int b, int a)
	//{
	//	typedef void(__thiscall* Fn)(void*, int, int, float, int, int, int, int);
	//	return CallVFunction<Fn>(this, 162)(this, centerx, centery, radius, r, g, b, a);
	//}
	void UnlockCursor()
	{
		typedef void(__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 66)(this);
	}
	void LockCursor()
	{
		typedef void(__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 67)(this);
	}

	void DrawOutlinedCircle(int x, int y, int radius, int segments)
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 103)(this, x, y, radius, segments);
	}
	void DrawSetTextureRGBA(int id, const unsigned char *rgba, int wide, int tall)
	{
		typedef void(__thiscall* Fn)(void*, int, const unsigned char*, int, int);
		return  CallVFunction<Fn>(this, 37)(this, id, rgba, wide, tall);
	}

	void SetCursorAlwaysVisible(bool vis)
	{
		typedef void(__thiscall* Fn)(void*, bool);
		return CallVFunction<Fn>(this, 52)(this, vis);
	}
	bool IsTextureIDValid(int TextureID)
	{
		typedef bool(__thiscall* Fn)(void*, int);
		return CallVFunction<Fn>(this, 42)(this, TextureID);
	}
	void DrawTexturedRect(int x0, int y0, int W, int T)
	{
		typedef void(__thiscall* Fn)(void*, int, int, int, int);
		return CallVFunction<Fn>(this, 41)(this, x0, y0, W, T);
	}
	void DrawPolygon(int count, Vertex_t* Vertexs, Color color)
	{
		static int texture_id = CreateNewTextureID(true);
		unsigned char buffer[4] = { 255, 255, 255, 255 };

	    DrawSetTextureRGBA(texture_id, buffer, 1, 1);
		SetDrawColor(color);
		DrawSetTexture(texture_id);
		DrawTexturedPolygon(count, Vertexs);
	}

	//void DrawString(HFont hFont, int x, int y, Color color, DWORD dwAlignment, const char* fmt, ...)
	//{
	//	va_list args;
	//	static char buf[1024];

	//	va_start(args, fmt);
	//	_vsnprintf(buf, sizeof(buf), fmt, args);
	//	va_end(args);

	//	static wchar_t wbuf[1024];
	//	MultiByteToWideChar(CP_UTF8, 0, buf, 256, wbuf, 256);

	//	int width, height;
	//	GetTextSize(hFont, wbuf, width, height);

	//	if (dwAlignment & FONT_RIGHT)
	//		x -= width;
	//	if (dwAlignment & FONT_CENTER)
	//		x -= width / 2;

	//	DrawSetTextFont(hFont);
	//	DrawSetTextColor(color);
	//	DrawSetTextPos(x, y - height / 2);
	//	DrawPrintText(wbuf, wcslen(wbuf));
	//}

	//void DrawString(HFont hFont, int x, int y, Color color, bool bCenter, const wchar_t* fmt, ...)
	//{
	//	int iWidth, iHeight;

	//	GetTextSize(hFont, fmt, iWidth, iHeight);
	//	DrawSetTextFont(hFont);
	//	DrawSetTextColor(color);
	//	DrawSetTextPos(bCenter ? x - iWidth / 2 : x, y - iHeight / 2);
	//	DrawPrintText(fmt, wcslen(fmt));
	//}

	void DrawRoundedBox(int x, int y, int w, int h, Color color) {
		std::vector<Vertex_t> buffer;
		for (float a = 90; a <= 270; a++)
			buffer.push_back(Vertex_t(Vector2D(x + (h / 2 * cosf(DEG2RAD(a))) + h / 2, y + (h / 2 * sinf(DEG2RAD(a)) + h / 2))));
		for (float a = -90; a <= 90; a++)
			buffer.push_back(Vertex_t(Vector2D(x + w + (h / 2 * cosf(DEG2RAD(a))) - h / 2, y + (h / 2 * sinf(DEG2RAD(a)) + h / 2))));
		DrawPolygon(buffer.size(), &buffer[0], color);
	}
	void DrawAngleCircle(int x, int y, int startangle, int endangle, int radius, Color color)
	{
		std::vector<Vertex_t> buffer;
		for (float a = startangle; a <= endangle; a++)
			buffer.push_back(Vertex_t(Vector2D(x + radius * cosf(DEG2RAD(a)), y + radius * sinf(DEG2RAD(a)))));
		buffer.push_back(Vertex_t(Vector2D(x, y)));
		DrawPolygon(buffer.size(), &buffer[0], color);
	}
	void DrawRoundedBox(int x, int y, int w, int h, int radius, Color color)
	{
		std::vector<Vertex_t> buffer;
		for (float a = 180; a <= 270; a++)
			buffer.push_back(Vertex_t(Vector2D(x + (radius * cosf(DEG2RAD(a))) + radius, y + (radius * sinf(DEG2RAD(a))) + radius)));
		for (float a = 270; a <= 360; a++)
			buffer.push_back(Vertex_t(Vector2D(x + w + (radius * cosf(DEG2RAD(a))) - radius, y + (radius * sinf(DEG2RAD(a))) + radius)));
		for (float a = 0; a <= 90; a++)
			buffer.push_back(Vertex_t(Vector2D(x + w + (radius * cosf(DEG2RAD(a))) - radius, y + h + (radius * sinf(DEG2RAD(a))) - radius)));
		for (float a = 90; a <= 180; a++)
			buffer.push_back(Vertex_t(Vector2D(x + (radius * cosf(DEG2RAD(a))) + radius, y + h + (radius * sinf(DEG2RAD(a))) - radius)));
		DrawPolygon(buffer.size(), &buffer[0], color);
	}

	void DrawTexturedPolygon(int vtxCount, FontVertex_t *vtx, bool bClipVertices = true)
	{
		typedef void(__thiscall* oDrawSetTextColor)(PVOID, int, FontVertex_t*, bool);
		CallVFunction<oDrawSetTextColor>(this, 106)(this, vtxCount, vtx, bClipVertices);
	}
	void DrawSetTexture(int id)
	{
		typedef void(__thiscall* Fn)(void*, int);
		return CallVFunction<Fn>(this, 38)(this, id);
	}
	int CreateNewTextureID(bool procedural)
	{
		typedef int(__thiscall* Fn)(void*, bool);
		return CallVFunction<Fn>(this, 43)(this, procedural);
	}

	int SetMouse(void* hz)
	{
		typedef int(__thiscall* Fn)(void*, void*);
		return CallVFunction<Fn>(this, 57)(this, hz);
	}

	Vector2D GetMousePosition() // bolbi ware
	{
		POINT mousePosition;
		GetCursorPos(&mousePosition);
		ScreenToClient(FindWindow(0, "Counter-Strike: Global Offensive"), &mousePosition);
		return { static_cast<float>(mousePosition.x), static_cast<float>(mousePosition.y) };
	}

	bool MouseInRegion(int x, int y, int x2, int y2) {
		if (GetMousePosition().x > x && GetMousePosition().y > y && GetMousePosition().x < x2 + x && GetMousePosition().y < y2 + y)
			return true;
		return false;
	}
	void DrawT(int X, int Y, Color Color, int Font, bool Center, const char* _Input, ...)
	{
		int apple = 0;
		char Buffer[2048] = { '\0' };
		va_list Args;

		va_start(Args, _Input);
		vsprintf_s(Buffer, _Input, Args);
		va_end(Args);

		size_t Size = strlen(Buffer) + 1;
		wchar_t* WideBuffer = new wchar_t[Size];

		mbstowcs_s(0, WideBuffer, Size, Buffer, Size - 1);

		int Width = 0, Height = 0;

		GetTextSize(Font, WideBuffer, Width, Height);

		if (Center)
			X -= Width / 2;

		DrawSetTextColor(Color.r(), Color.g(), Color.b(), Color.a());
		DrawSetTextFont(Font);
		DrawSetTextPos(X, Y - Height / 2);
		DrawPrintText(WideBuffer, wcslen(WideBuffer));
	}
	RECT GetTextSizeRect(DWORD font, const char* text) // ayyware or something
	{
		size_t origsize = strlen(text) + 1;
		const size_t newsize = 100;
		size_t convertedChars = 0;
		wchar_t wcstring[newsize];
		mbstowcs_s(&convertedChars, wcstring, origsize, text, _TRUNCATE);

		RECT rect; int x, y;
		GetTextSize(font, wcstring, x, y);
		rect.left = x; rect.bottom = y;
		rect.right = x;
		return rect;
	}
	void RoundedFilledRect(int x, int y, int width, int height, float radius, Color col) { // UC https://www.unknowncheats.me/forum/1498179-post4.html
	// TODO: make the quality not hardcoded -green
	// don't you have to give it quality in the formula you wrote? 8 + 4 * ( quality ) ? -dex
		constexpr int quality = 24;

		static Vertex_t verts[quality];

		Vector2D add = { 0, 0 };

		for (int i = 0; i < quality; i++) {
			float angle = (static_cast <float> (i) / -quality) * 6.28f - (6.28f / 16.f);

			verts[i].m_Position.x = radius + x + add.x + (radius * sin(angle));
			verts[i].m_Position.y = height - radius + y + add.y + (radius * cos(angle));

			if (i == 4) {
				add.y = -height + (radius * 2);
			}
			else if (i == 10) {
				add.x = width - (radius * 2);
			}
			else if (i == 16) {
				add.y = 0;
			}
			else if (i == 22) {
				add.x = 0;
			}
		}
		/*static int Texture = CreateNewTextureID(true);
		unsigned char Tex[4] = { 255,255,255,255 };
		DrawSetTextureRGBA(Texture, Tex, 1, 1);
		DrawSetTexture(Texture);*/
		SetDrawColor(col.r(), col.g(), col.b(), col.a());
		DrawTexturedPolygon(quality, verts);
	}

};
enum EFontFlags
{
	FONTFLAG_NONE,
	FONTFLAG_ITALIC = 0x001,
	FONTFLAG_UNDERLINE = 0x002,
	FONTFLAG_STRIKEOUT = 0x004,
	FONTFLAG_SYMBOL = 0x008,
	FONTFLAG_ANTIALIAS = 0x010,
	FONTFLAG_GAUSSIANBLUR = 0x020,
	FONTFLAG_ROTARY = 0x040,
	FONTFLAG_DROPSHADOW = 0x080,
	FONTFLAG_ADDITIVE = 0x100,
	FONTFLAG_OUTLINE = 0x200,
	FONTFLAG_CUSTOM = 0x400,
	FONTFLAG_BITMAP = 0x800,
};
