enum FlowType_t
{
	FLOW_OUTGOING = 0,
	FLOW_INCOMING = 1,
	FLOW_MAX = 2
};
enum Typetype_t
{
	TYPE_GENERIC = 0,
	TYPE_l_player = 1
};
class NET_SetConVar
{
public:
	NET_SetConVar(const char* name, const char* value);
	~NET_SetConVar();
private:
	DWORD pad[13];
};
class INetChannelInfo
{
public:
	enum
	{
		GENERIC = 0,	// must be first and is default group
		l_player,	// bytes for local player entity update
		OTHERPLAYERS,	// bytes for other players update
		ENTITIES,		// all other entity bytes
		SOUNDS,			// game sounds
		EVENTS,			// event messages
		TEMPENTS,		// temp entities
		USERMESSAGES,	// user messages
		ENTMESSAGES,	// entity messages
		VOICE,			// voice data
		STRINGTABLE,	// a stringtable update
		MOVE,			// client move cmds
		STRINGCMD,		// string command
		SIGNON,			// various signondata
		TOTAL			// must be last and is not a real group
	};

	virtual const char*	GetName(void) const = 0;	// get channel name
	virtual const char* GetAddress(void) const = 0; // get channel IP address as string
	virtual float GetTime(void) const = 0; // current net time
	virtual float GetTimeConnected(void) const = 0; // get connection time in seconds
	virtual int GetBufferSize(void) const = 0; // netchannel packet history size
	virtual int GetDataRate(void) const = 0; // send data rate in byte/sec

	virtual bool IsLoopback(void) const = 0; // true if loopback channel
	virtual bool IsTimingOut(void) const = 0; // true if timing out
	virtual bool IsPlayback(void) const = 0; // true if demo playback

	virtual float GetLatency(int iFlow) const = 0; // current latency (RTT), more accurate but jittering
	virtual float GetAvgLatency(int iFlow) const = 0; // average packet latency in seconds
	virtual float GetAvgLoss(int iFlow) const = 0; // avg packet loss[0..1]
	virtual float GetAvgChoke(int iFlow) const = 0; // avg packet choke[0..1]
	virtual float GetAvgData(int iFlow) const = 0; // data flow in bytes/sec
	virtual float GetAvgPackets(int iFlow) const = 0; // avg packets/sec
	virtual int GetTotalData(int iFlow) const = 0; // total flow in/out in bytes
	virtual int GetTotalPackets(int iFlow) const = 0;
	virtual int GetSequenceNr(int iFlow) const = 0; // last send seq number
	virtual bool IsValidPacket(int iFlow, int iFrameNumber) const = 0; // true if packet was not lost/dropped/chocked/flushed
	virtual float GetPacketTime(int iFlow, int iFrameNumber) const = 0; // time when packet was send
	virtual int GetPacketBytes(int iFlow, int iFrameNumber, int iGroup) const = 0; // group size of this packet
	virtual bool GetStreamProgress(int iFlow, int* pnReceived, int* pnTotal) const = 0; // TCP progress if transmitting
	virtual float GetTimeSinceLastReceived(void) const = 0; // get time since last recieved packet in seconds
	virtual float GetCommandInterpolationAmount(int iFlow, int iFrameNumber) const = 0;
	virtual void GetPacketResponseLatency(int iFlow, int iFrameNumber, int* pnLatencyMsecs, int* pnChoke) const = 0;
	virtual void GetRemoteFramerate(float* pflFrameTime, float* pflFrameTimeStdDeviation) const = 0;
	virtual float GetTimeoutSeconds(void) const = 0;
};
class NetChannel : public INetChannelInfo
{
public:

	virtual	~NetChannel(void) {};

	virtual void	SetDataRate(float rate) = 0;
	virtual bool	RegisterMessage(void *msg) = 0;
	virtual bool	UnregisterMessage(void *msg) = 0;
	virtual bool	StartStreaming(unsigned int challengeNr) = 0;
	virtual void	ResetStreaming(void) = 0;
	virtual void	SetTimeout(float seconds, bool bForceExact = false) = 0;
	virtual void	SetDemoRecorder(void *recorder) = 0;
	virtual void	SetChallengeNr(unsigned int chnr) = 0;

	virtual void	Reset(void) = 0;
	virtual void	Clear(void) = 0;
	virtual void	Shutdown(const char *reason) = 0;

	virtual void	ProcessPlayback(void) = 0;
	virtual bool	ProcessStream(void) = 0;
	virtual void	ProcessPacket(struct netpacket_s* packet, bool bHasHeader) = 0;

	virtual bool	SendNetMsg(INetMessage &msg, bool bForceReliable = false, bool bVoice = false) = 0;

	virtual bool	SendData(bf_write &msg, bool bReliable = true) = 0;
	virtual bool	SendFile(const char *filename, unsigned int transferID, bool isReplayDemo) = 0;
	virtual void	DenyFile(const char *filename, unsigned int transferID, bool isReplayDemo) = 0;
	virtual void	RequestFile_OLD(const char *filename, unsigned int transferID) = 0;	// get rid of this function when we version the 
	virtual void	SetChoked(void) = 0;
	virtual int		SendDatagram(bf_write *data) = 0;
	virtual bool	Transmit(bool onlyReliable = false) = 0;

	virtual const int	&GetRemoteAddress(void) const = 0;
	virtual INetChannelHandler *GetMsgHandler(void) const = 0;
	virtual int				GetDropNumber(void) const = 0;
	virtual int				GetSocket(void) const = 0;
	virtual unsigned int	GetChallengeNr(void) const = 0;
	virtual void			GetSequenceData(int &nOutSequenceNr, int &nInSequenceNr, int &nOutSequenceNrAck) = 0;
	virtual void			SetSequenceData(int nOutSequenceNr, int nInSequenceNr, int nOutSequenceNrAck) = 0;

	virtual void	UpdateMessageStats(int msggroup, int bits) = 0;
	virtual bool	CanPacket(void) const = 0;
	virtual bool	IsOverflowed(void) const = 0;
	virtual bool	IsTimedOut(void) const = 0;
	virtual bool	HasPendingReliableData(void) = 0;

	virtual void	SetFileTransmissionMode(bool bBackgroundMode) = 0;
	virtual void	SetCompressionMode(bool bUseCompression) = 0;
	virtual unsigned int RequestFile(const char *filename, bool isReplayDemoFile) = 0;
	virtual float	GetTimeSinceLastReceived(void) const = 0;	// get time since last received packet in seconds

	virtual void	SetMaxBufferSize(bool bReliable, int nBytes, bool bVoice = false) = 0;

	virtual bool	IsNull() const = 0;
	virtual int		GetNumBitsWritten(bool bReliable) = 0;
	virtual void	SetInterpolationAmount(float flInterpolationAmount) = 0;
	virtual void	SetRemoteFramerate(float flFrameTime, float flFrameTimeStdDeviation) = 0;

	// Max # of payload bytes before we must split/fragment the packet
	virtual void	SetMaxRoutablePayloadSize(int nSplitSize) = 0;
	virtual int		GetMaxRoutablePayloadSize() = 0;

	// For routing messages to a different handler
	virtual bool	SetActiveChannel(INetChannel *pNewChannel) = 0;
	virtual void	AttachSplitPlayer(int nSplitPlayerSlot, INetChannel *pChannel) = 0;
	virtual void	DetachSplitPlayer(int nSplitPlayerSlot) = 0;

	virtual bool	IsRemoteDisconnected() const = 0;

	virtual bool	WasLastMessageReliable() const = 0;

	char pad_0000[20]; //0x0000
	bool m_bProcessingMessages; //0x0014
	bool m_bShouldDelete; //0x0015
	char pad_0016[2]; //0x0016
	int32_t m_nOutSequenceNr;    //0x0018 last send outgoing sequence number
	int32_t m_nInSequenceNr;     //0x001C last received incoming sequnec number
	int32_t m_nOutSequenceNrAck; //0x0020 last received acknowledge outgoing sequnce number
	int32_t m_nOutReliableState; //0x0024 state of outgoing reliable data (0/1) flip flop used for loss detection
	int32_t m_nInReliableState;  //0x0028 state of incoming reliable data
	int32_t m_nChokedPackets;    //0x002C number of choked packets
	char pad_0030[1044]; //0x0030
}; //Size: 0x0444

class CEngine
{
public:
	void SetName(std::string name);
	INetChannelInfo* GetNetChannelInfo()
	{
		typedef INetChannelInfo* (__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 78)(this);
	}
	NetChannel* GetNetChannel()
	{
		typedef NetChannel*(__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 78)(this);
	}
	void GetScreenSize( int& Width, int& Height )
	{
		typedef void( __thiscall* Fn )(void*, int&, int&);
		return CallVFunction<Fn>(this, 5)(this, Width, Height);
	}

	bool GetPlayerInfo( int Index, player_info_t* PlayerInfo )	
	{
		typedef bool(__thiscall* Fn)(void*, int, player_info_t*);
		return CallVFunction<Fn>(this, 8)(this, Index, PlayerInfo);
	}

	int Getl_player()
	{
		typedef int(__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 12)(this);
	}
	int GetPlayerForUserID(int userid)
	{
		typedef int(__thiscall* Fn)(void*, int);
		return CallVFunction<Fn>(this, 9)(this, userid);
	}
	void ClientCmd( const char* Command )
	{
		typedef void(__thiscall* Fn)(void*, const char*);
		return CallVFunction<Fn>(this, 108)(this, Command);
		}

	void SetViewAngles(QAngle& Angles)
	{
		typedef void(__thiscall* Fn)(void*, QAngle&);
		return CallVFunction<Fn>(this, 19)(this, Angles);
	}

	void GetViewAngles(QAngle& angle)
	{
		typedef void(__thiscall* Fn)(void*, QAngle&);
		return CallVFunction<Fn>(this, 18)(this,angle);
	}

	matrix3x4_t& WorldToScreenMatrix()
	{
		typedef matrix3x4_t& (__thiscall* Fn)(void*);
		return CallVFunction<Fn>(this, 37)(this);
		
	
	}
	bool IsConnected()
	{
		typedef bool(__thiscall* Fn)(PVOID);
		return CallVFunction<Fn>(this, 27)(this);
	}

	bool IsInGame()
	{
		typedef bool(__thiscall* Fn)(PVOID);
		return CallVFunction<Fn>(this, 26)(this);
	}

	int Getmax_�lients()
	{
		typedef bool(__thiscall* Fn)(PVOID);
		return CallVFunction<Fn>(this, 20)(this);
	}

	float Time()
	{
		typedef float(__thiscall* Fn)(PVOID);
		return CallVFunction<Fn>(this, 14)(this);
	}


	void ClientCmd_Unrestricted(char  const* cmd)
	{
		typedef void(__thiscall* oClientCmdUnres)(PVOID, const char*, char);
		return CallVFunction<oClientCmdUnres>(this, 114)(this, cmd, 0);
	}
	//52
	char const    GetLevelName()
	{
		typedef char const(__thiscall* oClientCmdUnres)(PVOID);
		return CallVFunction<oClientCmdUnres>(this, 52)(this);
	}
	unsigned int GetEngineBuildNumber()
	{
		typedef unsigned int(__thiscall* OriginalFn)(PVOID);
		return CallVFunction< OriginalFn >(this, 104)(this);
	}

};