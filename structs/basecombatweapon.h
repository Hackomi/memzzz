#pragma once
#include "../sdk.h"

struct Weapon_Info
{
	char pad00[0xC8];
	int m_WeaponType; // 0xC8
	char padCC[0x20];
	int m_Damage; // 0xEC
	float m_ArmorRatio; // 0xF0
	char padF4[0x4];
	float m_Penetration; // 0xF8
	char padFC[0x8];
	float m_Range; // 0x104
	float m_RangeModifier; // 0x108
	char pad10C[0x10];
	bool m_HasSilencer; // 0x11C
};
enum WEAPONCLASS
{
	WEPCLASS_INVALID,
	WEPCLASS_RIFLE,
	WEPCLASS_PISTOL,
	WEPCLASS_SHOTGUN,
	WEPCLASS_SNIPER,
	WEPCLASS_SMG,
	WEPCLASS_MACHINEGUN,
	WEPCLASS_KNIFE,
};
enum ItemDefinitionIndex : int {
	WEAPON_NONE = 0,
	WEAPON_DEAGLE,
	WEAPON_ELITE,
	WEAPON_FIVESEVEN,
	WEAPON_GLOCK,
	WEAPON_AK47 = 7,
	WEAPON_AUG,
	WEAPON_AWP,
	WEAPON_FAMAS,
	WEAPON_G3SG1,
	WEAPON_GALILAR = 13,
	WEAPON_M249,
	WEAPON_M4A1 = 16,
	WEAPON_MAC10,
	WEAPON_P90 = 19,
	WEAPON_MP5SD = 23,
	WEAPON_UMP45,
	WEAPON_XM1014,
	WEAPON_BIZON,
	WEAPON_MAG7,
	WEAPON_NEGEV,
	WEAPON_SAWEDOFF,
	WEAPON_TEC9,
	WEAPON_TASER,
	WEAPON_HKP2000,
	WEAPON_MP7,
	WEAPON_MP9,
	WEAPON_NOVA,
	WEAPON_P250,
	WEAPON_SCAR20 = 38,
	WEAPON_SG553,
	WEAPON_SSG08,
	WEAPON_KNIFEGG,
	WEAPON_KNIFE,
	WEAPON_FLASHBANG,
	WEAPON_HEGRENADE,
	WEAPON_SMOKEGRENADE,
	WEAPON_MOLOTOV,
	WEAPON_DECOY,
	WEAPON_INCGRENADE,
	WEAPON_C4,
	WEAPON_HEALTHSHOT = 57,
	WEAPON_KNIFE_T = 59,
	WEAPON_M4A1_SILENCER,
	WEAPON_USP_SILENCER,
	WEAPON_CZ75A = 63,
	WEAPON_REVOLVER,
	WEAPON_TAGRENADE = 68,
	WEAPON_FISTS,
	WEAPON_BREACHCHARGE,
	WEAPON_TABLET = 72,
	WEAPON_MELEE = 74,
	WEAPON_AXE,
	WEAPON_HAMMER,
	WEAPON_SPANNER = 78,
	WEAPON_KNIFE_GHOST = 80,
	WEAPON_FIREBOMB,
	WEAPON_DIVERSION,
	WEAPON_FRAG_GRENADE,
	WEAPON_KNIFE_BAYONET = 500,
	WEAPON_KNIFE_FLIP = 505,
	WEAPON_KNIFE_GUT,
	WEAPON_KNIFE_KARAMBIT,
	WEAPON_KNIFE_M9_BAYONET,
	WEAPON_KNIFE_TACTICAL,
	WEAPON_KNIFE_FALCHION = 512,
	WEAPON_KNIFE_SURVIVAL_BOWIE = 514,
	WEAPON_KNIFE_BUTTERFLY,
	WEAPON_KNIFE_PUSH,
	WEAPON_KNIFE_URSUS = 519,
	WEAPON_KNIFE_GYPSY_JACKKNIFE,
	WEAPON_KNIFE_STILETTO = 522,
	WEAPON_KNIFE_WIDOWMAKER
};
struct CHudTexture
{
	char	szShortName[64];	//0x0000
	char	szTextureFile[64];	//0x0040
	bool	bRenderUsingFont;	//0x0080
	bool	bPrecached;			//0x0081
	char	cCharacterInFont;	//0x0082
	BYTE	pad_0x0083;			//0x0083
	int		hFont;				//0x0084
	int		iTextureId;			//0x0088
	float	afTexCoords[4];		//0x008C
	int		iPosX[4];			//0x009C
}; 
class CSWeaponInfo
{
public:
	char _0x0000[20];
	__int32 max_clip;			//0x0014 
	char _0x0018[12];
	__int32 max_reserved_ammo;	//0x0024 
	char _0x0028[96];
	char* hud_name;				//0x0088 
	char* weapon_name;			//0x008C 
	char _0x0090[60];
	__int32 type;				//0x00CC 
	__int32 price;				//0x00D0 
	__int32 reward;				//0x00D4 
	char _0x00D8[20];
	BYTE full_auto;				//0x00EC 
	char _0x00ED[3];
	__int32 damage;				//0x00F0 
	float armor_ratio;			//0x00F4 
	__int32 bullets;			//0x00F8 
	float penetration;			//0x00FC 
	char _0x0100[8];
	float range;				//0x0108 
	float range_modifier;		//0x010C 
	char _0x0110[16];
	BYTE silencer;				//0x0120 
	char _0x0121[15];
	float max_speed;			//0x0130 
	float max_speed_alt;		//0x0134 
	char _0x0138[76];
	__int32 recoil_seed;		//0x0184 
	char _0x0188[32];
};// Size=0x01A8
class CBomb
{
public:
	HANDLE GetOwnerEntity()
	{
		return *reinterpret_cast<HANDLE*>((DWORD)this + offys.m_hOwnerEntity);
	}
	bool IsDefused()
	{
		return *reinterpret_cast<bool*>((DWORD)this + offys.m_bBombDefused);
	}
	float C4BlowTime()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_flC4Blow);
	}
	float m_flDefuseCountDown()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_flDefuseCountDown);
	}
	float m_hBombDefuser()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_hBombDefuser);
	}
};
class CGrenade
{
public:
	bool IsPinPulled()
	{
		return *reinterpret_cast<bool*>((DWORD)this + offys.m_bPinPulled);
	}
	float GetThrowTime()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_fThrowTime);
	}
};

class CBaseCombatWeapon 
{
	template<class T>
	T GetFieldValue(int offset) {
		return *(T*)((DWORD)this + offset);
	}
	template<class T>
	T* GetFieldPointer(int offset) {
		return (T*)((DWORD)this + offset);
	}
public:
	unsigned char _0x3254[0x98];
	int    	m_weaponMode; // 0x32ec
	unsigned char _0x32f0[0x14];
	float  	m_fAccuracyPenalty; // 0x3304
	unsigned char _0x3308[0x8];
	int    	m_iRecoilIndex; // 0x3310
	float  	m_flRecoilIndex; // 0x3314
	int    	m_bBurstMode; // 0x3318
	float  	m_flPostponeFireReadyTime; // 0x331c
	int    	m_bReloadVisuallyComplete; // 0x3320
	int    	m_bSilencerOn; // 0x3321
	float  	m_flDoneSwitchingSilencer; // 0x3324
	unsigned char _0x3328[0x4];
	int    	m_iOriginalTeamNumber; // 0x332c
	unsigned char _0x3330[0xc];
	int    	m_hPrevOwner; // 0x333c
	unsigned char _0x3340[0x20];
	float  	m_fLastShotTime; // 0x3360
	unsigned char _0x3364[0x18];
	int    	m_iIronSightMode; // 0x337c
	void SetPattern(int skin, int quality, int seed, int stattrak, const char* name)
	{

		*(int*)((DWORD)this + offys.m_nFallbackPaintKit) = skin;
		*(int*)((DWORD)this + offys.m_iEntityQuality) = quality;
		*(int*)((DWORD)this + offys.m_nFallbackSeed) = seed;
		*(int*)((DWORD)this + offys.m_nFallbackStatTrak) = stattrak;
		*(float*)((DWORD)this + offys.m_flFallbackWear) = 0.0001f;

		if (name != "") {
			char* a = (char*)((DWORD)this + offys.m_szCustomName);
			sprintf_s(a, 32, "%s", name);
		}

		*(int*)((DWORD)this + offys.m_iItemIDHigh) = -1;
	}
	int* ItemIDHigh()
	{
		return (int*)((uintptr_t)this + 0x1F0);
	}
	int* ItemIDLow()
	{
		return (int*)((uintptr_t)this + 0x1F4);
	}

	int* FallbackPaintKit()
	{
		return (int*)((uintptr_t)this + offys.m_nFallbackPaintKit);
	}

	int* FallbackSeed()
	{
		return (int*)((uintptr_t)this + offys.m_nFallbackSeed);
	}

	float* FallbackWear()
	{
		return (float*)((uintptr_t)this + offys.m_flFallbackWear);
	}
	std::string GetNames()
	{
		///TODO: Test if szWeaponName returns proper value for m4a4 / m4a1-s or it doesnt recognize them.
		return std::string(this->GetCSWpnData()->weapon_name);
	}
	int* FallbackStatTrak()
	{
		return (int*)((uintptr_t)this + offys.m_nFallbackStatTrak);
	}

	int* OwnerXuidLow()
	{
		return (int*)((uintptr_t)this + offys.m_OriginalOwnerXuidLow);
	}
	int* OwnerXuidHigh()
	{
		return (int*)((uintptr_t)this + offys.m_OriginalOwnerXuidHigh);
	}
	char* GetCustomName()
	{
		return (char*)((uintptr_t)this + offys.m_szCustomName);
	}
	int* GetEntityQuality() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_iEntityQuality
		return (int*)((DWORD)this + 0x2D70 + 0x40 + 0x1DC);
	}
	HANDLE m_hWeaponWorldModel()
	{
		return *(HANDLE*)((uintptr_t)this + offys.m_hWeaponWorldModel);
	}
	enum WeaponType : byte
	{
		Shotgun,
		Pistol,
		Automatic,
		Sniper,
		Grenade,
		Knife,
		Bomb
	};

	inline int* GetFallbackPaintKit() {
		// DT_BaseAttributableItem -> m_nFallbackPaintKit: 0x3140
		return (int*)((DWORD)this + offys.m_nFallbackPaintKit);
	}


	int* ViewModelIndex()
	{
		return (int*)((uintptr_t)this + offys.m_iViewModelIndex);
	}

	int* WorldModelIndex()
	{
		return (int*)((uintptr_t)this + offys.m_iWorldModelIndex);
	}

	int* ModelIndex()
	{
		return (int*)((uintptr_t)this + offys.m_nModeIndex);
	}

	int* ItemDefinitionIndex()
	{
		return (int*)((uintptr_t)this + 0x1D8);
	}

	model_t* CBaseCombatWeapon::GetModel()
	{
		return *(model_t**)((DWORD)this + 0x6C);
	}
	Vector GetOrigin()
	{
		return *reinterpret_cast<Vector*>((DWORD)this + 0x0134);
	}
	HANDLE GetOwnerC_BaseEntity()
	{
		return *reinterpret_cast<HANDLE*>((DWORD)this + 0x0148);
	}
	float NextPrimaryAttack()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_flNextPrimaryAttack);
	}
	float NextSecondaryAttack()
	{
		return *reinterpret_cast<float*>((DWORD)this + 0x31CC);
	}
	float GetAccuracyPenalty()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_fAccuracyPenalty);
	}
	int Clip1()
	{
		return *reinterpret_cast<int*>((DWORD)this + offys.m_iClip1);
	}

	char* GetGunText()
	{
		int WeaponId = this->WeaponID();
		switch (WeaponId)
		{
		case WEAPON_KNIFE:
		case 500:
		case 505:
		case 506:
		case 507:
		case 508:
		case 509:
		case 512:
		case 514:
		case 515:
		case 516:
			return "KNIFE";
		case WEAPON_DEAGLE:
			return "DEAGLE";
		case WEAPON_ELITE:
			return "BERRETS";
		case WEAPON_FIVESEVEN:
			return "FIVESEVEN";
		case WEAPON_GLOCK:
			return "GLOCK";
		case WEAPON_HKP2000:
			return "P2000";
		case WEAPON_P250:
			return "P250";
		case WEAPON_USP_SILENCER:
			return "USP";
		case WEAPON_TEC9:
			return "TEC9";
		case WEAPON_CZ75A:
			return "CZ75A";
		case WEAPON_REVOLVER:
			return "REVOLVER";
		case WEAPON_MAC10:
			return "MAC10";
		case WEAPON_UMP45:
			return "UMP45";
		case WEAPON_BIZON:
			return "BIZON";
		case WEAPON_MP7:
			return "MP7";
		case WEAPON_MP9:
			return "MP9";
		case WEAPON_P90:
			return "P90";
		case WEAPON_GALILAR:
			return "GALILAR";
		case WEAPON_FAMAS:
			return "FAMAS";
		case WEAPON_M4A1_SILENCER:
			return "M4A1-S";
		case WEAPON_M4A1:
			return "M4A1";
		case WEAPON_AUG:
			return "AUG";
		case WEAPON_AK47:
			return "AK47";
		case WEAPON_G3SG1:
			return "G3SG1";
		case WEAPON_SCAR20:
			return "SCAR20";
		case WEAPON_AWP:
			return "AWP";
		case WEAPON_SSG08:
			return "SSG08";
		case WEAPON_XM1014:
			return "XM1014";
		case WEAPON_SAWEDOFF:
			return "SAWED";
		case WEAPON_MAG7:
			return "MAG7";
		case WEAPON_NOVA:
			return "NOVA";
		case WEAPON_NEGEV:
			return "NEGEV";
		case WEAPON_M249:
			return "M249";
		case WEAPON_TASER:
			return "TASER";
		case WEAPON_FLASHBANG:
			return "FLASHBANG";
		case WEAPON_HEGRENADE:
			return "GRENADE";
		case WEAPON_SMOKEGRENADE:
			return "SMOKE";
		case WEAPON_MOLOTOV:
			return "MOLOTOV";
		case WEAPON_DECOY:
			return "DECOY";
		case WEAPON_INCGRENADE:
			return "MOLOTOV";
		case WEAPON_C4:
			return "C4";
		default:
			return " ";
		}
	}

	int GetMaxAmmoReserve(void)
	{
		return *(int*)((DWORD)this + offys.m_iPrimaryReserveAmmoCount);
	}
	int GetLoadedAmmo()
	{
		return *(int*)((DWORD)this + offys.m_iClip1);
	}
	int WeaponID()
	{
		return GetItemDefinitionIndex();
		/*typedef int*(__thiscall* GetWeaponNameFn)(void*);
		GetWeaponNameFn Name = (GetWeaponNameFn)((*(PDWORD_PTR*)this)[458]);
		return Name(this);*/
	}
	short GetItemDefinitionIndex(void)
	{
		if (!this) return 0;
		return *(short*)((DWORD)this + offys.m_iItemDefinitionIndex);
	}
	float GetPostponeFireReadyTime()
	{
		return *reinterpret_cast<float*>((DWORD)this + offys.m_flPostponeFireReadyTime);
	}
	int GetZoomLevel()
	{
		return *reinterpret_cast<int*>((DWORD)this + 0x3330);
	}
	const char* GetWeaponName()
	{
		typedef const char*(__thiscall* GetWeaponNameFn)(void*);
		GetWeaponNameFn Name = (GetWeaponNameFn)((*(PDWORD_PTR*)this)[378]);
		return Name(this);
	}
	std::string GetName(bool Ammo)
	{
		const char* name = GetWeaponName();
		std::string Name = name;
		std::string NName;
		NName = Name.substr(7, Name.length() - 7);


		if (Ammo && !this->IsMiscWeapon())
		{
			char buffer[32]; sprintf_s(buffer, " [%i]", Clip1());
			NName.append(buffer);
			return NName;
		}
		return NName;
	}
	int GetWeaponType()
	{
		if (!this) return WEPCLASS_INVALID;
		auto id = this->WeaponID();
		switch (id)
		{
		case WEAPON_DEAGLE:
		case WEAPON_ELITE:
		case WEAPON_FIVESEVEN:
		case WEAPON_HKP2000:
		case WEAPON_USP_SILENCER:
		case WEAPON_CZ75A:
		case WEAPON_TEC9:
		case WEAPON_REVOLVER:
		case WEAPON_GLOCK:
		case WEAPON_P250:
			return WEPCLASS_PISTOL;
			break;
		case WEAPON_AK47:
		case WEAPON_M4A1:
		case WEAPON_M4A1_SILENCER:
		case WEAPON_GALILAR:
		case WEAPON_AUG:
		case WEAPON_FAMAS:
		case WEAPON_SG553:
			return WEPCLASS_RIFLE;
			break;
		case WEAPON_P90:
		case WEAPON_BIZON:
		case WEAPON_MP7:
		case WEAPON_MP9:
		case WEAPON_MAC10:
		case WEAPON_UMP45:
			return WEPCLASS_SMG;
			break;
		case WEAPON_AWP:
		case WEAPON_G3SG1:
		case WEAPON_SCAR20:
		case WEAPON_SSG08:
			return WEPCLASS_SNIPER;
			break;
		case WEAPON_NEGEV:
		case WEAPON_M249:
			return WEPCLASS_MACHINEGUN;
			break;
		case WEAPON_MAG7:
		case WEAPON_SAWEDOFF:
		case WEAPON_NOVA:
		case WEAPON_XM1014:
			return WEPCLASS_SHOTGUN;
			break;
		case WEAPON_KNIFE:
		case WEAPON_KNIFE_BAYONET:
		case WEAPON_KNIFE_BUTTERFLY:
		case WEAPON_KNIFE_FALCHION:
		case WEAPON_KNIFE_FLIP:
		case WEAPON_KNIFE_GUT:
		case WEAPON_KNIFE_KARAMBIT:
		case WEAPON_KNIFE_TACTICAL:
		case WEAPON_KNIFE_M9_BAYONET:
		case WEAPON_KNIFE_PUSH:
		case WEAPON_KNIFE_T:
			return WEPCLASS_KNIFE;
			break;

		default:
			return WEPCLASS_INVALID;
		}
	}

	float GetFloatRecoilIndex()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x32D0);
	}

	float GetSpread()
	{
		if (!this)
			return -1.f;
		typedef float(__thiscall* OriginalFn)(void*);
		return CallVFunction<OriginalFn>(this, 440)(this);
	}
	float GetCone()
	{
		if (!this)
			return -1.f;
		typedef float(__thiscall* OriginalFn)(void*);
		return CallVFunction<OriginalFn>(this, 439)(this);

	}

	CSWeaponInfo* GetWeaponInfo()
	{
		/*if (!this)
			return false;*/
		typedef CSWeaponInfo*(__thiscall* OriginalFn)(void*);
		return CallVFunction<OriginalFn>(this, 448)(this);
	}

	float GetInaccuracy()
	{
		if (this != nullptr)
		{
			typedef float(__thiscall* GetInaccuracyFn)(void*);
			return CallVFunction<GetInaccuracyFn>(this, 471)(this);
		}
	}

	void UpdateAccuracyPenalty()
	{
		if (!this)
			return;
		typedef void(__thiscall* UpdateAccuracyPenaltyFn)(void*);
		return CallVFunction<UpdateAccuracyPenaltyFn>(this, 472)(this);

	}
	int GetWeaponNum()
	{
		int defindex = WeaponID();
		switch (defindex)
		{
		case WEAPON_GLOCK:
			return 0;
		case WEAPON_CZ75A:
			return 1;
		case WEAPON_P250:
			return 2;
		case WEAPON_FIVESEVEN:
			return 3;
		case WEAPON_DEAGLE:
			return 4;
		case WEAPON_ELITE:
			return 5;
		case WEAPON_TEC9:
			return 6;
		case WEAPON_HKP2000:
			return 7;
		case WEAPON_USP_SILENCER:
			return 8;
		case WEAPON_REVOLVER:
			return 9;
		case WEAPON_MAC10:
			return 10;
		case WEAPON_MP9:
			return 11;
		case WEAPON_MP7:
			return 12;
		case WEAPON_UMP45:
			return 13;
		case WEAPON_BIZON:
			return 14;
		case WEAPON_P90:
			return 15;
		case WEAPON_GALILAR:
			return 16;
		case WEAPON_FAMAS:
			return 17;
		case WEAPON_AK47:
			return 18;
		case WEAPON_M4A1:
			return 19;
		case WEAPON_M4A1_SILENCER:
			return 20;
		case WEAPON_SG553:
			return 21;
		case WEAPON_AUG:
			return 22;
		case WEAPON_SSG08:
			return 23;
		case WEAPON_AWP:
			return 24;
		case WEAPON_G3SG1:
			return 25;
		case WEAPON_SCAR20:
			return 26;
		case WEAPON_NOVA:
			return 27;
		case WEAPON_XM1014:
			return 28;
		case WEAPON_SAWEDOFF:
			return 29;
		case WEAPON_MAG7:
			return 30;
		case WEAPON_M249:
			return 31;
		case WEAPON_NEGEV:
			return 32;
		default:
			return -1;
		}
		return -1;
	}
	bool IsRifle()
	{
		int iWeaponID = WeaponID();
	}
	bool IsScopeable()
	{
		int iWeaponID = WeaponID();
		return (iWeaponID == 38 || iWeaponID == 11 || iWeaponID == 9 || iWeaponID == 40 || iWeaponID == 8 || iWeaponID == WEAPON_SG553);
	}
	bool IsSniper()
	{
		int iWeaponID = WeaponID();
		return (iWeaponID == WEAPON_SSG08 || iWeaponID == WEAPON_AWP || iWeaponID == WEAPON_SCAR20 || iWeaponID == WEAPON_G3SG1);
	}
	bool IsPistol()
	{
		int iWeaponID = WeaponID();
		return (iWeaponID == WEAPON_GLOCK || iWeaponID == WEAPON_HKP2000
			|| iWeaponID == WEAPON_P250 || iWeaponID == WEAPON_DEAGLE
			|| iWeaponID == WEAPON_ELITE || iWeaponID == WEAPON_TEC9 || iWeaponID == WEAPON_USP_SILENCER
			|| iWeaponID == WEAPON_FIVESEVEN);
	}
	bool IsMiscWeapon()
	{
		if (!this)
			return false;
		
		std::string WeaponName = this->GetNames();

		if (WeaponName == "weapon_knife")
			return true;
		else if (WeaponName == "weapon_incgrenade")
			return true;
		else if (WeaponName == "weapon_decoy")
			return true;
		else if (WeaponName == "weapon_flashbang")
			return true;
		else if (WeaponName == "weapon_hegrenade")
			return true;
		else if (WeaponName == "weapon_smokegrenade")
			return true;
		else if (WeaponName == "weapon_molotov")
			return true;

		return false;
	}

	//bool IsMiscWeapon()
	//{
	//	int iWeaponID = WeaponID();
	//	return (iWeaponID == WEAPON_KNIFE
	//		|| iWeaponID == WEAPON_C4
	//		|| iWeaponID == WEAPON_HEGRENADE || iWeaponID == WEAPON_DECOY
	//		|| iWeaponID == WEAPON_FLASHBANG || iWeaponID == WEAPON_MOLOTOV
	//		|| iWeaponID == WEAPON_SMOKEGRENADE || iWeaponID == WEAPON_INCGRENADE || iWeaponID == WEAPON_KNIFE_T
	//		|| iWeaponID == 500 || iWeaponID == 505 || iWeaponID == 506
	//		|| iWeaponID == 507 || iWeaponID == 508 || iWeaponID == 509
	//		|| iWeaponID == 515);
	//}
	bool IsGun()
	{
		int id = this->GetItemDefinitionIndex();

		if (id == WEAPON_KNIFE || id == WEAPON_HEGRENADE || id == WEAPON_DECOY || id == WEAPON_INCGRENADE || id == WEAPON_MOLOTOV || id == WEAPON_C4 || id == WEAPON_TASER || id == WEAPON_FLASHBANG || id == WEAPON_SMOKEGRENADE || id == WEAPON_KNIFE)
			return false;
		else
			return true;
	}
	bool IsGrenade()
	{

		int iWeaponID = WeaponID();
		return (iWeaponID == WEAPON_HEGRENADE || iWeaponID == WEAPON_DECOY
			|| iWeaponID == WEAPON_FLASHBANG || iWeaponID == WEAPON_MOLOTOV
			|| iWeaponID == WEAPON_SMOKEGRENADE || iWeaponID == WEAPON_INCGRENADE);
	}
	bool IsKnife()
	{
		int iWeaponID = WeaponID();
		return (iWeaponID == 42 || iWeaponID == 59 || iWeaponID == 41
			|| iWeaponID == 500 || iWeaponID == 505 || iWeaponID == 506
			|| iWeaponID == 507 || iWeaponID == 508 || iWeaponID == 509
			|| iWeaponID == 515);
	}
	float GetPenetration()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x7C4);
	}
	float GetDamage()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x7C8);
	}
	float GetRange()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x7CC);
	}
	float GetRangeModifier()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x7D0);
	}
	float GetArmorRatio()
	{
		if (!this)
			return -1.f;
		return *reinterpret_cast<float*>((DWORD)this + 0x7AC);
	}
	CSWeaponInfo* GetCSWpnData();
	bool IsReloadingVisually();
};

class CBaseViewModel
{
public:
	CBaseViewModel(void);
	~CBaseViewModel(void);


	bool IsViewable(void) { return false; }

	virtual void					UpdateOnRemove(void);

	// Weapon client handling
	virtual void			SendViewModelMatchingSequence(int sequence);
	virtual void			SetWeaponModel(const char *pszModelname, CBaseCombatWeapon *weapon);

	void SendViewModelMatchingSequenceManual(int sequence)
	{
		typedef void(__thiscall* OriginalFn)(void*, int);
		return CallVFunction<OriginalFn>(this, 241)(this, sequence);
	}

};
enum DataUpdateType_t
{
	DATA_UPDATE_CREATED = 0,	// indicates it was created +and+ entered the pvs
								//	DATA_UPDATE_ENTERED_PVS,
								DATA_UPDATE_DATATABLE_CHANGED,
								//	DATA_UPDATE_LEFT_PVS,
								//	DATA_UPDATE_DESTROYED,		// FIXME: Could enable this, but it's a little worrying
								// since it changes a bunch of existing code
};
class C_BasePlayer
{
public:


	C_BasePlayer();
	virtual			~C_BasePlayer();
	virtual void	Spawn(void);
	virtual void	SharedSpawn(); // Shared between client and server.
	void*		Classify(void) { return 0; }
	// IClientC_BaseEntity overrides.
	virtual void	OnPreDataChanged(DataUpdateType_t updateType);
	virtual void	OnDataChanged(DataUpdateType_t updateType);
	virtual void	PreDataUpdate(DataUpdateType_t updateType);
	virtual void	PostDataUpdate(DataUpdateType_t updateType);
	virtual void	ReceiveMessage(int classID, void* &msg);
	virtual void	OnRestore();
	virtual void	MakeTracer(const Vector &vecTracerSrc, const void* &tr, int iTracerType);
	virtual void	GetToolRecordingState(KeyValues *msg);
	void	SetAnimationExtension(const char *pExtension);
	CBaseViewModel			*GetViewModel(int viewmodelindex = 0);
};

class GlowObjectDefinition_t
{
public:
	GlowObjectDefinition_t() { memset(this, 0, sizeof(*this)); }

	class C_BaseEntity* m_pEntity;    //0x0000
	Vector m_vGlowColor;           //0x0004
	float   m_flAlpha;                 //0x0010
	uint8_t pad_0014[4];               //0x0014
	float   m_flSomeFloat;             //0x0018
	uint8_t pad_001C[4];               //0x001C
	float   m_flAnotherFloat;          //0x0020
	bool    m_bRenderWhenOccluded;     //0x0024
	bool    m_bRenderWhenUnoccluded;   //0x0025
	bool    m_bFullBloomRender;        //0x0026
	uint8_t pad_0027[5];               //0x0027
	int32_t m_nGlowStyle;              //0x002C
	int32_t m_nSplitScreenSlot;        //0x0030
	int32_t m_nNextFreeSlot;           //0x0034

	bool IsUnused() const { return m_nNextFreeSlot != GlowObjectDefinition_t::ENTRY_IN_USE; }

	static const int END_OF_FREE_LIST = -1;
	static const int ENTRY_IN_USE = -2;
}; //Size: 0x0038 (56)

class CGlowObjectManager
{
public:

	int RegisterGlowObject(C_BaseEntity *pEntity, const Vector &vGlowColor, float flGlowAlpha, bool bRenderWhenOccluded, bool bRenderWhenUnoccluded, int nSplitScreenSlot)
	{
		int nIndex;
		if (m_nFirstFreeSlot == GlowObjectDefinition_t::END_OF_FREE_LIST)
		{
			nIndex = m_GlowObjectDefinitions.AddToTail();
		}
		else
		{
			nIndex = m_nFirstFreeSlot;
			m_nFirstFreeSlot = m_GlowObjectDefinitions[nIndex].m_nNextFreeSlot;
		}

		m_GlowObjectDefinitions[nIndex].m_hEntity = pEntity;
		m_GlowObjectDefinitions[nIndex].m_vGlowColor = vGlowColor;
		m_GlowObjectDefinitions[nIndex].m_flGlowAlpha = flGlowAlpha;
		m_GlowObjectDefinitions[nIndex].flUnk = 0.0f;
		m_GlowObjectDefinitions[nIndex].m_flBloomAmount = 1.0f;
		m_GlowObjectDefinitions[nIndex].l_playeriszeropoint3 = 0.0f;
		m_GlowObjectDefinitions[nIndex].m_bRenderWhenOccluded = bRenderWhenOccluded;
		m_GlowObjectDefinitions[nIndex].m_bRenderWhenUnoccluded = bRenderWhenUnoccluded;
		m_GlowObjectDefinitions[nIndex].m_bFullBloomRender = false;
		m_GlowObjectDefinitions[nIndex].m_nFullBloomStencilTestValue = 0;
		m_GlowObjectDefinitions[nIndex].m_nSplitScreenSlot = nSplitScreenSlot;
		m_GlowObjectDefinitions[nIndex].m_nNextFreeSlot = GlowObjectDefinition_t::ENTRY_IN_USE;

		return nIndex;
	}

	void UnregisterGlowObject(int nGlowObjectHandle)
	{
		Assert(!m_GlowObjectDefinitions[nGlowObjectHandle].IsUnused());

		m_GlowObjectDefinitions[nGlowObjectHandle].m_nNextFreeSlot = m_nFirstFreeSlot;
		m_GlowObjectDefinitions[nGlowObjectHandle].m_hEntity = NULL;
		m_nFirstFreeSlot = nGlowObjectHandle;
	}

	class GlowObjectDefinition_t
	{
	public:
		void set(float r, float g, float b, float a)
		{
			m_vGlowColor = Vector(r, g, b);
			m_flGlowAlpha = a;
			m_bRenderWhenOccluded = true;
			m_bRenderWhenUnoccluded = false;
			m_flBloomAmount = 1.0f;
		}

		C_BaseEntity* getEnt()
		{
			return m_hEntity;
		}

		bool IsUnused() const { return m_nNextFreeSlot != GlowObjectDefinition_t::ENTRY_IN_USE; }

	public:
		C_BaseEntity * m_hEntity;
		Vector            m_vGlowColor;
		float            m_flGlowAlpha;

		char            unknown[4]; //pad 
		float            flUnk; //confirmed to be treated as a float while reversing glow functions 
		float            m_flBloomAmount;
		float            l_playeriszeropoint3;


		bool            m_bRenderWhenOccluded;
		bool            m_bRenderWhenUnoccluded;
		bool            m_bFullBloomRender;
		char            unknown1[1]; //pad 


		int                m_nFullBloomStencilTestValue; // 0x28 
		int                iUnk; //appears like it needs to be zero  
		int                m_nSplitScreenSlot; //Should be -1 

											   // Linked list of free slots 
		int                m_nNextFreeSlot;

		// Special values for GlowObjectDefinition_t::m_nNextFreeSlot 
		static const int END_OF_FREE_LIST = -1;
		static const int ENTRY_IN_USE = -2;
	};

	CUtlVector< GlowObjectDefinition_t > m_GlowObjectDefinitions;
	int m_nFirstFreeSlot;
};