
#include <math.h>
enum TraceType_t
{
	TRACE_EVERYTHING = 0,
	TRACE_WORLD_ONLY,
	TRACE_ENTITIES_ONLY,
	TRACE_EVERYTHING_FILTER_PROPS,
};
struct cplane_t
{
	Vector  normal;
	float   dist;
	BYTE    type;
	BYTE    signbits;
	BYTE    pad[2];
};
struct csurface_t
{
	const char*             name;
	short                   surfaceProps;
	unsigned short  flags;
};


class CBaseTrace
{
public:
	Vector                  startpos;
	Vector                 endpos;
	cplane_t                plane;
	float                   fraction;
	int                             contents;
	unsigned short  dispFlags;
	bool                    allsolid;
	bool                    startsolid;
};



class CGameTrace : public CBaseTrace
{
public:


	float			fractionleftsolid;	// time we left a solid, only valid if we started in solid
	csurface_t		surface;			// surface hit (impact surface)

	int				hitgroup;			// 0 == generic, non-zero is specific body part

	short			physicsbone;		// physics bone hit by trace in studio
	unsigned short	worldSurfaceIndex;	// Index of the msurface2_t, if applicable

	C_BaseEntity*  m_pEnt;
	int			hitbox;

	bool DidHitWorld() const;

	
	bool DidHitNonWorldC_BaseEntity() const;

	bool DidHit() const;
	

};

struct trace_t
{
	bool                    DidHit() const { return fraction < 1 || allSolid || startSolid; };
	Vector	start;
	Vector	endpos;
	cplane_t	plane;
	float		fraction;
	int			contents;
	WORD		dispFlags;
	bool		allSolid;
	bool		startSolid;
	float		fractionLeftSolid;
	csurface_t	surface;
	int			hitgroup;
	short		physicsBone;
	WORD		m_nWorldSurfaceIndex;
	C_BaseEntity* m_pEnt;
	int			hitbox;
	bool DidHitWorld() const;
	bool DidHitNonWorldEntity() const;
};

typedef float vec_t;
class  VectorAligned : public Vector
{
public:
	inline VectorAligned(void) {};
	inline VectorAligned(vec_t X, vec_t Y, vec_t Z)
	{
		//		Init(X,Y,Z);
		//		X = 0.0f; Y=0.0f ;Z =0.0f;
	}

#ifdef VECTOR_NO_SLOW_OPERATIONS

private:
	// No copy constructors allowed if we're in optimal mode
	VectorAligned(const VectorAligned& vOther);
	VectorAligned(const Vector &vOther);

#else
public:
	explicit VectorAligned(const Vector &vOther)
	{
		//		Init(vOther.X, vOther.Y, vOther.Z);
		//		vOther.X=0.0f;vOther.Y=0.0f;vOther.Z =0.0;
	}

	VectorAligned& operator=(const Vector &vOther)
	{
		//		Init(vOther.X, vOther.Y, vOther.Z);
		x = vOther.x; y = vOther.y; z = vOther.z;
		return *this;
	}

#endif
	float w;	// this space is used anyway
};
struct C__Ray
{
	VectorAligned m_Start1; // starting point, centered within the extents
	VectorAligned m_Delta1; // direction + length of the ray
	VectorAligned m_StartOffset1; // Add this to m_Start to get the actual ray start
	VectorAligned m_Extents1; // Describes an axis aligned box extruded along a ray
	const matrix3x4_t* m_pWorldAxisTransform1;
	//const matrix3x4_t *m_pWorldAxisTransform;
	bool m_IsRay1; // are the extents zero?
	bool m_IsSwept1; // is delta != 0?


	C__Ray(Vector _start, Vector _end)
	{
		Init(_start, _end);
	}

	C__Ray(Vector _start, Vector _end, Vector _mins, Vector _maxs)
	{
		Init(_start, _end, _mins, _maxs);
	}

	void Init(const Vector& vecStart, const Vector& vecEnd)
	{
		m_Delta1 = vecEnd - vecStart;

		m_IsSwept1 = (m_Delta1.LengthSqr() != 0);

		m_Extents1.x = m_Extents1.y = m_Extents1.z = 0.0f;
		m_IsRay1 = true;

		m_StartOffset1.x = m_StartOffset1.y = m_StartOffset1.z = 0.0f;

		m_Start1 = vecStart;
	}

	void Init(Vector& vecStart, Vector& vecEnd, Vector min, Vector max)
	{
		m_Delta1 = vecEnd - vecStart;

		m_IsSwept1 = (m_Delta1.LengthSqr() != 0);

		m_Extents1.x = (max.x - min.x);
		m_Extents1.y = (max.y - min.y);
		m_Extents1.z = (max.z - min.z);
		m_IsRay1 = false;

		m_StartOffset1.x = m_StartOffset1.y = m_StartOffset1.z = 0.0f;

		m_Start1 = vecStart + ((max + min) * 0.5f);
	}

private:
};

struct Ray_t {
	VectorAligned  m_Start;  // starting point, centered within the extents
	VectorAligned  m_Delta;  // direction + length of the ray
	VectorAligned  m_StartOffset; // Add this to m_Start to get the actual ray start
	VectorAligned  m_Extents;     // Describes an axis aligned box extruded along a ray
	const matrix3x4_t *m_pWorldAxisTransform;
	bool m_IsRay;  // are the extents zero?
	bool m_IsSwept;     // is delta != 0?

	Ray_t() : m_pWorldAxisTransform(NULL) {}

	void Init(Vector const& start, Vector const& end) {
		m_Delta = end - start;

		m_IsSwept = (m_Delta.LengthSqr() != 0);

		m_Extents.Init();

		m_pWorldAxisTransform = NULL;
		m_IsRay = true;

		// Offset m_Start to be in the center of the box...
		m_StartOffset.Init();
		VectorCopy(start, m_Start);
	}

	void Init(Vector const& start, Vector const& end, Vector const& mins, Vector const& maxs) {
		m_Delta = end - start;

		m_pWorldAxisTransform = NULL;
		m_IsSwept = (m_Delta.LengthSqr() != 0);

		m_Extents = maxs - mins;
		m_Extents *= 0.5f;
		m_IsRay = (m_Extents.LengthSqr() < 1e-6);

		// Offset m_Start to be in the center of the box...
		m_StartOffset = maxs + mins;
		m_StartOffset *= 0.5f;
		m_Start = start + m_StartOffset;
		m_StartOffset *= -1.0f;
	}
	Vector InvDelta() const {
		Vector vecInvDelta;
		for (int iAxis = 0; iAxis < 3; ++iAxis) {
			if (m_Delta[iAxis] != 0.0f) {
				vecInvDelta[iAxis] = 1.0f / m_Delta[iAxis];
			}
			else {
				vecInvDelta[iAxis] = FLT_MAX;
			}
		}
		return vecInvDelta;
	}

private:
};
class ITraceFilter
{
public:
	virtual bool ShouldHitEntity(void* pEntity, int contentsMask) = 0;
	virtual TraceType_t GetTraceType() const = 0;
};

class C_TraceFilter : public ITraceFilter
{
public:

	explicit C_TraceFilter(C_BaseEntity* entity, TraceType_t tracetype = TRACE_EVERYTHING)
	{
		pSkip1 = entity;
	}

	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return (pEntityHandle != pSkip1);
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

	void* pSkip1;
};
class CTraceFilter : public ITraceFilter
{
public:
	
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return (pEntityHandle != pSkip1);
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

	void* pSkip1;
};

class CTraceFilterOneEntity : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return (pEntityHandle == pEntity);
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

	void* pEntity;
};

class CTraceFilterOneEntity2 : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return (pEntityHandle == pEntity);
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_ENTITIES_ONLY;
	}

	void* pEntity;
};

class CTraceFilterSkipTwoEntities : public ITraceFilter
{
public:
	CTraceFilterSkipTwoEntities(void* pPassEnt1, void* pPassEnt2)
	{
		passentity1 = pPassEnt1;
		passentity2 = pPassEnt2;
	}

	virtual bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return !(pEntityHandle == passentity1 || pEntityHandle == passentity2);
	}

	virtual TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

	void* passentity1;
	void* passentity2;
};

class CTraceEntity : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return !(pEntityHandle == pSkip1);
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_ENTITIES_ONLY;
	}

	void* pSkip1;
};
class CTraceFilterWorldAndPropsOnly : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return false;
	}
	virtual TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}
};
class CTraceFilterNoPlayer : public CTraceFilter
{
public:
	CTraceFilterNoPlayer() {}
	virtual bool ShouldHitC_BaseEntity(C_BaseEntity *pServerC_BaseEntity, int contentsMask)
	{
		if (pServerC_BaseEntity)
			return !pServerC_BaseEntity->IsPlayer();
		return false;
	}
};
class CTraceWorldOnly : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return false;
	}

	TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

	void* pSkip1;
};

class CTraceFilterWorldOnly : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* /*pServerEntity*/, int /*contentsMask*/)
	{
		return false;
	}
	virtual TraceType_t GetTraceType() const
	{
		return TRACE_WORLD_ONLY;
	}
};
class CTraceC_BaseEntity : public ITraceFilter
{
public:
	bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
	{
		return (pEntityHandle == pHit);
	}
	virtual TraceType_t	GetTraceType() const
	{
		return TRACE_ENTITIES_ONLY;
	}
	void* pHit;
};
class CEngineTrace
{
public:
	virtual int		GetPointContents(const Vector &vecAbsPosition/*,*/ /*int contentsMask = mask_all, IHandleEntity** ppEntity = NULL*/) = 0;

	// Returns the contents mask of the world only @ the world-space position (static props are ignored)
	virtual int		GetPointContents_WorldOnly(const Vector &vecAbsPosition/*, int contentsMask = mask_all*/) = 0;

	// Get the point contents, but only test the specific entity. This works
	// on static props and brush models.
	//
	// If the entity isn't a static prop or a brush model, it returns CONTENTS_EMPTY and sets
	// bFailed to true if bFailed is non-null.
	virtual int		GetPointContents_Collideable(/*ICollideable *pCollide,*/ const Vector &vecAbsPosition) = 0;

	// Traces a ray against a particular entity
	virtual void	ClipRayToEntity(const Ray_t &ray, unsigned int fMask,/* IHandleEntity *pEnt,*/ trace_t *pTrace) = 0;

	// Traces a ray against a particular entity
	virtual void	ClipRayToCollideable(const Ray_t &ray, unsigned int fMask,/* ICollideable *pCollide,*/ trace_t *pTrace) = 0;

	// A version that simply accepts a ray (can work as a traceline or tracehull)
	virtual void	Trace_Ray(const C__Ray &ray, unsigned int fMask, ITraceFilter *pTraceFilter, trace_t *pTrace) = 0;
	void TraceRay(const Ray_t &ray, unsigned int fMask, CTraceFilter *pTraceFilter, trace_t *pTrace)
	{
		typedef void(__thiscall* oTraceRay)(PVOID, const Ray_t&, unsigned int, CTraceFilter *, trace_t*);
		CallVFunction<oTraceRay>(this, 5)(this, ray, fMask, pTraceFilter, pTrace);
	}
	void TraceRay_NEW(const Ray_t &ray, unsigned int fMask, ITraceFilter* pTraceFilter, trace_t* pTrace)
	{
		typedef void(__thiscall* oTraceRay)(void*, const Ray_t&, unsigned int, ITraceFilter*, trace_t*);
		CallVFunction<oTraceRay>(this, 5)(this, ray, fMask, pTraceFilter, pTrace);
	}
	void ClipRayToC_BaseEntity(const Ray_t& ray, unsigned int nMask, C_BaseEntity* pC_BaseEntity, trace_t* pTrace)
	{
		typedef void(__thiscall* oClipRayToTrace)(PVOID, const Ray_t&, unsigned int, C_BaseEntity*, trace_t*);
		CallVFunction<oClipRayToTrace>(this, 3)(this, ray, nMask, pC_BaseEntity, pTrace);
	}
	void C_BaseEntityTraceRay(const Ray_t &ray, unsigned int fMask, CTraceC_BaseEntity *pTraceFilter, trace_t *pTrace)
	{
		typedef void(__thiscall* oTraceRay)(PVOID, const Ray_t&, unsigned int, CTraceC_BaseEntity*, trace_t*);
		CallVFunction<oTraceRay>(this, 5)(this, ray, fMask, pTraceFilter, pTrace);
	}
	int	GetPointContents(const Vector &vecAbsPosition, int contentsMask = MASK_ALL, C_BaseEntity* ppC_BaseEntity = NULL)
	{
		typedef int(__thiscall* fnGetPointContents)(void*, const Vector&, int, C_BaseEntity*);
		return CallVFunction<fnGetPointContents>(this, 0)(this, vecAbsPosition, contentsMask, ppC_BaseEntity);
	}

	bool IsVisible(C_BaseEntity* pLocalClientBaseC_BaseEntity, Vector vecOrigin, Vector vecFinal, C_BaseEntity* pClientBaseC_BaseEntity)
	{
		Ray_t ray;
		ray.Init(vecOrigin, vecFinal);

		CTraceFilter TraceFilter;
		TraceFilter.pSkip1 = pLocalClientBaseC_BaseEntity;

		trace_t trace;
		TraceRay(ray, MASK_SHOT, &TraceFilter, &trace);
	
		return (trace.m_pEnt == pClientBaseC_BaseEntity || trace.fraction >= 1.0f);
	}
	bool IsVisible(C_BaseEntity* pLocalClientBaseC_BaseEntity, Vector vecOrigin, Vector vecFinal, C_BaseEntity* pClientBaseC_BaseEntity, int& hitgroup)
	{
		Ray_t ray;
		ray.Init(vecOrigin, vecFinal);

		CTraceFilter* TraceFilter = new CTraceFilter();
		TraceFilter->pSkip1 = pLocalClientBaseC_BaseEntity;

		trace_t trace;
		TraceRay(ray, MASK_SHOT, TraceFilter, &trace);
		hitgroup = trace.hitgroup;
		delete TraceFilter;
		return (trace.m_pEnt == pClientBaseC_BaseEntity || trace.fraction >= 1.0f);
	}

};

