#pragma once
class IMoveHelper
{
public:
	void SetHost(C_BaseEntity *pPlayer)
	{
		typedef void(__thiscall* OriginalFn)(void*, C_BaseEntity *pPlayer);
		CallVFunction<OriginalFn>(this, 1)(this, pPlayer);
	}

	bool m_bFirstRunOfFunctions : 1;
	bool m_bGameCodeMovedPlayer : 1;
	int m_nPlayerHandle; // edict index on server, client entity handle on client=
	int m_nImpulseCommand; // Impulse command issued.
	QAngle m_vecViewAngles; // Command view angles (local space)
	QAngle m_vecAbsViewAngles; // Command view angles (world space)
	int m_nButtons; // Attack buttons.
	int m_nOldButtons; // From host_client->oldbuttons;
	float m_flForwardMove;
	float m_flSideMove;
	float m_flUpMove;
	float m_flMaxSpeed;
	float m_flClientMaxSpeed;
	Vector m_vecVelocity; // edict::velocity // Current movement direction.
	QAngle m_vecAngles; // edict::angles
	QAngle m_vecOldAngles;
	float m_outStepHeight; // how much you climbed this move
	Vector m_outWishVel; // This is where you tried 
	Vector m_outJumpVel; // This is your jump velocity
	Vector m_vecConstraintCenter;
	float m_flConstraintRadius;
	float m_flConstraintWidth;
	float m_flConstraintSpeedFactor;
	float m_flUnknown[5];
	Vector m_vecAbsOrigin;
};
class CPrediction
{
public:
	void SetLocalViewAngles(QAngle & ang)
	{
		typedef void(__thiscall* fn)(void*, QAngle &);
		CallVFunction<fn>(this, 13)(this, ang);
	}
	void SetupMove(C_BaseEntity* ent, c_user_cmd* cmd, void* move, void* movedata)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*, c_user_cmd*, void*, void*);
		CallVFunction<fn>(this, 20)(this, ent, cmd, move, movedata);
	}

	void FinishMove(C_BaseEntity* ent, c_user_cmd* cmd, void* movedata)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*, c_user_cmd*, void*);
		CallVFunction<fn>(this, 21)(this, ent, cmd, movedata);
	}

	void RunCommand(C_BaseEntity* pEntity, c_user_cmd* pCmd, void* moveHelper)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*, c_user_cmd*, void*);
		CallVFunction<fn>(this, 19)(this, pEntity, pCmd, moveHelper);
	}
};

class CGameMovement
{
public:
	void ProcessMovement(C_BaseEntity* ent, void* movedata)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*, void*);
		CallVFunction<fn>(this, 1)(this, ent, movedata);
	}

	void StartTrackPredictionErrors(C_BaseEntity* ent)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*);
		CallVFunction<fn>(this, 3)(this, ent);
	}

	void FinishTrackPredictionErrors(C_BaseEntity* ent)
	{
		typedef void(__thiscall* fn)(void*, C_BaseEntity*);
		CallVFunction<fn>(this, 4)(this, ent);
	}

	void DecayPunchAngle()
	{
		typedef void(__thiscall* fn)(void*);
		CallVFunction<fn>(this, 19)(this);
	}
};