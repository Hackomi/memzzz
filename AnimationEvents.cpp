#include "AnimationEvents.h"

void CAnimationEventManager::AnimationLoop()
{
	for (auto& Animation : AnimationList)
	{
		if (Animation.iState == ANIM_READY)
			continue;
		if (Animation.iState == ANIM_WAIT && Animation.flStartTime <= g_globals->realtime)
			Animation.iState = ANIM_PROCESS;
		if (Animation.iState == ANIM_PROCESS)
		{
			float Delta = Animation.EndValue - Animation.StartValue;
			float flDeltaTime = Animation.flEndTime - Animation.flStartTime;
			if (Delta == 0 || Animation.flEndTime < g_globals->realtime)
			{
				Animation.iState == ANIM_READY;
				*Animation.pOutValue = Animation.EndValue;
				continue;
			}
			*Animation.pOutValue = Animation.StartValue + (Delta * (g_globals->realtime - Animation.flStartTime) / flDeltaTime);
		}
	}
}

void CAnimationEventManager::RegisterAnimation(std::string name, float * pvalue)
{
	if (std::find(AnimationList.begin(), AnimationList.end(), name) == AnimationList.end())
	{
		AnimEvent_t Animation;
		Animation.szEventName = name;
		Animation.pOutValue = pvalue;
		AnimationList.push_back(Animation);
	}
}

void CAnimationEventManager::UpdateAnimation(std::string name, float startvalue, float needvalue, float starttime, float endtime)
{
	for (auto& Animation : AnimationList)
	{
		if (Animation.szEventName == name)
		{
			if (g_globals->realtime < starttime)
				Animation.iState = ANIM_WAIT;
			else if (g_globals->realtime < endtime)
				Animation.iState = ANIM_PROCESS;
			else
				Animation.iState = ANIM_READY;
			Animation.StartValue = startvalue;
			Animation.EndValue = needvalue;
			Animation.flStartTime = starttime;
			Animation.flEndTime = endtime;
			break;
		}
	}
}

bool AnimEvent_t::operator==(std::string name)
{
	return szEventName == name;
}
