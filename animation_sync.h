#pragma once
#include "Singleton.h"
#include "sdk.h"

class animation_sync : public Singleton<animation_sync>
{
private:
	void leet(C_BaseEntity *player) 
	{
		static ConVar *sv_pvsskipanimation = g_pCvar->FindVar(XorStr("sv_pvsskipanimation"));

		int32_t backup_sv_pvsskipanimation = sv_pvsskipanimation->GetInt();
		sv_pvsskipanimation->SetValue(0);

		*(int32_t*)((uintptr_t)player + 0xA30) = 0;
		*(int32_t*)((uintptr_t)player + 0x269C) = 0;

		int Backup = *(int*)((uintptr_t)player + 0x274);
		*(int*)((uintptr_t)player + 0x274) = 0;
		*(int*)((uintptr_t)player + 240) |= 8;
		Vector absOriginBackupLocal = player->GetAbsOrigin();
		player->SetAbsOrigin(player->GetOrigin());

		player->SetupBones(NULL, -1, BONE_USED_BY_ANYTHING, g_globals->curtime);

		player->SetAbsOrigin(absOriginBackupLocal);
		*(int*)((uintptr_t)player + 240) &= ~8;
		*(int*)((uintptr_t)player + 0x274) = Backup;
		sv_pvsskipanimation->SetValue(backup_sv_pvsskipanimation);
	};
public:
	class C_BaseAnimating;
	struct clientanimating_t
	{
		C_BaseAnimating *pAnimating;
		unsigned int	flags;
		clientanimating_t(C_BaseAnimating *_pAnim, unsigned int _flags) : pAnimating(_pAnim), flags(_flags) {}
	};

	bool UseFreestandAngle[65];
	float FreestandAngle[65];
	clientanimating_t *animating = nullptr;
	int animflags;

	const unsigned int FCLIENTANIM_SEQUENCE_CYCLE = 0x00000001;

	void anim_sync(C_BaseEntity * entity);

	void creat_move_part();

	void call_anim_sync();

	void third_person();

	float setupvelocity_rebuild(C_BaseEntity * entity);

	void disable_interoplation();

};