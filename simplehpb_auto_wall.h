#pragma once
#define	HITGROUP_GENERIC	0
#define	HITGROUP_HEAD		1
#define	HITGROUP_CHEST		2
#define	HITGROUP_STOMACH	3
#define HITGROUP_LEFTARM	4	
#define HITGROUP_RIGHTARM	5
#define HITGROUP_LEFTLEG	6
#define HITGROUP_RIGHTLEG	7
#define HITGROUP_GEAR		10			// alerts NPC, but doesn't do damage or bleed (1/100th damage)
struct FireBulletData1
{
	FireBulletData1(const Vector& eye_pos) : src(eye_pos)
	{
	}

	Vector src;
	trace_t enter_trace;
	Vector direction;
	CTraceFilter filter;
	float trace_length;
	float trace_length_remaining;
	float current_damage;
	int penetrate_count;
};
struct Autowall_Return_Info
{
	int damage;
	int hitgroup;
	int penetration_count;
	bool did_penetrate_wall;
	float thickness;
	Vector end;
	C_BaseEntity* hit_entity;
};
struct Autowall_Info
{
	Vector start;
	Vector end;
	Vector current_position;
	Vector direction;

	ITraceFilter* filter;
	trace_t enter_trace;

	float thickness;
	float current_damage;
	int penetration_count;
};

class CBulletHandler : public Singleton<CBulletHandler>
{
public:
	bool BreakableEntity(IClientEntity * entity);
	void TraceLine(Vector & absStart, Vector & absEnd, unsigned int mask, IClientEntity * ignore, trace_t * ptr);
	bool trace_to_exit(trace_t & enterTrace, trace_t & exitTrace, Vector startPosition, Vector direction);
	bool HandleBulletPenetration1(CSWeaponInfo * weaponData, trace_t & enterTrace, Vector & eyePosition, Vector direction, int & possibleHitsRemaining, float & currentDamage, float penetrationPower, bool sv_penetration_type, float ff_damage_reduction_bullets, float ff_damage_bullet_penetration);
	void GetBulletTypeParameters(float & maxRange, float & maxDistance, char * bulletType, bool sv_penetration_type);
	bool FireBullet(CBaseCombatWeapon * pWeapon, Vector & direction, float & currentDamage);
	float CanHit(Vector & point);
	float CanHit(Vector & start, Vector & point);
	float CanHit(C_BaseEntity * ent, Vector & point);
	bool trace_awall(float & damage);
	bool trace_awall(C_BaseEntity * m_local, C_BaseEntity * entity, Vector hit, float & damage);
	bool trace_awall(C_BaseEntity * m_local, Vector hit, float & damage);
	bool HandleBulletPenetration(CSWeaponInfo* wpn_data, FireBulletData1& BulletData);
	bool PenetrateWall(C_BaseEntity* pPlayer, CBaseCombatWeapon* pWeapon, const Vector& vecPoint, float& flDamage);
	bool SimulateFireBullet(C_BaseEntity* pPlayer, CBaseCombatWeapon* pWeapon, FireBulletData1& BulletData);
	void ScaleDamage1(trace_t & enterTrace, CSWeaponInfo * weaponData, float & currentDamage);
	void UTIL_TraceLine(Vector& vecStart, Vector& vecEnd, unsigned int nMask, C_BaseEntity* pCSIgnore, trace_t* pTrace);
	void UTIL_ClipTraceToPlayers(Vector & vecAbsStart, Vector & vecAbsEnd, unsigned int mask, ITraceFilter * filter, trace_t * tr);
	void ScaleDamage(int iHitgroup, C_BaseEntity* pPlayer, float flWeaponArmorRatio, float& flDamage);
	float GetDmg(C_BaseEntity * pPlayer, Vector point);
	bool IsPenetrable(C_BaseEntity * pLocal, C_BaseEntity * pPlayer, Vector point/*, float & damage*/);
	bool handle_penetration = false;

};