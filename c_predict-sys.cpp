#include "sdk.h"
#include "c_predict-sys.hpp"
#include "global.h"
#include "xor.h"
#include "GameUtils.h"

static char* cMoveData = nullptr;

void CPredictionSystem::EnginePrediction(c_user_cmd* pCmd)
{
	if (!g_pMoveHelper || !pCmd || !c_ayala::l_player)
		return;

	CMoveData C_MoveData;
	iFlagsBackup = *c_ayala::l_player->GetFlags();
	iButtonsBackup = pCmd->buttons;
	c_ayala::l_player->SetCurrentCommand(pCmd);
	if (!m_pPredictionRandomSeed || !m_pSetPredictionPlayer)
	{
		m_pPredictionRandomSeed = *reinterpret_cast<int**>(FindPatternIDA(XorStr("client_panorama.dll"), XorStr("8B 0D ? ? ? ? BA ? ? ? ? E8 ? ? ? ? 83 C4 04")) + 2);
		m_pSetPredictionPlayer = *reinterpret_cast<int**>(FindPatternIDA(XorStr("client_panorama.dll"), XorStr("89 35 ? ? ? ? F3 0F 10 48 20")) + 2);
	}
	if (!cMoveData)
		cMoveData = (char*)(calloc(1, sizeof(CMoveData)));
	g_pMoveHelper->SetHost(c_ayala::l_player);
	*m_pPredictionRandomSeed = MD5_PseudoRandom(pCmd->command_number) & 0x7FFFFFFF;
	*m_pSetPredictionPlayer = uintptr_t(c_ayala::l_player);
	g_globals->curtime = c_ayala::l_player->GetTickBase() * g_globals->interval_per_tick;
	g_globals->frametime = g_globals->interval_per_tick;
	pCmd->buttons |= *reinterpret_cast< uint8_t* >(uintptr_t(c_ayala::l_player) + 0x3310);
	if (pCmd->impulse)
		*reinterpret_cast< uint8_t* >(uintptr_t(c_ayala::l_player) + 0x31EC) = pCmd->impulse;
	C_MoveData.m_nButtons = pCmd->buttons;
	int buttonsChanged = pCmd->buttons ^ *reinterpret_cast<int*>(uintptr_t(c_ayala::l_player) + 0x31E8);
	*reinterpret_cast<int*>(uintptr_t(c_ayala::l_player) + 0x31DC) = (uintptr_t(c_ayala::l_player) + 0x31E8);
	*reinterpret_cast<int*>(uintptr_t(c_ayala::l_player) + 0x31E8) = pCmd->buttons;
	*reinterpret_cast<int*>(uintptr_t(c_ayala::l_player) + 0x31E0) = pCmd->buttons & buttonsChanged;  //m_afButtonPressed ~ The changed ones still down are "pressed"
	*reinterpret_cast<int*>(uintptr_t(c_ayala::l_player) + 0x31E4) = buttonsChanged & ~pCmd->buttons; //m_afButtonReleased ~ The ones not down are "released"

	g_pGameMovement->StartTrackPredictionErrors(c_ayala::l_player);

	iTickBaseBackup = c_ayala::l_player->GetTickBase();

	g_pPrediction->SetupMove(c_ayala::l_player, pCmd, g_pMoveHelper, reinterpret_cast< CMoveData* >(cMoveData));
	g_pGameMovement->ProcessMovement(c_ayala::l_player, reinterpret_cast< CMoveData* >(cMoveData));
	g_pPrediction->FinishMove(c_ayala::l_player, pCmd, reinterpret_cast< CMoveData* >(cMoveData));

	c_ayala::l_player->GetTickBase() = iTickBaseBackup;

	g_pGameMovement->FinishTrackPredictionErrors(c_ayala::l_player);

	c_ayala::l_player->SetCurrentCommand(nullptr);
	*m_pPredictionRandomSeed = -1;
	*m_pSetPredictionPlayer = 0;
	g_pMoveHelper->SetHost(0);

	*c_ayala::l_player->GetFlags() = iFlagsBackup;
	pCmd->buttons = iButtonsBackup;
}