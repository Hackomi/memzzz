﻿#pragma once
#include <vector>
#include <deque>
#include <unordered_map>
#include <set>
#include <algorithm>
#define SIZEOF(var) ( sizeof( var ) / sizeof( var[0] ))

class itemTimer {
public:
	itemTimer();
	itemTimer(float maxTime);
	float getTimeRemaining();
	float getTimeRemainingRatio();
	float getMaxTime();
	void setMaxTime(float);
	void startTimer();
private:
	float timeStarted;
	float maxTime;
};


class FloatingText
{
public:
	void Draw();
	float TimeCreated;
	float ExpireTime;
	int DamageAmt;
	FloatingText(C_BaseEntity* attachEnt, float lifetime, int Damage);

private:
	C_BaseEntity* pEnt;


};

class CScreen
{
public:
	int width, height;
};

class CGlobalPointers
{
public:
	C_BaseEntity* l_player;
	CBaseCombatWeapon* c_weapon;
	c_user_cmd*	user_cmd;
}; extern CGlobalPointers* g_GlobalPointers;
class CClientState
{
public:
	void ForceFullUpdate()
	{
		*(uint32_t*)((uintptr_t)this + 0x174) = -1;
	}

	char pad_0000[156];             //0x0000
	NetChannel* m_NetChannel;          //0x009C
	uint32_t m_nChallengeNr;        //0x00A0
	char pad_00A4[100];             //0x00A4
	uint32_t m_nSignonState;        //0x0108
	char pad_010C[8];               //0x010C
	float m_flNextCmdTime;          //0x0114
	uint32_t m_nServerCount;        //0x0118
	uint32_t m_nCurrentSequence;    //0x011C
	char pad_0120[8];               //0x0120
	int m_ClockDriftMgr; //0x0128
	uint32_t m_nDeltaTick;          //0x0178
	bool m_bPaused;                 //0x017C
	char pad_017D[3];               //0x017D
	uint32_t m_nViewEntity;         //0x0180
	uint32_t m_nPlayerSlot;         //0x0184
	char m_szLevelName[260];        //0x0188
	char m_szLevelNameShort[40];    //0x028C
	char m_szGroupName[40];         //0x02B4
	char pad_02DC[52];              //0x02DC
	uint32_t m_nmax_сlients;         //0x0310
	char pad_0314[18820];           //0x0314
	float m_flLastServerTickTime;   //0x4C98
	bool insimulation;              //0x4C9C
	char pad_4C9D[3];               //0x4C9D
	uint32_t oldtickcount;          //0x4CA0
	float m_tickRemainder;          //0x4CA4
	float m_frameTime;              //0x4CA8
	uint32_t lastoutgoingcommand;   //0x4CAC
	uint32_t chokedcommands;        //0x4CB0
	uint32_t last_command_ack;      //0x4CB4
	uint32_t command_ack;           //0x4CB8
	uint32_t m_nSoundSequence;      //0x4CBC
	char pad_4CC0[80];              //0x4CC0
	Vector viewangles;              //0x4D10
}; //Size: 0x4D1C
class CPlayerlistInfo {
public:
	int iHitbox = 0;

	bool bBaim = false;
	bool bOverrideResolver = false;

	vec_t pitch = 0.f;
	vec_t yaw = 0.f;
	vec_t roll = 0.f;

	QAngle viewangles = QAngle(pitch, yaw, roll);
};

namespace c_ayala // Global Stuff
{
	extern std::deque<std::tuple<Vector, float, Color>> hitscan_points;

	/*---------------------ENGINE & UTIL CLASSES---------------------*/
	extern QAngle							RealAngle;
	extern QAngle							FakeAngle;
	extern QAngle							LastAngle;
	extern QAngle							StrafeAngle;
	extern C_BaseEntity*					l_player;
	extern CBaseCombatWeapon*				c_weapon;
	extern c_user_cmd*					    user_cmd;
	extern CSWeaponInfo*					c_weapon_data;
	extern Vector							vecUnpredictedVel;
	extern Vector                           CameraOrigin;
	extern HWND								Window;
	extern CScreen							Screen;
	extern std::vector<std::string> weaponNames;

	extern bool								c_SendPacket;
	extern bool								ShowMenu;
	extern bool								Opened;
	extern bool								Init;
	extern bool								NewRound;
	extern bool	                            expired;
	extern bool								ClearRoundData;
	extern bool								bShouldChoke;
	extern bool								Panorama;
	extern bool	                            ClearFeed;
	extern DWORD                            CourierNew;

	extern float							PredictedTime;
	extern float                            viewMatrix[4][4];

	extern int								MenuTab;
	extern int								wbpoints;
	extern int								wbcurpoint;
	extern int                              localtime;

}

class CDataMapUtils {
public:
	int Find(datamap_t *pMap, const char* szName);
};

class CEncryption {
public:
	template<class U>
	void Encrypt(U& szString, int iSize) {
		if (bEncrypted) 
			return;

		for (UINT Iterator = 0; Iterator < iSize; Iterator++)
			szString[Iterator] += (2 * szKey[ (SIZEOF(szKey)) - Iterator ]);

		bEncrypted = true;
	}

	template<class U>
	void Decrypt(U& szString, int iSize) {
		if (!bEncrypted) 
			return;

		for (UINT Iterator = 0; Iterator < iSize; Iterator++)
			szString[Iterator] -= (2 * szKey[ (SIZEOF(szKey)) - Iterator ]);

		bEncrypted = false;
	}

	bool SetState(bool bEncryptState) {
		bEncrypted = bEncryptState;
	}

private:
	bool bEncrypted = false;
	char szKey[29] = { 'A', 'z', 'B', 'y', 'C', 'x', 'D', 'w', 'E', 'v', 'F', 'u', 'G', 't', 'H', 's', 'I', 'r', 'J' };
};
extern CEncryption* g_pEncrypt;

class CEncryptedString {
public:
	CEncryptedString(const char* szInput) {
		std::string szOutput = std::string(szInput);
		g_pEncrypt->Encrypt(szOutput, szOutput.size());

		szEncryptedString = szOutput.c_str();
	};

	const char* c_str() {
		std::string szInput = std::string(szEncryptedString);
		g_pEncrypt->Decrypt(szInput, szInput.size());

		return szInput.c_str();
	};

private:
	const char* szEncryptedString;
};

typedef CEncryptedString EString;

extern CDataMapUtils* g_pData;

extern CPlayerlistInfo* g_pPlayerlistInfo[64];
extern C_BaseEntity* c_ayala::l_player;
extern CBaseCombatWeapon* c_ayala::c_weapon;
extern c_user_cmd*	c_ayala::user_cmd;
