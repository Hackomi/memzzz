﻿#include "sdk.h"
#include "antim-aims.h"
#include "global.h"
#include "GameUtils.h"
#include "Math.h"
#include "c_track-manager.hpp"
#include "Autowall.h"
#include "simplehpb_auto_wall.h"
#define square1( x ) ( x * x )

template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
namespace FakeLatency
{
	extern DWORD ClientState;
}
CAntiaim* g_Antiaim = new CAntiaim;
inline float FastSqrt1(float x)
{
	unsigned int i = *(unsigned int*)&x;

	i += 127 << 23;
	i >>= 1;

	return *(float*)&i;
}
void MinWalk(float get_speed)
{
	if (get_speed <= 0.f)
		return;

	float min_speed = (float)(FastSqrt1(square1(c_ayala::user_cmd->forwardmove) + square1(c_ayala::user_cmd->sidemove) + square1(c_ayala::user_cmd->upmove)));
	if (min_speed <= 0.f)
		return;

	if ((c_ayala::user_cmd->buttons & IN_DUCK))
		get_speed *= 2.94117647f;

	if (min_speed <= get_speed)
		return;

	float kys = get_speed / min_speed;

	c_ayala::user_cmd->forwardmove *= kys;
	c_ayala::user_cmd->sidemove *= kys;
	c_ayala::user_cmd->upmove *= kys;
}
bool CAntiaim::Fakewalk(c_user_cmd *user_cmd) //Need upgrade
{
	if (GetAsyncKeyState(Menu.Antiaim.FakeWalkKey))
	{
		Vector velocity = c_ayala::vecUnpredictedVel;
		int32_t ticks_to_stop;
		const int32_t max_ticks = 13;
		DWORD ClientState = *(DWORD*)FakeLatency::ClientState;
		const int32_t chocked = *(DWORD*)(ClientState + 0x4D28);
		int32_t ticks_left = max_ticks - chocked;
		ticks_to_stop = 13 / 3;
		c_ayala::c_SendPacket = chocked >= 13;
		if (ticks_to_stop >= ticks_left || !chocked || c_ayala::c_SendPacket)
		{
			velocity = c_ayala::vecUnpredictedVel;
			float_t speed = velocity.Length2D();
			if (speed > 13 / 5.5 * g_pCvar->FindVar(XorStr("sv_accelerate"))->GetFloat())
			{
				QAngle direction;
				Math::VectorAngles(velocity, direction);

				direction.y = c_ayala::user_cmd->viewangles.y - direction.y;

				Vector forward;
				Math::AngleVectors(direction, forward);
				Vector negated_direction = forward * -speed;

				c_ayala::user_cmd->forwardmove = negated_direction.x;
				c_ayala::user_cmd->sidemove = negated_direction.y;
			}
			else
			{
				c_ayala::user_cmd->forwardmove = 0.f;
				c_ayala::user_cmd->sidemove = 0.f;
			}
		}
		return true;
	}
	return false;
}
void CAntiaim::UpdateLowerBodyYawTime(bool ignoremove)
{
	CBasePlayerAnimState* animstate = c_ayala::l_player->GetBasePlayerAnimState();
	if (animstate)
	{
		DWORD ClientState = *(DWORD*)FakeLatency::ClientState;
	/*	if (*(DWORD*)(ClientState + 0x4CB0) == 0)
		{*/
			if (/*animstate->m_velocity > 0.1f */c_ayala::l_player->GetAnimOverlay(6)->m_flPlaybackRate != 0 && !ignoremove)
			{
				flUpdateTime = 0.22f;
				flLowerBodyYawUpdateTime = GameUtils::GetCurTime() + 0.22f;
			}
			else if (GameUtils::GetCurTime() > flLowerBodyYawUpdateTime)
			{
				flUpdateTime = 1.1f;
				flLowerBodyYawUpdateTime = GameUtils::GetCurTime() + 1.1f;
			}
		/*}*/
	}
}
float CAntiaim::GetClosetYaw()
{
	C_BaseEntity* pEntity = nullptr;
	C_BaseEntity* pCheckEnt;
	int Points[2] = { 0 };
	float Distance = FLT_MAX;
	Vector LocalEyePos = c_ayala::l_player->GetEyePosition();
	for (int i = 0; i < g_globals->max_сlients; i++) 
	{
		pCheckEnt = g_pEntitylist->GetClientEntity(i);
		if (!pCheckEnt
			|| !pCheckEnt->isAlive()
			|| !pCheckEnt->IsPlayer()
			|| !pCheckEnt->IsEnemy()
			|| pCheckEnt == c_ayala::l_player || pCheckEnt->IsDormant())
			continue;
		float CheckDist = Vector(LocalEyePos - pCheckEnt->GetEyePosition()).Length2D();
		if (CheckDist < Distance)
		{
			Distance = CheckDist;
			pEntity = pCheckEnt;
		}
	}
	if (!pEntity)
		return c_ayala::user_cmd->viewangles.y + 180;
	Vector EntEyePos = pEntity->GetEyePosition();
	float Z = c_ayala::l_player->GetHedPos().z;
	float Dist = Vector(EntEyePos - pEntity->GetHedPos()).Length2D();

	float totarget = Math::NormalizeYaw(GameUtils::CalculateAngle(LocalEyePos, EntEyePos).y);
	Vector forward, up(0, 0, Z - LocalEyePos.z + 5);

	for (int i = 1; i <= Menu.Antiaim.AutoDirectionDistance; i++)
	{
		Math::AngleVectors(QAngle(0, totarget + 90, 0), forward);
		Points[0] += Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, pEntity).damage > 1 ? 1 : 0;
	//	g_pDebugOverlay->AddBoxOverlay(LocalEyePos + forward * Dist * i + up, Vector(2, 2, 2), Vector(-2, -2, -2), QAngle(), Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, pEntity).damage > 0 ? 255 : 0, 0, 0, 255, 0.1);
		Math::AngleVectors(QAngle(0, totarget - 90, 0), forward);
		Points[1] += Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, pEntity).damage > 1 ? 1 : 0;		
	//	g_pDebugOverlay->AddBoxOverlay(LocalEyePos + forward * Dist * i + up, Vector(2, 2, 2), Vector(-2, -2, -2), QAngle(), Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, pEntity).damage > 0 ? 255 : 0, 0, 0, 255, 0.1);
	}
	
	if (Points[0] > Points[1])
		return Math::NormalizeYaw(totarget - 90);
	else if (Points[0] < Points[1])
		return Math::NormalizeYaw(totarget + 90);
	else if (Points[0] == 0)
		return Math::NormalizeYaw(totarget + 180);
	else return Math::NormalizeYaw(totarget + 180);
}

float CAntiaim::GetWorstYaw()
{
	float RotateDelta = (c_ayala::l_player->GetEyePosition() - c_ayala::l_player->GetHedPos()).Length2D();
	float Z = c_ayala::l_player->GetHedPos().z + 5;
	float Points[12] = { 0 };
	int maxdamage = INT_MIN;
	int iResult = -1;
	for (int i = 0; i < g_globals->max_сlients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (!pEntity
			|| !pEntity->isAlive()
			|| !pEntity->IsPlayer()
			|| !pEntity->IsEnemy()
			|| pEntity == c_ayala::l_player)
			continue;
		for (int k = 1; k <= Menu.Antiaim.AutoDirectionDistance; k++)
			for (int i = 0; i < 12; i++)
			{
				Points[i] += (Autowall::Get().CalculateDamage(pEntity->GetEyePosition(), Vector(c_ayala::l_player->GetEyePosition().x + RotateDelta * k * cos(M_PI * 2 / 12 * i), c_ayala::l_player->GetEyePosition().y + RotateDelta * k * sin(M_PI * 2 / 12 * i), Z), pEntity).damage > 1) ? 1 : 0;
			}
	}

		for (int i = 0; i < 12; i++)
		{
			auto point = Points[i];
			if (point > maxdamage)
			{
				maxdamage = point;
				iResult = i;
			}
		}
		return GameUtils::CalculateAngle(c_ayala::l_player->GetEyePosition(), Vector(c_ayala::l_player->GetEyePosition().x + RotateDelta * cos(M_PI * 2 / 12 * iResult), c_ayala::l_player->GetEyePosition().y + RotateDelta * sin(M_PI * 2 / 12 * iResult), Z)).y;
}
void CAntiaim::Pitch()
{
	switch (Menu.Antiaim.Pitch)
	{
	case 1: c_ayala::user_cmd->viewangles.x = 89; break;
	case 2: c_ayala::user_cmd->viewangles.x += 1080; break;
	case 3: c_ayala::user_cmd->viewangles.x -= 89; break;
	}
}

float GetMaxFakeDelta()
{
	CBasePlayerAnimState* animstate = c_ayala::l_player->GetBasePlayerAnimState();
	float speedfactor = clamp<float>(animstate->m_flSpeedFraction(), 0.0f, 1.0f);
	float unk1 = ((animstate->m_flLandingRatio() * -0.3f) - 0.2f) * speedfactor;
	float unk2 = unk1 + 1.0f;
	if (animstate->m_fDuckAmount > 0.0f) {
		float max_velocity = clamp<float>(animstate->m_flMaxWeaponVelocity(), 0.0f, 1.0f);
		float duck_speed = animstate->m_fDuckAmount * max_velocity;
		unk2 += (duck_speed * (0.5f - unk2));
	}

	return animstate->yaw_desync_adjustment() * unk2;
}
void CAntiaim::FakeDuck()
{
	if (c_ayala::user_cmd->buttons & IN_DUCK)
	{
	
	}
}
void CAntiaim::RealYaw()
{
	if (GetAsyncKeyState(0x14))
	{
		auto weapon = c_ayala::l_player->GetWeapon();

		auto local_player = c_ayala::l_player;
		auto weapon_auto = weapon->GetItemDefinitionIndex() == WEAPON_G3SG1 && WEAPON_SCAR20;
		auto weapon_awp = weapon->GetItemDefinitionIndex() == WEAPON_AWP;
		auto weapon_ssg = weapon->GetItemDefinitionIndex() == WEAPON_SSG08;

		if (weapon_auto) {
			if (*local_player->GetFlags() & FL_ONGROUND) {
				MinWalk(40);
			}
		}
		if (weapon_awp) {
			if (*local_player->GetFlags() & FL_ONGROUND) {
				MinWalk(33);
			}
		}
		if (weapon_ssg) {
			if (*local_player->GetFlags() & FL_ONGROUND) {
				MinWalk(65);
			}
		}
		if (!weapon_awp && !weapon_auto && !weapon_ssg) { //for every weapon btw
			if (*local_player->GetFlags() & FL_ONGROUND) {
				MinWalk(34);
			}
		}
		c_ayala::l_player->GetBasePlayerAnimState()->m_flUnknownFraction = 0.f;
	}
	CBasePlayerAnimState* animstate = c_ayala::l_player->GetBasePlayerAnimState();
	if (c_ayala::l_player->GetAnimOverlay(6)->m_flPlaybackRate == 0)
	{
		switch (Menu.Antiaim.AntiaimMode)
		{
		case 0: {
			c_ayala::user_cmd->viewangles.y = GetClosetYaw() + Math::RandomFloat(Menu.Antiaim.Jitterrange, -Menu.Antiaim.Jitterrange);
		//	float range = GetMaxFakeDelta();
		//	bool change_angle;
		//	if (GetKeyState(VK_DOWN))
		//		change_angle = true;
		//	else
		//		change_angle = false;

		//	if (c_ayala::user_cmd->command_number % 2)
		//	{
		//		c_ayala::c_SendPacket = true;
		//		c_ayala::user_cmd->viewangles.y = c_ayala::user_cmd->viewangles.y + (change_angle ? range : -range);
		//	}
		//	else
		//	{
		//		c_ayala::c_SendPacket = false;
		///*		if (change_angle)
		//			c_ayala::user_cmd->viewangles.y -= 90;
		//		else
		//			c_ayala::user_cmd->viewangles.y += 90;*/
		//		c_ayala::user_cmd->viewangles.y += c_ayala::l_player->LowerBodyYaw() + 154.f;
 	//		}
		}
		break;
		case 1:
		{
			float range = GetMaxFakeDelta();

			static bool flip;
			auto InitFlip = []() {
				static clock_t start1_t1 = clock();
				double timeSoFar1 = (double)(clock() - start1_t1) / CLOCKS_PER_SEC;
				if (timeSoFar1 < 0.05)
					return;
				flip = !flip;
				start1_t1 = clock();
			};
			InitFlip();

			bool change_angle;
			if (GetKeyState(VK_DOWN))
				change_angle = true;
			else
				change_angle = false;

			if (!c_ayala::c_SendPacket)
				c_ayala::user_cmd->viewangles.y = c_ayala::user_cmd->viewangles.y + (flip ? range : -range);
			else
			{
				if (change_angle)
					c_ayala::user_cmd->viewangles.y -= 90;
				else
					c_ayala::user_cmd->viewangles.y += 90;
			}
		}
		break;
		case 2:
		{
			float range = GetMaxFakeDelta();

			static bool flip;
			auto InitFlip = []() {
				static clock_t start1_t1 = clock();
				double timeSoFar1 = (double)(clock() - start1_t1) / CLOCKS_PER_SEC;
				if (timeSoFar1 < 0.05)
					return;
				flip = !flip;
				start1_t1 = clock();
			};
			InitFlip();




			if (c_ayala::c_SendPacket)
				c_ayala::user_cmd->viewangles.y = c_ayala::user_cmd->viewangles.y + (flip ? range : -range);
			else
				c_ayala::user_cmd->viewangles.y = GetClosetYaw();
		}
		case 3:
		{
			static float Spin;
			Spin += Menu.Antiaim.spinspeed * 10;
			c_ayala::user_cmd->viewangles.y = GetClosetYaw() + Spin;
		}
	}
	}
	else
	{
		switch (Menu.Antiaim.AntiaimMode)
		{
		case 0: {
			c_ayala::user_cmd->viewangles.y = GetClosetYaw() + Math::RandomFloat(Menu.Antiaim.Jitterrange, -Menu.Antiaim.Jitterrange);

		}
		break;
		case 1:
			c_ayala::user_cmd->viewangles.y = GetClosetYaw() + Math::RandomFloat(Menu.Antiaim.Jitterrange, -Menu.Antiaim.Jitterrange);
			break;
		case 2:
		{
			static float Spin;
			Spin += Menu.Antiaim.spinspeed * 10;
			c_ayala::user_cmd->viewangles.y = GetClosetYaw() + Spin;
		}
		}
	}
}

void CAntiaim::Run()
{
	if (Menu.Antiaim.AntiaimEnable)
	{
		static Vector last_choked_angles;
		CBasePlayerAnimState* animstate = c_ayala::l_player->GetBasePlayerAnimState();
		static int iChokedPackets = -1;
		CGrenade* pCSGrenade = (CGrenade*)c_ayala::l_player->GetWeapon();	
		bool ignoremove = false;
		DWORD ClientState = *(DWORD*)FakeLatency::ClientState;
		int32_t chocked = *(DWORD*)(ClientState + 0x4D28);
		iChocked = chocked;
		if (c_ayala::user_cmd->buttons & IN_USE
			|| c_ayala::l_player->GetMoveType() == MOVETYPE_LADDER && c_ayala::l_player->GetVelocity().Length() > 0
			|| c_ayala::l_player->GetMoveType() == MOVETYPE_NOCLIP
			|| *c_ayala::l_player->GetFlags() & FL_FROZEN
			|| pCSGrenade && pCSGrenade->GetThrowTime() > 0.f)
		{
			c_ayala::c_SendPacket = true;
			return; 
		}
		if (!Fakewalk(c_ayala::user_cmd))
		{
			c_ayala::c_SendPacket = chocked >= Menu.Misc.FakelagAmount;
			c_ayala::PredictedTime = c_ayala::l_player->GetBasePlayerAnimState()->m_fDuckAmount;
			//fake_crouch();
			//ignoremove = true;
			//UpdateLowerBodyYawTime(ignoremove);
		}
		Pitch();
		RealYaw();}
	else
		c_ayala::c_SendPacket = true;
}