﻿#include "sdk.h"
#include "global.h"
#include "Draw.h"
#include "GameUtils.h"
#include <chrono>
CDataMapUtils* g_pData = new CDataMapUtils;
CEncryption* g_pEncrypt = new CEncryption;
C_BaseEntity* c_ayala::l_player = nullptr;
CBaseCombatWeapon* c_ayala::c_weapon = nullptr;
CSWeaponInfo* c_ayala::c_weapon_data = nullptr;
c_user_cmd*	c_ayala::user_cmd = nullptr;
std::deque<std::tuple<Vector, float, Color>> c_ayala::hitscan_points;
QAngle c_ayala::StrafeAngle = QAngle();
QAngle c_ayala::RealAngle = QAngle();
bool c_ayala::c_SendPacket = true;
bool c_ayala::ShowMenu = false;
bool c_ayala::Opened = false;
bool c_ayala::NewRound = false;
bool c_ayala::ClearRoundData = false;
bool c_ayala::Init = false;
bool c_ayala::bShouldChoke = false;
bool c_ayala::Panorama = false;
bool c_ayala::ClearFeed = false;
DWORD c_ayala::CourierNew;
bool c_ayala::expired;
int c_ayala::wbpoints = -1;
int c_ayala::localtime;
int c_ayala::wbcurpoint = 0;
int c_ayala::MenuTab;
float c_ayala::PredictedTime = 0.f;
float c_ayala::viewMatrix[4][4] = { 0 };
HWND c_ayala::Window = nullptr;
CScreen c_ayala::Screen = CScreen();

Vector c_ayala::vecUnpredictedVel = Vector(0, 0, 0);
Vector c_ayala::CameraOrigin = Vector();

int CDataMapUtils::Find(datamap_t* pMap, const char* szName) {
	while (pMap)
	{
		for (int i = 0; i < pMap->dataNumFields; i++)
		{
			if (pMap->dataDesc[i].fieldName == NULL)
				continue;

			if (strcmp(szName, pMap->dataDesc[i].fieldName) == 0)
				return pMap->dataDesc[i].fieldOffset[TD_OFFSET_NORMAL];

			if (pMap->dataDesc[i].fieldType == FIELD_EMBEDDED)
			{
				if (pMap->dataDesc[i].td)
				{
					unsigned int offset;

					if ((offset = Find(pMap->dataDesc[i].td, szName)) != 0)
						return offset;
				}
			}
		}
		pMap = pMap->baseMap;
	}

	return 0;
}