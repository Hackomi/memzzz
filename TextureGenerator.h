#pragma once
#include "sdk.h"
class IPTTexture
{
protected:
	std::vector<unsigned char> Texture;
	int Height, Width, TextureId;
public:
	virtual unsigned char* GetTexture() { return Texture.data(); };
	virtual int GetHeight() { return Height; };
	virtual int GetWidth() { return Width; };
	virtual int GetTextureId() { return TextureId; };
	virtual void CreateTexture() = 0;
};

class CColorPickerTexture : public IPTTexture
{
	void CreateTexture()
	{
		TextureId = g_pSurface->CreateNewTextureID(true);
		for (int y = 0; y < Width; ++y)
		{
			for (int x = 0; x < Height; ++x)
			{
				Color color = Color::FromHSB((float)x / (float)Height, y > (float)Width / 2 ? 1 : (float)y / ((float)Width / 2), y <= (float)Width / 2 ? 1 : 1 - (((float)y - (float)Width / 2) / ((float)Width / 2)));
				color.SetAlpha(255);
				Texture.push_back(color.r());
				Texture.push_back(color.g());
				Texture.push_back(color.b());
				Texture.push_back(color.a());
			}
		}
		g_pSurface->DrawSetTextureRGBA(TextureId, Texture.data(), Height, Width);
	}
public:
	CColorPickerTexture(int h, int w)
	{
		Height = h;
		Width = w;
		CreateTexture();
	}
};