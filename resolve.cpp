#include "sdk.h"
#include "resolve.h"
#include "global.h"
#include "Math.h"
#include "autowall.h"
#include "GameUtils.h"
template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
static double GetNumberOfTicksChoked(C_BaseEntity* pEntity)
{

	double flSimulationTime = pEntity->GetSimulationTime();

	double flSimDiff = ((double)c_ayala::l_player->GetTickBase() * g_globals->interval_per_tick) - flSimulationTime;

	return TIME_TO_TICKS(max(0.0f, flSimDiff));

}
bool Resolve::lbypredict(C_BaseEntity *entity)
{	
	int Index = entity->Index();

		//if (entity->GetAnimOverlay(3)->m_flWeight < 0.01f && entity->GetAnimOverlay(3)->m_flCycle < 0.69999f) // shake detection
		//{
		//	entity->angs()->y = entity->LowerBodyYaw();
		//	return true;
		//}
	   static float NextLBYUpdate[64];
	   static float oldlby[64];
		if (entity->LowerBodyYaw() != oldlby[Index])
		{
			entity->angs()->y = Math::NormalizeYaw(entity->LowerBodyYaw());
			oldlby[Index] = entity->LowerBodyYaw();
			Add[Index] = g_globals->interval_per_tick + 1.1f;
			NextLBYUpdate[Index] = entity->GetOldSimulationTime() + Add[Index];
			return true;
		}
		else if (entity->GetOldSimulationTime() >= NextLBYUpdate[Index])
		{
			Add[Index] = 1.1f;
			NextLBYUpdate[Index] = entity->GetOldSimulationTime() + Add[Index];
			return true;
		}
	return false;
}
std::vector<float> Resolve::GetYaw(C_BaseEntity *entity)
{
	float RotateDelta = (entity->GetEyePosition() - entity->GetHedPos()).Length2D();
	float Z = entity->GetHedPos().z + 5;
	float Points[4] = { 0 };
	int mindamage = INT_MAX;
	bool bResult = false;

	bResult = true;
	for (int k = 1; k <= 15; k++)
		for (int i = 0; i < 4; i++)
		{
			Points[i] += (Autowall::Get().CalculateDamage(c_ayala::l_player->GetEyePosition(), Vector(entity->GetEyePosition().x + RotateDelta * k * cos(M_PI * 2 / 4 * i), entity->GetEyePosition().y + RotateDelta * k * sin(M_PI * 2 / 4 * i), Z), c_ayala::l_player).damage > 1) ? 1 : 0;
		}
	std::vector<float> result;
	if (bResult)
		for (int i = 0; i < 4; i++)
		{
			auto point = Points[i];
			if (point < mindamage)
			{
				result.clear();
				mindamage = point;
			}
			if (point == mindamage)
				result.push_back(GameUtils::CalculateAngle(entity->GetEyePosition(), Vector(entity->GetEyePosition().x + RotateDelta * cos(M_PI * 2 / 4 * i), entity->GetEyePosition().y + RotateDelta * sin(M_PI * 2 / 4 * i), Z)).y);
		}
	return result;
}float Resolve::GetClosetYaw(C_BaseEntity *pEntity)
{
	int Points[2] = { 0 };
	float Distance = FLT_MAX;
	Vector LocalEyePos = pEntity->GetEyePosition();
	Vector EntEyePos = c_ayala::l_player->GetEyePosition();
	float Z = pEntity->GetHedPos().z;
	float Dist = Vector(EntEyePos - c_ayala::l_player->GetHedPos()).Length2D();

	float totarget = Math::NormalizeYaw(GameUtils::CalculateAngle(LocalEyePos, EntEyePos).y);
	Vector forward, up(0, 0, Z - LocalEyePos.z + 5);

	for (int i = 1; i <= 15; i++)
	{
		Math::AngleVectors(QAngle(0, totarget + 90, 0), forward);
		Points[0] += Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, c_ayala::l_player).damage > 1 ? 1 : 0;

		Math::AngleVectors(QAngle(0, totarget - 90, 0), forward);
		Points[1] += Autowall::Get().CalculateDamage(EntEyePos, LocalEyePos + forward * Dist * i + up, c_ayala::l_player).damage > 1 ? 1 : 0;
	}

	if (Points[0] > Points[1])
		return Math::NormalizeYaw(totarget - 90);
	else if (Points[0] < Points[1])
		return Math::NormalizeYaw(totarget + 90);
	else if (Points[0] == 0)
		return Math::NormalizeYaw(totarget + 180);
	else return Math::NormalizeYaw(totarget + 180);
}
float Resolve::AntiFreestand(C_BaseEntity* entity)
{
	auto BestTarget = c_ayala::l_player;
	Vector src3D, dst3D, forward, right, up, src, dst;
	float back_two, right_two, left_two;
	trace_t tr;
	Ray_t ray, ray2, ray3, ray4, ray5;
	CTraceFilter filter;

	const Vector to_convert = entity->GetEyeAngles();
	Math::AngleVectors(to_convert, &forward, &right, &up);

	filter.pSkip1 = entity;
	src3D = entity->GetEyePosition();
	dst3D = src3D + (forward * 369); //Might want to experiment with other numbers, incase you don't know what the number does, its how far the trace will go. Lower = shorter.

	ray.Init(src3D, dst3D);
	g_pEngineTrace->TraceRay(ray, MASK_SHOT, &filter, &tr);
	back_two = (tr.endpos - tr.start).Length();

	ray2.Init(src3D + right * 35, dst3D + right * 35);
	g_pEngineTrace->TraceRay(ray2, MASK_SHOT, &filter, &tr);
	right_two = (tr.endpos - tr.start).Length();

	ray3.Init(src3D - right * 35, dst3D - right * 35);
	g_pEngineTrace->TraceRay(ray3, MASK_SHOT, &filter, &tr);
	left_two = (tr.endpos - tr.start).Length();
	if (left_two > right_two)
	{
		return - 90;
	}
	else if (right_two > left_two)
	{
		return + 90;
	}
	else
	{
		return - 180;
	}
}

RecordTick_t Resolve::Resolver(RecordTick_t tick)
{
	return false;
	if (tick.nTickFlags & TICK_Resolved)
		return tick;

	RecordTick_t ResolvedTick;
	ResolvedTick = tick;
	int Index = tick.iIndex;

	C_BaseEntity* TickEntity = g_pEntitylist->GetClientEntity(Index);
	
	if (!TickEntity
		|| !TickEntity->isAlive()
		|| !TickEntity->IsPlayer())
		return tick;

	if (TickEntity->IsDormant())
		ChengeLBYAfterDormant[Index] = false;
	else
		ChengeLBYAfterDormant[Index] = true;

	/*if (tick.nTickFlags & TICK_Normal)
		NormalTicks[Index]++;
	else
		NormalTicks[Index] = 0;

	if (NormalTicks[Index] > 15)
	{
		tick.nTickFlags &= TICK_Resolved;
		return;
	}*/

	float LowerBodyYaw = TickEntity->LowerBodyYaw();
	CBasePlayerAnimState* AnimState = TickEntity->GetBasePlayerAnimState();
	if (*TickEntity->GetFlags() & FL_ONGROUND)
	{
		if (!Resolve::Get().fakewalking(TickEntity))
		{
			lastlbymove[Index] = LowerBodyYaw;
			ResolvedTick.angAbsAngle.y = LowerBodyYaw;
			ResolvedTick.angEyeAngle.y = LowerBodyYaw;
			ResolvedTick.nTickFlags &= TICK_Resolved;
			return ResolvedTick;
		}
		else
		{
			float BestYaw = GetClosetYaw(TickEntity);
			if (Resolve::Get().lbypredict(TickEntity))
			{
				ResolvedTick.angAbsAngle.y = Math::NormalizeYaw(LowerBodyYaw);
				ResolvedTick.angEyeAngle.y = Math::NormalizeYaw(LowerBodyYaw);
			}
			else
			{
				ResolvedTick.angAbsAngle.y = Math::NormalizeYaw(BestYaw);
				ResolvedTick.angEyeAngle.y = Math::NormalizeYaw(BestYaw);
			}
		}
	}
	else
	{
		ResolvedTick.angAbsAngle.y = Math::NormalizeYaw(TickEntity->GetBasePlayerAnimState()->m_flGoalFeetYaw);
		ResolvedTick.angEyeAngle.y = Math::NormalizeYaw(TickEntity->GetBasePlayerAnimState()->m_flGoalFeetYaw);
	}
	return ResolvedTick;
}

bool Resolve::fakewalking(C_BaseEntity *entity)
{
	CBasePlayerAnimState *animations = entity->GetBasePlayerAnimState();
	if (entity->GetAnimOverlay(6)->m_flPlaybackRate == 0)
		return true;
	return false;
}