#include "animation_sync.h"
#include "sdk.h"
#include "Math.h"
#include "GameUtils.h"
#include "simple_auto_wall.h"
#include "simplehpb_auto_wall.h"
#include "Autowall.h"
#include "auto_wall.h"

void animation_sync::anim_sync(C_BaseEntity * entity)
{
	if (!c_ayala::l_player
		|| !c_ayala::l_player->GetHealth()
		|| !entity)
		return;

	/*if (entity != c_ayala::l_player)
	{
		*(int*)((uintptr_t)entity + 240) |= 8;
		*(int*)((uintptr_t)entity + 0x274) = 0;
	}*/
	static float oldSimtime[65];
	static float storedSimtime[65];
	static float ShotTime[65];
	static float SideTime[65][3];
	static int LastDesyncSide[65];
	static bool Delaying[65];
	static AnimationLayer StoredLayers[64][15];
	static float StoredPosParams[65][24];
	static Vector oldEyeAngles[65];
	static float oldGoalfeetYaw[65];
	bool update = false;
	bool shot = false;
	matrix3x4_t Matrix[128];
	//entity->HandleBoneSetup(entity, 0x0007FF00, Matrix);
	static bool jittering[65];
	auto animations = entity->GetBasePlayerAnimState();
	auto RemapVal = [](float val, float A, float B, float C, float D) -> float
	{
		if (A == B)
			return val >= B ? D : C;
		return C + (D - C) * (val - A) / (B - A);
	};
	float* PosParams = (float*)((uintptr_t)entity + 0x2774);

	if (storedSimtime[entity->Index()] != entity->GetSimulationTime())
	{
		jittering[entity->Index()] = false;


		if (entity->GetWeapon() /*&& !entity->isk()*/)
		{
			if (ShotTime[entity->Index()] != entity->GetWeapon()->m_fLastShotTime)
			{
				shot = true;
				ShotTime[entity->Index()] = entity->GetWeapon()->m_fLastShotTime;
			}
			else
				shot = false;
		}
		else
		{
			shot = false;
			ShotTime[entity->Index()] = 0.f;
		}

		float angToLocal = Math::NormalizeYaw(GameUtils::CalculateAngle(c_ayala::l_player->GetOrigin(), entity->GetOrigin()).y);

		float Back = Math::NormalizeYaw(angToLocal);
		float DesyncFix = 0;
		float Resim = Math::NormalizeYaw((0.24f / (entity->GetSimulationTime() - oldSimtime[entity->Index()]))*(oldEyeAngles[entity->Index()].y - entity->GetEyeAngles().y));

		if (Resim > 58.f)
			Resim = 58.f;
		if (Resim < -58.f)
			Resim = -58.f;

		if (entity->GetVelocity().Length2D() > 0.5f && !shot)
		{
			float Delta = Math::NormalizeYaw(Math::NormalizeYaw(GameUtils::CalculateAngle(Vector(0, 0, 0), entity->GetVelocity()).y) - Math::NormalizeYaw(Math::NormalizeYaw(animations->m_flGoalFeetYaw + RemapVal(PosParams[11], 0, 1, -60, 60)) + Resim));

			int CurrentSide = 0;

			if (Delta < 0)
			{
				CurrentSide = 1;
				SideTime[entity->Index()][1] = g_globals->curtime;
			}
			else if (Delta > 0)
			{
				CurrentSide = 2;
				SideTime[entity->Index()][2] = g_globals->curtime;
			}

			if (LastDesyncSide[entity->Index()] == 1)
			{
				Resim += (58.f - Resim);
				DesyncFix += (58.f - Resim);
			}
			if (LastDesyncSide[entity->Index()] == 2)
			{
				Resim += (-58.f - Resim);
				DesyncFix += (-58.f - Resim);
			}

			if (LastDesyncSide[entity->Index()] != CurrentSide)
			{
				Delaying[entity->Index()] = true;

				if (.5f < (g_globals->curtime - SideTime[entity->Index()][LastDesyncSide[entity->Index()]]))
				{
					LastDesyncSide[entity->Index()] = CurrentSide;
					Delaying[entity->Index()] = false;
				}
			}

			if (!Delaying[entity->Index()])
				LastDesyncSide[entity->Index()] = CurrentSide;
		}
		else if (!shot)
		{
			float Brute = UseFreestandAngle[entity->Index()] ? Math::NormalizeYaw(Back + FreestandAngle[entity->Index()]) : entity->LowerBodyYaw();

			float Delta = Math::NormalizeYaw(Math::NormalizeYaw(Brute - Math::NormalizeYaw(Math::NormalizeYaw(animations->m_flGoalFeetYaw + RemapVal(PosParams[11], 0, 1, -60, 60))) + Resim));

			if (Delta > 58.f)
				Delta = 58.f;
			if (Delta < -58.f)
				Delta = -58.f;

			Resim += Delta;
			DesyncFix += Delta;

			if (Resim > 58.f)
				Resim = 58.f;
			if (Resim < -58.f)
				Resim = -58.f;
		}

		float Equalized = Math::NormalizeYaw(Math::NormalizeYaw(animations->m_flGoalFeetYaw + RemapVal(PosParams[11], 0, 1, -60, 60)) + Resim);

		float JitterDelta = fabs(Math::NormalizeYaw(oldEyeAngles[entity->Index()].y - entity->GetEyeAngles().y));

		if (JitterDelta >= 70.f && !shot)
			jittering[entity->Index()] = true;

		if (entity != c_ayala::l_player && entity->GetTeamNum() != c_ayala::l_player->GetTeamNum() && (*entity->GetFlags() & FL_ONGROUND) /*&&*/)
		{
			if (jittering[entity->Index()])
				animations->m_flGoalFeetYaw = Math::NormalizeYaw(entity->GetEyeAngles().y + DesyncFix);
			else
				animations->m_flGoalFeetYaw = Equalized;

			entity->SetLowerBodyYaw(animations->m_flGoalFeetYaw);
		}

		oldEyeAngles[entity->Index()] = entity->GetEyeAngles();

		oldSimtime[entity->Index()] = storedSimtime[entity->Index()];

		storedSimtime[entity->Index()] = entity->GetSimulationTime();

		update = true;
	}
	clientanimating_t *animating = nullptr;
	int animflags;

	for (unsigned int i = 0; i < g_ClientSideAnimationList->count; i++)
	{
		clientanimating_t *tanimating = (clientanimating_t*)g_ClientSideAnimationList->Retrieve(i, sizeof(clientanimating_t));
		C_BaseEntity *pAnimEntity = (C_BaseEntity*)tanimating->pAnimating;
		if (pAnimEntity == entity)
		{
			animating = tanimating;
			animflags = tanimating->flags;
			tanimating->flags |= FCLIENTANIM_SEQUENCE_CYCLE;
			break;
		}
	}
	float OldInterpAmount = g_globals->interpolation_amount;
	float flPoseParameter[24];
	CAnimationLayer backup_layers[15];
	std::memcpy(backup_layers, entity->GetAnimOverlays(), (sizeof(CAnimationLayer) * 15));
	auto old_curtime = g_globals->curtime;
	auto old_frametime = g_globals->frametime;
	auto old_m_vVelocityX = animations->m_vVelocityX;
	auto old_m_vVelocityY = animations->m_vVelocityY;
	auto old_m_flFeetSpeedUnknownForwardOrSideways = animations->m_flFeetSpeedUnknownForwardOrSideways;
	auto old_m_flFeetSpeedForwardsOrSideWays = animations->m_flFeetSpeedForwardsOrSideWays;
	auto old_GetAbsAngles = entity->GetAbsAngles();
	auto old_GetVelocity = entity->GetVelocity();
	auto old_GetFlags = entity->GetFlags();
	auto old_GetAbsOrigin = entity->GetAbsOrigin();
	auto old_fraction = animations->m_flUnknownFraction;
	auto old_ragpos = entity->get_ragdoll_pos();
	g_globals->curtime = entity->GetOldSimulationTime() + g_globals->interval_per_tick;
	g_globals->frametime = g_globals->interval_per_tick;


	if (animations)
		animations->m_iLastClientSideAnimationUpdateFramecount = g_globals->framecount - 1;

	*entity->GetClientSideAnimation() = true;
	entity->UpdateClientSideAnimation();
	*entity->GetClientSideAnimation() = false;

	std::memcpy(entity->GetAnimOverlays(), backup_layers, (sizeof(CAnimationLayer) * 15));
	g_globals->curtime = old_curtime;
	g_globals->frametime = old_frametime;
	if (entity != c_ayala::l_player) animations->m_flUnknownFraction = old_fraction = 0.f;
	if (animating) animating->flags = animflags;
	entity->get_ragdoll_pos() = old_ragpos;
	//leets(entity);
	if (entity != c_ayala::l_player && entity->GetTeamNum() != c_ayala::l_player->GetTeamNum() && (*entity->GetFlags() & FL_ONGROUND) /*&& g_Menu.Config.Resolver*/)
		entity->SetLowerBodyYaw(animations->m_flGoalFeetYaw);
	//animations = StoredAnimState[entity->Index()];
	if (entity == c_ayala::l_player) // vezual effet da
	{
		entity->SetAbsAngle(Vector(0.f, animations->m_flGoalFeetYaw, 0.f));
		entity->SetLowerBodyYaw(animations->m_flGoalFeetYaw);
	}
}

void animation_sync::creat_move_part() 
{
	if (!c_ayala::l_player->isAlive())
		return;

	if (!c_ayala::l_player->GetWeapon()/* || c_ayala::l_player->IsKnifeorNade()*/)
		return;


	for (int i = 0; i < g_pEngine->Getmax_�lients(); ++i)
	{
		C_BaseEntity* pPlayerEntity = g_pEntitylist->GetClientEntity(i);

		if (!pPlayerEntity
			|| !pPlayerEntity->isAlive()
			|| pPlayerEntity == c_ayala::l_player
			|| pPlayerEntity->GetTeamNum() == c_ayala::l_player->GetTeamNum())
		{
			UseFreestandAngle[i] = false;
			continue;
		}

		if (abs(pPlayerEntity->GetVelocity().Length2D()) > 29.f)
			UseFreestandAngle[pPlayerEntity->Index()] = false;

		if (abs(pPlayerEntity->GetVelocity().Length2D()) <= 29.f && !UseFreestandAngle[pPlayerEntity->Index()])
		{
			bool Autowalled = false, HitSide1 = false, HitSide2 = false;

			float angToLocal = GameUtils::CalculateAngle(c_ayala::l_player->GetOrigin(), pPlayerEntity->GetOrigin()).y;
			Vector ViewPoint = c_ayala::l_player->GetOrigin() + Vector(0, 0, 90);

			Vector2D Side1 = { (45 * sin(Math::GRD_TO_BOG(angToLocal))),(45 * cos(Math::GRD_TO_BOG(angToLocal))) };
			Vector2D Side2 = { (45 * sin(Math::GRD_TO_BOG(angToLocal + 180))) ,(45 * cos(Math::GRD_TO_BOG(angToLocal + 180))) };

			Vector2D Side3 = { (50 * sin(Math::GRD_TO_BOG(angToLocal))),(50 * cos(Math::GRD_TO_BOG(angToLocal))) };
			Vector2D Side4 = { (50 * sin(Math::GRD_TO_BOG(angToLocal + 180))) ,(50 * cos(Math::GRD_TO_BOG(angToLocal + 180))) };

			Vector Origin = pPlayerEntity->GetOrigin();

			Vector2D OriginLeftRight[] = { Vector2D(Side1.x, Side1.y), Vector2D(Side2.x, Side2.y) };

			Vector2D OriginLeftRightLocal[] = { Vector2D(Side3.x, Side3.y), Vector2D(Side4.x, Side4.y) };

			for (int side = 0; side < 2; side++)
			{
				Vector OriginAutowall = { Origin.x + OriginLeftRight[side].x,  Origin.y - OriginLeftRight[side].y , Origin.z + 90 };
				Vector OriginAutowall2 = { ViewPoint.x + OriginLeftRightLocal[side].x,  ViewPoint.y - OriginLeftRightLocal[side].y , ViewPoint.z };

				if (c_autowall.CanHitFloatingPoint(OriginAutowall, ViewPoint))
				{
					if (side == 0)
					{
						HitSide1 = true;
						FreestandAngle[pPlayerEntity->Index()] = 90;
					}
					else if (side == 1)
					{
						HitSide2 = true;
						FreestandAngle[pPlayerEntity->Index()] = -90;
					}

					Autowalled = true;
				}
				else
				{
					for (int side222 = 0; side222 < 2; side222++)
					{
						Vector OriginAutowall222 = { Origin.x + OriginLeftRight[side222].x,  Origin.y - OriginLeftRight[side222].y , Origin.z + 90 };

						if (c_autowall.CanHitFloatingPoint(OriginAutowall222, OriginAutowall2))
						{
							if (side222 == 0)
							{
								HitSide1 = true;
								FreestandAngle[pPlayerEntity->Index()] = 90;
							}
							else if (side222 == 1)
							{
								HitSide2 = true;
								FreestandAngle[pPlayerEntity->Index()] = -90;
							}

							Autowalled = true;
						}
					}
				}
			}

			if (Autowalled)
			{
				if (HitSide1 && HitSide2)
					UseFreestandAngle[pPlayerEntity->Index()] = false;
				else
					UseFreestandAngle[pPlayerEntity->Index()] = true;
			}
		}
	}
}

void animation_sync::call_anim_sync()
{
	//return;
	for (int i = 1; i < g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (pEntity && !pEntity->IsDormant()/*&& Menu.Ragebot.AutomaticFire*/)
		{
			anim_sync(pEntity);
		}
	}
}
void animation_sync::third_person()
{
	static bool enabledtp = false, check = false;
	if (GetAsyncKeyState(Menu.Misc.TPKey))
	{
		if (!check)
			enabledtp = !enabledtp;
		check = true;
	}
	else
		check = false;
	if (enabledtp)
	{
		c_ayala::l_player->SetVisualEyeAngles(c_ayala::RealAngle);
	}
	if (enabledtp)
	{
		g_pInput->m_fCameraInThirdPerson = true;
	}
	else
	{
		g_pInput->m_fCameraInThirdPerson = false;
	}
}
float animation_sync::setupvelocity_rebuild(C_BaseEntity * entity)
{
	CBasePlayerAnimState *v3 = entity->GetBasePlayerAnimState();
	auto feet = -360.0;
	*reinterpret_cast<float*>((DWORD)v3 + 116 + 0x10) = *reinterpret_cast<float*>((DWORD)v3 + 112 + 0x10);
	auto v47 = *reinterpret_cast<float*>((DWORD)v3 + 112 + 0x10);
	auto v146 = -360.0;
	if (v47 >= -360.0)
	{
		feet = fminf(v47, 360.0);
		v146 = feet;
	}
	auto v48 = *reinterpret_cast<float*>((DWORD)v3 + 104 + 0x10) - feet;
	*reinterpret_cast<float*>((DWORD)v3 + 112 + 0x10) = feet;
	auto yaw = *reinterpret_cast<float*>((DWORD)v3 + 104 + 0x10);
	auto v155 = fmod(v48, 360.0);
	auto yaw_feet_delta = v155;
	if (yaw <= v146)
	{
		if (v155 <= -180.0)
			yaw_feet_delta = v155 + 360.0;
	}
	else if (v155 >= 180.0)
	{
		yaw_feet_delta = v155 - 360.0;
	}
	//auto speed_fraction = v3->m_flSpeedFraction*;
	auto speed_fraction_clamped = 0.f;
	auto duck_amount = 0.f;
	auto v56 = 0.f;
	//if (speed_fraction >= 0.0)
	//	speed_fraction_clamped = fminf(speed_fraction, 1.0);
	//else
	//	speed_fraction_clamped = 0.0;
	//duck_amount = v3->m_fDuckAmount;
	auto v54 = (float)((float)((float)(*reinterpret_cast<float*>((DWORD)v3 + 0x114 + 0x10) * -0.30000001) - 0.19999999) * speed_fraction_clamped) + 1.0;
	if (duck_amount > 0.0)
	{
		auto v55 = *reinterpret_cast<float*>((DWORD)v3 + 236 + 0x10);
		if (v55 >= 0.0)
			v56 = fminf(v55, 1.0);
		else
			v56 = 0.0;
		v54 = v54 + (float)((float)(v56 * duck_amount) * (float)(0.5 - v54));
	}
	auto max_rotation = *(float *)(v3 + 0x2B4 + 0x10) * v54;
	auto inverted_max_rotation = *(float *)(v3 + 0x2B0 + 0x10) * v54;
	if (yaw_feet_delta <= max_rotation)
	{
		if (inverted_max_rotation > yaw_feet_delta)
			*(float *)(v3 + 0x70 + 0x10) = inverted_max_rotation + yaw;
	}
	else
	{
		*(float *)(v3 + 0x70 + 0x10) = yaw - max_rotation;
	}
	auto goal_feet_yaw = fmod(*reinterpret_cast<float*>((DWORD)v3 + 0x70 + 0x10), 360.0);
	auto goal_feet_yaw_clamped = goal_feet_yaw;
	if (goal_feet_yaw > 180.0)
		goal_feet_yaw_clamped = goal_feet_yaw - 360.0;
	if (goal_feet_yaw_clamped < -180.0)
		goal_feet_yaw_clamped = goal_feet_yaw_clamped + 360.0;
	auto v60 = *reinterpret_cast<float*>((DWORD)v3 + 220 + 0x10);
	return *reinterpret_cast<float*>((DWORD)v3 + 112 + 0x10) = goal_feet_yaw_clamped;
}
void animation_sync::disable_interoplation()
{
	for (int i = 1; i < g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (pEntity)
		{
			if (pEntity->GetHealth() > 0)
			{
				if (i != g_pEngine->Getl_player())
				{
					VarMapping_t* map = pEntity->GetVarMap();
					if (map)
					{
						if (Menu.Ragebot.PositionAdjustment)
						{
							map->m_nInterpolatedEntries = 0;
						}
						else
						{
							if (map->m_nInterpolatedEntries == 0)
								map->m_nInterpolatedEntries = 6;
						}
					}
				}
			}
		}
	}
}
