#include "imvisuals.h"
#include "GameUtils.h"
#include "Math.h"
#include "AnimationEvents.h"
#include "AW_hitmarker.h"
#include "bullet_log.h"
#include "antim-aims.h"
template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
void CImVisuals::Run()
{
	while (pDrawList == nullptr)
		pDrawList = ImGui::GetWindowDrawList();
	

	C_BaseEntity* pLocal = c_ayala::l_player;
	DrawCustomUI();

	CAnimationEventManager::Get().AnimationLoop();

	if (!Menu.Visuals.EspEnable ||
		/*c_ayala::DoScreenshot ||
		g_pEngine->IsDrawingLoadingImage() ||*/
		!g_pEngine->IsInGame() ||
		!g_pEngine->IsConnected() || !c_ayala::l_player->isAlive()||
		pLocal == nullptr)
	{
		if (!bDataClear)
		{
			vecDormantRestore.clear();
			vecOffScreenData.clear();
			vecImpactsData.clear();

			for (int i = 1; i <= g_globals->max_�lients; i++)
				drawData[i].Clear();

			bDataClear = true;
		}

		return;
	}

	SetupExtendedESP();

	for (int i = 1; i <= g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);

		if (pEntity == nullptr || !pEntity->isAlive() || !pEntity->IsPlayer() || /*pLocal->GetObserverTargetHandle() == pEntity ||*/ pEntity->GetTeamNum() == pLocal->GetTeamNum() /*|| !pEntity->IsValid()*/)
			continue;
		UpdateDormantInfo(pEntity);
		
		if (!pEntity->IsDormant())
		{
			if (Menu.Visuals.BoundingBox)
				DrawCornersBox(pEntity);
			if (Menu.Visuals.Health)
				DrawHealthBar(pEntity, true);
			DrawTextInfo(pEntity);

			if (pEntity != pLocal)
				CollectOffScreenData(pEntity);
		}
	}

	DrawHitTracers(false);
	if (Menu.Visuals.OutOfPOVArrows)
	DrawOffScreen();

	DestroyExtendedESP();
}

void CImVisuals::Event(IGameEvent* pEvent)
{
	if (pEvent == nullptr)
		return;

	if (!g_pEngine->IsInGame() || !g_pEngine->IsConnected())
		return;

	if (!strcmp(pEvent->GetName(), XorStr("bullet_impact")))
	{
		BulletImpact_t impact;
		impact.vecImpact = Vector(pEvent->GetFloat(XorStr("x")), pEvent->GetFloat(XorStr("y")), pEvent->GetFloat(XorStr("z")));
		impact.nIndex = g_pEngine->GetPlayerForUserID(pEvent->GetInt(XorStr("userid")));
		impact.bWorldImpact = false/*!U::CrossSpaceCheck(impact.vecImpact, 1, nullptr, true)*/;
		vecImpactsData.push_back(impact);
	}
}

void CImVisuals::DrawHitTracers(bool bWorldImpacts)
{
	for (auto impact : vecImpactsData)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(impact.nIndex);

		if (!pEntity->IsValid())
			continue;

		/*if (!impact.bWorldImpact || (impact.bWorldImpact && bWorldImpacts))
		{
			BeamInfo_t beamInfo;
			beamInfo.iType = TE_BEAMPOINTS;
			beamInfo.pszModelName = XorStr("sprites/purplelaser1.vmt");
			beamInfo.iModelIndex = -1;
			beamInfo.flHaloScale = 0.0f;
			beamInfo.flLife = 5;
			beamInfo.flWidth = 2.0f;
			beamInfo.flEndWidth = 2.0f;
			beamInfo.flFadeLength = 0.0f;
			beamInfo.flAmplitude = 2.0f;
			beamInfo.flBrightness = 255.f;
			beamInfo.flSpeed = 0.1f;
			beamInfo.iStartFrame = 0;
			beamInfo.flFrameRate = 0.f;
			beamInfo.flRed = impact.bWorldImpact ? 255 : 0;
			beamInfo.flGreen = impact.bWorldImpact ? 255 : 150;
			beamInfo.flBlue = impact.bWorldImpact ? 255 : 50;
			beamInfo.nSegments = 2;
			beamInfo.bRenderable = true;
			beamInfo.iFlags = FBEAM_ONLYNOISEONCE | FBEAM_NOTILE | FBEAM_HALOBEAM;
			beamInfo.vecStart = pEntity->GetEyePosition();
			beamInfo.vecEnd = impact.vecImpact;

			Beam_t* pBeam = I::ViewRenderBeams->CreateBeamPoints(beamInfo);

			if (pBeam != nullptr)
				I::ViewRenderBeams->DrawBeam(pBeam);
		}*/
	}

	vecImpactsData.clear();
}

void CImVisuals::DrawImpacts(BulletImpact_t impact, bool bWorldImpacts)
{
	Vector vecImpactOrigin;
	/*if (!impact.bWorldImpact || (impact.bWorldImpact && bWorldImpacts) && D::WorldToScreenDX9(impact.vecImpact, vecImpactOrigin))
	{

	}*/
}

void CImVisuals::CollectOffScreenData(C_BaseEntity* pEntity)
{
	QAngle angView;
	g_pEngine->GetViewAngles(angView);

	int iWidth, iHeight;
	g_pEngine->GetScreenSize(iWidth, iHeight);

	float flAngle = Math::NormalizeYaw((angView - GameUtils::CalculateAngle(c_ayala::CameraOrigin, pEntity->GetEyePosition())).y - 90.f);
	if (flAngle > -45.f || flAngle < -135.f)
	{
		OffScreenData_t data;
		data.flDistance = fabsf((pEntity->GetAbsOrigin() - c_ayala::CameraOrigin).Length2D());
		data.flRadius = DEG2RAD(flAngle);

		while (data.flRadius < -M_PI)
			data.flRadius += M_PI * 2;
		while (data.flRadius > M_PI)
			data.flRadius -= M_PI * 2;

		vecOffScreenData.push_back(data);
	}
}

void CImVisuals::DrawOffScreen()
{
	float flRadius = 200;
	float flRealRadius = 420;
	float flRadiusCoefficient = flRadius / flRealRadius;

	int iWidth, iHeight;
	g_pEngine->GetScreenSize(iWidth, iHeight);

	// Arrows
	for (int i = 0; i < vecOffScreenData.size(); i++)
	{
		ImVec2 vecBuffer;
		OffScreenData_t data = vecOffScreenData[i];

		int iAlpha = 255 / (data.flDistance / 400);
		iAlpha = clamp<int>(iAlpha, 0, 255);

		vecBuffer.x = flRealRadius * cosf(data.flRadius - DEG2RAD(0.2f * flRadiusCoefficient)) + iWidth / 2;
		vecBuffer.y = flRealRadius * sinf(data.flRadius - DEG2RAD(0.2f * flRadiusCoefficient)) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		vecBuffer.x = (flRealRadius - 10.f) * cosf(data.flRadius - DEG2RAD(4.f * flRadiusCoefficient)) + iWidth / 2;
		vecBuffer.y = (flRealRadius - 10.f) * sinf(data.flRadius - DEG2RAD(4.f * flRadiusCoefficient)) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		for (float k = data.flRadius + M_PI; k >= data.flRadius + M_PI - M_HPI; k -= M_PI / 180.f)
		{
			vecBuffer.x = (flRealRadius + 8.f) * cosf(data.flRadius - DEG2RAD(3.f * flRadiusCoefficient)) + 5 * cosf(k) + iWidth / 2;
			vecBuffer.y = (flRealRadius + 8.f)* sinf(data.flRadius - DEG2RAD(3.f * flRadiusCoefficient)) + 5 * sinf(k) + iHeight / 2;
			pDrawList->PathLineTo(vecBuffer);
		}

		vecBuffer.x = (flRealRadius + 50.f) * cosf(data.flRadius) + iWidth / 2;
		vecBuffer.y = (flRealRadius + 50.f) * sinf(data.flRadius) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		pDrawList->PathFillConvex(ImColor(255, 0, 0, iAlpha));

		vecBuffer.x = flRealRadius * cosf(data.flRadius + DEG2RAD(0.2f * flRadiusCoefficient)) + iWidth / 2;
		vecBuffer.y = flRealRadius * sinf(data.flRadius + DEG2RAD(0.2f * flRadiusCoefficient)) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		vecBuffer.x = (flRealRadius - 10.f) * cosf(data.flRadius + DEG2RAD(4.f * flRadiusCoefficient)) + iWidth / 2;
		vecBuffer.y = (flRealRadius - 10.f) * sinf(data.flRadius + DEG2RAD(4.f * flRadiusCoefficient)) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		for (float k = data.flRadius + M_PI; k <= data.flRadius + M_PI + M_HPI; k += M_PI / 180)
		{
			vecBuffer.x = (flRealRadius + 8.f) * cosf(data.flRadius + DEG2RAD(3.f * flRadiusCoefficient)) + 5.f * cosf(k) + iWidth / 2;
			vecBuffer.y = (flRealRadius + 8.f) * sinf(data.flRadius + DEG2RAD(3.f * flRadiusCoefficient)) + 5.f * sinf(k) + iHeight / 2;
			pDrawList->PathLineTo(vecBuffer);
		}

		vecBuffer.x = (flRealRadius + 50.f) * cosf(data.flRadius) + iWidth / 2;
		vecBuffer.y = (flRealRadius + 50.f) * sinf(data.flRadius) + iHeight / 2;
		pDrawList->PathLineTo(vecBuffer);

		pDrawList->PathFillConvex(ImColor(255, 0, 0, iAlpha));
	}

	/*auto bSort = [](OffScreenData_t a, OffScreenData_t b) { return a.flRadius < b.flRadius; };
	std::sort(vecOffScreenData.begin(), vecOffScreenData.end(), bSort);
	ImVector<ImDrawVert> vecVertData;
	ImDrawVert imVert;*/

	// Fade
	/*for (float a = -M_2PI; a <= M_2PI; a += M_2PI / 360.f)
	{
		imVert.pos.x = (flRealRadius - 20.f) * cosf(a) + iWidth / 2;
		imVert.pos.y = (flRealRadius - 20.f) * sinf(a) + iHeight / 2;
		if (!vecOffScreenData.empty())
		{
			int iThickness = 30 / (vecOffScreenData[0].flDistance / 100);
			if (a + iThickness * (M_2PI / 360.f) > vecOffScreenData[0].flRadius)
			{
				float flDelta = fabsf(vecOffScreenData[0].flRadius - a);
				int iAlpha = 255;
				iAlpha = clamp(iAlpha, 0, 255);

				iAlpha *= 1 - (flDelta / (iThickness * (M_2PI / 360.f)));
				imVert.col = ImColor(255, 0, 0, iAlpha);
				if (a > vecOffScreenData[0].flRadius + iThickness * (M_2PI / 360.f) || (vecOffScreenData.size() > 1 && flDelta > fabsf(vecOffScreenData[1].flRadius - a) - M_2PI / 360.f))
					vecOffScreenData.erase(vecOffScreenData.begin());
			}
			else
				imVert.col = ImColor(0, 100, 255, 25);
		}
		else
			imVert.col = ImColor(0, 100, 255, 25);

		vecVertData.push_back(imVert);

		imVert.pos.x = (flRealRadius - 32.f) * cosf(a) + iWidth / 2;
		imVert.pos.y = (flRealRadius - 32.f) * sinf(a) + iHeight / 2;
		imVert.col = ImColor(0, 0, 0, 10);
		vecVertData.push_back(imVert);
	}*/

	//pDrawList->AddCustomPolyFilled(vecVertData.Data, vecVertData.Size);
	vecOffScreenData.clear();
}

void CImVisuals::DrawTextInfo(C_BaseEntity* pEntity) //Entity already checked
{
	RECT box = GetBox(pEntity);
	DrawData_t& playerData = drawData[pEntity->GetIndex()];
	float flTextOffset[4][2] = { 0 };

	float flDistance = fabsf((pEntity->GetAbsOrigin() - c_ayala::CameraOrigin).Length2D());

	playerData.flTextSize = 100 / (flDistance / 100);

	DrawData_t::DrawInfo_t info;

	//info.flTextSize = playerData.flTextSize;
	info.flTextSize = Menu.Visuals.VisualsSize;
	info.iAlpha = playerData.iAlpha;
	if (Menu.Visuals.Health)
	{
		info.iType = info.BAR;
		info.BarData.flValue = playerData.flDrawHealth;
		info.BarData.flMaxValue = 100;
		info.BarData.imColor = ImColor(int(255 - (playerData.flDrawHealth * 2.55f)), int(playerData.flDrawHealth * 2.55f), 50);
		info.iPosition = info.LEFT;
		info.BarData.bInverse = true;
		playerData.vecDrawData.push_back(info);
	}
	CBaseCombatWeapon* pWeapon = pEntity->GetWeapon();
	
	if (pWeapon && Menu.Visuals.Ammo) 
	{
		auto animLayer = pEntity->GetAnimOverlay2(1);
		if (animLayer.m_pOwner)
		{
			auto activity = pEntity->GetSequenceActivity(animLayer.m_nSequence);

			info.iType = info.BAR;
			info.BarData.szName = "AMMO:";
			info.BarData.flValue = pWeapon->Clip1();
			info.BarData.flMaxValue = pWeapon->GetCSWpnData()->max_clip;
			info.BarData.imColor = ImColor(0, 150, 255);
			info.iPosition = info.BOTTOM;
			info.BarData.bInverse = false;
			if (activity == 967 && animLayer.m_flWeight != 0.f)
			{
				info.BarData.flValue = animLayer.m_flCycle;
				info.BarData.flMaxValue = 1.f;
			}
			playerData.vecDrawData.push_back(info);
		}
	}
	if (Menu.Visuals.Name)
	{
	info.imColor = ImColor(255, 255, 255);
	info.iType = info.TEXT;
	info.szText = pEntity->GetName();
	info.iPosition = info.TOP;
	playerData.vecDrawData.push_back(info);
	}
	if (Menu.Visuals.Scoped)
	{
	info.imColor = ImColor(255, 255, 255);
	info.iType = info.TEXT;
	info.szText = "ZOOM";
	info.iPosition = info.RIGHT;
	if (pEntity->IsScoped())
		playerData.vecDrawData.push_back(info);
	}
	if (Menu.Visuals.Flags)
	{
	std::stringstream ss;
	info.imColor = ImColor(255, 0, 0);
	if (pEntity->HasHelmet())
		ss << "H";
	if (pEntity->GetArmor() > 0)
		ss << "K";
	info.szText = ss.str();
	info.iPosition = info.RIGHT;
	if (ss.str().size() > 0)
		playerData.vecDrawData.push_back(info);
	}
	/*if (Menu.Visuals.Monitor)
	{
		info.szText = "chokedTicks:" + std::to_string(GetChokedPackets1(pEntity));
		info.iPosition = info.RIGHT;
		info.imColor = ImColor(0, 255, 0);
		playerData.vecDrawData.push_back(info);
	}*/



	//info.szText = "Flashed";
	//info.imColor = ImColor(255, 255, 0);
	/*if (pEntity->IsFlashed())
		playerData.vecTextData.push_back(info);*/

	for (auto textData : playerData.vecDrawData)
	{
		if (textData.iType == DrawData_t::DrawInfo_t::TEXT)
		{
			textData.imColor.Value.w = playerData.iAlpha;
			ImVec2 vecTextSize = ImGui::GetFont()->CalcTextSizeA(textData.flTextSize, FLT_MAX, 0, textData.szText.c_str());
			ImVec2 vecBoxSize = ImVec2(vecTextSize.x + 6, vecTextSize.y + 6);
			switch (textData.iPosition)
			{
			case DrawData_t::DrawInfo_t::TOP:
				DrawTextInfo(textData, ImVec2((box.left + box.right) / 2 - vecBoxSize.x / 2 + 3, box.top - vecBoxSize.y + 3 + flTextOffset[0][0]), vecTextSize);
				flTextOffset[0][0] -= vecBoxSize.y;
				break;
			case DrawData_t::DrawInfo_t::RIGHT:
				DrawTextInfo(textData, ImVec2(box.right + 3 + flTextOffset[1][1], box.top + 3 + flTextOffset[1][0]), vecTextSize);
				flTextOffset[1][0] += vecBoxSize.y;
				break;
			case DrawData_t::DrawInfo_t::BOTTOM:
				DrawTextInfo(textData, ImVec2((box.left + box.right) / 2 - vecBoxSize.x / 2 + 3, box.bottom + 3 + flTextOffset[2][0]), vecTextSize);
				flTextOffset[2][0] += vecBoxSize.y;
				break;
			case DrawData_t::DrawInfo_t::LEFT:
				DrawTextInfo(textData, ImVec2(box.left - vecBoxSize.x + 3 - flTextOffset[3][1], box.top + 3 + flTextOffset[3][0]), vecTextSize);
				flTextOffset[3][0] += vecBoxSize.y;
				break;
			}
		}
		if (textData.iType == DrawData_t::DrawInfo_t::BAR)
		{
			textData.imColor.Value.w = playerData.iAlpha;
			ImVec2 vecTextSize = ImGui::GetFont()->CalcTextSizeA(textData.flTextSize, FLT_MAX, 0, textData.szText.c_str());
			ImVec2 BoxSize = ImVec2(box.right - box.left,box.bottom - box.top);
			switch (textData.iPosition)
			{
			case DrawData_t::DrawInfo_t::TOP:
				DrawBarInfo(textData, ImVec2(box.left, box.top - Menu.Visuals.VisualsSize + flTextOffset[0][0]), ImVec2(BoxSize.x, Menu.Visuals.VisualsSize));
				flTextOffset[0][0] -= Menu.Visuals.VisualsSize;
				break;
			case DrawData_t::DrawInfo_t::RIGHT:
				DrawBarInfo(textData, ImVec2(box.right + flTextOffset[1][1], box.top), ImVec2(Menu.Visuals.VisualsSize, BoxSize.y));
				flTextOffset[1][1] += Menu.Visuals.VisualsSize;
				break;
			case DrawData_t::DrawInfo_t::BOTTOM:
				DrawBarInfo(textData, ImVec2(box.left, box.bottom + flTextOffset[2][0]), ImVec2(BoxSize.x, Menu.Visuals.VisualsSize));
				flTextOffset[2][0] += Menu.Visuals.VisualsSize;
				break;
			case DrawData_t::DrawInfo_t::LEFT:
				DrawBarInfo(textData, ImVec2(box.left - Menu.Visuals.VisualsSize - flTextOffset[3][1], box.top), ImVec2(Menu.Visuals.VisualsSize, BoxSize.y));
				flTextOffset[3][1] += Menu.Visuals.VisualsSize;
				break;
			}
		}
	}
	
	playerData.vecDrawData.clear();
}

RECT CImVisuals::GetBox(C_BaseEntity* pEntity)
{
	Vector min, max;
	const bool bDynamic = true;

	if (bDynamic)
	{
		pEntity->ComputeHitboxSurroundingBox(&min, &max);
		max.z += 5;
		min.z += 5;
	}
	else
	{
		pEntity->GetRenderBounds(min, max);
	/*	min = pEntity->GetCollideable()->VecMins();
		max = pEntity->GetCollideable()->VecMaxs();*/
	}

	Vector vecPoints[] = {
		Vector(min.x, min.y, min.z),
		Vector(min.x, max.y, min.z),
		Vector(max.x, max.y, min.z),
		Vector(max.x, min.y, min.z),
		Vector(max.x, max.y, max.z),
		Vector(min.x, max.y, max.z),
		Vector(min.x, min.y, max.z),
		Vector(max.x, min.y, max.z)
	};

	RECT pos{};
	Vector vecPointsTransformed[8];

	if (!bDynamic)
	{
		VMatrix& matTransformed = pEntity->GetCollisionBoundTrans();

		for (int i = 0; i < 8; i++)
			vecPointsTransformed[i] = Math::VectorTransform(vecPoints[i], matTransformed);
	}

	Vector vecScreen[8] = {};
	for (int i = 0; i < 8; i++)
	{
		if (!GameUtils::WorldToScreen(bDynamic ? vecPoints[i] : vecPointsTransformed[i], vecScreen[i]))
			return { -1000, -1000, -1000, -1000 };
	}

	pos.left = INT_MAX;
	pos.top = INT_MAX;
	pos.right = INT_MIN;
	pos.bottom = INT_MIN;

	for (int i = 1; i < 8; i++)
	{
		if (pos.left > vecScreen[i].x)
			pos.left = vecScreen[i].x;
		if (pos.top > vecScreen[i].y)
			pos.top = vecScreen[i].y;
		if (pos.right < vecScreen[i].x)
			pos.right = vecScreen[i].x;
		if (pos.bottom < vecScreen[i].y)
			pos.bottom = vecScreen[i].y;
	}

	return pos;
}

void CImVisuals::DrawBox(C_BaseEntity* pEntity)
{
	RECT box = GetBox(pEntity);
	DrawData_t& playerData = drawData[pEntity->GetIndex()];
	pDrawList->AddRect(ImVec2(box.left, box.top), ImVec2(box.right, box.bottom), ImColor(255, 255, 255, playerData.iAlpha));
	pDrawList->AddRect(ImVec2(box.left - 1, box.top - 1), ImVec2(box.right + 2, box.bottom + 2), ImColor(255, 255, 255, playerData.iAlpha));
	pDrawList->AddRect(ImVec2(box.left + 1, box.top + 1), ImVec2(box.right - 2, box.bottom - 2), ImColor(255, 255, 255, playerData.iAlpha));
}

void CImVisuals::DrawCornersBox(C_BaseEntity* pEntity)
{
	RECT box = GetBox(pEntity);
	DrawData_t& playerData = drawData[pEntity->GetIndex()];

	//Top lines
	pDrawList->AddLine(ImVec2(box.left, box.top), ImVec2(box.left - (box.left - box.right) / 5, box.top), ImColor(1, 1, 1, playerData.iAlpha));
	pDrawList->AddLine(ImVec2(box.right + (box.left - box.right) / 5, box.top), ImVec2(box.right, box.top), ImColor(1, 1, 1, playerData.iAlpha));

	//Botom lines
	pDrawList->AddLine(ImVec2(box.left, box.bottom), ImVec2(box.left - (box.left - box.right) / 5, box.bottom), ImColor(1, 1, 1, playerData.iAlpha));
	pDrawList->AddLine(ImVec2(box.right + (box.left - box.right) / 5, box.bottom), ImVec2(box.right, box.bottom), ImColor(1, 1, 1, playerData.iAlpha));

	//Left lines
	pDrawList->AddLine(ImVec2(box.left, box.top), ImVec2(box.left, box.top - (box.top - box.bottom) / 5), ImColor(1, 1, 1, playerData.iAlpha));
	pDrawList->AddLine(ImVec2(box.left, box.bottom + (box.top - box.bottom) / 5), ImVec2(box.left, box.bottom), ImColor(1, 1, 1, playerData.iAlpha));

	//Right lines
	pDrawList->AddLine(ImVec2(box.right, box.top), ImVec2(box.right, box.top - (box.top - box.bottom) / 5), ImColor(1, 1, 1, playerData.iAlpha));
	pDrawList->AddLine(ImVec2(box.right, box.bottom + (box.top - box.bottom) / 5), ImVec2(box.right, box.bottom), ImColor(1, 1, 1, playerData.iAlpha));
}

void CImVisuals::DrawHealthBar(C_BaseEntity* pEntity, bool bAnimated)
{
	RECT box = GetBox(pEntity);
	DrawData_t& playerData = drawData[pEntity->GetIndex()];

	int iRed = 255 - (pEntity->GetHealth() * 2.55f);
	int iGreen = pEntity->GetHealth() * 2.55f;
	//pDrawList->AddRectFilled(ImVec2(box.left - 6, box.bottom + (box.top - box.bottom)), ImVec2(box.left - 2, box.bottom), ImColor(40, 40, 40, (int)(playerData.iAlpha * 0.5f)));
	//pDrawList->AddRect(ImVec2(box.left - 6, box.bottom + (box.top - box.bottom)), ImVec2(box.left - 2, box.bottom), ImColor(0, 0, 0, playerData.iAlpha));

	if (bAnimated) // Need recode
	{
		if (playerData.flHealth < pEntity->GetHealth() || playerData.flDrawHealth < pEntity->GetHealth())
		{
			playerData.flHealth = pEntity->GetHealth();
			playerData.flDrawHealth = pEntity->GetHealth();
			playerData.flTriggerHealth = pEntity->GetHealth();
		}
		if (playerData.flTriggerHealth > pEntity->GetHealth())
		{
			playerData.flReduceTime = g_globals->curtime + 0.5f;
			playerData.flTriggerHealth = pEntity->GetHealth();
		}

		if (Animate(playerData.flDrawHealth, playerData.flHealth - pEntity->GetHealth(), pEntity->GetHealth(), playerData.flReduceTime))
			playerData.flHealth = pEntity->GetHealth();

		//pDrawList->AddRectFilled(ImVec2(box.left - 5, box.bottom + ((box.top - box.bottom) * playerData.flDrawHealth / 100.f)), ImVec2(box.left - 3, box.bottom), ImColor(255, 255, 255, playerData.iAlpha));
	}
	else
	{
		playerData.flTriggerHealth = pEntity->GetHealth();
		playerData.flHealth = pEntity->GetHealth();
		playerData.flDrawHealth = pEntity->GetHealth();
	}

	//pDrawList->AddRectFilled(ImVec2(box.left - 5, box.bottom + ((box.top - box.bottom) * pEntity->GetHealth() / 100.f)), ImVec2(box.left - 3, box.bottom), ImColor(iRed, iGreen, 50, playerData.iAlpha));
}

void CImVisuals::UpdateSoundInfo()
{
	static CUtlVector<SndInfo_t> vecSoundList;
	vecSoundList.RemoveAll();
	g_pEngineSound->GetActiveSounds(vecSoundList);

	if (!vecSoundList.Count())
		return;

	for (int i = 0; i < vecSoundList.Count(); i++)
	{
		SndInfo_t& sound = vecSoundList[i];

		if (sound.m_nSoundSource == 0 || sound.m_nSoundSource > 64)
			continue;

		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(sound.m_nSoundSource);

		if (pEntity == nullptr || pEntity == c_ayala::l_player || !sound.m_pOrigin || sound.m_pOrigin->IsZero()/* || pEntity != DrawData[sound.iSoundSource].pEntity*/)
			continue;
		if (!pEntity->IsDormant())
			continue;

		if (!sound.m_bFromServer)
			continue;

		drawData[sound.m_nSoundSource].dormantData = sound;
		drawData[sound.m_nSoundSource].dormantData.flSoundRecordTime = g_globals->curtime;
	}
}

void CImVisuals::UpdateDormantInfo(C_BaseEntity* pEntity)
{
	DrawData_t& playerData = drawData[pEntity->GetIndex()];
	
	if (pEntity->IsDormant())
	{
		if (playerData.dormantData.flSoundRecordTime + 2.5f > g_globals->curtime)
		{
			Vector src3D = (playerData.dormantData.vecSoundPosition) + Vector(0, 0, 1);
			Vector dst3D = src3D - Vector(0, 0, 100);

			Ray_t ray;
			ray.Init(src3D, dst3D);
			CTraceFilter filter;
			filter.pSkip1 = pEntity;

			trace_t trace;
			g_pEngineTrace->TraceRay(ray, MASK_PLAYERSOLID, &filter, &trace);

			if (trace.allSolid)
			{
				playerData.dormantData.flUpdateDormantPositionTime = playerData.dormantData.flSoundRecordTime = -1;
			}
			else
			{
				playerData.dormantData.vecDormantPosition = ((trace.fraction < 0.97f) ? trace.endpos : playerData.dormantData.vecSoundPosition);
				playerData.dormantData.flUpdateDormantPositionTime = g_globals->curtime;
			}
		}
	}
	else
	{
		playerData.dormantData.vecDormantPosition = pEntity->GetAbsOrigin();
		playerData.dormantData.flUpdateDormantPositionTime = g_globals->curtime;
		playerData.iAlpha = 255;
	}

	if (pEntity->IsDormant() && playerData.dormantData.flUpdateDormantPositionTime >= g_globals->curtime)
	{
		vecDormantRestore.push_back(pEntity);
		playerData.iAlpha = 25;
		pEntity->SetAbsOrigin(playerData.dormantData.vecDormantPosition);
		pEntity->GetOrigin() = playerData.dormantData.vecDormantPosition;
		*(bool*)((DWORD)pEntity + offys.Dormant) = false;
	}
}
namespace FakeLatency
{
	extern DWORD ClientState;
}

void CImVisuals::DrawCustomUI()
{
	if (Menu.Visuals.Noscope) {
		if (c_ayala::l_player && c_ayala::l_player->isAlive())
		{
			if (c_ayala::c_weapon)
			{
				if (c_ayala::c_weapon->IsSniper())
				{
					int centerX = static_cast<int>(c_ayala::Screen.width * 0.5f);
					int centerY = static_cast<int>(c_ayala::Screen.height * 0.5f);
					static float offset;
					offset += M_2PI / 900;
					if (offset > M_2PI)
						offset = 0;
					if (c_ayala::c_weapon && c_ayala::l_player->IsScoped() && c_ayala::l_player->isAlive())
					{
						ImVector<ImDrawVert> vecVertData;
						ImDrawVert imVert;
						float spread = (c_ayala::c_weapon->GetSpread() + c_ayala::c_weapon->GetInaccuracy()) * 500;
						for (float a = 0; a <= M_2PI; a += M_2PI / 4)
						{
							imVert.pos.x = (spread - spread / 3) * cosf(a) + centerX;
							imVert.pos.y = (spread - spread / 3) * sinf(a) + centerY;
							imVert.col = ImColor::HSV((a + offset) / M_2PI, 1, 1, 0);
							vecVertData.push_back(imVert);

							imVert.pos.x = (spread)* cosf(a) + centerX;
							imVert.pos.y = (spread)* sinf(a) + centerY;
							imVert.col = ImColor::HSV((a + offset) / M_2PI, 1, 1, 0.5);
							vecVertData.push_back(imVert);
						}
						pDrawList->AddCustomPolyFilled(vecVertData.Data, vecVertData.Size);

						pDrawList->AddLine(ImVec2(0, centerY), ImVec2(centerX - spread, centerY), ImColor::HSV((M_PI + offset) / M_2PI, 1, 1));
						pDrawList->AddLine(ImVec2(centerX + spread, centerY), ImVec2(c_ayala::Screen.width, centerY), ImColor::HSV(offset / M_2PI, 1, 1));

						pDrawList->AddLine(ImVec2(centerX, 0), ImVec2(centerX, centerY - spread), ImColor::HSV((M_PI + M_PI / 2 + offset) / M_2PI, 1, 1));
						pDrawList->AddLine(ImVec2(centerX, centerY + spread), ImVec2(centerX, c_ayala::Screen.height), ImColor::HSV((M_PI / 2 + offset) / M_2PI, 1, 1));

					}
				}
			}
		}
	}
	
	IndicatorInfo_t Ind;
	//if (c_ayala::l_player && c_ayala::l_player->isAlive())
	//{
	//	if (Math::NormalizeYaw(c_ayala::l_player->LowerBodyYaw() - c_ayala::RealAngle.y) > 35)
	//	{
	//		Ind.IndicatorType = IndicatorInfo_t::TIMER;
	//		Ind.bShowInfo = true;
	//		Ind.szInfo = "LBY";
	//		Ind.Percent = (g_Antiaim->flLowerBodyYawUpdateTime - GameUtils::GetCurTime()) / g_Antiaim->flUpdateTime * 100;
	//		Ind.flSize = 30;
	//		Ind.imColor = ImColor(0, 255, 0);
	//		vecIndicatorInfo.push_back(Ind);
	//	}
	//	//else
	//	//{
	//	//	Ind.IndicatorType = IndicatorInfo_t::TEXT;
	//	//	//Ind.szInfo = "LBY";
	//	//	Ind.flSize = 30;
	//	//	Ind.imColor = ImColor(255, 0, 0);
	//	//	vecIndicatorInfo.push_back(Ind);
	//	//}
	///*	if (Menu.Misc.FakelagAmount > 0)
	//	{
	//		
	//		Ind.IndicatorType = IndicatorInfo_t::TIMER;
	//		Ind.szInfo = "FakeLag";
	//		Ind.bShowInfo = true;
	//		Ind.Percent = ((float)g_Antiaim->iChocked / (float)Menu.Misc.FakelagAmount) * 100;
	//		Ind.flSize = 30;
	//		Ind.imColor = ImColor(255, 255, 255);
	//		vecIndicatorInfo.push_back(Ind);
	//	}*/
	//}
	auto bSortF = [](IndicatorInfo_t ind1, IndicatorInfo_t ind2) { 
		if (ind1.Column == ind2.Column)
			return ind1.Row <= ind2.Row;
		return ind1.Column <= ind2.Column;
	};
	std::sort(vecIndicatorInfo.begin(), vecIndicatorInfo.end(), bSortF);
	int Column = 0, MaxWight = 0, Offset[2] = { 0 }, oldColumn = 0, oldRow = 0, RowOffset = 0;
	for (auto info : vecIndicatorInfo)
	{
		if (oldRow == info.Row || oldColumn == info.Column)
			RowOffset++;
		ImVec2 TextSize = ImGui::GetFont()->CalcTextSizeA(info.flSize, FLT_MAX, 0, info.szInfo.c_str());
		if (Column == info.Column)
		{
			if (info.IndicatorType == IndicatorInfo_t::TIMER)
			{
				if (TextSize.x > MaxWight)
					MaxWight = TextSize.x;
			}
			else if (info.IndicatorType == IndicatorInfo_t::TEXT)
			{
				if (TextSize.x + TextSize.y > MaxWight)
					MaxWight = TextSize.x + TextSize.y;
			}
		}
		else
		{
			Offset[1] += MaxWight + 15;
			Column = info.Column;
			MaxWight = RowOffset = Offset[0] = 0;
		}
		Offset[0] -= TextSize.y + 2;
		DrawIndicator(info, ImVec2(Offset[1] + 10, Offset[0] + c_ayala::Screen.height - 10));
	}
	vecIndicatorInfo.clear();
}

void CImVisuals::SetupExtendedESP()
{
	UpdateSoundInfo();
	bDataClear = false;
}

void CImVisuals::DestroyExtendedESP()
{
	for (C_BaseEntity* pRestoreEntity : vecDormantRestore)
		*(bool*)((DWORD)pRestoreEntity + offys.Dormant) = true;

	vecDormantRestore.clear();
	vecOffScreenData.clear();
}

bool CImVisuals::Animate(float& flIn, float flDelta, float flNeed, float flTime)
{
	float flDeltaTime = flTime - g_globals->curtime;
	if (flDeltaTime < 1 && flDeltaTime >= 0)
		flIn = flNeed + (flDelta * flDeltaTime);
	if (flDeltaTime <= 0)
	{
		flIn = flNeed;
		return true;
	}
	return false;
}

void CImVisuals::DrawTextInfo(DrawData_t::DrawInfo_t textData, ImVec2 pos, ImVec2 size)
{
	int iAlpha = textData.iAlpha;
	if (iAlpha > 130)
		iAlpha = 130;
	ImColor Inner = ImColor(79, 79, 79, iAlpha), Outer = ImColor(39, 39, 39, iAlpha);
	pDrawList->AddRectFilledMultiColor(ImVec2(pos.x - 3, pos.y - 3), ImVec2(pos.x + size.x + 3, pos.y + size.y + 3), Inner, Inner, Outer, Outer);
	pDrawList->AddText(ImGui::GetFont(), textData.flTextSize, pos, textData.imColor, textData.szText.c_str());
	pDrawList->AddRect(ImVec2(pos.x - 3, pos.y - 3), ImVec2(pos.x + size.x + 3, pos.y + size.y + 3), ImColor(0, 0, 0, textData.iAlpha));
}

void CImVisuals::DrawBarInfo(DrawData_t::DrawInfo_t textData, ImVec2 pos, ImVec2 size)
{
	float offset = 0;
	ImVec2 FreeSpace;
	ImVec2 BarSize;
	int iAlpha = textData.iAlpha;
	if (iAlpha > 130)
		iAlpha = 130;
	ImColor Inner = ImColor(79, 79, 79, iAlpha), Outer = ImColor(39, 39, 39, iAlpha);
	switch (textData.iPosition)
	{
	case DrawData_t::DrawInfo_t::BOTTOM:
	case DrawData_t::DrawInfo_t::TOP:
		offset = 0;
		FreeSpace = ImVec2(size.y / 3, size.y / 3);
		BarSize = ImVec2(size.x, size.y - FreeSpace.y * 2);
		if (textData.iPosition == DrawData_t::DrawInfo_t::BOTTOM)
			pDrawList->AddRectFilledMultiColor(pos, ImVec2(pos.x + size.x, pos.y + size.y), Inner, Inner, Outer, Outer);
		else
			pDrawList->AddRectFilledMultiColor(pos, ImVec2(pos.x + size.x, pos.y + size.y), Outer, Outer, Inner, Inner);
		pDrawList->AddRect(pos, ImVec2(pos.x + size.x, pos.y + size.y), ImColor(19, 19, 19, iAlpha));
		if (textData.BarData.szName != std::string("ERROR"))
		{
			ImVec2 vecTextSize = ImGui::GetFont()->CalcTextSizeA(textData.flTextSize, FLT_MAX, 0, textData.BarData.szName.c_str());
			if (vecTextSize.x + FreeSpace.x * 4 < size.x)
			{ 
				offset = vecTextSize.x + FreeSpace.x;
				pDrawList->AddText(ImGui::GetFont(), textData.flTextSize, ImVec2(pos.x + FreeSpace.x, pos.y), textData.imColor, textData.BarData.szName.c_str());
			}
		}
		offset += FreeSpace.x;
		BarSize.x -= offset + FreeSpace.x;
		pDrawList->AddRect(ImVec2(pos.x + offset - 1, pos.y + FreeSpace.y - 1), ImVec2(pos.x + offset + BarSize.x + 1, pos.y + FreeSpace.y + BarSize.y + 1), ImColor(19, 19, 19, iAlpha));
		BarSize.x *= textData.BarData.flValue / textData.BarData.flMaxValue;
		if (textData.BarData.bInverse)
			pDrawList->AddRectFilled(ImVec2(pos.x + size.x - FreeSpace.x - BarSize.x, pos.y + FreeSpace.y), ImVec2(pos.x + size.x - FreeSpace.x, pos.y + FreeSpace.y + BarSize.y), textData.BarData.imColor);
		else
			pDrawList->AddRectFilled(ImVec2(pos.x + offset, pos.y + FreeSpace.y), ImVec2(pos.x + offset + BarSize.x, pos.y + FreeSpace.y + BarSize.y), textData.BarData.imColor);
	break;
	case DrawData_t::DrawInfo_t::RIGHT:
	case DrawData_t::DrawInfo_t::LEFT:
		FreeSpace = ImVec2(size.x / 3, size.x / 3);
		BarSize = ImVec2(size.x, size.y - FreeSpace.y * 2);
		if (textData.iPosition == DrawData_t::DrawInfo_t::RIGHT)
			pDrawList->AddRectFilledMultiColor(pos, ImVec2(pos.x + size.x, pos.y + size.y), Inner, Outer, Outer, Inner);
		else
			pDrawList->AddRectFilledMultiColor(pos, ImVec2(pos.x + size.x, pos.y + size.y), Outer, Inner, Inner, Outer);
		pDrawList->AddRect(pos, ImVec2(pos.x + size.x, pos.y + size.y), ImColor(19, 19, 19, iAlpha));
		BarSize.x -= FreeSpace.x * 2;
		pDrawList->AddRect(ImVec2(pos.x - 1 + FreeSpace.x, pos.y + FreeSpace.y - 1), ImVec2(pos.x + BarSize.x + 1 + FreeSpace.x, pos.y + FreeSpace.y + BarSize.y + 1), ImColor(19, 19, 19, iAlpha));
		BarSize.y *= textData.BarData.flValue / textData.BarData.flMaxValue;
		if (textData.BarData.bInverse) 
			pDrawList->AddRectFilled(ImVec2(pos.x + FreeSpace.x, pos.y + FreeSpace.y), ImVec2(pos.x + FreeSpace.x + BarSize.x, pos.y + FreeSpace.y + BarSize.y), textData.BarData.imColor);
		else 
			pDrawList->AddRectFilled(ImVec2(pos.x + FreeSpace.x, pos.y + size.y - FreeSpace.y), ImVec2(pos.x + FreeSpace.x + BarSize.x, pos.y + size.y - FreeSpace.y - BarSize.y), textData.BarData.imColor);
	break;
	}
}

void CImVisuals::DrawIndicator(IndicatorInfo_t info, ImVec2 pos)
{
	ImVec2 TextSize = ImGui::GetFont()->CalcTextSizeA(info.flSize, FLT_MAX, 0, info.szInfo.c_str());
	switch (info.IndicatorType)
	{
	case IndicatorInfo_t::BAR:
		if (info.bShowInfo)
			pDrawList->AddText(ImGui::GetFont(), info.flSize, pos, info.imColor, info.szInfo.c_str());
		break;
	case IndicatorInfo_t::TEXT:
		pDrawList->AddText(ImGui::GetFont(), info.flSize, pos, info.imColor, info.szInfo.c_str());
		break;
	case IndicatorInfo_t::TIMER:
		if (info.bShowInfo)
			pDrawList->AddText(ImGui::GetFont(), info.flSize, pos, info.imColor, info.szInfo.c_str());
		ImVector<ImDrawVert> vecVertData;
		ImDrawVert imVert;
		for (float a = 0; a <= M_2PI * info.Percent / 100; a += M_2PI / 180)
		{
			imVert.pos.x = (TextSize.y / 2 - 2) * cosf(a) + pos.x + TextSize.x + TextSize.y / 2 + 4;
			imVert.pos.y = (TextSize.y / 2 - 2) * sinf(a) + pos.y + TextSize.y / 2;
			imVert.col = info.imColor;
			vecVertData.push_back(imVert);

			imVert.pos.x = (TextSize.y / 2 - 5) * cosf(a) + pos.x + TextSize.x + TextSize.y / 2 + 4;
			imVert.pos.y = (TextSize.y / 2 - 5) * sinf(a) + pos.y + TextSize.y / 2;
			imVert.col = info.imColor;
			vecVertData.push_back(imVert);
		}
		pDrawList->AddCustomPolyFilled(vecVertData.Data, vecVertData.Size);
		pDrawList->AddCircleFilled(ImVec2(pos.x + TextSize.x + TextSize.y / 2 + 4, pos.y + TextSize.y / 2), TextSize.y / 2 - 2, ImColor(39, 39, 39, 100), 100);
		break;
	}
}
