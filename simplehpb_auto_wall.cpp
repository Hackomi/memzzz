#include "Hooks.h"
#include "simplehpb_auto_wall.h"
#include "GameUtils.h"
#include "Math.h"
#define DAMAGE_NO		0
#define DAMAGE_EVENTS_ONLY	1
#define DAMAGE_YES		2
#define DAMAGE_AIM		3
#define CHAR_TEX_ANTLION		'A'
#define CHAR_TEX_BLOODYFLESH	'B'
#define	CHAR_TEX_CONCRETE		'C'
#define CHAR_TEX_DIRT			'D'
#define CHAR_TEX_EGGSHELL		'E' ///< the egg sacs in the tunnels in ep2.
#define CHAR_TEX_FLESH			'F'
#define CHAR_TEX_GRATE			'G'
#define CHAR_TEX_ALIENFLESH		'H'
#define CHAR_TEX_CLIP			'I'
#define CHAR_TEX_PLASTIC		'L'
#define CHAR_TEX_METAL			'M'
#define CHAR_TEX_SAND			'N'
#define CHAR_TEX_FOLIAGE		'O'
#define CHAR_TEX_COMPUTER		'P'
#define CHAR_TEX_SLOSH			'S'
#define CHAR_TEX_TILE			'T'
#define CHAR_TEX_CARDBOARD		'U'
#define CHAR_TEX_VENT			'V'
#define CHAR_TEX_WOOD			'W'
#define CHAR_TEX_GLASS			'Y'
#define CHAR_TEX_WARPSHIELD		'Z' ///< wierd-looking jello effect for advisor shield.
template<class T>
void Normalize3(T& vec)
{
	for (auto i = 0; i < 3; i++) {
		while (vec[i] < -180.0f) vec[i] += 360.0f;
		while (vec[i] > 180.0f) vec[i] -= 360.0f;
	}
	vec[2] = 0.f;
}
inline bool trace_t::DidHitWorld() const
{
	return m_pEnt->Index() == 0;
}
inline bool trace_t::DidHitNonWorldEntity() const
{
	return m_pEnt != NULL && !DidHitWorld();
}
bool CBulletHandler::BreakableEntity(IClientEntity* entity)
{

	ClientClass* pClass = (ClientClass*)entity->GetClientClass();

	if (!pClass)
	{
		return false;
	}

	if (pClass == nullptr)
	{
		return false;
	}

	return pClass->m_ClassID == 27 || pClass->m_ClassID == 28;

}
void CBulletHandler::TraceLine(Vector& absStart, Vector& absEnd, unsigned int mask, IClientEntity* ignore, trace_t* ptr)
{
	Ray_t ray;
	ray.Init(absStart, absEnd);
	CTraceFilter filter;
	filter.pSkip1 = ignore;

	g_pEngineTrace->TraceRay(ray, mask, &filter, ptr);
}
bool CBulletHandler::trace_to_exit(trace_t& enterTrace, trace_t& exitTrace, Vector startPosition, Vector direction)
{
	Vector start, end;
	float maxDistance = 90.f, rayExtension = 4.f, currentDistance = 0;
	int firstContents = 0;

	while (currentDistance <= maxDistance)
	{
		currentDistance += rayExtension;

		start = startPosition + direction * currentDistance;

		if (!firstContents)
		{
			firstContents = g_pEngineTrace->GetPointContents(start, MASK_SHOT_HULL | CONTENTS_HITBOX, nullptr);
		}

		int pointContents = g_pEngineTrace->GetPointContents(start, MASK_SHOT_HULL | CONTENTS_HITBOX, nullptr);

		if (!(pointContents & MASK_SHOT_HULL) || pointContents & CONTENTS_HITBOX && pointContents != firstContents)
		{
			end = start - (direction * rayExtension);

			TraceLine(start, end, MASK_SHOT_HULL | CONTENTS_HITBOX, nullptr, &exitTrace);

			if (exitTrace.startSolid && exitTrace.surface.flags & SURF_HITBOX)
			{
				TraceLine(start, startPosition, MASK_SHOT_HULL, exitTrace.m_pEnt, &exitTrace);

				if (exitTrace.DidHit() && !exitTrace.startSolid)
				{
					start = exitTrace.endpos;
					return true;
				}
				continue;
			}

			if (exitTrace.DidHit() && !exitTrace.startSolid)
			{

				if (BreakableEntity(enterTrace.m_pEnt) && BreakableEntity(exitTrace.m_pEnt))
				{
					return true;
				}

				if (enterTrace.surface.flags & SURF_NODRAW || !(exitTrace.surface.flags & SURF_NODRAW) && (exitTrace.plane.normal.Dot(direction) <= 1.f))
				{
					float multAmount = exitTrace.fraction * 4.f;
					start -= direction * multAmount;
					return true;
				}

				continue;
			}

			if (!exitTrace.DidHit() || exitTrace.startSolid)
			{
				if (enterTrace.DidHitNonWorldEntity() && BreakableEntity(enterTrace.m_pEnt))
				{
					//auto t = enterTrace;
					exitTrace = enterTrace;
					exitTrace.endpos = start + direction;
					return true;
				}

				continue;
			}
		}
	}
	return false;
}

bool TraceToExit(Vector& end, trace_t& tr, float x, float y, float z, float x2, float y2, float z2, trace_t* trace)
{

	typedef bool(__fastcall* TraceToExitFn)(Vector&, trace_t&, float, float, float, float, float, float, trace_t*);
	static TraceToExitFn TraceToExit = (TraceToExitFn)Utilities::Memory::FindPatternIDA(XorStr("client_panorama.dll"), XorStr("55 8B EC 83 EC 30 F3 0F 10 75"));
	if (!TraceToExit)
	{
		return false;
	}//(Vector&, trace_t&, float, float, float, float, float, float, trace_t*);
	_asm
	{
		push trace
		push z2
		push y2
		push x2
		push z
		push y
		push x
		mov edx, tr
		mov ecx, end
		call TraceToExit
		add esp, 0x1C
	}
}
bool CBulletHandler::HandleBulletPenetration1(CSWeaponInfo* weaponData, trace_t& enterTrace, Vector& eyePosition, Vector direction, int& possibleHitsRemaining, float& currentDamage, float penetrationPower, bool sv_penetration_type, float ff_damage_reduction_bullets, float ff_damage_bullet_penetration)
{
	//Because there's been issues regarding this- putting this here.
	if (&currentDamage == nullptr)
	{
		handle_penetration = false;
		return false;
	}

	auto local = c_ayala::l_player;//(IClientEntity*)Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());

	FireBulletData1 data(local->GetEyePosition());
	data.filter = CTraceFilter();
	data.filter.pSkip1 = local;
	trace_t exitTrace;
	C_BaseEntity* pEnemy = (C_BaseEntity*)enterTrace.m_pEnt;
	surfacedata_t* enterSurfaceData = g_pPhysics->GetSurfaceData(enterTrace.surface.surfaceProps);
	int enterMaterial = enterSurfaceData->game.material;

	float enterSurfPenetrationModifier = enterSurfaceData->game.flPenetrationModifier;
	float enterDamageModifier = enterSurfaceData->game.flDamageModifier;
	float thickness, modifier, lostDamage, finalDamageModifier, combinedPenetrationModifier;
	bool isSolidSurf = ((enterTrace.contents >> 3) & CONTENTS_SOLID);
	bool isLightSurf = ((enterTrace.surface.flags >> 7) & SURF_LIGHT);

	if (possibleHitsRemaining <= 0
		|| (enterTrace.surface.name == (const char*)0x2227c261 && exitTrace.surface.name == (const char*)0x2227c868)
		|| (!possibleHitsRemaining && !isLightSurf && !isSolidSurf && enterMaterial != CHAR_TEX_GRATE && enterMaterial != CHAR_TEX_GLASS)
		|| weaponData->penetration <= 0.f
		|| !trace_to_exit(enterTrace, exitTrace, enterTrace.endpos, direction)
		&& !(g_pEngineTrace->GetPointContents(enterTrace.endpos, MASK_SHOT_HULL, nullptr) & MASK_SHOT_HULL))
	{
		handle_penetration = false;
		return false;
	}

	surfacedata_t* exitSurfaceData = g_pPhysics->GetSurfaceData(exitTrace.surface.surfaceProps);
	int exitMaterial = exitSurfaceData->game.material;
	float exitSurfPenetrationModifier = exitSurfaceData->game.flPenetrationModifier;
	float exitDamageModifier = exitSurfaceData->game.flDamageModifier;

	if (sv_penetration_type)
	{
		if (enterMaterial == CHAR_TEX_GRATE || enterMaterial == CHAR_TEX_GLASS)
		{
			combinedPenetrationModifier = 3.f;
			finalDamageModifier = 0.05f;
		}
		else if (isSolidSurf || isLightSurf)
		{
			combinedPenetrationModifier = 1.f;
			finalDamageModifier = 0.16f;
		}
		else if (enterMaterial == CHAR_TEX_FLESH && (!pEnemy->IsEnemy() && ff_damage_reduction_bullets == 0.f))
		{
			if (ff_damage_bullet_penetration == 0.f)
			{
				handle_penetration = false;
				return false;
			}
			combinedPenetrationModifier = ff_damage_bullet_penetration;
			finalDamageModifier = 0.16f;
		}
		else
		{
			combinedPenetrationModifier = (enterSurfPenetrationModifier + exitSurfPenetrationModifier) / 2.f;
			finalDamageModifier = 0.16f;
		}

		if (enterMaterial == exitMaterial)
		{
			if (exitMaterial == CHAR_TEX_CARDBOARD || exitMaterial == CHAR_TEX_WOOD)
			{
				combinedPenetrationModifier = 3.f;
			}
			else if (exitMaterial == CHAR_TEX_PLASTIC)
			{
				combinedPenetrationModifier = 2.f;
			}
		}

		thickness = (exitTrace.endpos - enterTrace.endpos).LengthSqr();
		modifier = fmaxf(1.f / combinedPenetrationModifier, 0.f);

		lostDamage = fmaxf(
			((modifier * thickness) / 24.f)
			+ ((currentDamage * finalDamageModifier)
				+ (fmaxf(3.75f / penetrationPower, 0.f) * 3.f * modifier)), 0.f);

		if (lostDamage > currentDamage)
		{
			handle_penetration = false;
			return false;
		}

		if (lostDamage > 0.f)
		{
			currentDamage -= lostDamage;
		}

		if (currentDamage < 1.f)
		{
			handle_penetration = false;
			return false;
		}

		eyePosition = exitTrace.endpos;
		--possibleHitsRemaining;

		handle_penetration = true;
		return true;
	}
	else
	{
		combinedPenetrationModifier = 1.f;

		if (isSolidSurf || isLightSurf)
		{
			finalDamageModifier = 0.99f;
		}
		else
		{
			finalDamageModifier = fminf(enterDamageModifier, exitDamageModifier);
			combinedPenetrationModifier = fminf(enterSurfPenetrationModifier, exitSurfPenetrationModifier);
		}

		if (enterMaterial == exitMaterial && (exitMaterial == CHAR_TEX_METAL || exitMaterial == CHAR_TEX_WOOD))
		{
			combinedPenetrationModifier += combinedPenetrationModifier;
		}

		thickness = (exitTrace.endpos - enterTrace.endpos).LengthSqr();

		if (sqrt(thickness) <= combinedPenetrationModifier * penetrationPower)
		{
			currentDamage *= finalDamageModifier;
			eyePosition = exitTrace.endpos;
			--possibleHitsRemaining;
			handle_penetration = true;
			return true;
		}
		handle_penetration = false;
		return false;
	}
}
void CBulletHandler::GetBulletTypeParameters(float& maxRange, float& maxDistance, char* bulletType, bool sv_penetration_type)
{
	if (sv_penetration_type)
	{
		maxRange = 35.0;
		maxDistance = 3000.0;
	}
	else
	{
		//Play tribune to framerate. Thanks, stringcompare
		//Regardless I doubt anyone will use the old penetration system anyway; so it won't matter much.
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_338MAG")))
		{
			maxRange = 45.0;
			maxDistance = 8000.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_762MM")))
		{
			maxRange = 39.0;
			maxDistance = 5000.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_556MM")) || !strcmp(bulletType, XorStr("BULLET_PLAYER_556MM_SMALL")) || !strcmp(bulletType, XorStr("BULLET_PLAYER_556MM_BOX")))
		{
			maxRange = 35.0;
			maxDistance = 4000.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_57MM")))
		{
			maxRange = 30.0;
			maxDistance = 2000.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_50AE")))
		{
			maxRange = 30.0;
			maxDistance = 1000.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_357SIG")) || !strcmp(bulletType, XorStr("BULLET_PLAYER_357SIG_SMALL")) || !strcmp(bulletType, XorStr("BULLET_PLAYER_357SIG_P250")) || !strcmp(bulletType, XorStr("BULLET_PLAYER_357SIG_MIN")))
		{
			maxRange = 25.0;
			maxDistance = 800.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_9MM")))
		{
			maxRange = 21.0;
			maxDistance = 800.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_45ACP")))
		{
			maxRange = 15.0;
			maxDistance = 500.0;
		}
		if (!strcmp(bulletType, XorStr("BULLET_PLAYER_BUCKSHOT")))
		{
			maxRange = 0.0;
			maxDistance = 0.0;
		}
	}
}

bool CBulletHandler::FireBullet(CBaseCombatWeapon* pWeapon, Vector& direction, float& currentDamage)
{
	if (!pWeapon)
	{
		return false;
	}

	auto local = c_ayala::l_player;

	bool sv_penetration_type;

	float currentDistance = 0.f, penetrationPower, penetrationDistance, maxRange, ff_damage_reduction_bullets, ff_damage_bullet_penetration, rayExtension = 40.f;
	Vector eyePosition = local->GetEyePosition();

	static ConVar* penetrationSystem = g_pCvar->FindVar(XorStr("sv_penetration_type"));
	static ConVar* damageReductionBullets = g_pCvar->FindVar(XorStr("ff_damage_reduction_bullets"));
	static ConVar* damageBulletPenetration = g_pCvar->FindVar(XorStr("ff_damage_bullet_penetration"));

	sv_penetration_type = penetrationSystem->GetBool();
	ff_damage_reduction_bullets = damageReductionBullets->GetFloat();
	ff_damage_bullet_penetration = damageBulletPenetration->GetFloat();
	CSWeaponInfo* GetCSWeaponData();

	CSWeaponInfo* weaponData = pWeapon->GetCSWpnData();
	trace_t enterTrace;
	CTraceFilter filter;

	filter.pSkip1 = local;

	if (!weaponData)
	{
		return false;
	}

	maxRange = weaponData->range;

	GetBulletTypeParameters(penetrationPower, penetrationDistance, weaponData->hud_name /* ��� ������ ���� ������ ���*/, sv_penetration_type);

	if (sv_penetration_type)
	{
		penetrationPower = weaponData->penetration;
	}

	int possibleHitsRemaining = 4;

	currentDamage = weaponData->damage;

	while (possibleHitsRemaining > 0 && currentDamage >= 1.f)
	{
		maxRange -= currentDistance;

		Vector end = eyePosition + direction * maxRange;

		TraceLine(eyePosition, end, MASK_SHOT_HULL | CONTENTS_HITBOX, local, &enterTrace);
		UTIL_ClipTraceToPlayers(eyePosition, end + direction * rayExtension, MASK_SHOT_HULL | CONTENTS_HITBOX, &filter, &enterTrace); //  | CONTENTS_HITBOX

		surfacedata_t* enterSurfaceData = g_pPhysics->GetSurfaceData(enterTrace.surface.surfaceProps);

		float enterSurfPenetrationModifier = enterSurfaceData->game.flPenetrationModifier;

		int enterMaterial = enterSurfaceData->game.material;

		if (enterTrace.fraction == 1.f)
		{
			break;
		}

		currentDistance += enterTrace.fraction * maxRange;

		currentDamage *= pow(weaponData->range_modifier, (currentDistance / 500.f));

		if (currentDistance > penetrationDistance && weaponData->penetration > 0.f || enterSurfPenetrationModifier < 0.1f)
		{
			break;
		}

		bool canDoDamage = (enterTrace.hitgroup != HITGROUP_GEAR && enterTrace.hitgroup != HITGROUP_GENERIC);

		if (canDoDamage && static_cast<C_BaseEntity*>(enterTrace.m_pEnt)->IsEnemy())
		{
			ScaleDamage1(enterTrace, weaponData, currentDamage);
			return true;
		}

		if (!HandleBulletPenetration1(weaponData, enterTrace, eyePosition, direction, possibleHitsRemaining, currentDamage, penetrationPower, sv_penetration_type, ff_damage_reduction_bullets, ff_damage_bullet_penetration))
		{
			break;
		}
	}
	return false;
}

////////////////////////////////////// Usage Calls //////////////////////////////////////
float CBulletHandler::CanHit(Vector& point)
{
	auto local = c_ayala::l_player;//(IClientEntity*)Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());

	if (!local || !local->isAlive())
	{
		return -1.f;
	}

	FireBulletData1 data(local->GetEyePosition());// = FireBulletData(local->GetEyePosition());
	data.filter = CTraceFilter();
	data.filter.pSkip1 = local;
	Vector angles, direction;
	Vector tmp = point - local->GetEyePosition();
	float currentDamage = 0;

	direction = tmp;
	direction.NormalizeInPlace();

	if (FireBullet(local->GetWeapon(), direction, currentDamage))
	{
		return currentDamage;
	}
	return -1.f;
}

float CBulletHandler::CanHit(Vector& start, Vector& point)
{
	auto local = c_ayala::l_player;//(IClientEntity*)Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());

	if (!local || !local->isAlive())
	{
		return -1.f;
	}

	FireBulletData1 data(start);// = FireBulletData(local->GetEyePosition());
	data.filter = CTraceFilter();
	data.filter.pSkip1 = local;
	Vector angles, direction;
	Vector tmp = point - start;
	float currentDamage = 0;


	direction = tmp;
	direction.NormalizeInPlace();

	if (FireBullet(local->GetWeapon(), direction, currentDamage))
	{
		return currentDamage;
	}
	return -1.f;
}

float CBulletHandler::CanHit(C_BaseEntity* ent, Vector& point)
{

	if (!ent || !ent->isAlive())
	{
		return -1.f;
	}

	FireBulletData1 data(ent->GetEyePosition());
	data.filter = CTraceFilter();
	data.filter.pSkip1 = ent;
	Vector angles, direction;
	Vector tmp = point - ent->GetEyePosition();
	float currentDamage = 0;

	//VectorAngles(tmp, angles);
	//AngleVectors(angles, &direction);
	direction = tmp;
	direction.NormalizeInPlace();

	if (FireBullet(ent->GetWeapon(), direction, currentDamage))
	{
		return currentDamage;
	}
	return -1.f;
}

bool CBulletHandler::trace_awall(float& damage)
{
	auto local = c_ayala::l_player;
	if (!local)
	{
		return false;
	}
	FireBulletData1 data(local->GetEyePosition());
	Vector eyepos = local->GetEyePosition();
	data.filter = CTraceFilter();
	data.filter.pSkip1 = local;

	QAngle EyeAng = QAngle(0, 0, 0);
	g_pEngine->GetViewAngles(EyeAng);

	Vector dst, forward;

	Math::AngleVectors(EyeAng, forward);
	dst = data.src + (forward * 8196.f);

	QAngle angles = QAngle(0, 0, 0);
	angles = GameUtils::CalculateAngle(data.src, dst);
	Math::AngleVectors(angles, data.direction);
	Normalize3(data.direction);

	CBaseCombatWeapon* weapon = c_ayala::l_player->GetWeapon();

	if (!weapon)
	{
		return false;
	}

	data.penetrate_count = 1;
	data.trace_length = 0.0f;

	CSWeaponInfo* weaponData = weapon->GetCSWpnData();

	if (!weaponData)
	{
		return false;
	}

	data.current_damage = (float)weaponData->damage;

	data.trace_length_remaining = weaponData->range - data.trace_length;

	Vector end = data.src + data.direction * data.trace_length_remaining;

	TraceLine(data.src, end, MASK_SHOT | CONTENTS_GRATE, local, &data.enter_trace);

	if (data.enter_trace.fraction == 1.0f)
	{
		return false;
	}

	if (FireBullet(weapon, data.direction, damage))
	{
		return true;
	}

	//if (!HandleBulletPenetration(weaponData, data.enter_trace, eyepos, end, possibleHitsRemaining,
	//	currentDamage, penetrationPower, sv_penetration_type, ff_damage_reduction_bullets, ff_damage_bullet_penetration)) {
	//	damage = data.current_damage;
	//	return true;
	//}

	return false;
}
bool CBulletHandler::trace_awall(C_BaseEntity* m_local, C_BaseEntity* entity, Vector hit, float& damage)
{
	if (m_local->isAlive())
	{
		FireBulletData1 data(m_local->GetEyePosition());// = FireBulletData(m_local->GetEyePosition());
		data.filter = CTraceFilter();
		data.filter.pSkip1 = m_local;
		//QAngle eye_angle; g_pEngineClient->GetViewAngles(eye_angle);
		//Vector dst;// , forward;
		//Math::AngleVectors(eye_angle, forward);
		//dst = data.src + (m_local->GetEyePos() * 8196.f);
		QAngle angles;
		angles = GameUtils::CalculateAngle(data.src, hit);
		angles.Normalize();
		Math::AngleVectors(angles, data.direction);
		//VectorNormalize(data.direction);
		auto m_weapon = m_local->GetWeapon();
		if (m_weapon)
		{
			data.penetrate_count = 1;
			data.trace_length = 0.0f;
			CSWeaponInfo* weapon_data = m_weapon->GetCSWpnData();
			if (weapon_data)
			{
				data.current_damage = weapon_data->damage;
				data.trace_length_remaining = weapon_data->range - data.trace_length;
				Vector end = data.src + data.direction * data.trace_length_remaining;
				TraceLine(data.src, end, MASK_SHOT | CONTENTS_GRATE, m_local, &data.enter_trace);
				if (data.enter_trace.fraction == 1.0f)
				{
					return false;
				}
				if (handle_penetration)
				{
					damage = data.current_damage;
					return true;
				}
			}
		}
	}
	return false;
}

bool CBulletHandler::trace_awall(C_BaseEntity* m_local, Vector hit, float& damage)
{
	if (m_local && m_local->isAlive())
	{
		FireBulletData1 data(m_local->GetEyePosition());// = FireBulletData(m_local->GetEyePosition());
		data.filter = CTraceFilter();
		data.filter.pSkip1 = m_local;
		//QAngle eye_angle; g_pEngineClient->GetViewAngles(eye_angle);
		//Vector dst;// , forward;
		//Math::AngleVectors(eye_angle, forward);
		//dst = data.src + (m_local->GetEyePos() * 8196.f);
		QAngle angles;
		angles = GameUtils::CalculateAngle(data.src, hit);
		angles.Normalize();
		Math::AngleVectors(angles, data.direction);
		//VectorNormalize(data.direction);
		auto m_weapon = m_local->GetWeapon();
		if (m_weapon)
		{
			data.penetrate_count = 1;
			data.trace_length = 0.0f;
			CSWeaponInfo* weapon_data = m_weapon->GetCSWpnData();
			if (weapon_data)
			{
				data.current_damage = weapon_data->damage;
				data.trace_length_remaining = weapon_data->range - data.trace_length;
				Vector end = data.src + data.direction * data.trace_length_remaining;
				TraceLine(data.src, end, MASK_SHOT | CONTENTS_GRATE, m_local, &data.enter_trace);
				if (data.enter_trace.fraction == 1.0f)
				{
					return false;
				}
				if (handle_penetration)
				{
					damage = data.current_damage;
					return true;
				}
			}
		}
	}
	return false;
}


bool CBulletHandler::HandleBulletPenetration(CSWeaponInfo *wpn_data, FireBulletData1 &data)
{
	surfacedata_t *enter_surface_data = g_pPhysics->GetSurfaceData(data.enter_trace.surface.surfaceProps);
	int enter_material = enter_surface_data->game.material;
	float enter_surf_penetration_mod = *(float*)((DWORD)enter_surface_data + 88);

	data.trace_length += data.enter_trace.fraction * data.trace_length_remaining;
	data.current_damage *= pow((wpn_data->range_modifier), (data.trace_length * 0.002));

	if ((data.trace_length > 3000.f) || (enter_surf_penetration_mod < 0.1f))
		data.penetrate_count = 0;

	if (data.penetrate_count <= 0)
		return false;

	Vector dummy;
	trace_t trace_exit;
	if (!TraceToExit(dummy, data.enter_trace, data.enter_trace.endpos.x, data.enter_trace.endpos.y, data.enter_trace.endpos.z, data.direction.x, data.direction.y, data.direction.z, &trace_exit))
		return false;

	surfacedata_t *exit_surface_data = g_pPhysics->GetSurfaceData(trace_exit.surface.surfaceProps);
	int exit_material = exit_surface_data->game.material;

	float exit_surf_penetration_mod = *(float*)((DWORD)exit_surface_data + 88);
	float final_damage_modifier = 0.16f;
	float combined_penetration_modifier = 0.0f;

	if (((data.enter_trace.contents & CONTENTS_GRATE) != 0) || (enter_material == 89) || (enter_material == 71))
	{
		combined_penetration_modifier = 3.0f;
		final_damage_modifier = 0.05f;
	}
	else
	{
		combined_penetration_modifier = (enter_surf_penetration_mod + exit_surf_penetration_mod) * 0.5f;
	}

	if (enter_material == exit_material)
	{
		if (exit_material == 87 || exit_material == 85)
			combined_penetration_modifier = 3.0f;
		else if (exit_material == 76)
			combined_penetration_modifier = 2.0f;
	}

	float v34 = fmaxf(0.f, 1.0f / combined_penetration_modifier);
	float v35 = (data.current_damage * final_damage_modifier) + v34 * 3.0f * fmaxf(0.0f, (3.0f / wpn_data->penetration) * 1.25f);
	float thickness = (trace_exit.endpos - data.enter_trace.endpos).Length();

	thickness *= thickness;
	thickness *= v34;
	thickness /= 24.0f;

	float lost_damage = fmaxf(0.0f, v35 + thickness);

	if (lost_damage > data.current_damage)
		return false;

	if (lost_damage >= 0.0f)
		data.current_damage -= lost_damage;

	if (data.current_damage < 1.0f)
		return false;

	data.src = trace_exit.endpos;
	data.penetrate_count--;

	return true;
}

//
//bool CBulletHandler::HandleBulletPenetration(CCSWeaponInfo* wpn_data, FireBulletData& BulletData)
//{
//	typedef bool(__thiscall* HandleBulletPenetrationFn)(IBasePlayer*, float*, int*, char*, trace_t*, Vector*, int, float, float, int, int, float, int*, Vector*, int, int, float*);
//	static HandleBulletPenetrationFn BulletHandler = reinterpret_cast<HandleBulletPenetrationFn>(csgo->Utils.FindPatternV2("client_panorama.dll",
//		(PBYTE)"\x53\x8B\xDC\x83\xEC\x08\x83\xE4\xF8\x83\xC4\x04\x55\x8B\x6B\x04\x89\x6C\x24\x04\x8B\xEC\x83\xEC\x78\x56\x8B\x73\x2C",
//		"xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));
//
//	char usestaticvalues = 0;
//	auto enter_surface_data = interfaces.phys_props->GetSurfaceData(BulletData.enter_trace.surface.surfaceProps);
//	int material = enter_surface_data->game.material;
//
//	if (!BulletHandler)
//		return false;
//
//	csgo->InAutowall = true;
//	bool result = BulletHandler(csgo->local, &wpn_data->penetration, &material, wpn_data->weapon_name, &BulletData.enter_trace, &BulletData.direction, 0.f, enter_surface_data->game.flPenetrationModifier, enter_surface_data->game.flDamageModifier, 0, 0x1003, wpn_data->penetration, &BulletData.penetrate_count, &BulletData.src, 0.f, 0.f, &BulletData.current_damage);
//	csgo->InAutowall = false;
//	return result;
//}

bool CBulletHandler::PenetrateWall(C_BaseEntity * pPlayer, CBaseCombatWeapon * pWeapon, const Vector & vecPoint, float & flDamage)
{
	if (!pPlayer || !pWeapon)
		return false;

	FireBulletData1 BulletData(pPlayer->GetEyePosition());
	BulletData.filter.pSkip1 = pPlayer;

	Vector qAngles = GameUtils::CalculateAngle(BulletData.src, vecPoint);

	Math::AngleVectors(qAngles, &BulletData.direction);
	BulletData.direction.Normalize();

	if (!SimulateFireBullet(pPlayer, pWeapon, BulletData))
		return false;

	flDamage = BulletData.current_damage;

	return true;
}

bool CBulletHandler::SimulateFireBullet(C_BaseEntity* pPlayer, CBaseCombatWeapon* pWeapon, FireBulletData1& BulletData)
{
	if (!pPlayer || !pWeapon)
		return false;

	BulletData.penetrate_count = 4;
	BulletData.trace_length = 0.0f;
	CSWeaponInfo* c_weapon_data = pPlayer->GetWeapon()->GetCSWpnData();

	if (!c_weapon_data)
		return false;

	BulletData.current_damage = (float)c_weapon_data->damage;

	while ((BulletData.penetrate_count > 0) && (BulletData.current_damage >= 1.0f))
	{
		BulletData.trace_length_remaining = c_weapon_data->range - BulletData.trace_length;

		Vector end = BulletData.src + BulletData.direction * BulletData.trace_length_remaining;

		UTIL_TraceLine(BulletData.src, end, MASK_SHOT | CONTENTS_GRATE, pPlayer, &BulletData.enter_trace);
		UTIL_ClipTraceToPlayers(BulletData.src, end + BulletData.direction * 40.f, MASK_SHOT | CONTENTS_GRATE, &BulletData.filter, &BulletData.enter_trace);


		if (BulletData.enter_trace.fraction == 1.0f)
			break;

		if ((BulletData.enter_trace.hitgroup <= 7) && (BulletData.enter_trace.hitgroup > 0))
		{
			BulletData.trace_length += BulletData.enter_trace.fraction * BulletData.trace_length_remaining;
			BulletData.current_damage *= pow(c_weapon_data->range, BulletData.trace_length * 0.002);

			auto pEntity = reinterpret_cast<C_BaseEntity*>(BulletData.enter_trace.m_pEnt);
			if (pEntity->GetTeamNum() == c_ayala::l_player->GetTeamNum())
				return false;

			ScaleDamage(BulletData.enter_trace.hitgroup, pEntity, c_weapon_data->armor_ratio, BulletData.current_damage);
			return true;
		}

		if (HandleBulletPenetration(c_weapon_data, BulletData))
			break;
	}
	return false;
}
void CBulletHandler::ScaleDamage1(trace_t& enterTrace, CSWeaponInfo* weaponData, float& currentDamage)
{

	auto pLocal = c_ayala::l_player;//Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());
	CBaseCombatWeapon* weapon = pLocal->GetWeapon();//(C_BaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	bool hasHeavyArmor = false;
	int armorValue = ((C_BaseEntity*)enterTrace.m_pEnt)->GetArmor();
	int hitGroup = enterTrace.hitgroup;

	if (!pLocal)
	{
		return;
	}

	if (weapon->IsGrenade() || weapon->IsKnife())
	{
		return;
	}

	auto IsArmored = [&enterTrace]()->bool
	{
		C_BaseEntity* targetEntity = (C_BaseEntity*)enterTrace.m_pEnt;
		switch (enterTrace.hitgroup)
		{
		case HITGROUP_HEAD:
			return !!targetEntity->HasHelmet();
		case HITGROUP_GENERIC:
		case HITGROUP_CHEST:
		case HITGROUP_STOMACH:
		case HITGROUP_LEFTARM:
		case HITGROUP_RIGHTARM:
			return true;
		default:
			return false;
		}
	};

	switch (hitGroup)
	{
	case HITGROUP_HEAD:
		currentDamage *= 2.f;
		break;
	case HITGROUP_STOMACH:
		currentDamage *= 1.25f;
		break;
	case HITGROUP_LEFTLEG:
	case HITGROUP_RIGHTLEG:
		currentDamage *= 0.75f;
		break;
	default:
		break;
	}

	if (armorValue > 0 && IsArmored())
	{
		float bonusValue = 1.f, armorBonusRatio = 0.5f, armorRatio = weaponData->armor_ratio / 2.f;

		if (hasHeavyArmor)
		{
			armorBonusRatio = 0.33f;
			armorRatio *= 0.5f;
			bonusValue = 0.33f;
		}

		auto NewDamage = currentDamage * armorRatio;

		if (((currentDamage - (currentDamage * armorRatio)) * (bonusValue * armorBonusRatio)) > armorValue)
		{
			NewDamage = currentDamage - (armorValue / armorBonusRatio);
		}

		currentDamage = NewDamage;
	}
}

void CBulletHandler::ScaleDamage(int iHitgroup, C_BaseEntity* pPlayer, float flWeaponArmorRatio, float& flDamage)
{
	int armor = pPlayer->GetArmor();
	float ratio;

	switch (iHitgroup)
	{
	case HITGROUP_HEAD:
		flDamage *= 4.f;
		break;
	case HITGROUP_STOMACH:
		flDamage *= 1.25f;
		break;
	case HITGROUP_LEFTLEG:
	case HITGROUP_RIGHTLEG:
		flDamage *= 0.75f;
		break;
	}

	if (armor > 0)
	{
		switch (iHitgroup)
		{
		case HITGROUP_HEAD:
			if (pPlayer->HasHelmet())
			{
				ratio = (flWeaponArmorRatio * 0.5) * flDamage;
				if (((flDamage - ratio) * 0.5) > armor)
					ratio = flDamage - (armor * 2.0);
				flDamage = ratio;
			}
			break;
		case HITGROUP_GENERIC:
		case HITGROUP_CHEST:
		case HITGROUP_STOMACH:
		case HITGROUP_LEFTARM:
		case HITGROUP_RIGHTARM:
			ratio = (flWeaponArmorRatio * 0.5) * flDamage;
			if (((flDamage - ratio) * 0.5) > armor)
				ratio = flDamage - (armor * 2.0);
			flDamage = ratio;
			break;
		}
	}
}
float CBulletHandler::GetDmg(C_BaseEntity * pPlayer, Vector point/*, float& damage*/)
{
	if (!pPlayer || !c_ayala::l_player)
		return false;

	if (!c_ayala::c_weapon)
		return false;

	float damage = 0.f;

	FireBulletData1 BulletData(c_ayala::l_player->GetEyePosition());
	BulletData.filter.pSkip1 = c_ayala::l_player;

	Vector qAngles = GameUtils::CalculateAngle(BulletData.src, point);

	Math::AngleVectors(qAngles, &BulletData.direction);
	BulletData.direction.Normalize();

	if (!SimulateFireBullet(pPlayer, c_ayala::c_weapon, BulletData))
		return false;

	/*if (BulletData.current_damage < Menu.Ragebot.Mindamage && BulletData.current_damage < pPlayer->GetHealth())
		return false;*/

	damage = BulletData.current_damage;

	return damage;
}

bool CBulletHandler::IsPenetrable(C_BaseEntity * pLocal, C_BaseEntity * pPlayer, Vector point/*, float& damage*/)
{
	if (!pPlayer || !pLocal)
		return false;

	if (!c_ayala::c_weapon)
		return false;

	FireBulletData1 BulletData(pLocal->GetEyePosition());
	BulletData.filter.pSkip1 = pLocal;

	Vector qAngles = GameUtils::CalculateAngle(BulletData.src, point);

	Math::AngleVectors(qAngles, &BulletData.direction);
	BulletData.direction.Normalize();

	if (!SimulateFireBullet(pLocal, c_ayala::c_weapon, BulletData))
		return false;

	if (BulletData.current_damage < Menu.Ragebot.Mindamage && BulletData.current_damage < pPlayer->GetHealth())
		return false;

	//damage = BulletData.current_damage;

	return true;
}

void CBulletHandler::UTIL_TraceLine(Vector& vecStart, Vector& vecEnd, unsigned int nMask, C_BaseEntity* pCSIgnore, trace_t* pTrace)
{
	Ray_t ray;
	ray.Init(vecStart, vecEnd);

	CTraceFilter filter;
	filter.pSkip1 = pCSIgnore;
	g_pEngineTrace->TraceRay(ray, nMask, &filter, pTrace);
}

void CBulletHandler::UTIL_ClipTraceToPlayers(Vector& vecAbsStart, Vector& vecAbsEnd, unsigned int mask, ITraceFilter* filter, trace_t* tr)
{
	static uintptr_t dwAddress;

	if (!dwAddress)
		dwAddress = Utilities::Memory::FindPatternIDA(XorStr("client_panorama.dll"), XorStr("53 8B DC 83 EC 08 83 E4 F0 83 C4 04 55 8B 6B 04 89 6C 24 04 8B EC 81 EC ? ? ? ? 8B 43 10"));

	_asm
	{
		MOV		EAX, filter
		LEA		ECX, tr
		PUSH	ECX
		PUSH	EAX
		PUSH	mask
		LEA		EDX, vecAbsEnd
		LEA		ECX, vecAbsStart
		CALL	dwAddress
		ADD		ESP, 0xC
	}
}