﻿#include "hooks.h"
#include "global.h"
#include "Misc.h"
#include "Menu.h"
#include "fake_latency.hpp"
#include "Math.h"
#include "GameUtils.h"
#include "c_track-manager.hpp"
#include "aim.h"
#include "c_predict-sys.hpp"
#include "antim-aims.h"
#include "animation_sync.h"
#include "legit-bot.h"
#include <Psapi.h>

static CPredictionSystem* Prediction = new CPredictionSystem();



static int Ticks = 0;
static int LastReserve = 0;
namespace FakeLatency
{
	char *clientdllstr = new char[11]{ 25, 22, 19, 31, 20, 14, 84, 30, 22, 22, 0 }; /*client.dll*/
	char *enginedllstr = new char[11]{ 31, 20, 29, 19, 20, 31, 84, 30, 22, 22, 0 }; /*engine.dll*/
	char *cam_tofirstperson_sig = new char[51]{ 59, 75, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 56, 67, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 60, 60, 90, 90, 67, 74, 90, 90, 67, 74, 0 }; /*A1  ??  ??  ??  ??  B9  ??  ??  ??  ??  FF  90  90*/
	char *cam_tothirdperson_sig = new char[51]{ 59, 75, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 56, 67, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 60, 60, 90, 90, 67, 74, 90, 90, 66, 57, 0 }; /*A1  ??  ??  ??  ??  B9  ??  ??  ??  ??  FF  90  8C*/
	char *clientstatestr = new char[51]{ 66, 56, 90, 90, 73, 62, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 66, 59, 90, 90, 60, 67, 90, 90, 60, 73, 90, 90, 74, 60, 90, 90, 75, 75, 90, 90, 78, 79, 90, 90, 60, 57, 0 }; /*8B  3D  ??  ??  ??  ??  8A  F9  F3  0F  11  45  FC*/
	char *updateclientsideanimfnsigstr = new char[95]{ 79, 79, 90, 90, 66, 56, 90, 90, 63, 57, 90, 90, 79, 75, 90, 90, 79, 76, 90, 90, 66, 56, 90, 90, 60, 75, 90, 90, 66, 74, 90, 90, 56, 63, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 74, 74, 90, 90, 74, 74, 90, 90, 74, 74, 90, 90, 77, 78, 90, 90, 73, 76, 90, 90, 66, 56, 90, 90, 74, 76, 90, 90, 60, 60, 90, 90, 67, 74, 90, 90, 69, 69, 90, 90, 69, 69, 90, 90, 74, 74, 90, 90, 74, 74, 0 }; /*55  8B  EC  51  56  8B  F1  80  BE  ??  ??  00  00  00  74  36  8B  06  FF  90  ??  ??  00  00*/
	void DecStr(char *adr, const unsigned len)
	{
		for (unsigned i = 0; i < len; i++)
		{
			adr[i] ^= 50;
			adr[i] ^= 72;
		}
	}

	void EncStr(char *adr, const unsigned len)
	{
		for (unsigned i = 0; i < len; i++)
		{
			adr[i] ^= 72;
			adr[i] ^= 50;
		}
	}

	void DecStr(char *adr)
	{
		int len = strlen(adr);
		for (unsigned i = 0; i < len; i++)
		{
			adr[i] ^= 50;
			adr[i] ^= 72;
		}
	}

	void EncStr(char *adr)
	{
		int len = strlen(adr);
		for (unsigned i = 0; i < len; i++)
		{
			adr[i] ^= 72;
			adr[i] ^= 50;
		}
	}
	HANDLE FindHandle(std::string name)
	{
		return GetModuleHandle(name.c_str());
	}
	uintptr_t FindMemoryPattern(HANDLE ModuleHandle, char* strpattern, int length)
	{
		unsigned char *signature = new unsigned char[length + 1];
		bool *skippable = new bool[length + 1];
		int signaturelength = 0;
		for (int byteoffset = 0; byteoffset < length - 1; byteoffset += 2)
		{
			char charhex[4];  
			*(short*)charhex = *(short*)&strpattern[byteoffset];
			if (charhex[0] != ' ')
			{
				if (charhex[0] == '?')
				{
					signature[signaturelength] = '?';
					skippable[signaturelength] = true;
				}
				else
				{
					charhex[2] = NULL;  
					signature[signaturelength] = (unsigned char)std::stoul(charhex, nullptr, 16);
					skippable[signaturelength] = false;
				}
				signaturelength++;
			}
		}
		int searchoffset = 0;
		int maxoffset = signaturelength - 1;


		MODULEINFO dllinfo;
		GetModuleInformation(GetCurrentProcess(), (HMODULE)ModuleHandle, &dllinfo, sizeof(MODULEINFO));
		DWORD endadr = (DWORD)ModuleHandle + dllinfo.SizeOfImage;
		DWORD adrafterfirstmatch = NULL;
		for (DWORD adr = (DWORD)ModuleHandle; adr < endadr; adr++)
		{
			if (skippable[searchoffset] || *(char*)adr == signature[searchoffset] || *(unsigned char*)adr == signature[searchoffset])
			{
				if (searchoffset == 0)
				{
					adrafterfirstmatch = adr + 1;
				}
				searchoffset++;
				if (searchoffset > maxoffset)
				{
					delete[] signature;
					delete[] skippable;
					return adr - maxoffset;
				}
			}
			else if (adrafterfirstmatch)
			{
				adr = adrafterfirstmatch;
				searchoffset = 0;
				adrafterfirstmatch = NULL;
			}
		}

		delete[] signature;
		delete[] skippable;
		return NULL;
	}
	HANDLE EngineHandle = NULL;
	HANDLE ClientHandle = NULL;
	DWORD ClientState = NULL;
}

int __fastcall Hooked_SendDatagram(INetChannel* netchan, void*, bf_write* datagram)
{
	if (!g_pEngine->IsConnected() || !g_pEngine->IsInGame())
		return HNetchan->GetOriginalMethod<SendDatagramFn>(46)(netchan, datagram);
	int instate = netchan->m_nInReliableState;
	int insequencenr = netchan->m_nInSequenceNr;

	if (Menu.Ragebot.FakeLatency)
		g_BacktrackHelper->AddLatencyToNetchan(netchan, Menu.Ragebot.FakeLatencyAmount);

	int ret = HNetchan->GetOriginalMethod<SendDatagramFn>(46)(netchan, datagram);

	netchan->m_nInReliableState = instate;
	netchan->m_nInSequenceNr = insequencenr;
	const auto current_choke = netchan->m_nChokedPackets;
	netchan->m_nChokedPackets = 0;
	ret;
	--netchan->m_nOutSequenceNr;
	netchan->m_nChokedPackets = current_choke;
	return ret;

}
void FakeDuck()
{
	c_ayala::user_cmd->buttons |= IN_BULLRUSH;
	if (Menu.Ragebot.VelocityReduce)
	{
		c_ayala::user_cmd->buttons |= IN_BULLRUSH;
		bool do_once = false, _do;
		auto NetChannel = g_pEngine->GetNetChannel();
		DWORD ClientState = *(DWORD*)FakeLatency::ClientState;
		int32_t chocked = *(DWORD*)(ClientState + 0x4D28);
		int limit = Menu.Misc.FakelagAmount / 2;
		bool crouch = chocked > limit;
		
		if (crouch)
			c_ayala::user_cmd->buttons |= IN_DUCK;
		else
			c_ayala::user_cmd->buttons &= ~IN_DUCK;
	}
}
bool __fastcall Hooks::CreateMove(void* thisptr, void*, float flInputSampleTime, c_user_cmd* cmd)
{
	if (!g_pEngine->IsConnected() || !g_pEngine->IsInGame())
	{
		if (HNetchan)
		{
			HNetchan->ReleaseVMT();
			HNetchan = nullptr;
		}
		return true;
	}
	if (Menu.Ragebot.FakeLatency)
	{
		if (!HNetchan)
		{
			DWORD ClientState = *(DWORD*)FakeLatency::ClientState;

			if (ClientState)
			{
				DWORD NetChannel = *(DWORD*)(*(DWORD*)FakeLatency::ClientState + 0x9C);
				if (NetChannel)
				{
					HNetchan = new VMT((DWORD**)NetChannel);
					HNetchan->HookVM((void*)Hooked_SendDatagram, 46);
					HNetchan->ApplyVMT();
				}
			}

		}
	}
	else
	{
		if (HNetchan)
		{
			HNetchan->ReleaseVMT();
			HNetchan = nullptr;
		}
	}
	if (cmd)
	{
		if (!cmd->command_number)
			return true;

		static bool last_c_SendPacket;
		QAngle org_view = cmd->viewangles;

		C_BaseEntity* pl_player = g_pEntitylist->GetClientEntity(g_pEngine->Getl_player());
		CBaseCombatWeapon* pWeapon = pl_player->GetWeapon();

		PVOID pebp;
		__asm mov pebp, ebp;
		bool* pbc_SendPacket = (bool*)(*(PDWORD)pebp - 0x1C);
		bool& bc_SendPacket = *pbc_SendPacket;
		if (pl_player)
		{
			c_ayala::l_player = pl_player;
			c_ayala::user_cmd = cmd;

			if (pl_player->GetHealth() > 0)
			{
				if (Menu.Ragebot.FakeLatency)
					g_BacktrackHelper->UpdateIncomingSequences();
				c_ayala::vecUnpredictedVel = c_ayala::l_player->GetVelocity();
				if(pWeapon)
				{
				c_ayala::c_weapon = pWeapon;
				c_ayala::c_weapon_data = pWeapon->GetCSWpnData();
				}
				g_Misc->Bunnyhop();
				g_Misc->AutoStrafe();
	
				CBackTrackManager::Get().UpdateTicks();

				if (Menu.LegitBot.bEnable)
					CLegitBot::Get().Run();
				if (Menu.Ragebot.EnableAimbot)
				{
    				g_Antiaim->Run();
					CRageBot::Get().Run();
				}
				animation_sync::Get().creat_move_part();

				g_Misc->FixMovement();
				g_Misc->FixCmd();
				FakeDuck();
				cmd = c_ayala::user_cmd;
				bc_SendPacket = c_ayala::c_SendPacket;
				//grenade_prediction::instance().Tick(c_ayala::user_cmd->buttons);
				c_ayala::RealAngle = c_ayala::user_cmd->viewangles;
			}
			else
				c_ayala::c_SendPacket = true;
		}
	}
	return false;
}

int nTickBaseShift = 5;
bool bInSendMove =  false;
bool bFirstSendMovePack = false;
bool __fastcall Hooks::Writeuser_cmdDeltaToBuffer(void* *ECX, void* EDX, int nSlot, bf_write* buf, int from, int to, bool isNewCmd)
{
	return /*clientVMT->GetOriginalMethod<Writeuser_cmdDeltaToBuffer>(24)(ECX, nSlot, buf, from, to, isNewCmd)*/false;
	//
	//if (nTickBaseShift <= 0)
	//{
	//	nTickBaseShift = 5;
	//	return clientVMT->GetOriginalMethod<Write::user_cmdDeltaToBufferFn>(24)(ECX, nSlot, buf, from, to, isNewCmd);
	//}

	//if (from != -1)
	//	return true;

	//// CL_SendMove function
	//auto CL_SendMove = []()
	//{
	//	using CL_SendMove_t = void(__fastcall*)(void);
	//	static CL_SendMove_t CL_SendMoveF = (CL_SendMove_t)FindPatternIDA(("engine.dll"), ("55 8B EC A1 ? ? ? ? 81 EC ? ? ? ? B9 ? ? ? ? 53 8B 98"));

	//	CL_SendMoveF();
	//};

	//// Write::user_cmd function
	//auto Write::user_cmd = [](bf_write *buf, c_::user_cmd *in, c_::user_cmd *out)
	//{
	//	//using Write::user_cmd_t = void(__fastcall*)(bf_write*, c_::user_cmd*, c_::user_cmd*);
	//	static DWORD Write::user_cmdF = (DWORD)FindPatternIDA(("client_panorama.dll"), ("55 8B EC 83 E4 F8 51 53 56 8B D9 8B 0D"));

	//	__asm
	//	{
	//		mov     ecx, buf
	//		mov     edx, in
	//		push	out
	//		call    Write::user_cmdF
	//		add     esp, 4
	//	}
	//};

	///*uintptr_t framePtr;
	//__asm mov framePtr, ebp;
	//auto msg = reinterpret_cast<CCLCMsg_Move_t*>(framePtr + 0xFCC);*/
	//int *pNumBackupCommands = (int*)(reinterpret_cast<uintptr_t>(buf) - 0x30);
	//int *pNumNewCommands = (int*)(reinterpret_cast<uintptr_t>(buf) - 0x2C);
	//auto net_channel = g_pClientState->m_NetChannel;

	//int32_t new_commands = *pNumNewCommands;

	//if (!bInSendMove)
	//{
	//	if (new_commands <= 0)
	//		return false;

	//	bInSendMove = true;
	//	bFirstSendMovePack = true;
	//	nTickBaseShift += new_commands;

	//	while (nTickBaseShift > 0)
	//	{
	//		CL_SendMove();
	//		//net_channel->Transmit(false);
	//		bFirstSendMovePack = false;
	//	}

	//	bInSendMove = false;
	//	return false;
	//}

	//if (!bFirstSendMovePack)
	//{
	//	int32_t loss = min(nTickBaseShift, 10);

	//	nTickBaseShift -= loss;
	//	net_channel->m_nOutSequenceNr += loss;
	//}

	//int32_t next_cmdnr = g_pClientState->lastoutgoingcommand + g_pClientState->chokedcommands + 1;
	//int32_t total_new_commands = min(nTickBaseShift, 62);
	//nTickBaseShift -= total_new_commands;

	//from = -1;
	//*pNumNewCommands = total_new_commands;
	//*pNumBackupCommands = 0;

	//for (to = next_cmdnr - new_commands + 1; to <= next_cmdnr; to++)
	//{
	//	if (!clientVMT->GetOriginalMethod<Write::user_cmdDeltaToBufferFn>(24)(ECX, nSlot, buf, from, to, true))
	//		return false;

	//	from = to;
	//}

	//c_::user_cmd *last_realCmd = g_pInput->Get::user_cmd(nSlot, from);
	//c_::user_cmd fromCmd;

	//if (last_realCmd)
	//	fromCmd = *last_realCmd;

	//c_::user_cmd toCmd = fromCmd;
	//toCmd.command_number++;
	//toCmd.tick_count += 200;

	//for (int i = new_commands; i <= total_new_commands; i++)
	//{
	//	Write::user_cmd(buf, &toCmd, &fromCmd);
	//	fromCmd = toCmd;
	//	toCmd.command_number++;
	//	toCmd.tick_count++;
	//}

	//return true;
}