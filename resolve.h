#pragma once
#include "Singleton.h"
#include "c_track-manager.hpp"

class Resolve : public Singleton<Resolve>
{
private:
	int NormalTicks[64];
	int lastlbymove[64];
	int missedshot[64];
	//float NextLBYUpdate[64];
	//float oldlby[64];
	float DetectionFloat[64];
	float Add[64];
	Vector OldVelocity[64];
	bool ChengeLBYAfterDormant[64];
	enum
	{
		back,
		right,
		left
	};
public:
	bool lbypredict(C_BaseEntity * entity);
	std::vector<float> GetYaw(C_BaseEntity * entity);
	float GetClosetYaw(C_BaseEntity * entity);
	bool fakewalking(C_BaseEntity * entity);
	float AntiFreestand(C_BaseEntity * entity);
	RecordTick_t Resolver(RecordTick_t tick);
};