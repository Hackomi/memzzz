#pragma once
#include "sdk.h"
namespace GameUtils
{
	bool WorldToScreen(const Vector& in, Vector& position);
	std::vector<Vector> GetMultiplePointsForHitbox(C_BaseEntity* pBaseEntity, int iHitbox, VMatrix BoneMatrix[128]);

	float BestHitPoint(C_BaseEntity * player, int prioritized, float minDmg, mstudiohitboxset_t * hitset, VMatrix matrix[], Vector & vecOut);

	Vector CalculateBestPoint(C_BaseEntity * player, int prioritized, float minDmg, bool onlyPrioritized, VMatrix matrix[]);
	


	bool isVisible(C_BaseEntity* lul, int bone);
	float GetFov(const Vector& viewAngle, const Vector& aimAngle);
	float GetFoV(QAngle qAngles, Vector vecSource, Vector vecDestination, bool bDistanceBased);
	QAngle CalculateAngle(Vector vecOrigin, Vector vecOther);
	float server_time();
	float GetCurTime();
	Vector GetBonePosition(C_BaseEntity * pPlayer, int Bone, VMatrix MatrixArray[128]);
	Vector GetBonePosition(C_BaseEntity * pPlayer, int Bone, VMatrix MatrixArray[128]);
	void TraceLine(Vector& vecAbsStart, Vector& vecAbsEnd, unsigned int mask, C_BaseEntity* ignore, trace_t* ptr);
	bool IsVisible_Fix(Vector vecOrigin, Vector vecOther, unsigned int mask, C_BaseEntity* pC_BaseEntity, C_BaseEntity* pIgnore, int& hitgroup);
	bool IsAbleToShoot();
	bool IsBreakableEntity(C_BaseEntity* pBaseEntity);
	void UTIL_TraceLine_simpleWay(Vector & vecAbsStart, Vector & vecAbsEnd, unsigned int mask, C_BaseEntity * ign, trace_t * tr);
	void UTIL_TraceLine(const Vector & vecAbsStart, const Vector & vecAbsEnd, unsigned int mask, C_BaseEntity * ignore, int collisionGroup, trace_t * ptr);
	void UTIL_ClipTraceToPlayers(const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, ITraceFilter* filter, trace_t* tr);
	void ClipTraceToPlayers(const Vector & vecAbsStart, const Vector & vecAbsEnd, unsigned int mask, ITraceFilter * filter, CGameTrace * tr);
	void calculate_angle(Vector src, Vector dst, Vector &angles);
	char* GetWeaponName(int confignum);
	
}