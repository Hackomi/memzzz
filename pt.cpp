﻿#include "hooks.h"
#include "GameUtils.h"
#include "Draw.h"
//#include "Menu.h"
#include "Misc.h"
#include "global.h"
#include "SpoofedConvar.h"
#include "Hitmarker.h"
#include "global.h"
#include "AW_hitmarker.h"
#include "event_log.h"
#include <memory>
#include "bullet_log.h"
#include <iomanip>     
#include "antim-aims.h"
#include "filatovgui.h"

#define TICK_INTERVAL			( g_globals->interval_per_tick )
#define TIME_TO_TICKS( dt )		( floorf(( 0.5f + (float)(dt) / TICK_INTERVAL ) ) )
#define TICKS_TO_TIME( t )		( TICK_INTERVAL *( t ) )

bool first_frame_passed = false;
void DrawPixel(int x, int y, Color col)
{
	g_pSurface->SetDrawColor(col);
	g_pSurface->DrawFilledRect(x, y, x + 1, y + 1);
}

void __fastcall Hooks::PaintTraverse(void* ecx/*thisptr*/, void* edx, unsigned int vgui_panel, bool force_repaint, bool allow_force) // cl
{
	if (Menu.Visuals.Noscope && strcmp("HudZoom", g_pPanel->GetName(vgui_panel)) == 0)
		return;

	panelVMT->GetOriginalMethod<PaintTraverseFn>(41)(ecx, vgui_panel, force_repaint, allow_force);

	static bool bSpoofed = false;
	DrawPixel(1, 1, Color(0, 0, 0));

	if (Menu.Misc.TPKey > 0 && !bSpoofed)
	{
		ConVar* svcheats = g_pCvar->FindVar("sv_cheats");
		SpoofedConvar* svcheatsspoof = new SpoofedConvar(svcheats);
		svcheatsspoof->SetInt(1);
		bSpoofed = true;
	}

	static bool spoofed = false;
	if (!spoofed) {
		ConVar* sv_showimpacts = g_pCvar->FindVar("sv_showimpacts");
		SpoofedConvar* sv_showimpacts_spoofed = new SpoofedConvar(sv_showimpacts);
		sv_showimpacts_spoofed->SetInt(2);
		spoofed = true;
	}

	if (!strstr(g_pPanel->GetName(vgui_panel), XorStr("MatSystemTopPanel")))
		return;

	hitmarker_2->paint();

	int cur_height, cur_width; g_pEngine->GetScreenSize(cur_width, cur_height);

	if (!first_frame_passed || cur_width != c_ayala::Screen.width || cur_height != c_ayala::Screen.height)
	{
		first_frame_passed = true;
		g_pEngine->GetScreenSize(cur_width, cur_height);
		c_ayala::Screen.height = cur_height;
		c_ayala::Screen.width = cur_width;
	}
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			c_ayala::viewMatrix[i][j] = g_pEngine->WorldToScreenMatrix()[i][j];
		}
	}
	if (c_ayala::Opened)
		CMenu::Get().Render();
	
	if (Menu.Visuals.RemoveParticles)
	{
		auto postprocessing = g_pCvar->FindVar("mat_postprocess_enable");
		auto postprocessingspoof = new SpoofedConvar(postprocessing);
		postprocessingspoof->SetInt(0);
	}
}