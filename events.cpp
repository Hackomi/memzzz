﻿#include "hooks.h"
#include <time.h>
#include "Mmsystem.h"
#include <thread>
#include "Hitmarker.h"
#include "global.h"
#include "Menu.h"
#include "controls.h"
#include "Math.h"
#include "bullet_log.h"
#include "event_log.h"
#include "AW_hitmarker.h"
#include "sounds.h"
#include "Render.h"
#include "fake_latency.hpp"
#include "resolve.h"
#pragma comment(lib, "winmm.lib") 

#define EVENT_HOOK( x )
#define TICK_INTERVAL			(g_globals->interval_per_tick)
#define TIME_TO_TICKS( dt )		( (int)( 0.5f + (float)(dt) / TICK_INTERVAL ) )

std::vector<impact_info> impacts;
std::vector<hitmarker_info> hitmarkers;

cGameEvent g_Event;

std::vector<cbullet_tracer_info> logs;

std::vector<trace_info> trace_logs;

std::deque<Vector> dequeBulletImpacts;

bool bPlayerHurt[64]; //когда player_hurt called
bool bBulletImpact[64]; //когда bullet_impact called

C_BaseEntity* get_player(int userid) {
	int i = g_pEngine->GetPlayerForUserID(userid);
	return g_pEntitylist->GetClientEntity(i);
}

float cur() {
	int g_tick = 0;
	c_user_cmd* g_pLastCmd = nullptr;
	if (!g_pLastCmd || g_pLastCmd->hasbeenpredicted) {
		g_tick = c_ayala::l_player->GetTickBase();
	}
	else {
		++g_tick;
	}
	g_pLastCmd = c_ayala::user_cmd;
	float curtime = g_tick * g_globals->interval_per_tick;
	return curtime;
}

int showtime = 3;


char* HitgroupToName(int hitgroup)
{
	switch (hitgroup)
	{
	case HITGROUP_HEAD:
		return "head";
	case HITGROUP_CHEST:
		return "chest";
	case HITGROUP_STOMACH:
		return "stomach";
	case HITGROUP_LEFTARM:
		return "left arm";
	case HITGROUP_RIGHTARM:
		return "right arm";
	case HITGROUP_LEFTLEG:
		return "left leg";
	case HITGROUP_RIGHTLEG:
		return "right leg";
	default:
		return "body";

	}
}

player_info_t GetInfo(int Index)
{
	player_info_t Info;
	g_pEngine->GetPlayerInfo(Index, &Info);
	return Info;
}

void cGameEvent::FireGameEvent(IGameEvent *event)
{
	/*short   m_nUserID		user ID who was hurt
	short	attacker	user ID who attacked
	byte	health		remaining health points
	byte	armor		remaining armor points
	string	weapon		weapon name attacker used, if not the world
	short	dmg_health	damage done to health
	byte	dmg_armor	damage done to armor
	byte	hitgroup	hitgroup that was damaged*/
	const char* szEventName = event->GetName();
	if (!szEventName)
		return;

	if (!strcmp(szEventName, "round_end"))
		c_ayala::ClearRoundData = true;

	if (!strcmp(szEventName, "round_prestart"))
		c_ayala::NewRound = true;

	if (!strcmp(szEventName, "round_start"))
	{
		c_ayala::NewRound = false;
		c_ayala::ClearRoundData = false;
	}
	if (strstr(szEventName, "round_start"))
	{
		c_ayala::ClearFeed = true;
	}
	else
	{
		c_ayala::ClearFeed = false;
	}

	if (Menu.Visuals.Hitmarker)
	{
		if (strcmp(szEventName, "player_hurt") == 0)
		{
			auto attacker = get_player(event->GetInt("attacker"));
			auto victim = get_player(event->GetInt("userid"));

			if (!attacker || !victim || attacker != c_ayala::l_player)
				return;

			Vector enemypos = victim->GetOrigin();
			impact_info best_impact;
			float best_impact_distance = -1;
			float time = g_globals->curtime;

			for (int i = 0; i < impacts.size(); i++)
			{
				auto iter = impacts[i];
				if (time > iter.time + 1.f)
				{
					impacts.erase(impacts.begin() + i);
					continue;
				}
				Vector position = Vector(iter.x, iter.y, iter.z);
				float distance = position.DistTo(enemypos);
				if (distance < best_impact_distance || best_impact_distance == -1)
				{
					best_impact_distance = distance;
					best_impact = iter;
				}
			}
			if (best_impact_distance == -1)
				return;

			hitmarker_info info;
			info.impact = best_impact;
			info.alpha = 255;

			hitmarkers.push_back(info);

			int Attacker = event->GetInt("attacker");

			if (g_pEngine->GetPlayerForUserID(Attacker) == g_pEngine->Getl_player())
			{
				switch (Menu.Visuals.htSound)
				{
				case 0:
					PlaySoundA(hitmarker_sound, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 1:
					PlaySoundA(hitmarker_sound2, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 2:
					PlaySoundA(hitmarker_sound3, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 3:
					PlaySoundA(porn, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 4:
					PlaySoundA(piu, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 5:
					PlaySoundA(one, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 6:
					PlaySoundA(water, nullptr, SND_MEMORY | SND_ASYNC | SND_NOSTOP);
					break;
				case 7:
					g_pEngine->ClientCmd_Unrestricted("play buttons\\arena_switch_press_02.wav");
					break;
				}
			}
		}
	}
	if (strstr(event->GetName(), "bullet_impact"))
	{
		C_BaseEntity* ent = get_player(event->GetInt("userid"));
		if (!ent || ent != c_ayala::l_player)
			return;

		impact_info info;
		info.x = event->GetFloat("x");
		info.y = event->GetFloat("y");
		info.z = event->GetFloat("z");

		info.time = g_globals->curtime;

		impacts.push_back(info);
	}

	if (strcmp(szEventName, "bullet_impact") == 0)
	{
		auto* index = g_pEntitylist->GetClientEntity(g_pEngine->GetPlayerForUserID(event->GetInt("userid")));

		if (c_ayala::l_player)
		{
			Vector position(event->GetFloat("x"), event->GetFloat("y"), event->GetFloat("z"));

			if (index)

				trace_logs.push_back(trace_info(index->GetEyePosition(), position, g_globals->curtime, event->GetInt("userid")));
		}
	}

	if (strstr(event->GetName(), "bullet_impact"))
	{
		auto index = g_pEngine->GetPlayerForUserID(event->GetInt("userid"));

		if (index != g_pEngine->Getl_player())
			return;

		auto local = static_cast<C_BaseEntity*>(g_pEntitylist->GetClientEntity(index)); // nahuya debil c_ayala::l_player
		if (!local)
			return;

		Vector position(event->GetFloat("x"), event->GetFloat("y"), event->GetFloat("z"));

		Ray_t ray;
		ray.Init(local->GetEyePosition(), position);

		CTraceFilter filter;
		filter.pSkip1 = local;

		trace_t tr;
		g_pEngineTrace->TraceRay(ray, MASK_SHOT, &filter, &tr);

		float Red, Green, Blue;
		Red = Menu.Colors.Bulletracer[0] * 255;
		Green = Menu.Colors.Bulletracer[1] * 255;
		Blue = Menu.Colors.Bulletracer[2] * 255;
		logs.push_back(cbullet_tracer_info(local->GetEyePosition(), position, g_globals->curtime, Color(Red, Green, Blue)));
	}
}


int cGameEvent::GetEventDebugID()
{
	return 42;
}

void cGameEvent::RegisterSelf()
{
	g_pGameEventManager->AddListener(this, "player_connect", false);
	g_pGameEventManager->AddListener(this, "player_hurt", false);
	g_pGameEventManager->AddListener(this, "round_start", false);
	g_pGameEventManager->AddListener(this, "round_end", false);
	g_pGameEventManager->AddListener(this, "round_prestart", false);
	g_pGameEventManager->AddListener(this, "player_death", false);
	g_pGameEventManager->AddListener(this, "weapon_fire", false);
	g_pGameEventManager->AddListener(this, "bullet_impact", false);
}

void cGameEvent::Register()
{
	EVENT_HOOK(FireEvent);
}
