#pragma once
#include "Singleton.h"
struct FireBulletData
{
	Vector src;
	trace_t enter_trace;
	Vector direction;
	CTraceFilter filter;
	float trace_length;
	float trace_length_remaining;
	float current_damage;
	int penetrate_count;
};
class Autowall : public Singleton<Autowall>
{
public:
	struct Autowall_Return_Info
	{
		int damage;
		int percent;
		int hitgroup;
		int penetration_count;
		bool did_penetrate_wall;
		float thickness;
		Vector end;
		C_BaseEntity* hit_entity;
	};
	struct Autowall_Info
	{
		Vector start;
		Vector end;
		Vector current_position;
		Vector direction;

		ITraceFilter* filter;
		trace_t enter_trace;
		float trace_length;
		float trace_length_remaining;
		float thickness;
		float current_damage;
		int penetration_count;
	};
	  void ScaleDamage(C_BaseEntity * entity, CSWeaponInfo * weapon_info, int hitgroup, float & current_damage);
	  bool CanPenetrate(CSWeaponInfo * wpn_data, Autowall_Info & data);
	Autowall_Return_Info CalculateDamage(Vector start, Vector end, C_BaseEntity* from_entity = nullptr, C_BaseEntity* to_entity = nullptr, int specific_hitgroup = -1);

	bool CanPenetrate(C_BaseEntity* attacker, Autowall_Info& info, CSWeaponInfo* weapon_data);
	inline bool IsAutowalling() const
	{
		return is_autowalling;
	}
private:
	bool is_autowalling = false;
};

extern Autowall autowall;