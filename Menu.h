#pragma once
#include "sdk.h"
#include "Singleton.h"
class CMenu : public Singleton<CMenu>
{
private:
	int x = 0, y = 0;
public:
	void DrawBackground();
	void DrawTabsButton();
	void DrawMain();
};

