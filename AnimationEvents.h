#pragma once
#include "sdk.h"
#include "Singleton.h"
enum EAnimState
{
	ANIM_READY = 0,
	ANIM_PROCESS,
	ANIM_WAIT
};


struct AnimEvent_t
{
	std::string szEventName = XorStr("Error");
	float StartValue = -1;
	float EndValue = -1;
	float flStartTime = -1;
	float flEndTime = -1;
	int iState = ANIM_READY;
	float* pOutValue = nullptr;
	bool operator== (std::string name);
};

class CAnimationEventManager : public Singleton<CAnimationEventManager>
{
private:
	std::deque<AnimEvent_t> AnimationList;
public:
	void AnimationLoop();
	void RegisterAnimation(std::string name, float* pvalue);
	void UpdateAnimation(std::string name, float startvalue, float needvalue, float starttime, float endtime);
};