#include "hooks.h"
#include <intrin.h>
#include "Utilities.h"
SetupBones_t oSetupBones = NULL;

bool __fastcall Hooks::SetupBones_h(void* ECX, void* EDX, VMatrix *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
{
	// Supposed to only setupbones tick by tick, instead of frame by frame.
	if (ECX /*&& ((ClientClass*)ECX)->cl()*/)
	{
		ClientClass* pNetworkable = ((ClientClass*)ECX);
		if (pNetworkable && pNetworkable->m_ClassID == 38)
		{
			static auto host_timescale = g_pCvar->FindVar(("host_timescale"));
			auto player = (C_BaseEntity*)ECX;
			float OldCurTime = g_globals->curtime;
			float OldRealTime = g_globals->realtime;
			float OldFrameTime = g_globals->frametime;
			float OldAbsFrameTime = g_globals->absoluteframetime;
			float OldAbsFrameTimeStart = g_globals->absoluteframestarttimestddev;
			float OldInterpAmount = g_globals->interpolation_amount;
			int OldFrameCount = g_globals->framecount;
			int OldTickCount = g_globals->tickcount;

			g_globals->curtime = player->GetSimulationTime();
			g_globals->realtime = player->GetSimulationTime();
			g_globals->frametime = g_globals->interval_per_tick * host_timescale->GetFloat();
			g_globals->absoluteframetime = g_globals->interval_per_tick * host_timescale->GetFloat();
			g_globals->absoluteframestarttimestddev = player->GetSimulationTime() - g_globals->interval_per_tick * host_timescale->GetFloat();
			g_globals->interpolation_amount = 0;
			g_globals->framecount = TIME_TO_TICKS(player->GetSimulationTime());
			g_globals->tickcount = TIME_TO_TICKS(player->GetSimulationTime());

			*(int*)((int)player + 240) |= 8; // IsNoInterpolationFrame
			bool ret_value = oSetupBones(player, pBoneToWorldOut, nMaxBones, boneMask, g_globals->curtime);
			*(int*)((int)player + 240) &= ~8; // (1 << 3)

			g_globals->curtime = OldCurTime;
			g_globals->realtime = OldRealTime;
			g_globals->frametime = OldFrameTime;
			g_globals->absoluteframetime = OldAbsFrameTime;
			g_globals->absoluteframestarttimestddev = OldAbsFrameTimeStart;
			g_globals->interpolation_amount = OldInterpAmount;
			g_globals->framecount = OldFrameCount;
			g_globals->tickcount = OldTickCount;
			return ret_value;
		}
	}
	return oSetupBones(ECX, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
}
