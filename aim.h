#pragma once
#include "Singleton.h"

class CRageBot : public Singleton<CRageBot>
{
public:
	void CompensateInaccuracies();
	void Run();
private:
	std::vector<Vector> Multipoint(C_BaseEntity* player, HitboxData_t hitbox, VMatrix matrix);
	bool HitChance(C_BaseEntity* Target, Vector AimPoint);
	void CMDAdjustment();

	struct {
		int iTargetingType = 0; // 0 - Lowest HP | 1 - HZ
		
	}Setting;

	std::deque<C_BaseEntity*> TargetEntity;
	RecordTick_t Target;
	bool bFindGoodTarget = false;
}; 