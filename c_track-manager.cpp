#include "sdk.h"
#include "c_track-manager.hpp"
#include "global.h"
#include "Autowall.h"
#include "resolve.h"
#include "simple_auto_wall.h"
#include "simplehpb_auto_wall.h"
#include "auto_wall.h"
#include "animation_sync.h"

template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}

bool CBackTrackManager::IsVisibleTick(RecordTick_t tick, bool smokecheck, bool bodycheck)
{
	C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(tick.iIndex);
	if (!pEntity || !pEntity->isAlive() || !pEntity->IsPlayer())
		return false;

	bool bVisible = false;

	Vector vecPoints[] = {
	Vector(tick.vecMin.x, tick.vecMin.y, tick.vecMin.z),
	Vector(tick.vecMin.x, tick.vecMax.y, tick.vecMin.z),
	Vector(tick.vecMax.x, tick.vecMax.y, tick.vecMin.z),
	Vector(tick.vecMax.x, tick.vecMin.y, tick.vecMin.z),
	Vector(tick.vecMax.x, tick.vecMax.y, tick.vecMax.z),
	Vector(tick.vecMin.x, tick.vecMax.y, tick.vecMax.z),
	Vector(tick.vecMin.x, tick.vecMin.y, tick.vecMax.z),
	Vector(tick.vecMax.x, tick.vecMin.y, tick.vecMax.z)
	};
		for (Vector CheckPos : vecPoints)
		{
			Ray_t ray;
			ray.Init(c_ayala::l_player->GetEyePosition(), CheckPos);
			CTraceFilter filter;
			filter.pSkip1 = c_ayala::l_player;

			trace_t trace;
			g_pEngineTrace->TraceRay(ray, MASK_SHOT, &filter, &trace);
			if (trace.fraction > .95f)
			{
				bVisible = true;
				break;
			}
		}

	if (bVisible && bodycheck)
	{
		auto VectorTransform_Wrapper = [](const Vector& in1, const VMatrix &in2, Vector &out)
		{
			auto VectorTransform = [](const float *in1, const VMatrix& in2, float *out)
			{
				auto DotProducts = [](const float *v1, const float *v2)
				{
					return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
				};
				out[0] = DotProducts(in1, in2[0]) + in2[0][3];
				out[1] = DotProducts(in1, in2[1]) + in2[1][3];
				out[2] = DotProducts(in1, in2[2]) + in2[2][3];
			};
			VectorTransform(&in1.x, in2, &out.x);
		};
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(tick.iIndex);
		if (!pEntity || !pEntity->isAlive() || !pEntity->IsPlayer())
			return false;
		studiohdr_t* pStudioModel = g_pModelInfo->GetStudioModel(pEntity->GetModel());
		if (!pStudioModel)
			return false;
		mstudiohitboxset_t* set = pStudioModel->pHitboxSet(0);
		if (!set)
			return false;
		for (int i = 0; i < set->numhitboxes; i++) {

			mstudiobbox_t *hitbox = set->pHitbox(i);
			if (!hitbox)
				continue;

			Vector max;
			Vector min;

			VectorTransform_Wrapper(hitbox->bbmax, tick.boneMatrix[hitbox->bone], max);
			VectorTransform_Wrapper(hitbox->bbmin, tick.boneMatrix[hitbox->bone], min);

			auto center = (min + max) * 0.5f;
			Ray_t ray;
			ray.Init(c_ayala::l_player->GetEyePosition(), center);
			CTraceFilter filter;
			filter.pSkip1 = c_ayala::l_player;

			trace_t trace;
			g_pEngineTrace->TraceRay(ray, MASK_SHOT, &filter, &trace);

			if (trace.fraction > .95f)
			{
				bVisible = true;
				break;
			}
			bVisible = false;
		}
	}

	//if (smokecheck)
	//	bVisible = bVisible && !U::LineGoesThroughSmoke(c_ayala::l_player->GetEyePosition(), pEntity->GetBonePosition(8, tick.boneMatrix));

	return bVisible;
}

bool CBackTrackManager::CanHitTick(RecordTick_t tick, int flag)
{
	C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(tick.iIndex);
	if (!c_ayala::l_player || !pEntity || !pEntity->IsPlayer())
		return false;
	Vector vecPoints[] = {
		Vector(tick.vecMin.x, tick.vecMin.y, tick.vecMin.z),
		Vector(tick.vecMin.x, tick.vecMax.y, tick.vecMin.z),
		Vector(tick.vecMax.x, tick.vecMax.y, tick.vecMin.z),
		Vector(tick.vecMax.x, tick.vecMin.y, tick.vecMin.z),
		Vector(tick.vecMax.x, tick.vecMax.y, tick.vecMax.z),
		Vector(tick.vecMin.x, tick.vecMax.y, tick.vecMax.z),
		Vector(tick.vecMin.x, tick.vecMin.y, tick.vecMax.z),
		Vector(tick.vecMax.x, tick.vecMin.y, tick.vecMax.z)
	};
	Vector Center = (tick.vecMax + tick.vecMin) * .5;
	Vector vecCrossPoints[] = {
		Vector(tick.vecMin.x,Center.y,Center.z),
		Vector(tick.vecMax.x,Center.y,Center.z),
		Vector(Center.x,tick.vecMin.y,Center.z),
		Vector(Center.x,tick.vecMax.y,Center.z),
		Vector(Center.x,Center.y,tick.vecMin.z),
		Vector(Center.x,Center.y,tick.vecMax.z)
	};
	if (flag & ECheckFlag::Cross)
	{
		for (Vector CheckDot : vecCrossPoints)
		{
			if (/*utowall::Get().CalculateDamage(c_ayala::l_player->GetEyePosition(), CheckDot, c_ayala::l_player, pEntity).percent > Menu.Ragebot.Mindamage*/c_autowall.CanHitFloatingPoint(CheckDot, c_ayala::l_player->GetEyePosition())/* && c_autowall.Damage(CheckDot) > Menu.Ragebot.Mindamage*//*> Menu.Ragebot.Mindamage*/)
				return true;
		}
	}
	if (flag & ECheckFlag::Corners)
	{
		for (Vector CheckDot : vecPoints)
		{
			//if (/*Autowall::Get().CalculateDamage(c_ayala::l_player->GetEyePosition(), CheckDot, c_ayala::l_player, pEntity).percent > Menu.Ragebot.Mindamage*/c_autowall.CanHitFloatingPoint(CheckDot, c_ayala::l_player->GetEyePosition()) && c_autowall.Damage(CheckDot) > Menu.Ragebot.Mindamage /*> Menu.Ragebot.Mindamage*/)
				return true;
		}
	}
	if (flag & ECheckFlag::Center)
	{
		if (/*Autowall::Get().CalculateDamage(c_ayala::l_player->GetEyePosition(), Center, c_ayala::l_player, pEntity).percent > Menu.Ragebot.Mindamage */c_autowall.CanHitFloatingPoint(Center, c_ayala::l_player->GetEyePosition())/* && c_autowall.Damage(Center) > Menu.Ragebot.Mindamage*//* > Menu.Ragebot.Mindamage*/)
			return true;
	}
	return false;
}

void CBackTrackManager::SetRecord(C_BaseEntity * pEntity, RecordTick_t tick)
{
	if (!pEntity || pEntity->GetIndex() != tick.iIndex || !pEntity->IsPlayer() || pEntity->IsDormant())
		return;	
	pEntity->InvalidateBoneCache();
	//*(int*)((uintptr_t)pEntity + 0xA30) = g_globals->framecount;
	//*(int*)((uintptr_t)pEntity + 0xA28) = 0;
	pEntity->SetAbsOrigin(tick.vecOrigin);
	*pEntity->angs() = tick.angEyeAngle;
	pEntity->SetAbsAngle(tick.angAbsAngle);
	//*(int*)((int)pEntity + 236) |= 8;
	pEntity->SetupBones(NULL, -1, 0x0007FF00, g_globals->curtime);
	//*(int*)((int)pEntity + 236) &= ~8;
	//*(int*)(pEntity + 2600) |= 0xA;
	//CBasePlayerAnimState *animations = pEntity->GetBasePlayerAnimState();
	//if (animations)
	//{
	//	animations->m_fDuckAmount = tick.flDuckAmount;
	//	animations->m_flFeetCycle = tick.flFeetCycle;
	//	animations->m_flFeetYawRate = tick.flFeetYawRate;
	//	animations->m_flEyePitch = tick.flEyePitch;
	//	animations->m_flEyeYaw = tick.flEyeYaw;
	//	animations->m_flGoalFeetYaw = tick.flGoalFeetYaw;
	//	animations->m_flCurrentFeetYaw = tick.flCurrentFeetYaw;
	//	animations->m_flCurrentTorsoYaw = tick.flCurrentTorsoYaw;
	//	animations->m_flUnknownVelocityLean = tick.flVelocityLean;
	//	animations->m_flLeanAmount = tick.flLeanAmount;
	//}
}

void CBackTrackManager::SetAnimState(C_BaseEntity * pEntity, RecordTick_t tick)
{
	if (!pEntity || pEntity->GetIndex() != tick.iIndex || !pEntity->IsPlayer() || pEntity->IsDormant())
		return;
	/*const auto animstate = *reinterpret_cast<uintptr_t*>(uintptr_t(pEntity) + ptrdiff_t(0x38A0));
	auto& UpdateTime = *reinterpret_cast<float*>(uintptr_t(animstate) + ptrdiff_t(0x6C));
	auto& NeedSkipUpdate = *reinterpret_cast<bool*>(uintptr_t(animstate) + ptrdiff_t(0x108));
	auto& NeedUpdate = *reinterpret_cast<bool*>(uintptr_t(animstate) + ptrdiff_t(0x109));
	*/
	//UpdateTime = g_globals->curtime;

	//////NeedSkipUpdate = false;
	////NeedUpdate = true;
	//for (int i = 0; i < 24; i++)
	//	*(float*)((DWORD)pEntity + offys.m_flPoseParameter + sizeof(float) * i) = tick.flPoseParameter[i];
	//std::memcpy(pEntity->GetAnimOverlays(), tick.cAnimationLayer, (sizeof(CAnimationLayer) * 15));

	//for (int i = 0; i < tick.iLayerCount; i++)
	//{
	//	AnimationLayer& Layer = pEntity->GetAnimOverlay2(i);
	//	Layer.m_flCycle = tick.cAnimationLayer[i].m_flCycle;
	//	Layer.m_flWeight = tick.cAnimationLayer[i].m_flWeight;
	//	Layer.m_nOrder = tick.cAnimationLayer[i].m_nOrder;
	//	Layer.m_nSequence = tick.cAnimationLayer[i].m_nSequence;
	//}
	//pEntity->UpdateClientSideAnimation();

	pEntity->SetupBones(NULL, 128, BONE_USED_BY_ANYTHING, g_globals->curtime);
}

void CBackTrackManager::UpdateTicks()
{
	for (int i = 0; i < g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (!pEntity->IsValid()
			|| pEntity == c_ayala::l_player)
			continue;
	
		int index = pEntity->GetIndex();
		RecordTick_t Tick(pEntity);
		//Resolve::Get().Resolver(Tick);
		RecordedTicks[index].push_back(Tick);
		while (!RecordedTicks[index].empty() && abs(RecordedTicks[index][0].flSimulationTime - pEntity->GetSimulationTime()) > 1)
			RecordedTicks[index].erase(RecordedTicks[index].begin());
	}
}

bool CBackTrackManager::IsValidTick(RecordTick_t tick)
{
	return GetTickOffset(tick) <= .2f;
}

int CBackTrackManager::GetEstimatedServerTickCount(float latency)
{
	return TIME_TO_TICKS(latency) + 1 + g_globals->tickcount;
}

bool CBackTrackManager::DataIsClear()
{
	for (auto ticks : RecordedTicks)
		if (!ticks.empty())
			return false;
	return true;
}

float CBackTrackManager::GetLerpTime()
{
	static ConVar* minupdate, *maxupdate, *updaterate, *interprate, *cmin, *cmax, *interp;
	if (!minupdate)
		minupdate = g_pCvar->FindVar(XorStr("sv_minupdaterate"));
	if (!maxupdate)
		maxupdate = g_pCvar->FindVar(XorStr("sv_maxupdaterate"));
	if (!updaterate)
		updaterate = g_pCvar->FindVar(XorStr("cl_updaterate"));
	if (!interprate)
		interprate = g_pCvar->FindVar(XorStr("cl_interp_ratio"));
	if (!cmin)
		cmin = g_pCvar->FindVar(XorStr("sv_client_min_interp_ratio"));
	if (!cmax)
		cmax = g_pCvar->FindVar(XorStr("sv_client_max_interp_ratio"));
	if (!interp)
		interp = g_pCvar->FindVar(XorStr("cl_interp"));

	float UpdateRate = updaterate->GetValue();
	float LerpRatio = interprate->GetValue();

	return std::min<float>(LerpRatio / UpdateRate, interp->GetValue());
}

float CBackTrackManager::GetTickOffset(RecordTick_t tick)
{
	float outlatency;
	float inlatency;
	static ConVar* sv_maxunlag;
	if (!sv_maxunlag)
		sv_maxunlag = g_pCvar->FindVar("sv_maxunlag");
	INetChannelInfo *nci = g_pEngine->GetNetChannelInfo();
	if (nci)
	{
		inlatency = nci->GetLatency(FLOW_INCOMING);
		outlatency = nci->GetLatency(FLOW_OUTGOING);
	}
	else
		inlatency = outlatency = 0.0f;

	float totaloutlatency = inlatency;

	float servertime = TICKS_TO_TIME(GetEstimatedServerTickCount(outlatency));

	float correct = clamp<float>(totaloutlatency + GetLerpTime(), .0f, sv_maxunlag->GetValue());
	int iTargetTick = TIME_TO_TICKS(tick.flSimulationTime);
	float deltaTime = correct - (servertime - TICKS_TO_TIME(iTargetTick));
	return fabsf(deltaTime);
}

RecordTick_t CBackTrackManager::GetLastValidRecord(C_BaseEntity * pEntity)
{
	for (int i = 0; i < RecordedTicks[pEntity->GetIndex()].size(); i++)
	{
		if (IsValidTick(RecordedTicks[pEntity->GetIndex()][i]))
			return RecordedTicks[pEntity->GetIndex()][i];
	}
	return RecordTick_t(pEntity);
}

std::vector<RecordTick_t> CBackTrackManager::GetScanTicks(C_BaseEntity * pEntity)
{
	std::vector<RecordTick_t> result;
	float MinTickOffset = FLT_MAX;
	bool FindFirstValidTick = false;
	RecordTick_t FirstTick;
	RecordTick_t MiddleTick;
	RecordTick_t LastTick;

	switch (Menu.Ragebot.iPositionAdjustment)
	{
	case 0:
		for (auto Tick : RecordedTicks[pEntity->GetIndex()])
		{
			if (IsValidTick(Tick))
				LastTick = Tick;
		}
		result.push_back(LastTick);
		break;
	case 1:
		for (auto Tick : RecordedTicks[pEntity->GetIndex()])
		{
			float TickOffset = GetTickOffset(Tick);
			if (TickOffset < MinTickOffset)
			{
				MiddleTick = Tick;
				MinTickOffset = TickOffset;
			}

			if (!FindFirstValidTick && TickOffset <= .2f)
			{
				FindFirstValidTick = true;
				FirstTick = Tick;
			}

			if (TickOffset <= .2f)
				LastTick = Tick;
		}
		result.push_back(FirstTick);
		result.push_back(LastTick);
		result.push_back(MiddleTick);
		break;
	case 2:
		for (auto Tick : RecordedTicks[pEntity->GetIndex()])
		{
			static bool Switch = false;
			Switch = !Switch;
			float TickOffset = GetTickOffset(Tick);
			if (TickOffset <= .2f && Switch)
				result.push_back(Tick);
		}
		break;
	case 3:
		for (auto Tick : RecordedTicks[pEntity->GetIndex()])
		{
			float TickOffset = GetTickOffset(Tick);
			if (TickOffset <= .2f)
				result.push_back(Tick);
		}
		break;
	}
	return result;
}

std::vector<RecordTick_t> CBackTrackManager::GetLastValidRecords()
{
	std::vector<RecordTick_t> records;
	for (int i = 0; i < g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);
		if (!pEntity->IsValid() || !pEntity->IsPlayer())
			continue;
		records.push_back(GetLastValidRecord(pEntity));
	}
	return records;
}
void CBackTrackManager::ClearData()
{
	for (int i = 0; i < g_globals->max_�lients; i++)
		RecordedTicks[i].clear();
}

RecordTick_t::RecordTick_t(C_BaseEntity * pEntity)
{
	int32_t backup_effects = *(int32_t*)((uintptr_t)pEntity + 240);//���� �� ��������
	*(int32_t*)((uintptr_t)pEntity + 240) |= 8;

	iIndex = pEntity->GetIndex();
	flSimulationTime = pEntity->GetSimulationTime();
	vecOrigin = pEntity->GetAbsOrigin();
	angEyeAngle = pEntity->GetEyeAngles();
	angAbsAngle = pEntity->GetAbsAngles();
	vecAbsOrigin = pEntity->GetOrigin();
	vecVelocity = pEntity->GetVelocity();
	iFlags = *pEntity->GetFlags();
	flLowerBodyYaw = pEntity->LowerBodyYaw();
	iLayerCount = pEntity->GetNumAnimOverlays();
	pEntity->ComputeHitboxSurroundingBox(&vecMin, &vecMax);
	for (int i = 0; i < 24; i++)
		flPoseParameter[i] = *(float*)((DWORD)pEntity + offys.m_flPoseParameter + sizeof(float) * i);
	pEntity->SetupBones(boneMatrix, 128, BONE_USED_BY_ANYTHING, g_globals->curtime);
	//animation_sync::Get().call_anim_sync(boneMatrix);
	//std::memcpy(cAnimationLayer, pEntity->GetAnimOverlays(), (sizeof(CAnimationLayer) * 15));
	//for (int i = 0; i < iLayerCount; i++)
	//{
	//	AnimationLayer Layer = pEntity->GetAnimOverlay2(i);
	//cAnimationLayer[i].m_flCycle = Layer.m_flCycle;
	//	cAnimationLayer[i].m_flWeight = Layer.m_flWeight;
	//	cAnimationLayer[i].m_nOrder = Layer.m_nOrder;
	//	cAnimationLayer[i].m_nSequence = Layer.m_nSequence;
	//}
	studiohdr_t* pStudioModel = g_pModelInfo->GetStudioModel(pEntity->GetModel());
	if (pStudioModel) {
		mstudiohitboxset_t* set = pStudioModel->pHitboxSet(0);
		if (set) 
		{
			for (int i = 0; i < set->numhitboxes; i++)
			{
				mstudiobbox_t *hitbox = set->pHitbox(i);
				if (!hitbox)
					continue;
				
				HitboxData_t Data;
				Data.flRadius = hitbox->radius;
				Data.iBone = hitbox->bone;
				Data.iGroup = hitbox->group;
				Data.iHitbox = i;
				Data.vecBBmax = hitbox->bbmax;
				Data.vecBBmin = hitbox->bbmin;
				if (Menu.Ragebot.HitboxSetting.Enable[Data.NumToGroup()])
					HitboxData.push_back(Data);
			}
		}
	}
	CBasePlayerAnimState *animations = pEntity->GetBasePlayerAnimState();
	if (animations)
	{
		flDuckAmount = animations->m_fDuckAmount;
		flFeetCycle = animations->m_flFeetCycle;
		flFeetYawRate = animations->m_flFeetYawRate;
		flEyePitch = animations->m_flEyePitch;
		flEyeYaw = animations->m_flEyeYaw;
		flGoalFeetYaw = animations->m_flGoalFeetYaw;
		flCurrentFeetYaw = animations->m_flCurrentFeetYaw;
		flCurrentTorsoYaw = animations->m_flCurrentTorsoYaw;
		flVelocityLean = animations->m_flUnknownVelocityLean;
		flLeanAmount = animations->m_flLeanAmount;
	}

	nTickFlags = pEntity->GetOldSimulationTime() == pEntity->GetSimulationTime() ? TICK_Break : TICK_Normal;
	if (vecVelocity.Length2D() > 0.1)
		nTickFlags |= TICK_Moving;
	if (!(iFlags & FL_ONGROUND))
		nTickFlags |= TICK_InAir;	
	*(int32_t*)((uintptr_t)pEntity + 0xF0) = backup_effects;

}
int HitboxData_t::GetPriority()
{
	return Menu.Ragebot.HitboxSetting.Priority[NumToGroup()];
}

Vector HitboxData_t::GetMPSize()
{
	int X;
	int Y;
	switch (Menu.Ragebot.HitboxSetting.MPSize[NumToGroup()])
	{
	case 0:
		X = 0;
		Y = 0; break;
	case 1:
		X = 1;
		Y = 0; break;
	case 2:
		X = 1;
		Y = 1; break;
	case 3:
		X = 2;
		Y = 1; break;
	case 4:
		X = 2;
		Y = 2; break;
	}
	return Vector(X, Y, Menu.Ragebot.HitboxSetting.PointScale[NumToGroup()]);
}

EHitboxGroup HitboxData_t::NumToGroup()
{
	switch (iHitbox)
	{
	case HEAD:
		return HEADGROUP; break;
	case NECK:
		return NECKGROUP; break;
	case PELVIS:
		return PELVISGROUP; break;
	case BELLY:
	case THORAX:
		return STOMACHGROUP; break;
	case LOWER_CHEST:
	case UPPER_CHEST:
		return CHESTGROUP; break;
	case RIGHT_THIGH:
	case LEFT_THIGH:
	case RIGHT_CALF:
	case LEFT_CALF:
	case RIGHT_FOOT:
	case LEFT_FOOT:
		return LEGSGROUP; break;
	case RIGHT_HAND:
	case LEFT_HAND:
	case RIGHT_UPPER_ARM:
	case RIGHT_FOREARM:
	case LEFT_UPPER_ARM:
	case LEFT_FOREARM:
		return ARMSGROUP; break;
	}
}
void CBackTrackManager::set_interpolation_flags(C_BaseEntity* entity, int flag)
{
	auto var_map = reinterpret_cast<uintptr_t>(entity) + 36; // tf2 = 20
	auto var_map_list_count = *reinterpret_cast<int*>(var_map + 20);

	for (auto index = 0; index < var_map_list_count; index++)
		*reinterpret_cast<uintptr_t*>(*reinterpret_cast<uintptr_t*>(var_map) + index * 12) = flag;
}
