#include "hooks.h"
#include "global.h"
#include "Menu.h"
#include "sdk.h"
//#include "Grenadec_prediction.hpp"

ClientVariables Menu;

void __fastcall Hooks::OverrideView(void* _this, void* _edx, CViewSetup* setup)
{
	if (g_pEngine->IsInGame() && g_pEngine->IsConnected())
	{
		c_ayala::CameraOrigin = setup->origin;
		if (c_ayala::l_player && c_ayala::l_player->GetHealth() > 0)
		{
			
			if (!c_ayala::l_player->IsScoped())
			{
				setup->fov = 90 + Menu.Misc.PlayerFOV;
			}
			else
			{
				if (Menu.Misc.static_scope)
					setup->fov = 90 + Menu.Misc.PlayerFOV;
				else {

				}
			}
		}
	}
	//grenade_prediction::instance().View(setup);

	clientmodeVMT->GetOriginalMethod<OverrideViewFn>(18)(_this, setup);
}

float __stdcall GGetViewModelFOV()
{
	float fov = clientmodeVMT->GetOriginalMethod<oGetViewModelFOV>(35)();

	if (g_pEngine->IsConnected() && g_pEngine->IsInGame())
	{
		if (c_ayala::l_player)
		{
			fov += Menu.Misc.PlayerViewmodel;
		}
	}
	return fov;
}