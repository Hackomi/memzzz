#pragma once


struct c_FireBulletData
{
	c_FireBulletData(const Vector &eyePos, C_BaseEntity* entity) : src(eyePos), filter(entity)
	{
	}

	Vector          src;
	trace_t         enter_trace;
	Vector          direction;
	C_TraceFilter   filter;
	float           trace_length;
	float           trace_length_remaining;
	float           current_damage;
	int             penetrate_count;
};

class c_Autowall
{
public:
	bool CanHitFloatingPoint(const Vector &point, const Vector &source);
	float Damage(const Vector &point);
private:
};
extern c_Autowall c_autowall;