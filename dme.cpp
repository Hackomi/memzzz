#include "hooks.h"
#include "Menu.h"
#include "global.h"
#include "MaterialHelper.h"
#include "xor.h"
#include "c_track-manager.hpp"
#include "Chams.h"
#include "animation_sync.h"
#define FCVAR_CHEAT (1<<14) 
template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}
void __fastcall Hooks::scene_end(void* thisptr, void* edx) {

	if (Menu.Visuals.ChamsEnable)
	{
		//float Red, Green, Blue, RedZ, GreenZ, BlueZ;

		static IMaterial* CoveredLit = g_MaterialHelper->CreateMaterial(true);
		static IMaterial* OpenLit = g_MaterialHelper->CreateMaterial(false);
		static IMaterial* CoveredFlat = g_MaterialHelper->CreateMaterial(true, false);
		static IMaterial* OpenFlat = g_MaterialHelper->CreateMaterial(false, false);

		static IMaterial* backtrack = g_MaterialHelper->CreateMaterial(true, false);

		static IMaterial* materialMetall = g_MaterialHelper->CreateAdditiveMaterial(false, true, 0.9f, 0.9f);
		static IMaterial* materialMetallnZ = g_MaterialHelper->CreateAdditiveMaterial(true, true, 0.9f, 0.9f);
		
		IMaterial *covered;
		IMaterial *open;
		//	IMaterial *ebopen = g_pMaterialSystem->FindMaterial("models/inventory_items/trophy_majors/crystal_clear", TEXTURE_GROUP_OTHER);

		switch (Menu.Visuals.ChamsStyle)
		{
		case 0:
			covered = CoveredLit;
			open = OpenLit;
			break;
		case 1:
			covered = CoveredFlat;
			open = OpenFlat;
			break;

		}
		//if (c_ayala::l_player && c_ayala::l_player->isAlive())
		//{
		//	QAngle Backup = c_ayala::l_player->GetAbsAngles();
		//	c_ayala::l_player->SetAbsAngle(QAngle(0, c_ayala::l_player->LowerBodyYaw(), 0));
		//	c_ayala::l_player->UpdateClientSideAnimation();
		//	g_pRenderView->SetColorModulation(Menu.Colors.PlayerChams);
		//	g_pModelRender->ForcedMaterialOverride(OpenFlat);
		//	c_ayala::l_player->draw_model(0x1/*STUDIO_RENDER*/, 255);
		//	//g_pModelRender->ForcedMaterialOverride(materialMetall);
		//	//c_ayala::l_player->draw_model(0x1/*STUDIO_RENDER*/, 255);
		//	c_ayala::l_player->SetAbsAngle(Backup);
		//}

	
		for (int i = 1; i < g_pEngine->Getmax_�lients(); ++i) {
			C_BaseEntity* ent = (C_BaseEntity*)g_pEntitylist->GetClientEntity(i);

			//if (ent == c_ayala::l_player && c_ayala::l_player != nullptr)
			//{
			//	if (c_ayala::l_player->isAlive())
			//	{
			//		if (b_IsThirdPerson && Menu.Visuals.ChamsL)
			//		{

			//			g_pRenderView->SetColorModulation(Menu.Colors.PlayerChamsl);

			//			g_pModelRender->ForcedMaterialOverride(open);


			//			c_ayala::l_player->draw_model(0x1/*STUDIO_RENDER*/, 255);
			//			g_pModelRender->ForcedMaterialOverride(nullptr);
			//		}
			//	}
			//}
				//if (c_ayala::l_player->isAlive())
				//{
				//		Vector OrigAng;
				//		OrigAng = c_ayala::l_player->GetEyeAngles();
				//		c_ayala::l_player->SetAbsAngle(Vector(0, animation_sync::Get().setupvelocity_rebuild(c_ayala::l_player), 0)); // paste here ur AA.y value or pLocal->GetLby() (for example)
				//		bool LbyColor = false; // u can make LBY INDICATOR. When LbyColor is true. Color will be Green , if false it will be White
				//		float NormalColor[3] = { 1, 1, 1 };
				//		float lbyUpdateColor[3] = { 0, 1, 0 };
				//		g_pRenderView->SetColorModulation(LbyColor ? lbyUpdateColor : NormalColor);
				//		g_pModelRender->ForcedMaterialOverride(open);
				//		c_ayala::l_player->DrawModel(0x1, 255);
				//		g_pModelRender->ForcedMaterialOverride(nullptr);
				//		c_ayala::l_player->SetAbsAngle(OrigAng);
				//}
			if (ent->IsValidRenderable() && Menu.Visuals.ChamsPlayer)
			{
				//Vector orig = ent->GetAbsOrigin();
				if (Menu.Visuals.ChamsPlayerWall)
				{
					g_pRenderView->SetColorModulation(Menu.Colors.PlayerChamsWall);
					g_pRenderView->SetBlend(Menu.Colors.PlayerChamsWall[3]);
					g_pModelRender->ForcedMaterialOverride(covered);
					ent->draw_model(0x1/*STUDIO_RENDER*/, 255);
					
					g_pRenderView->SetColorModulation(Menu.Colors.PlayerChamsWallRf);
					g_pRenderView->SetBlend(Menu.Colors.PlayerChamsWallRf[3]);
					g_pModelRender->ForcedMaterialOverride(materialMetallnZ);
					ent->draw_model(0x1/*STUDIO_RENDER*/, 255);
					g_pModelRender->ForcedMaterialOverride(nullptr);
				}
				g_pRenderView->SetColorModulation(Menu.Colors.PlayerChams);
				g_pRenderView->SetBlend(Menu.Colors.PlayerChams[3]);
				g_pModelRender->ForcedMaterialOverride(open);
				ent->draw_model(0x1/*STUDIO_RENDER*/, 255);
				
				g_pRenderView->SetColorModulation(Menu.Colors.PlayerChamsRf);
				g_pRenderView->SetBlend(Menu.Colors.PlayerChamsRf[3]);
				g_pModelRender->ForcedMaterialOverride(materialMetall);
				ent->draw_model(0x1/*STUDIO_RENDER*/, 255);
				g_pModelRender->ForcedMaterialOverride(nullptr);
				//ent->SetAbsOrigin(orig);
			}
			/*if (Menu.Visuals.ShowBacktrack)
			{
				if (!ent
					|| !ent->isAlive()
					|| !ent->IsPlayer()
					|| !ent->IsEnemy()
					|| ent == c_ayala::l_player)
					continue;
				RecordTick_t Backup(ent);
				auto Tick = CBackTrackManager::Get().GetLastValidRecord(ent);

				float TickOffset = CBackTrackManager::Get().GetTickOffset(Tick);
				if (TickOffset <= .2f)
				{
					g_pRenderView->SetColorModulation(Menu.Colors.PlayerChams);
					CBackTrackManager::Get().SetAnimState(ent, Tick);
					g_pModelRender->ForcedMaterialOverride(backtrack);
					ent->UpdateClientSideAnimation();
					ent->draw_model(0x1, 80);
					CBackTrackManager::Get().SetAnimState(ent, Backup);
					ent->UpdateClientSideAnimation();
				}
			}*/
		}
		g_pModelRender->ForcedMaterialOverride(nullptr);
	}

	static auto scene_end_o = renderviewVMT->GetOriginalMethod< decltype(&scene_end) >(9);
	scene_end_o(thisptr, edx);
}

void __fastcall Hooks::DrawModelExecute(void* ecx, void* edx, void* * ctx, void *state, const ModelRenderInfo_t &pInfo, VMatrix *pCustomBoneToWorld)
{
	static IMaterial* CoveredFlat = g_MaterialHelper->CreateGlowMaterial(true, false, 1.f, Menu.Colors.ChamsHistory[0], Menu.Colors.ChamsHistory[1], Menu.Colors.ChamsHistory[2]);
	static float BufferColor[3];

	if (Menu.Colors.ChamsHistory[0] != BufferColor[0] || Menu.Colors.ChamsHistory[1] != BufferColor[1] || Menu.Colors.ChamsHistory[2] != BufferColor[2])
	{
		CoveredFlat = g_MaterialHelper->CreateGlowMaterial(true, false, 1.f, Menu.Colors.ChamsHistory[0], Menu.Colors.ChamsHistory[1], Menu.Colors.ChamsHistory[2]);
		BufferColor[0] = Menu.Colors.ChamsHistory[0];
		BufferColor[1] = Menu.Colors.ChamsHistory[1];
		BufferColor[2] = Menu.Colors.ChamsHistory[2];
	}

	if (!c_ayala::l_player || !g_pEngine->IsConnected() || !g_pEngine->IsInGame())
	{
		modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, pInfo, pCustomBoneToWorld);
		return;
	}

	auto ent = g_pEntitylist->GetClientEntity(pInfo.entity_index);

	const char* ModelName = g_pModelInfo->GetModelName((model_t*)pInfo.pModel);

	/*float step = M_PI * 2.f / 300;
	static float counter[64];
	counter[pInfo.entity_index] += step;
	if (counter[pInfo.entity_index] > (M_PI * 2.0f))
		counter[pInfo.entity_index] = 0.f;
	pCustomBoneToWorld[8][0][3] += 30.f * cosf(counter[pInfo.entity_index]);
	pCustomBoneToWorld[8][1][3] += 30.f * sinf(counter[pInfo.entity_index]);
	pCustomBoneToWorld[8][2][3] += 5;*/

	if (ent == c_ayala::l_player)
	{
		/*if (Menu.Visuals.lagModel)
		{
			c_ayala::l_player->SetAbsOrigin(c_ayala::fakeOrigin);

			c_ayala::l_player->SetAbsOrigin(c_ayala::l_player->GetAbsOrigin());
		}*/
		
		if (c_ayala::l_player->IsScoped())
		{
			if (Menu.Visuals.blendonscope)
			{
				g_pRenderView->SetBlend(0.5f);
				g_pModelRender->ForcedMaterialOverride(nullptr);
			}
			else
				g_pRenderView->SetBlend(Menu.Visuals.playeralpha / 255.f);
		}
		else
			g_pRenderView->SetBlend(Menu.Visuals.playeralpha / 255.f);
	}
	//g_pRenderView->SetBlend(Menu.Visuals.playeralpha / 155.f);
	if (ent && c_ayala::l_player) // nullptr checks
	{
		if (ent->GetTeamNum() == c_ayala::l_player->GetTeamNum() && ent != c_ayala::l_player ) // team check and if the modelentity = pEnt
		{
	IMaterial* entity = g_pMaterialSystem->FindMaterial(XorStr("models/player"), (TEXTURE_GROUP_MODEL)); // get the material mdl = Info.Model
	entity->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true); // setting the materialflag to not draw it
	g_pModelRender->ForcedMaterialOverride(entity); // overriding material
		}
	}
	const auto mdl = pInfo.pModel;

	if (Menu.Colors.Props) {
		if (strstr(ModelName, XorStr("models/props"))) {
			g_pRenderView->SetBlend(0.5f);
		}
	}
	auto b_IsThirdPerson = g_pInput->m_fCameraInThirdPerson;
	/*if (c_ayala::l_player->isAlive() && Menu.Visuals.ChamsL)
	{
		VMatrix boneMatrix[128];
		if (!c_ayala::c_SendPacket)
			c_ayala::l_player->SetupBones(boneMatrix, 128, BONE_USED_BY_ANYTHING, g_globals->curtime);
		ModelRenderInfo_t MyInfo = pInfo;
		MyInfo.pModelToWorld = boneMatrix;
		CoveredFlat->ColorModulate(Menu.Colors.ChamsHistory[0] * 255, Menu.Colors.ChamsHistory[1] * 255, Menu.Colors.ChamsHistory[2] * 255);
		g_pModelRender->ForcedMaterialOverride(CoveredFlat);
		modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, MyInfo, boneMatrix);
	}*/
	if (Menu.Misc.WireHand) {
		if (strstr(ModelName, XorStr("arms"))) {
			static IMaterial* mat = g_MaterialHelper->CreateMaterial(true, false, true);
			mat->ColorModulate(Menu.Colors.styleshands[0] * 255, Menu.Colors.styleshands[1] * 255, Menu.Colors.styleshands[2] * 255);
			g_pModelRender->ForcedMaterialOverride(mat);
		}
	}

	IMaterial *xblur_mat = g_pMaterialSystem->FindMaterial(XorStr("dev/blurfilterx_nohdr"), TEXTURE_GROUP_OTHER, true);
	IMaterial *yblur_mat = g_pMaterialSystem->FindMaterial(XorStr("dev/blurfiltery_nohdr"), TEXTURE_GROUP_OTHER, true);
	IMaterial *scope = g_pMaterialSystem->FindMaterial(XorStr("dev/scope_bluroverlay"), TEXTURE_GROUP_OTHER, true);
	xblur_mat->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
	yblur_mat->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
	scope->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
	VMatrix blandedmatrix[128];
	ent->correctmatrix(blandedmatrix);
	if (strstr(ModelName, XorStr("models/player"))) modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, pInfo, blandedmatrix);
	else modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, pInfo, pCustomBoneToWorld);
	if (ent && ent->IsPlayer() && ent->isAlive() && ent->IsEnemy() && Menu.Visuals.ShowBacktrack && ent != c_ayala::l_player)
	{
	switch  (Menu.Visuals.ShowBacktrackMethod)
	{
	case 0:
	{
		auto Ticks = CBackTrackManager::Get().GetScanTicks(ent);
		for (auto Tick : Ticks)
		{
			if (!CBackTrackManager::Get().IsValidTick(Tick))
				continue;
			ModelRenderInfo_t MyInfo = pInfo;
			MyInfo.angles = Tick.angAbsAngle;
			MyInfo.origin = Tick.vecOrigin;
			MyInfo.pModelToWorld = Tick.boneMatrix;
			CoveredFlat->ColorModulate(Menu.Colors.ChamsHistory[0] * 255, Menu.Colors.ChamsHistory[1] * 255, Menu.Colors.ChamsHistory[2] * 255);
			g_pModelRender->ForcedMaterialOverride(CoveredFlat);
			modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, MyInfo, Tick.boneMatrix);
			/*float TickOffset = CBackTrackManager::Get().GetTickOffset(Tick);
			if (TickOffset <= flMin && TickOffset <= .2f)
			{
				Best = Tick;
				flMin = TickOffset;
			}*/
		}
	}break;
	case 1:
	{
		  auto Tick = CBackTrackManager::Get().GetLastValidRecord(ent);
		  CoveredFlat->ColorModulate(Menu.Colors.ChamsHistory[0] * 255, Menu.Colors.ChamsHistory[1] * 255, Menu.Colors.ChamsHistory[2] * 255);
		  g_pModelRender->ForcedMaterialOverride(CoveredFlat);
		  modelrenderVMT->GetOriginalMethod<DrawModelExecuteFn>(21)(ecx, ctx, state, pInfo, Tick.boneMatrix);
	}break;
	}
	}
	g_pModelRender->ForcedMaterialOverride(NULL);
}
