#include "sdk.h"
#include "Hitmarker.h"
#include "GameUtils.h"

CHitmarker* g_Hitmarker = new CHitmarker;

void CHitmarker::update_end_time()
{
	this->end_time = GameUtils::GetCurTime() + 0.2f;
}
void CHitmarker::draw()
{
	if (g_globals->curtime > this->end_time)
		return;
}

void CHitmarker::play_sound()
{
	if(Menu.Visuals.Hitmarker == 2)
		g_pEngine->ClientCmd_Unrestricted("play buttons\\arena_switch_press_02.wav");
}