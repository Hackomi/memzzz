#include "sdk.h"
#include "Math.h"
#include "global.h"
#include "GameUtils.h"
#include "xor.h"


CBaseCombatWeapon* C_BaseEntity::GetWeapon()
{
	ULONG WeaponUlong = *(PULONG)((DWORD)this + offys.m_hActiveWeapon); // hActiveWeapon
	return (CBaseCombatWeapon*)(g_pEntitylist->GetClientEntityFromHandle(WeaponUlong));
}
std::vector< CBaseCombatWeapon* > C_BaseEntity::GetWeaponsData()
{
	std::vector< CBaseCombatWeapon* > list = {};
	for (auto i = 0; i < 64; ++i)
	{
		auto Weapon = g_pEntitylist->GetClientEntityFromHandle((HANDLE)this->m_hMyWeapons()[i]);
		if (Weapon)
		{
			list.push_back((CBaseCombatWeapon*)Weapon);
		}
	}
	return list;
}
std::string C_BaseEntity::GetName()
{
	player_info_t Info;
	g_pEngine->GetPlayerInfo(this->GetIndex(), &Info);
	return Info.m_szPlayerName;
}
DWORD GetCSWpnDataAddr;

bool C_BaseEntity::IsEnemy()
{
	return this->GetTeamNum() != c_ayala::l_player->GetTeamNum();
}
void C_BaseEntity::correctmatrix(VMatrix *Matrix)
{
	if (this)
	{
		int Backup = *(int*)((uintptr_t)this + 0x274);
		*(int*)((uintptr_t)this + 0x274) = 0;
		*(int*)((uintptr_t)this + 240) |= 8;
		Vector absOriginBackupLocal = this->GetAbsOrigin();
		this->SetAbsOrigin(this->GetOrigin());
		this->SetupBones(Matrix, 128, 0x0007FF00, g_globals->curtime); // can be used 0x00000100 but idk hehe 
		this->SetAbsOrigin(absOriginBackupLocal);
		*(int*)((uintptr_t)this + 240) &= ~8;
		*(int*)((uintptr_t)this + 0x274) = Backup;
	}
}

void C_BaseEntity::CopyPoseParameters(float* dest)
{
	float* flPose = (float*)((DWORD)this + 0x2764);
	memcpy(dest, flPose, sizeof(float) * 24);

}

bool C_BaseEntity::ComputeHitboxSurroundingBox(Vector *pVecWorldMins, Vector *pVecWorldMaxs)
{
	if (!this)
		return false;
	typedef bool(__thiscall* SComputeHitboxSurroundingBox)(void*, Vector *, Vector *);
	static auto oComputeHitboxSurroundingBox = (SComputeHitboxSurroundingBox)(offys.dwComputeHitboxSurroundingBox);

	return oComputeHitboxSurroundingBox(this, pVecWorldMins, pVecWorldMaxs);
}
void C_BaseEntity::InvalidateBoneCache()
{
	auto model_bone_counter = **(unsigned long**)(offys.InvalidateBoneCache + 0xA);
	*(std::uint32_t*)(this + 0x2924) = 0xFF7FFFFFu;
	*(std::uint32_t *)(this + 0x2690) = (model_bone_counter - 1u);
}
void C_BaseEntity::SetCurrentCommand(c_user_cmd *cmd)
{
	static int offset = g_pNetVars->GetOffset("DT_BasePlayer", "m_hConstraintEntity");
	*Member<c_user_cmd**>(this, (offset - 0xC)) = cmd;
}


std::array<float, 24> &C_BaseEntity::m_flPoseParameter()
{
	static int _m_flPoseParameter = g_pNetVars->GetOffset("CBaseAnimating", "m_flPoseParameter");
	return *(std::array<float, 24>*)((uintptr_t)this + _m_flPoseParameter);
}

bool C_BaseEntity::IsValidRenderable() {

	if (!this || this == nullptr || c_ayala::l_player == nullptr)
		return false;

	if (this == c_ayala::l_player)
		return false;
	if (this->GetTeamNum() == c_ayala::l_player->GetTeamNum())
		return false;
	if (this->IsDormant())
		return false;
	if (!this->isAlive())
		return false;

	return true;
}
void C_BaseEntity::UpdateClientSideAnimation()
{
	if (!this)
		return;
	
	typedef void(__thiscall* Fn)(void*);
	return CallVFunction<Fn>(this, 219)(this);
}
void C_BaseEntity::SetAbsOrigin(const Vector &origin)
{
	using SetAbsOriginFn = void(__thiscall*)(void*, const Vector &origin);
	static SetAbsOriginFn SetAbsOrigin = (SetAbsOriginFn)FindPatternIDA(c_ayala::Panorama ? "client_panorama.dll" : "client.dll", "55 8B EC 83 E4 F8 51 53 56 57 8B F1 E8");
	SetAbsOrigin(this, origin);
}

bool C_BaseEntity::IsValidTarget() {

	if (!this || this == nullptr)
		return false;

	ClientClass* pClass = (ClientClass*)this->GetClientClass(); // Needed to check clientclass after nullptr check that was causing a crash

	if (this == c_ayala::l_player)
		return false;
	
	if (!pClass)
		return false;

	if (pClass->m_ClassID != 38)
		return false;

	if (this->GetTeamNum() == c_ayala::l_player->GetTeamNum())
		return false;

	if (this->IsDormant())
		return false;

	if (!this->isAlive())
		return false;

	if (this->IsProtected())
		return false;

	return true;
}

void C_BaseEntity::SetAbsAngle(Vector wantedang)
{
	typedef void(__thiscall* SetAngleFn)(void*, const Vector &);
	static SetAngleFn SetAngle = (SetAngleFn)((DWORD)Utilities::Memory::FindPatternIDA(c_ayala::Panorama ? "client_panorama.dll" : "client.dll", "55 8B EC 83 E4 F8 83 EC 64 53 56 57 8B F1"));
	SetAngle(this, wantedang);
}

bool C_BaseEntity::SetupBones2(matrix3x4_t* pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
{
	static DWORD m_nWritableBones = g_pNetVars->GetOffset("DT_BaseAnimating", "m_nForceBone");
	static DWORD m_dwOcclusionArray = *(uintptr_t*)(FindPattern(c_ayala::Panorama ? "client_panorama.dll" : "client.dll", (PBYTE) "\xA1\x00\x00\x00\x00\x8B\xB7\x00\x00\x00\x00\x89\x75\xF8", "x????xx????xxx") + 0x1); //"A1 ? ? ? ? 8B B7 ? ? ? ? 89 75 F8"
	static DWORD m_bDidCheckForOcclusion = *(uintptr_t*)(FindPattern(c_ayala::Panorama ? "client_panorama.dll" : "client.dll", (PBYTE) "\xA1\x00\x00\x00\x00\x8B\xB7\x00\x00\x00\x00\x89\x75\xF8", "x????xx????xxx") + 0x7); //"A1 ? ? ? ? 8B B7 ? ? ? ? 89 75 F8"

	*(int*)((uintptr_t)this + m_nWritableBones) = 0;
	*(int*)((uintptr_t)this + m_bDidCheckForOcclusion) = reinterpret_cast< int* >(m_dwOcclusionArray)[1];

	__asm
	{
		mov edi, this
		lea ecx, dword ptr ds : [edi + 0x4]
		mov edx, dword ptr ds : [ecx]
		push currentTime
		push boneMask
		push nMaxBones
		push pBoneToWorldOut
		call dword ptr ds : [edx + 0x34]
	}

	//typedef bool(__thiscall* oSetupBones)(PVOID, matrix3x4*, int, int, float);
	//return getvfunc< oSetupBones>(this, 13)(this, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
}

bool C_BaseEntity::canHit(Vector end, C_BaseEntity* ent) {
	Ray_t ray;
	trace_t tr;
	CTraceFilter traceFilter;
	traceFilter.pSkip1 = this;
	ray.Init(this->GetEyePosition(), end);
	g_pEngineTrace->ClipRayToC_BaseEntity(ray, MASK_SHOT, ent, &tr); // ignore grate

	if (!tr.m_pEnt)
		return false;

	C_BaseEntity *pEnt = (C_BaseEntity*)tr.m_pEnt;

	if (pEnt->GetTeamNum() != this->GetTeamNum())
		return true;

	return false;
}

Vector C_BaseEntity::GetBonePos(int i)
{
	VMatrix boneMatrix[128];
	if (this->SetupBones(boneMatrix, 128, BONE_USED_BY_HITBOX, g_globals->curtime))
	{
		return Vector(boneMatrix[i][0][3], boneMatrix[i][1][3], boneMatrix[i][2][3]);
	}
	return Vector(0, 0, 0);
}
Vector C_BaseEntity::GetBonePos(int i, VMatrix* matrix)
{
	return Vector(matrix[i][0][3], matrix[i][1][3], matrix[i][2][3]);
}

Vector C_BaseEntity::GetHedPos()
{
	return this->GetBonePos(8);

}
CSWeaponInfo* CBaseCombatWeapon::GetCSWpnData()
{
	if (!this)
		return false;
	typedef CSWeaponInfo*(__thiscall* OriginalFn)(void*);
	return  CallVFunction<OriginalFn>(this, 448)(this);
}
#define TIME_TO_TICKS( dt )	( ( int )( 0.5f + ( float )( dt ) / g_globals->interval_per_tick ) )
int C_BaseEntity::GetChockedPackets()
{
	if (GetSimulationTime() > GetOldSimulationTime())
		return TIME_TO_TICKS(fabs(GetSimulationTime() - GetOldSimulationTime()));
	return 0;
}
Vector& C_BaseEntity::m_vecNetworkOrigin() 
{
	
	static int offset = g_pNetVars->GetOffset("DT_CSPlayer", "m_flFriction") - sizeof(Vector);
	return *(Vector*)((DWORD)this + offset);
}

float C_BaseEntity::GetOldSimulationTime()
{

		static uintptr_t offset = g_pNetVars->GetOffset("DT_CSPlayer", "m_flSimulationTime") +0x4;
	return *(float*)((DWORD)this + offset);
}
bool C_BaseEntity::SetupBones(VMatrix *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
{
	//void *pRenderable = (void*)(this + 0x4);
	//typedef bool(__thiscall* OriginalFn)(PVOID, VMatrix*, int, int, float);
	//return CallVFunction<OriginalFn>(pRenderable, 13)(pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime);

	__asm
	{
		mov edi, this
		lea ecx, dword ptr ds : [edi + 0x4]
		mov edx, dword ptr ds : [ecx]
		push currentTime
		push boneMask
		push nMaxBones
		push pBoneToWorldOut
		call dword ptr ds : [edx + 0x34]
	}
}

bool C_BaseEntity::IsTargettingLocal()
{

	Vector src, dst, forward;
	trace_t tr;

	if (!this || !c_ayala::l_player || c_ayala::l_player->GetHealth() < 0)
		return false;

	Vector viewangle = this->GetEyeAngles();

	Math::AngleVectors(viewangle, &forward);
	forward *= 8142.f;
	src = this->GetEyePosition();
	dst = src + forward;

	Ray_t ray;
	ray.Init(src, dst);
	CTraceC_BaseEntity filter;
	filter.pHit = c_ayala::l_player;

	g_pEngineTrace->TraceRay_NEW(ray, MASK_SHOT, &filter, &tr);
	

	if (tr.m_pEnt && tr.m_pEnt->GetTeamNum() != this->GetTeamNum()/*== c_ayala::l_player*/)
		return true;

	return false;
}
bool C_BaseEntity::IsPlayer()
{
	ClientClass* pClass = (ClientClass*)this->GetClientClass();
	return pClass->m_ClassID == 38; // ������ �� 35 � 38 ����� .-.
}

bool CBaseCombatWeapon::IsReloadingVisually() {
	static int m_bReloadVisuallyComplete = g_pNetVars->GetOffset(XorStr("DT_WeaponCSBase"), XorStr("m_bReloadVisuallyComplete"));
	return !GetFieldValue<bool>(m_bReloadVisuallyComplete);
}

float_t &C_BaseEntity::m_flMaxspeed()
{
	static unsigned int _m_flMaxspeed = g_pData->Find(GetPredDescMap(), "m_flMaxspeed");
	return *(float_t*)((uintptr_t)this + _m_flMaxspeed);
}

float_t &C_BaseEntity::m_surfaceFriction()
{
	static unsigned int _m_surfaceFriction = g_pData->Find(GetPredDescMap(), "m_surfaceFriction");
	return *(float_t*)((uintptr_t)this + _m_surfaceFriction);
}
AnimationLayer *C_BaseEntity::GetAnimOverlays()
{
	return *(AnimationLayer**)((DWORD)this + 0x2980);
}

AnimationLayer *C_BaseEntity::GetAnimOverlay(int i)
{
	if (i < 15)
		return &GetAnimOverlays()[i];
}
AnimationLayer& C_BaseEntity::GetAnimOverlay2(int Index)
{
	return (*(AnimationLayer**)((DWORD)this + 0x2980))[Index];
}
int C_BaseEntity::GetSequenceActivity(int sequence)
{
	auto hdr = g_pModelInfo->GetStudioModel(this->GetModel());

	if (hdr == nullptr)
		return -1;

	if (!hdr)
		return -1;

	// sig for stuidohdr_t version: 53 56 8B F1 8B DA 85 F6 74 55
	// sig for C_BaseAnimating version: 55 8B EC 83 7D 08 FF 56 8B F1 74 3D
	// c_csplayer vfunc 242, follow calls to find the function.

	static auto get_sequence_activity = reinterpret_cast<int(__fastcall*)(void*, studiohdr_t*, int)>(offys.getSequenceActivity);

	return get_sequence_activity(this, hdr, sequence);
}


CBasePlayerAnimState* C_BaseEntity::GetBasePlayerAnimState()
{
	static int basePlayerAnimStateOffset = 0x3900;
	if (!this) return false;
	return *(CBasePlayerAnimState**)((DWORD)this + basePlayerAnimStateOffset);
	if (!this) return false;

}

void C_BaseEntity::SetBasePlayerAnimState(CBasePlayerAnimState * state) {
	*reinterpret_cast< CBasePlayerAnimState ** >(uintptr_t(this) + 0x3900) = state;
}
CCSPlayerAnimState *C_BaseEntity::GetPlayerAnimState()
{
	return *(CCSPlayerAnimState**)((DWORD)this + 0x3900);
}

void C_BaseEntity::UpdateAnimationState(CCSGOPlayerAnimState *state, QAngle angle)
{
	static auto UpdateAnimState = FindPatternIDA((c_ayala::Panorama ? "client_panorama.dll" : "client.dll"), "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 F3 0F 11 54 24");
	if (!UpdateAnimState)
		return;

	__asm
	{
		mov ecx, state

		movss xmm1, dword ptr[angle + 4]
		movss xmm2, dword ptr[angle]

		call UpdateAnimState
	}
}

void C_BaseEntity::ResetAnimationState(CCSGOPlayerAnimState *state)
{
	using ResetAnimState_t = void(__thiscall*)(CCSGOPlayerAnimState*);
	static auto ResetAnimState = (ResetAnimState_t)FindPatternIDA((c_ayala::Panorama ? "client_panorama.dll" : "client.dll"), "56 6A 01 68 ? ? ? ? 8B F1");
	if (!ResetAnimState)
		return;

	ResetAnimState(state);
}

void C_BaseEntity::CreateAnimationState(CCSGOPlayerAnimState *state)
{
	using CreateAnimState_t = void(__thiscall*)(CCSGOPlayerAnimState*, C_BaseEntity*);
	static auto CreateAnimState = (CreateAnimState_t)FindPatternIDA(c_ayala::Panorama ? "client_panorama.dll" : "client.dll", "55 8B EC 56 8B F1 B9 ? ? ? ? C7 46");
	if (!CreateAnimState)
		return;

	CreateAnimState(state, this);
}

//void CBaseAnimatinc_ayala::SetBoneMatrix(VMatrix * boneMatrix)
//{
//	if (!this)
//		return;
//	VMatrix* matrix = *(VMatrix**)((DWORD)this + 2470 * sizeof(float));
//	studiohdr_t *hdr = g_pModelInfo->GetStudioModel(this->GetModel());
//	if (!hdr)
//		return;
//	int size = hdr->numbones;
//	if (matrix) {
//		for (int i = 0; i < size; i++)
//			memcpy(matrix + i, boneMatrix + i, sizeof(VMatrix));
//	}
//}
//
//void CBaseAnimatinc_ayala::GetBoneMatrix(VMatrix * boneMatrix)
//{
//	VMatrix* matrix = *(VMatrix**)((DWORD)this + 2470 * sizeof(float));
//	studiohdr_t *hdr = g_pModelInfo->GetStudioModel(this->GetModel());
//	if (!hdr)
//		return;
//	int size = hdr->numbones;
//	if (matrix) {
//		for (int i = 0; i < size; i++)
//			memcpy(boneMatrix + i, matrix + i, sizeof(VMatrix));
//	}
//}
