﻿//#pragma once
//// "GameUtils.h"
//
//#include <string>
//#define RandomInt(nMin, nMax) (rand() % (nMax - nMin + 1) + nMin);
//std::unordered_map<const char*, const char*> g_KillIconCfg;
//#define INVALID_EHANDLE_INDEX 0xFFFFFFFF
//HANDLE worldmodel_handle;
//CBaseCombatWeapon* worldmodel;
//
//int GetKnifeDefinitionIndex(int si)
//{
//	// 0 - Bayonet, 1 - Flip, 2 - Gut, 3 - Karambit, 4 - M9Bayonet, 5 - Huntsman, 6 - Falchion, 7 - Bowie, 8 - Butterfly, 9 - Daggers, 10 - Golden
//	switch (si)
//	{
//	case 0:
//		return 500;
//	case 1:
//		return 505;
//	case 2:
//		return 506;
//	case 3:
//		return 507;
//	case 4:
//		return 508;
//	case 5:
//		return 509;
//	case 6:
//		return 512;
//	case 7:
//		return 514;
//	case 8:
//		return 515;
//	case 9:
//		return 516;
//	default:
//		return -1;
//	}
//}
//
//int GetCurrentKnifeModel(int currentKnife)
//{
//	// 0 - Bayonet, 1 - Flip, 2 - Gut, 3 - Karambit, 4 - M9Bayonet, 5 - Huntsman, 6 - Falchion, 7 - Bowie, 8 - Butterfly, 9 - Daggers, 10 - Golden
//	switch (currentKnife)
//	{
//	case 0:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_bayonet.mdl"); //Bayo
//	case 1:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_flip.mdl"); //Flip
//	case 2:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_gut.mdl"); //Gut
//	case 3:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_karam.mdl"); //Karambit
//	case 4:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_m9_bay.mdl"); //M9 Bayo
//	case 5:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_tactical.mdl"); //Huntsman
//	case 6:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_falchion_advanced.mdl"); //Falchion
//	case 7:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_survival_bowie.mdl"); //Bowie
//	case 8:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_butterfly.mdl"); //Butterfly
//	case 9:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_push.mdl"); //Daggers
//	case 10:
//		return g_pModelInfo->GetModelIndex("models/weapons/v_knife_gg.mdl"); //GunGame
//	default:
//		return 0;
//	}
//}
//
//void SkinChanger()
//{
//    auto pLocal = c_ayala::l_player;
//	player_info_t l_playerInfo;
//	auto weapons = pLocal->m_hMyWeapons();
//	for (size_t i = 0; weapons[i] != INVALID_EHANDLE_INDEX; i++)
//	{
//		C_BaseEntity *pEntity = g_pEntitylist->GetClientEntityFromHandle(weapons[i]);
//		if (pEntity)
//		{
//			ClientClass * pClass = (ClientClass*)pEntity->GetClientClass();
//			CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)pEntity;
//
//		if (Menu.Skinchanger.Enabled)
//			{
//
//				int weapon = *pWeapon->ItemDefinitionIndex();
//				if (weapon)
//				{
//					if (Menu.Skinchanger.W[weapon].ChangerSkin != 0)
//					{
//						*pWeapon->FallbackPaintKit() = Menu.Skinchanger.W[weapon].ChangerSkin;
//					}
//
//				}
//			if (Menu.Skinchanger.knifemodel > 0)
//				{
//					if (pClass->m_ClassID)
//					{
//						if (pClass->m_ClassID == (int)EClassIds::CKnife)
//						{
//
//							worldmodel_handle = pWeapon->m_hWeaponWorldModel();
//							if (worldmodel_handle) worldmodel = (CBaseCombatWeapon*)g_pEntitylist->GetClientEntityFromHandle(worldmodel_handle);
//
//
//							*pWeapon->GetEntityQuality() = 3;
//
//
//							*pWeapon->ModelIndex() = GetCurrentKnifeModel(Menu.Skinchanger.knifemodel - 1); // m_nModelIndex
//
//							*pWeapon->ViewModelIndex() = GetCurrentKnifeModel(Menu.Skinchanger.knifemodel - 1);
//
//							if (worldmodel) *worldmodel->ModelIndex() = GetCurrentKnifeModel(Menu.Skinchanger.knifemodel - 1) + 1;
//
//							*pWeapon->ItemDefinitionIndex() = GetKnifeDefinitionIndex(Menu.Skinchanger.knifemodel - 1);
//
//							*pWeapon->FallbackPaintKit() = Menu.Skinchanger.W[GetKnifeDefinitionIndex(Menu.Skinchanger.knifemodel - 1)].ChangerSkin; // Lore 
//						}
//
//					}
//				}
//			}
//
//
//			*pWeapon->OwnerXuidLow() = 0;
//			*pWeapon->OwnerXuidHigh() = 0;
//			*pWeapon->FallbackStatTrak() = -1;
//			*pWeapon->FallbackWear() = 0.001f;
//			*pWeapon->FallbackSeed() = 0;
//			*pWeapon->ItemIDHigh() = 1;
//
//
//		}
//
//	}
//
//}