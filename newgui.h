#pragma once
#include <Windows.h>
#include <string>
#include <filesystem>
#include "TextureGenerator.h"
namespace fs = std::experimental::filesystem;

struct ColorV2
{
	float red, green, blue, alpha;
};

enum
{
	check_box,
	slider,
	combo_box,
	multi_box
};

class Menuu
{
public:
	bool MenuOpened;
	void Render();

private:
	struct
	{
		float x = 0.f, y = 0.f;
	}Pos; // lol

	enum
	{
		check_box,
		slider,
		combo_box,
		multi_box
	};

	int ControlsX;
	int GroupTabBottom;
	int OffsetY;
	int OldOffsetY;
	int TabOffset;
	int AddWidth = 0;
	int SubTabOffset;
	float SubTabSize; // cpp fuckin sux had to make this a float or the whole thing crashes
	float TabSize;
	int GroupTabPos[4];

	int TabNum = 0;
	int SubTabNum = 0;
	int PreviousControl = -1;

	void Tab(std::string name);
	void SubTab(std::string name);
	void CheckBox(std::string name, bool* item);
	void Slider(int max, std::string name, int* item);
	void SliderFloat(int max, std::string name, float * item);
	void ComboBox(std::string name, std::vector< std::string > itemname, int* item);
	void MultiComboBox(std::string name, std::vector< std::string > itemname, bool* item);
	void ColorPicker(std::string name, Color& item);
};

extern Menuu g_Menu;