#pragma once

typedef uint32_t HTTPRequestHandle;
typedef uint64_t SteamAPICall_t;
enum EHTTPMethod
{
	HTTP_METHOD_INVALID = 0,
	HTTP_METHOD_GET,
	HTTP_METHOD_HEAD,
	HTTP_METHOD_POST,
	HTTP_METHOD_PUT,
	HTTP_METHOD_DELETE,
	HTTP_METHOD_OPTIONS,
	HTTP_METHOD_PATCH
};
class ISteamHTTP
{
public:

	// Initializes a new HTTP request, returning a handle to use in further operations on it.  Requires
	// the method (GET or POST) and the absolute URL for the request.  Only http requests (ie, not https) are
	// currently supported, so this string must start with http:// or https:// and should look like http://store.steampowered.com/app/250/ 
	// or such.
	virtual HTTPRequestHandle CreateHTTPRequest(EHTTPMethod eHTTPRequestMethod, const char *pchAbsoluteURL) = 0;

	// Set a context value for the request, which will be returned in the HTTPRequestCompleted_t callback after
	// sending the request.  This is just so the caller can easily keep track of which callbacks go with which request data.
	virtual bool SetHTTPRequestContextValue(HTTPRequestHandle hRequest, uint64_t ulContextValue) = 0;

	// Set a timeout in seconds for the HTTP request, must be called prior to sending the request.  Default
	// timeout is 60 seconds if you don't call this.  Returns false if the handle is invalid, or the request
	// has already been sent.
	virtual bool SetHTTPRequestNetworkActivityTimeout(HTTPRequestHandle hRequest, uint32_t unTimeoutSeconds) = 0;

	// Set a request header value for the request, must be called prior to sending the request.  Will 
	// return false if the handle is invalid or the request is already sent.
	virtual bool SetHTTPRequestHeaderValue(HTTPRequestHandle hRequest, const char *pchHeaderName, const char *pchHeaderValue) = 0;

	// Set a GET or POST parameter value on the request, which is set will depend on the EHTTPMethod specified
	// when creating the request.  Must be called prior to sending the request.  Will return false if the 
	// handle is invalid or the request is already sent.
	virtual bool SetHTTPRequestGetOrPostParameter(HTTPRequestHandle hRequest, const char *pchParamName, const char *pchParamValue) = 0;

	// Sends the HTTP request, will return false on a bad handle, otherwise use SteamCallHandle to wait on
	// asynchronous response via callback.
	//
	// Note: If the user is in offline mode in Steam, then this will add a only-if-cached cache-control 
	// header and only do a local cache lookup rather than sending any actual remote request.
	virtual bool SendHTTPRequest(HTTPRequestHandle hRequest, SteamAPICall_t *pCallHandle) = 0;

	// Sends the HTTP request, will return false on a bad handle, otherwise use SteamCallHandle to wait on
	// asynchronous response via callback for completion, and listen for HTTPRequestHeadersReceived_t and 
	// HTTPRequestDataReceived_t callbacks while streaming.
	virtual bool SendHTTPRequestAndStreamResponse(HTTPRequestHandle hRequest, SteamAPICall_t *pCallHandle) = 0;

	// Defers a request you have sent, the actual HTTP client code may have many requests queued, and this will move
	// the specified request to the tail of the queue.  Returns false on invalid handle, or if the request is not yet sent.
	virtual bool DeferHTTPRequest(HTTPRequestHandle hRequest) = 0;

	// Prioritizes a request you have sent, the actual HTTP client code may have many requests queued, and this will move
	// the specified request to the head of the queue.  Returns false on invalid handle, or if the request is not yet sent.
	virtual bool PrioritizeHTTPRequest(HTTPRequestHandle hRequest) = 0;

	// Checks if a response header is present in a HTTP response given a handle from HTTPRequestCompleted_t, also 
	// returns the size of the header value if present so the caller and allocate a correctly sized buffer for
	// GetHTTPResponseHeaderValue.
	virtual bool GetHTTPResponseHeaderSize(HTTPRequestHandle hRequest, const char *pchHeaderName, uint32_t *unResponseHeaderSize) = 0;

	// Gets header values from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the
	// header is not present or if your buffer is too small to contain it's value.  You should first call 
	// BGetHTTPResponseHeaderSize to check for the presence of the header and to find out the size buffer needed.
	virtual bool GetHTTPResponseHeaderValue(HTTPRequestHandle hRequest, const char *pchHeaderName, uint8_t *pHeaderValueBuffer, uint32_t unBufferSize) = 0;

	// Gets the size of the body data from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the 
	// handle is invalid.
	virtual bool GetHTTPResponseBodySize(HTTPRequestHandle hRequest, uint32_t *unBodySize) = 0;

	// Gets the body data from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the 
	// handle is invalid or is to a streaming response, or if the provided buffer is not the correct size.  Use BGetHTTPResponseBodySize first to find out
	// the correct buffer size to use.
	virtual bool GetHTTPResponseBodyData(HTTPRequestHandle hRequest, uint8_t *pBodyDataBuffer, uint32_t unBufferSize) = 0;

	// Gets the body data from a streaming HTTP response given a handle from HTTPRequestDataReceived_t. Will return false if the 
	// handle is invalid or is to a non-streaming response (meaning it wasn't sent with SendHTTPRequestAndStreamResponse), or if the buffer size and offset 
	// do not match the size and offset sent in HTTPRequestDataReceived_t.
	virtual bool GetHTTPStreamingResponseBodyData(HTTPRequestHandle hRequest, uint32_t cOffset, uint8_t *pBodyDataBuffer, uint32_t unBufferSize) = 0;

	// Releases an HTTP response handle, should always be called to free resources after receiving a HTTPRequestCompleted_t
	// callback and finishing using the response.
	virtual bool ReleaseHTTPRequest(HTTPRequestHandle hRequest) = 0;

	// Gets progress on downloading the body for the request.  This will be zero unless a response header has already been
	// received which included a content-length field.  For responses that contain no content-length it will report
	// zero for the duration of the request as the size is unknown until the connection closes.
	virtual bool GetHTTPDownloadProgressPct(HTTPRequestHandle hRequest, float *pflPercentOut) = 0;

	// Sets the body for an HTTP Post request.  Will fail and return false on a GET request, and will fail if POST params
	// have already been set for the request.  Setting this raw body makes it the only contents for the post, the pchContentType
	// parameter will set the content-type header for the request so the server may know how to interpret the body.
	virtual bool SetHTTPRequestRawPostBody(HTTPRequestHandle hRequest, const char *pchContentType, uint8_t *pubBody, uint32_t unBodyLen) = 0;
};

typedef int32_t SteamPipeHandle;
// handle to single instance of a steam user
typedef int32_t SteamUserHandle;
class ISteamUser;
class ISteamGameServer;
class ISteamFriends;
class ISteamUtils;
class ISteamMatchmaking;
class ISteamContentServer;
class ISteamMatchmakingServers;
class ISteamUserStats;
class ISteamApps;
class ISteamNetworking;
class ISteamRemoteStorage;
class ISteamScreenshots;
class ISteamMusic;
class ISteamMusicRemote;
class ISteamGameServerStats;
class ISteamPS3OverlayRender;
class ISteamHTTP;
class ISteamController;
class ISteamUGC;
class ISteamAppList;
class ISteamHTMLSurface;
class ISteamInventory;
class ISteamVideo;
class ISteamParentalSettings;

class ISteamClient
{
public:
	virtual SteamPipeHandle CreateSteamPipe() = 0;
	virtual bool BReleaseSteamPipe(SteamPipeHandle hSteamPipe) = 0;
	virtual SteamUserHandle ConnectToGlobalUser(SteamPipeHandle hSteamPipe) = 0;
	virtual SteamUserHandle CreateLocalUser(SteamPipeHandle *phSteamPipe, int eAccountType) = 0;
	virtual void ReleaseUser(SteamPipeHandle hSteamPipe, SteamUserHandle hUser) = 0;
	virtual ISteamUser *GetISteamUser(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamGameServer *GetISteamGameServer(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual void SetLocalIPBinding(uint32_t unIP, uint16_t usPort) = 0;
	virtual ISteamFriends *GetISteamFriends(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamUtils *GetISteamUtils(SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamMatchmaking *GetISteamMatchmaking(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamMatchmakingServers *GetISteamMatchmakingServers(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual void *GetISteamGenericInterface(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamUserStats *GetISteamUserStats(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamGameServerStats *GetISteamGameServerStats(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamApps *GetISteamApps(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamNetworking *GetISteamNetworking(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamRemoteStorage *GetISteamRemoteStorage(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamScreenshots *GetISteamScreenshots(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual void RunFrame() = 0;
	virtual uint32_t GetIPCCallCount() = 0;
	virtual void SetWarningMessageHook(int pFunction) = 0;
	virtual bool ShutdownIfAllPipesClosed() = 0;
	virtual ISteamHTTP *GetISteamHTTP(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual int *GetISteamUnifiedMessages(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamController *GetISteamController(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamUGC *GetISteamUGC(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamAppList *GetISteamAppList(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamMusic *GetISteamMusic(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamMusicRemote *GetISteamMusicRemote(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamHTMLSurface *GetISteamHTMLSurface(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual void Set_SteamAPI_CPostAPIResultInProcess(int func) = 0;
	virtual void Remove_SteamAPI_CPostAPIResultInProcess(int func) = 0;
	virtual void Set_SteamAPI_CCheckCallbackRegisteredInProcess(int func) = 0;
	virtual ISteamInventory *GetISteamInventory(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
	virtual ISteamVideo *GetISteamVideo(SteamUserHandle hSteamuser, SteamPipeHandle hSteamPipe, const char *pchVersion) = 0;
};