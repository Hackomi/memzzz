#pragma once
#include "sdk.h"
#include "Singleton.h"
#include "Serversounds.h"

struct DrawData_t 
{
	C_BaseEntity* pEntity = nullptr;
	ImColor imVisibleColor;
	ImColor imHiddenColor;
	float flHealth = 1.f, flDrawHealth = 1.f, flReduceTime = 1.f, flTriggerHealth = 1.f;
	float flTextSize = 1.f;
	int iAlpha = 0;

	struct DormantData_t 
	{
		Vector vecDormantPosition = Vector(0, 0, 0), vecSoundPosition = Vector(0, 0, 0);
		float flSoundRecordTime = 0.f, flUpdateDormantPositionTime = 0.f;
		void operator=(SndInfo_t sound) {vecSoundPosition = *sound.m_pOrigin;}
	} dormantData;

	struct DrawInfo_t
	{
		enum EInfoType
		{
			TEXT = 0,
			BAR
		};
		enum EInfoPosition
		{
			TOP = 0,
			RIGHT,
			BOTTOM,
			LEFT
		};

		struct 
		{
			float flValue = -1.f;
			float flMaxValue = -1.f;
			std::string szName = "ERROR";
			ImColor imColor = ImColor(255, 255, 255);
			bool bInverse = false;
		} BarData;
		float flTextSize = -1.f;
		std::string szText = "ERROR";
		EInfoPosition iPosition = RIGHT;
		EInfoType iType = TEXT;
		int iAlpha = 255;
		ImColor imColor = ImColor(255, 255, 255);
	};

	std::vector<DrawInfo_t> vecDrawData;

	void Clear() 
	{
		flHealth = flDrawHealth = flReduceTime = flTriggerHealth = flTextSize = 1.f;
		iAlpha = 0;
		dormantData.flSoundRecordTime = dormantData.flUpdateDormantPositionTime = 1.f;
		dormantData.vecDormantPosition = dormantData.vecSoundPosition = Vector(0, 0, 0);
	}
};

struct OffScreenData_t
{
	float flRadius;
	float flDistance;
};

struct BulletImpact_t
{
	Vector vecImpact;
	int nIndex;
	bool bWorldImpact;
};
struct IndicatorInfo_t
{
	enum
	{
		TEXT,
		BAR,
		TIMER
	} IndicatorType;
	std::string szInfo = "Error";
	ImColor imColor = ImColor(255, 255, 255, 255);
	float flSize;
	bool bShowInfo;
	float Percent = 0;
	int Row = 0;
	int Column = 0;
};
class CImVisuals : public Singleton<CImVisuals>
{
public:
	void Run();
	void Event(IGameEvent* pEvent);
	ImDrawList* pDrawList = nullptr;
	void DrawCustomUI();
private:
	// Data
	DrawData_t drawData[64];
	std::vector<C_BaseEntity*> vecDormantRestore;
	std::deque<OffScreenData_t> vecOffScreenData;
	std::deque<BulletImpact_t> vecImpactsData;
	std::deque<IndicatorInfo_t> vecIndicatorInfo;
	bool bDataClear;

	// World
	void DrawHitTracers(bool bWorldImpacts);
	void DrawImpacts(BulletImpact_t impact, bool bWorldImpacts);

	// Players
	void CollectOffScreenData(C_BaseEntity* pEntity); //�������� ���������� � ��� ���� �� �����
	void DrawTextInfo(C_BaseEntity* pEntity);
	RECT GetBox(C_BaseEntity* pEntity); //������� ����� �������
	void DrawBox(C_BaseEntity* pEntity);
	void DrawCornersBox(C_BaseEntity* pEntity);
	void DrawHealthBar(C_BaseEntity* pEntity, bool bAnimated);
	void UpdateDormantInfo(C_BaseEntity* pEntity);

	void DrawLby();

	// Local
	void DrawOffScreen();

	// Other
	void DrawGrenades();
	void OtherEntity();
	void unfilled_circle(int x, int y, int r, int p, D3DCOLOR color, float percentage_of_filling, int thickness);
	// Grouping
	void SetupExtendedESP();
	void DestroyExtendedESP();

	// Other Function
	void UpdateSoundInfo();
	bool Animate(float& flIn, float flDelta, float flNeed, float flTime);
	void DrawTextInfo(DrawData_t::DrawInfo_t textData, ImVec2 pos, ImVec2 size);
	void DrawBarInfo(DrawData_t::DrawInfo_t textData, ImVec2 pos, ImVec2 size);
	void DrawIndicator(IndicatorInfo_t info, ImVec2 pos);
};