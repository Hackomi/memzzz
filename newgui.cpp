#include "sdk.h"
#include "hooks.h"
#include "Menu.h"
#include "global.h"
#include "Hitmarker.h"
#include "imgui\imconfig.h"
#include "ImGUI\imgui.h"
#include "ImGUI\imgui_internal.h"
#include "ImGUI\stb_rect_pack.h"
#include "ImGUI\stb_textedit.h"
#include "ImGUI\stb_truetype.h"
#include "ImGUI\imgui_impl_dx9.h"
#include "Items.h"
#include "event_log.h"
#include "Config.h"
#include "imgui\ColorVar.h"
#include "GameUtils.h"
#include "fake_latency.hpp"
#include <Shlobj.h>
#include "imvisuals.h"
#include "Font.h"
#include "DynSkin.h"
#include "Math.h"
#include "newgui.h"

Menuu g_Menu;

void Menuu::Render()
{
	static bool Pressed = false;

	if (!Pressed && GetAsyncKeyState(VK_INSERT))
		Pressed = true;
	else if (Pressed && !GetAsyncKeyState(VK_INSERT))
	{
		Pressed = false;
		MenuOpened = !MenuOpened;
	}

	static Vector2D oldPos;
	static Vector2D mousePos;

	static int dragX = 0;
	static int dragY = 0;
	static int Width = 400;
	static int Height = 340;

	static int iScreenWidth, iScreenHeight;

	static std::string Title = XorStr("filatov ne ubevai meny pozalsuta"); 
	static std::string bottomText = XorStr("copyright (c) oayala.cLeb");

	static bool Dragging = false;
	bool click = false;

	if (MenuOpened)
	{
		if (GetAsyncKeyState(VK_LBUTTON))
			click = true;

		g_pEngine->GetScreenSize(iScreenWidth, iScreenHeight);
		Vector2D MousePos = g_pSurface->GetMousePosition();

		if (Dragging && !click)
		{
			Dragging = false;
		}

		if (Dragging && click)
		{
			Pos.x = MousePos.x - dragX;
			Pos.y = MousePos.y - dragY;
		}

		if (g_pSurface->MouseInRegion(Pos.x, Pos.y, Width, 20))
		{
			Dragging = true;
			dragX = MousePos.x - Pos.x;
			dragY = MousePos.y - Pos.y;
		}

		if (Pos.x < 0)
			Pos.x = 0;
		if (Pos.y < 0)
			Pos.y = 0;
		if ((Pos.x + Width + AddWidth) > iScreenWidth)
			Pos.x = iScreenWidth - Width - AddWidth;
		if ((Pos.y + Height) > iScreenHeight)
			Pos.y = iScreenHeight - Height;
		AddWidth = 0;
		g_pSurface->RoundedFilledRect(Pos.x, Pos.y, Width, Height, 10, Color(25, 25, 25, 225));
		g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 20, Width, Height - 40, 5, Color(33, 33, 33, 255));

		GroupTabPos[0] = Pos.x + 85;
		GroupTabPos[1] = Pos.y + 25;
		GroupTabPos[2] = Width - 91;
		GroupTabPos[3] = Height - 50;

		ControlsX = GroupTabPos[0];
		GroupTabBottom = GroupTabPos[1] + GroupTabPos[3];

		g_pSurface->DrawT(Pos.x + 6, Pos.y + 2, Color(255, 255, 255, 255), c_ayala::CourierNew, false, Title.c_str()); // title
		g_pSurface->DrawT(Pos.x + 6, Pos.y + (Height - 18), Color(255, 255, 255, 255), c_ayala::CourierNew, false, bottomText.c_str()); 

		OffsetY = GroupTabPos[1] + 7;

		static bool CfgInitLoad = true;
		static bool CfgInitSave = true;

		static int SaveTab = 0;

		if (g_pSurface->MouseInRegion(Pos.x, Pos.y + 295, 80, 26))
		{
			if (CfgInitLoad && click)
			{
				SaveTab = 0;

				Config->Save();
				CfgInitLoad = false;
			}
		}
		else
			CfgInitLoad = true;

		if (g_pSurface->MouseInRegion(Pos.x, Pos.y + 270, 80, 26))
		{
			if (CfgInitSave && click)
			{
				SaveTab = 1;

				Config->Load();
				CfgInitSave = false;
			}
		}
		else
			CfgInitSave = true;

		g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 270, 70, 50, 5, Color(119, 119, 119, 255));

		if (SaveTab == 0)
		{
			g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 295, 80, 25, 5, Color(120, 209, 109, 255));
			g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 295, 70, 25, 5, Color(53, 53, 53, 255));
		}
		else
		{
			g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 270, 80, 25, 5, Color(120, 209, 109, 255));
			g_pSurface->RoundedFilledRect(Pos.x, Pos.y + 270, 70, 25, 5, Color(53, 53, 53, 255));
		}

		g_pSurface->DrawT(Pos.x + 6, Pos.y + 270 + 2, Color(255, 255, 255, 255), c_ayala::CourierNew, false, "save");
		g_pSurface->DrawT(Pos.x + 6, Pos.y + 295 + 2, Color(255, 255, 255, 255), c_ayala::CourierNew, false, "load");

		TabOffset = 0;
		SubTabOffset = 0;
		PreviousControl = -1;
		OldOffsetY = 0;

		Tab("aim");
		{
			SubTab("main"); // hetchansh i mindmag i td
			{
				CheckBox("resolver", &Menu.Ragebot.AutomaticResolver);
			}

			SubTab("hitscan"); // scani
			{
			}

			SubTab("target"); // vibor targeta ili eshe cho
			{
				static Color color, color2, color3, color4, color5;
				CheckBox("Enable", &Menu.LegitBot.bEnable);
				//Slider(100, "Test", (int*)&Menu.LegitBot.flFov[0]);
				ColorPicker("Test1", color2);
				CheckBox("Enable", &Menu.LegitBot.bEnable);
				ColorPicker("Test2", color3);
				CheckBox("Enable", &Menu.LegitBot.bEnable);
				ColorPicker("Test3", color4);
				CheckBox("Enable", &Menu.LegitBot.bEnable);
				ColorPicker("Test4", color5);
				CheckBox("Enable", &Menu.LegitBot.bEnable);
				ColorPicker("Test5", color);
			}
		}
		Tab("legit");
		{
			
		}
		Tab("visual");
		{
			SubTab("esp"); // vezuali
			{
			
			}

			SubTab("render"); // chams
			{
			}
		}

		Tab("antiaim"); // ontiaims
		{
			SubTab("Stand");
			{
				CheckBox("Stand", &Menu.Antiaim.AntiaimEnable);
			}
			SubTab("Move");
			{
				CheckBox("Move", &Menu.Antiaim.AntiaimEnable);
			}
			SubTab("Jump");
			{
				CheckBox("Jump", &Menu.Antiaim.AntiaimEnable);
			}
		}

		Tab("misc"); // mesk
		{
		
		}

		TabSize = TabOffset;
		SubTabSize = SubTabOffset;
	}
}

void Menuu::Tab(std::string name)
{
	int TabArea[4] = { Pos.x, Pos.y + 20 + (TabOffset * 25), 80, 26 };

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(TabArea[0], TabArea[1], TabArea[2], TabArea[3]))
		TabNum = TabOffset;

	if (TabOffset == 0)
		g_pSurface->RoundedFilledRect(TabArea[0], TabArea[1], 70, (TabSize * 25), 5, Color(119, 119, 119, 255));

	if (TabOffset == TabNum)
	{
		g_pSurface->RoundedFilledRect(TabArea[0], TabArea[1], 80, TabArea[3], 5, Color(81, 115, 244, 255));
		g_pSurface->RoundedFilledRect(TabArea[0], TabArea[1], 70, TabArea[3], 5, Color(53, 53, 53, 255));
	}

	g_pSurface->DrawT(TabArea[0] + 6, TabArea[1] + 2, Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());

	TabOffset += 1;
	PreviousControl = -1;
}

void Menuu::SubTab(std::string name)
{
	if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;

	RECT TextSize = g_pSurface->GetTextSizeRect(c_ayala::CourierNew, name.c_str());

	static int TabSkip = 0;

	if (SubTabOffset == 0)
		g_pSurface->RoundedFilledRect(GroupTabPos[0], GroupTabPos[1], GroupTabPos[2], 21, 6, Color(119, 119, 119, 255));

	if (SubTabSize != 0 && TabSkip == TabNum)
	{
		/*if (TabNum >= SubTabSize)
			TabNum = 0;*/

		int TabLength = (GroupTabPos[2] / SubTabSize);

		int GroupTabArea[4] = { (GroupTabPos[0]) + (TabLength * SubTabOffset), GroupTabPos[1], TabLength, 21 };

		if ((GroupTabArea[0] + GroupTabArea[3]) <= (GroupTabPos[0] + GroupTabPos[2]))
		{
			int TextPosition[2] = { GroupTabArea[0] + (TabLength / 2) - (TextSize.right / 2), (GroupTabArea[1] + 10) - (TextSize.bottom / 2) };

			if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(GroupTabArea[0], GroupTabArea[1], GroupTabArea[2], GroupTabArea[3]))
				SubTabNum = SubTabOffset;

			int Offset = ((SubTabSize - 1) == SubTabOffset) ? 0 : 1;

			if (((SubTabSize - 1) == SubTabOffset) && (((TabLength * SubTabSize) > GroupTabPos[2]) || ((TabLength * SubTabSize) < GroupTabPos[2])))
				Offset = (GroupTabPos[2] - (TabLength * SubTabSize));

			if (SubTabNum == SubTabOffset)
				g_pSurface->RoundedFilledRect(GroupTabArea[0], GroupTabArea[1], GroupTabArea[2] + Offset, GroupTabArea[3], 5, Color(53, 53, 53, 255));

			g_pSurface->DrawT(TextPosition[0], TextPosition[1], Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());
		}
	}

	if (TabSkip != TabNum) // frame skip for drawing
		TabSkip = TabNum;

	if (SubTabOffset == SubTabNum)
		OffsetY += 20;

	SubTabOffset += 1;
	PreviousControl = -1;
}

void Menuu::CheckBox(std::string name, bool* item)
{
	if (GroupTabBottom <= OffsetY + 16)
		return;

	if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;

	if (SubTabOffset != 0)
		if (SubTabOffset - 1 != SubTabNum)
			return;

	static bool pressed = false;

	if (!GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 277, OffsetY, 16, 16))
	{
		if (pressed)
			*item = !*item;
		pressed = false;
	}

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 277, OffsetY, 16, 16) && !pressed)
		pressed = true;

	if (*item == true)
		g_pSurface->RoundedFilledRect(ControlsX + 277, OffsetY, 16, 16, 5, Color(120, 209, 109, 255));
	else
		g_pSurface->RoundedFilledRect(ControlsX + 277, OffsetY, 16, 16, 5, Color(192, 97, 108, 255));

	g_pSurface->DrawT(ControlsX + 6, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());

	OldOffsetY = OffsetY;
	OffsetY += 26;
	PreviousControl = check_box;
}

void Menuu::Slider(int max, std::string name, int* item)
{
	if (GroupTabBottom <= OffsetY + 16)
		return;

	if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;

	if (SubTabOffset != 0)
		if (SubTabOffset - 1 != SubTabNum)
			return;

	float pixelValue = max / 114.f;

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY + 5, 120, 8))
		*item = (g_pSurface->GetMousePosition().x - (ControlsX + 155)) * pixelValue;

	if (*item > max)
		*item = max;
	if (*item < 0)
		*item = 0;

	g_pSurface->DrawT(ControlsX + 6, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());
	g_pSurface->RoundedFilledRect(ControlsX + 153, OffsetY + 5, 120, 8, 5, Color(62, 62, 62, 255));
	g_pSurface->RoundedFilledRect(ControlsX + 153 + (*item / pixelValue), OffsetY + 5, 6, 9, 5, Color(119, 119, 119, 255));

	g_pSurface->DrawT(ControlsX + 292, OffsetY + 1, Color(255, 255, 255, 255), c_ayala::CourierNew, true, std::to_string(*item).c_str());

	OldOffsetY = OffsetY;
	OffsetY += 26;
	PreviousControl = slider;
}

void Menuu::ComboBox(std::string name, std::vector< std::string > itemname, int* item)
{
	if (GroupTabBottom <= OffsetY + 16)
		return;

	if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;

	if (SubTabOffset != 0)
		if (SubTabOffset - 1 != SubTabNum)
			return;

	bool pressed = false;
	bool open = false;
	static bool selectedOpened = false;
	static bool clickRest;
	static bool rest;
	static std::string nameSelected;

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY, 140, 16) && !clickRest)
	{
		nameSelected = name;
		pressed = true;
		clickRest = true;
	}
	else if (!GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY, 140, 16))
		clickRest = false;

	if (pressed)
	{
		if (!rest)
			selectedOpened = !selectedOpened;

		rest = true;
	}
	else
		rest = false;

	if (nameSelected == name)
		open = selectedOpened;

	g_pSurface->DrawT(ControlsX + 6, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());
	g_pSurface->RoundedFilledRect(ControlsX + 153, OffsetY, 140, 16, 5, Color(119, 119, 119, 255));

	if (open)
	{
		g_pSurface->RoundedFilledRect(ControlsX + 153, OffsetY, 140, 17 + (itemname.size() * 16), 5, Color(119, 119, 119, 255));

		for (int i = 0; i < itemname.size(); i++)
		{
			if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY + 16 + i * 16, 140, 16))
				*item = i;

			if (*item == i)
				g_pSurface->RoundedFilledRect(ControlsX + 154, OffsetY + 16 + (i * 16), 138, 16, 5, Color(53, 53, 53, 255));

			g_pSurface->DrawT(ControlsX + 159, OffsetY + 16 + (i * 16), Color(255, 255, 255, 255), c_ayala::CourierNew, false, itemname.at(i).c_str());
		}
	}

	g_pSurface->DrawT(ControlsX + 159, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, itemname.at(*item).c_str());

	OldOffsetY = OffsetY;

	if (open)
		OffsetY += 26 + (itemname.size() * 16);
	else
		OffsetY += 26;

	PreviousControl = combo_box;
}

void Menuu::MultiComboBox(std::string name, std::vector< std::string > itemname, bool* item)
{
	if (GroupTabBottom <= OffsetY + 16)
		return;

	if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;

	if (SubTabOffset != 0)
		if (SubTabOffset - 1 != SubTabNum)
			return;

	static bool multiPressed = false;
	bool pressed = false;
	bool open = false;
	static bool selectedOpened = false;
	static bool clickRest;
	static bool rest;
	static std::string nameSelected;
	std::string itemsSelected = "";
	int lastItem = 0;

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY, 140, 16) && !clickRest)
	{
		nameSelected = name;
		pressed = true;
		clickRest = true;
	}
	else if (!GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY, 140, 16))
		clickRest = false;

	if (pressed)
	{
		if (!rest)
			selectedOpened = !selectedOpened;

		rest = true;
	}
	else
		rest = false;

	if (nameSelected == name)
		open = selectedOpened;

	g_pSurface->DrawT(ControlsX + 6, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, name.c_str());
	g_pSurface->RoundedFilledRect(ControlsX + 153, OffsetY, 140, 16, 5, Color(119, 119, 119, 255));

	if (open)
	{
		g_pSurface->RoundedFilledRect(ControlsX + 153, OffsetY, 140, 17 + (itemname.size() * 16), 5, Color(119, 119, 119, 255));

		for (int i = 0; i < itemname.size(); i++)
		{
			if (!GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY + 16 + (i * 16), 140, 16))
			{
				if (multiPressed)
					item[i] = !item[i];
				multiPressed = false;
			}

			if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + 153, OffsetY + 16 + (i * 16), 140, 16) && !multiPressed)
				multiPressed = true;

			if (item[i])
				g_pSurface->RoundedFilledRect(ControlsX + 154, OffsetY + 16 + (i * 16), 138, 16, 5, Color(120, 209, 109, 255));
			else
				g_pSurface->RoundedFilledRect(ControlsX + 154, OffsetY + 16 + (i * 16), 138, 16, 5, Color(192, 97, 108, 255));

			g_pSurface->DrawT(ControlsX + 159, OffsetY + 16 + (i * 16), Color(255, 255, 255, 255), c_ayala::CourierNew, false, itemname.at(i).c_str());
		}

	}

	bool items = false;

	// man look at all these for loops i sure am retarded

	for (int i = 0; i < itemname.size(); i++)
	{
		if (item[i])
		{
			if (lastItem < i)
				lastItem = i;
		}
	}

	for (int i = 0; i < itemname.size(); i++)
	{
		if (item[i])
		{
			items = true;
			RECT TextSize = g_pSurface->GetTextSizeRect(c_ayala::CourierNew, itemsSelected.c_str());
			RECT TextSizeGonaAdd = g_pSurface->GetTextSizeRect(c_ayala::CourierNew, itemname.at(i).c_str());
			if (TextSize.right + TextSizeGonaAdd.right < 130)
				itemsSelected += itemname.at(i) + ((lastItem == i) ? "" : ", ");
		}
	}

	if (!items)
		itemsSelected = "off";

	g_pSurface->DrawT(ControlsX + 159, OffsetY, Color(255, 255, 255, 255), c_ayala::CourierNew, false, itemsSelected.c_str());

	OldOffsetY = OffsetY;

	if (open)
		OffsetY += 26 + (itemname.size() * 16);
	else
		OffsetY += 26;

	PreviousControl = multi_box;
}

void Menuu::ColorPicker(std::string name, Color& item) // best coder in the universe
{
	if (GroupTabBottom <= OffsetY + 16)
		return;

	/*if (TabOffset - 1 != TabNum || TabOffset == 0)
		return;*/

	/*if (SubTabOffset != 0)
		if (SubTabOffset - 1 != SubTabNum)
			return;*/

	if (PreviousControl == slider || PreviousControl == -1)
		return;

	int CtrXoffset = 0;

	if (PreviousControl != check_box)
		CtrXoffset = 132;
	else
		CtrXoffset = 256;

	int yoffset = OldOffsetY + 10;
	int xoffset = ControlsX + 330;

	bool pressed = false;
	bool open = false;
	static bool selectedOpened = false;
	static bool clickRest;
	static bool rest;
	static std::string nameSelected;

	if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + CtrXoffset, OldOffsetY, 16, 16) && !clickRest)
	{
		nameSelected = name;
		pressed = true;
		clickRest = true;
	}
	else if (!GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(ControlsX + CtrXoffset, OldOffsetY, 16, 16))
		clickRest = false;

	if (pressed)
	{
		if (!rest)
			selectedOpened = !selectedOpened;

		rest = true;
	}
	else
		rest = false;

	if (nameSelected == name)
		open = selectedOpened;
	if (open)
	{
		if (AddWidth == 0)
			AddWidth = 280;

		int CPosX = Pos.x + 410, CPosY = Pos.y;
		g_pSurface->RoundedFilledRect(CPosX, CPosY, 270, 270, 10, Color(25, 25, 25, 225));
		static CColorPickerTexture ColorPicker(200, 200);
		g_pSurface->SetDrawColor(255, 255, 255, 255);
		g_pSurface->DrawSetTexture(ColorPicker.GetTextureId());
		g_pSurface->DrawTexturedRect(CPosX + 10, CPosY + 10, CPosX + 260, CPosY + 260);
		Vector2D MousePos = g_pSurface->GetMousePosition();
		float OffsetX, OffsetY;

		if (GetAsyncKeyState(VK_LBUTTON) && g_pSurface->MouseInRegion(CPosX + 10, CPosY + 10, 250, 250))
		{
			OffsetX = MousePos.x - (CPosX + 10);
			OffsetY = MousePos.y - (CPosY + 10);
		

		if (OffsetX > 250)
			OffsetX = 250;
		if (OffsetX < 0)
			OffsetX = 0;
		if (OffsetY > 250)
			OffsetY = 250;
		if (OffsetY < 0)
			OffsetY = 0;

		g_pSurface->SetDrawColor(255, 255, 255, 255);
		g_pSurface->DrawOutlinedRect(CPosX + 10 + OffsetX - 1, CPosY + 10 + OffsetY - 1, CPosX + 10 + OffsetX + 1, CPosY + 10 + OffsetY + 1);

		item = Color::FromHSB(OffsetX / 250, OffsetY > 125 ? 1 : OffsetY / 125, OffsetY <= 125 ? 1 : 1 - ((OffsetY - 125) / 125));
		}
	}
	
	g_pSurface->RoundedFilledRect(ControlsX + CtrXoffset, OldOffsetY, 16, 16, 5, item);
}