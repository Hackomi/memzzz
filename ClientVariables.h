#pragma once
#include <set>
#include <map>
#include <unordered_map>
extern void GUI_Init();
struct skinInfo
{
	int seed = -1;
	int paintkit;
	std::string tagName;
};


class ClientVariables
{
public:
	struct {
		bool bEnable;
		int iSettingType;
		int iCustomIndex;
		int iBackTrackType[33];
		int iFovType[33];
		int iSmoothType[33];
		float flSmooth[33];
		float flFov[33];
		float flRecoil[33];
		float flPrediction[33];
		int iDelay[33];
		float flSwitchTargetDelay[33];
		int iAimType[33];
		bool bUseSpecificAim[33];
		int iSpecificAimType[33];
	} LegitBot;
	struct Ragebot
	{
		bool EnableAimbot = false;
		int AimbotSelection = 0;
		bool AutomaticFire = false;
		bool AutomaticScope = false;
		bool SilentAimbot = false;
		bool NoRecoil = false;
		bool NoSpread = false;
		bool PositionAdjustment = false;
		int iPositionAdjustment = 0;
		int Preferbodyaim = 0;
		int Hitbox = 0;
		int Hitscan = 0;
		bool AutowallHitscan = false;
		bool Autowall = false;
		int Multipoint = 0.f;
		float Headscale = 0.0f;
		float Bodyscale = 0.0f;
		float Fakewalksp = 1.f;
		int Mindamage = 1.f;
		bool Hitchance = false;
		int Minhitchance = 0.f;
		bool AutomaticResolver;
		int ResolverOverride = 0;
		bool VelocityReduce = 0;
		bool FriendlyFire = false;
		bool Quickstop = false;
		int multimemez;
		int multimemez2;
		bool headaimwalking;
		bool HitScanBones[8];
		bool AutomaticRevolver;
		bool NewAutomaticRevolver;
		int NewAutomaticRevolverFactor;
		bool FakeLatency;
		int fakepingkey;
		float FakeLatencyAmount;
		struct 
		{
			bool Enable[7];
			int MPSize[7];
			int Priority[7];
			float PointScale[7];
		} HitboxSetting;
	} Ragebot;

	struct Antiaim
	{
		bool AntiaimEnable = false;
		bool Lines = false;
		bool Indicator = false;
		bool MANUALIndicator = false;
		bool LMIndicator = false;
		bool LAGIndicator = false;
		bool PINGIndicator = false;
		int Pitch = 0;

		int Yaw = 0;
		int YawAdd = 0;
		int YawRunning = 0;
		int YawRunningAdd = 0;
		int FakeYaw = 0;
		int FakeYawAdd = 0;

		int Fakewalk = 0;

		int breakaa;
		int FreestandingDelta = 0;
		int AutoDirectionDistance = 0;
		int AtPlayer = 0;
		bool EnableFakeWalk = false;
		int spinspeed;
		float Jitterspeed;
		bool Disableaa = false;
		int Jitterrange = 0.f;
		bool Randomizejitter = false;
		int AntiaimMode = 0;
		bool BreakMoveLBY = false;
		int MoveLBYDelta = 180;
		int DOWN = 0;
		int LEFT = 0;
		int RIGHT = 0;
		int FakeWalkKey = 0;
	} Antiaim;

	struct Visuals
	{
		bool EspEnable = false;
		bool EnemyOnly = false;
		bool BoundingBox = false;
		bool Bones = false;
		bool Health = false;
		bool Armor = false;
		bool Scoped = false;
		bool Flags = false;
		bool Fake = false;
		bool Dlight = false;
		bool Name = false;
		bool Weapon = false;
		bool Ammo = false;
		bool AllItems = false;
		bool Rank = false;
		bool Radar = false;
		bool NoFlash = false;
		int thirdperson_dist = 100;
		bool Monitor = false;
		bool Wins = false;
		bool Glow = false;
		bool LGlow = false;
		bool PulseLGlow = false;
		bool LineofSight = false;
		bool SnapLines = false;
		bool GrenadePrediction = false;
		int Crosshair = 0;
		bool SpreadCrosshair = false;
		bool RecoilCrosshair = false;
		bool FartherESP = false;
		bool localESP = false;
		//Cbase/filers
		int DroppedWeapons = 0;
		bool Hostage = false;
		bool ThrownNades = false;
		bool l_player = false;
		bool BulletTracers = false;
		bool Bomb = false;
		bool Spectators = false;
		bool OutOfPOVArrows = false;
		bool DamageIndicators = false;
		bool shaokpidork = false;
		bool lbytimer = false;
		//Effects/world
		bool nightmode = false;
		bool blendonscope = false;
		int playeralpha = 255;
		int scopeplayeralpha = 255;
		int enemyalpha = 255;
		int Skybox = 0;
		int FlashbangAlpha = 0;
		bool Nosmoke = false;
		bool Noscope = false;
		bool RemoveParticles = false;
		bool Novisrevoil = false;
		bool Hitmarker = false;
		bool ChamsEnable = false;
		bool ShowBacktrack = false;
		int ShowBacktrackMethod = 0;
		int ChamsStyle = 0;
		bool ChamsL = false;
		bool lagModel = false;
		bool crosssnip = false;
		bool Chamsenemyonly = false;
		bool ChamsPlayer = false;
		bool ChamsPlayerWall = false;
		bool ChamsHands = false;
		bool ChamsHandsWireframe = false;
		bool WeaponWireframe = false;
		bool WeaponChams = false;
		float VisualsSize = 15.f;
		struct
		{
			int resolution = 5;
			int type;
		}Spread;
		int htSound = 0;
	} Visuals;

	struct Misc
	{
		bool AntiUT = true;
		bool WireHand = false;
		bool static_scope = false;
		int PlayerFOV = 0.f;
		int PlayerViewmodel = 0.f;
		int TPangles = 0;
		int TPKey = 0;
		int MenuKey = 0x2d;
		int WalkbotSet = 0x2d;
		int WalkbotDelete = 0x2d;
		int WalkbotStart = 0x2d;
		bool Clantag;
		bool skins;
		bool AutoJump = false;
		bool AutoStrafe = false;
		bool AutoAccept = false;
		bool Prespeed = false;
		int Retrack = 0.f;
		int PrespeedKey = 0;
		bool FakelagEnable = false;
		bool FakelagOnground = false;
		int FakelagMode = 0;
		int FakelagAmount = 0.f;
		bool FakelagShoot = false;
		int ConfigSelection = 0;
		bool Walkbot = false;
		bool WalkbotBunnyhop = false;
		//int WalkbotSetPoint = 0;
		//int WalkbotDeletePoint = 0;
		//int WalkbotStart = 0;
		bool FakewalkEnable = false;
		int FakewalkKey = 0;
		bool legitbacktrack;
		bool Spectators = true;
		char configname[128];
		bool FPSAbuse;
		int FPSValue;
	} Misc;

	struct 
	{
		bool Enabled;
		int knifemodel;
		int glove;
		int gloveskin;
		int WeaponSkin;
		struct
		{
			bool ChangerEnabled = false;
			int ChangerSkin = 0;
		} W[519]; // CHANGE HERE
		bool SkinFilter;
		std::unordered_map<std::string, std::set<std::string>> weaponSkins;
		std::unordered_map<std::string, skinInfo> skinMap;
		std::unordered_map<std::string, std::string> skinNames;
		std::unordered_map<std::string, std::string> weaponNames;
	} Skinchanger;

	struct CPlayerlist
	{
		bool bEnabled;
		int iPlayer;
		char* szPlayers[64] = {
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" ", " ", " ", " ", " ", " ", " ", " ", " ",
			" "
		};
	} Playerlist;

	struct ColorV2
	{
		float red, green, blue, alpha;
	};
	struct NigColors
	{
		ColorV2 GlowColor = { 255.f,255.f,255.f,255.f };
		ColorV2 BoxColor = { 255.f,255.f,255.f,255.f };


		float BoundingBox[4] = { 1.f, 1.f, 1.f, 1.f };
		float PlayerChams[4] = { 1.f, 1.f, 1.f, 1.f };
		float PlayerChamsRf[4] = { 1.f, 1.f, 1.f, 1.f };
		float ChamsHistory[4] = { 1.f, 1.f, 1.f, 1.f };
		float chamsalpha[4] = { 1.f, 1.f, 1.f, 1.f };
		float PlayerChamsl[4] = { 1.f, 1.f, 1.f, 1.f };
		float styleshands[4] = { 1.f, 1.f, 1.f, 1.f };
		float PlayerChamsWall[4] = { 1.f, 1.f, 1.f, 1.f };
		float PlayerChamsWallRf[4] = { 1.f, 1.f, 1.f, 1.f };
		float Skeletons[4] = { 1.f, 1.f, 1.f, 1.f };
		float Bulletracer[4] = { 1.f, 1.f, 1.f, 1.f };

		float WireframeHand[4] = { 1.f, 1.f, 1.f, 1.f };
		float ChamsHand[4] = { 1.f, 1.f, 1.f, 1.f };
		float ChamsWeapon[4] = { 1.f, 1.f, 1.f, 1.f };
		float WireframeWeapon[4] = { 1.f, 1.f, 1.f, 1.f };
		float Glow[4] = { 1.f, 1.f, 1.f, 1.f };
		float LGlow[4] = { 1.f, 1.f, 1.f, 1.f };
		float DroppedWeapon[3] = { 1.f, 1.f, 1.f };
		float Bomb[3] = { 1.f, 1.f, 1.f };
		float PlantedBomb[3] = { 1.f, 1.f, 1.f };
		float Hostage[3] = { 1.f, 1.f, 1.f };
		float GrenadePrediction[3] = { 1.f, 1.f, 1.f };
		float FakeAngleGhost[3] = { 1.f, 1.f, 1.f };
		float SpreadCrosshair[4] = { 1.f, 1.f, 1.f };
		float Snaplines[3] = { 1.f, 1.f, 1.f };
		float DamageIndicator[3] = { 1.f, 1.f, 1.f };
		float lby_timer[3] = { 0.16f, 0.32f, 0.73f };
		float ammo[3] = { 0.16f, 0.32f, 0.73f };
		float Radar[3] = { 0.16f, 0.32f, 0.73f };
		bool Props;
	}Colors;
	struct {
		int tabs;
		int ragesubtabs;
	}Gui;
};

extern ClientVariables Menu;
