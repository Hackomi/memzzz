#include "sdk.h"
#include "Config.h"
#include <Shlobj.h>
#include "GameUtils.h"
#define RAGE	XorStr("Ragebot")

#define ANTIAIM	XorStr("Antiaim")

#define ESP	    XorStr("Visuals")
#define MISC    XorStr("Misc")
#define LEGIT	XorStr("Legit")
inline char* strcats(char* s1, char* s2)
{
	std::string ss1(s1);
	std::string ss2(s2);
	std::string Buff = ss1 + ss2;
	char *Buffer = _strdup(Buff.c_str());
	return Buffer;
}

#define F false
#define T true
#define V 0

void Confic_ayala::Setup()
{

	/*----------------------------RAGEBOT------------------------------------*/
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[0], F, RAGE, "HitboxSetting0");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[1], F, RAGE, "HitboxSetting1");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[2], F, RAGE, "HitboxSetting2");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[3], F, RAGE, "HitboxSetting3");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[4], F, RAGE, "HitboxSetting4");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[5], F, RAGE, "HitboxSetting5");
	SetupValue(Menu.Ragebot.HitboxSetting.Enable[6], F, RAGE, "HitboxSetting6");

	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[0], V, RAGE, "MPSize0");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[1], V, RAGE, "MPSize1");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[2], V, RAGE, "MPSize2");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[3], V, RAGE, "MPSize3");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[4], V, RAGE, "MPSize4");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[5], V, RAGE, "MPSize5");
	SetupValue(Menu.Ragebot.HitboxSetting.MPSize[6], V, RAGE, "MPSize6");

	SetupValue(Menu.Ragebot.HitboxSetting.Priority[0], V, RAGE, "Priority0");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[1], V, RAGE, "Priority1");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[2], V, RAGE, "Priority2");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[3], V, RAGE, "Priority3");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[4], V, RAGE, "Priority4");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[5], V, RAGE, "Priority5");
	SetupValue(Menu.Ragebot.HitboxSetting.Priority[6], V, RAGE, "Priority6");

	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[0], V, RAGE, "PointScale0");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[1], V, RAGE, "PointScale1");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[2], V, RAGE, "PointScale2");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[3], V, RAGE, "PointScale3");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[4], V, RAGE, "PointScale4");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[5], V, RAGE, "PointScale5");
	SetupValue(Menu.Ragebot.HitboxSetting.PointScale[6], V, RAGE, "PointScale6");

	SetupValue(Menu.Ragebot.iPositionAdjustment, V, RAGE, XorStr("posjust"));
	SetupValue(Menu.Ragebot.EnableAimbot, F, RAGE, XorStr("Aimbot_Enable"));
	SetupValue(Menu.Ragebot.SilentAimbot, F, RAGE, XorStr("Aimbot_Silent"));
	SetupValue(Menu.Ragebot.PositionAdjustment, F, RAGE, XorStr("Aimbot_Backtrack"));
	SetupValue(Menu.Ragebot.AutomaticFire, F, RAGE, XorStr("Auto_Fire"));
	SetupValue(Menu.Ragebot.VelocityReduce, F, RAGE, XorStr("Velocity_Reduce"));
	SetupValue(Menu.Ragebot.AutomaticRevolver, F, RAGE, XorStr("Auto_R8"));
	SetupValue(Menu.Ragebot.AutomaticScope, F, RAGE, XorStr("Auto_Scope"));
	SetupValue(Menu.Ragebot.Autowall, F, RAGE, XorStr("Auto_Wall"));
	SetupValue(Menu.Ragebot.AutomaticResolver, F, RAGE, XorStr("Resolver"));
	//SetupValue(Menu.Ragebot.ResolverOverride, F, RAGE, "Resolver_Override");
	SetupValue(Menu.Ragebot.NoRecoil, F, RAGE, XorStr("Remove_Recoil"));
	SetupValue(Menu.Ragebot.NoSpread, F, RAGE, XorStr("Remove_Spread"));// ����� ����� � �������� ������� ����� ����� ������� ������ ��
	SetupValue(Menu.Ragebot.Minhitchance, 0.f, RAGE, XorStr("Hitchance"));
	SetupValue(Menu.Ragebot.Mindamage, 0.f, RAGE, XorStr("Mindamage"));
	SetupValue(Menu.Ragebot.FakeLatency, F, RAGE, XorStr("Fake_Ping"));
	SetupValue(Menu.Ragebot.FakeLatencyAmount, 0.1f, RAGE, XorStr("Fake_Ping_Amount"));
	SetupValue(Menu.Ragebot.Hitbox, V, RAGE, XorStr("Priority_Hitbox"));
	SetupValue(Menu.Ragebot.Hitscan, V, RAGE, XorStr("Hitscan_Type"));
	SetupValue(Menu.Ragebot.Headscale, V, RAGE, XorStr("Multipoint_Scale_Head"));
	SetupValue(Menu.Ragebot.Bodyscale, V, RAGE, XorStr("Multipoint_Scale_Other"));
	/*-----------------------------ANTIAIM------------------------------------*/
	SetupValue(Menu.Antiaim.AntiaimEnable, F, ANTIAIM, XorStr("Antiaim_Enable"));
	SetupValue(Menu.Antiaim.Pitch, V, ANTIAIM, XorStr("Antiaim_Pitch"));
	SetupValue(Menu.Antiaim.AntiaimMode, V, ANTIAIM, XorStr("AntiaimMode"));
	SetupValue(Menu.Antiaim.Jitterrange, V, ANTIAIM, XorStr("Jitterrange"));
	SetupValue(Menu.Antiaim.spinspeed, V, ANTIAIM, XorStr("spinspeed"));
	SetupValue(Menu.Antiaim.Jitterrange, V, ANTIAIM, XorStr("Jitterrange"));
	SetupValue(Menu.Antiaim.Jitterspeed, V, ANTIAIM, XorStr("Jitterspeed"));
	SetupValue(Menu.Antiaim.AutoDirectionDistance, V, ANTIAIM, XorStr("AutoDirectionDistance"));
	SetupValue(Menu.Antiaim.FreestandingDelta, V, ANTIAIM, XorStr("Antiaim_LBY_Delta"));
	SetupValue(Menu.Misc.FakelagEnable, F, ANTIAIM, XorStr("Antiaim_Fakelag_Enable"));
	SetupValue(Menu.Misc.FakelagOnground, F, ANTIAIM, XorStr("Antiaim_Fakelag_Enable_On_Ground"));
	SetupValue(Menu.Misc.FakelagMode, V, ANTIAIM, XorStr("Antiaim_Fakelag_Type"));
	SetupValue(Menu.Misc.FakelagAmount, 1, ANTIAIM, XorStr("Antiaim_Fakelag_Amount"));
	SetupValue(Menu.Antiaim.FakeWalkKey, V, ANTIAIM, XorStr("fakewalkKey"));
	/*-----------------------------VISUALS------------------------------------*/
	SetupValue(Menu.Visuals.EspEnable, F, ESP, XorStr("Visuals_Enable"));

	/*------------------------------COLORS------------------------------------*/
	//Box
	SetupValue(Menu.Visuals.BoundingBox, F, ESP, XorStr("Visuals_Box"));

	SetupValue(Menu.Colors.BoundingBox[0], V, ESP, "Visuals_Box_Color[R]");
	SetupValue(Menu.Colors.BoundingBox[1], V, ESP, "Visuals_Box_Color[G]");
	SetupValue(Menu.Colors.BoundingBox[2], V, ESP, "Visuals_Box_Color[B]");

	//Radar
	SetupValue(Menu.Visuals.Radar, F, ESP, XorStr("Visuals_Radar"));

	SetupValue(Menu.Colors.Radar[0], V, ESP, "Visuals_Radar_Color[R]");
	SetupValue(Menu.Colors.Radar[1], V, ESP, "Visuals_Radar_Color[G]");
	SetupValue(Menu.Colors.Radar[2], V, ESP, "Visuals_Radar_Color[B]");

	//Skeleton [show backtrack]
	SetupValue(Menu.Visuals.Bones, F, ESP, XorStr("Visuals_Skeleton"));

	SetupValue(Menu.Colors.Skeletons[0], V, ESP, "Visuals_Skeleton_Color[R]");
	SetupValue(Menu.Colors.Skeletons[1], V, ESP, "Visuals_Skeleton_Color[G]");
	SetupValue(Menu.Colors.Skeletons[2], V, ESP, "Visuals_Skeleton_Color[B]");

	//Glow
	SetupValue(Menu.Visuals.Glow, F, ESP, XorStr("Visuals_Glow"));

	SetupValue(Menu.Colors.Glow[0], V, ESP, "Visuals_Glow_Color[R]");
	SetupValue(Menu.Colors.Glow[1], V, ESP, "Visuals_Glow_Color[G]");
	SetupValue(Menu.Colors.Glow[2], V, ESP, "Visuals_Glow_Color[B]");
	SetupValue(Menu.Colors.Glow[3], 200, ESP, "Visuals_Glow_Color[A]");

	//Ammo-Bar
	SetupValue(Menu.Visuals.Ammo, F, ESP, XorStr("Visuals_Ammo"));
	SetupValue(Menu.Visuals.OutOfPOVArrows, F, ESP, XorStr("Visuals_Arrows"));
	
	SetupValue(Menu.Colors.ammo[0], V, ESP, "Visuals_Ammo_Color[R]");
	SetupValue(Menu.Colors.ammo[1], V, ESP, "Visuals_Ammo_Color[G]");
	SetupValue(Menu.Colors.ammo[2], V, ESP, "Visuals_Ammo_Color[B]");

	//LBY Timer [show time before lby update (shake]

	SetupValue(Menu.Colors.lby_timer[0], V, ESP, "Visuals_LBY_Timer_Color[R]");
	SetupValue(Menu.Colors.lby_timer[1], V, ESP, "Visuals_LBY_Timer_Color[G]");
	SetupValue(Menu.Colors.lby_timer[2], V, ESP, "Visuals_LBY_Timer_Color[B]");

	//Bullet Tracer [Beam]
	SetupValue(Menu.Visuals.BulletTracers, F, ESP, XorStr("Visuals_Bullet_Tracer"));

	SetupValue(Menu.Colors.Bulletracer[0], V, ESP, "Visuals_Bullet_Tracer_Color[R]");
	SetupValue(Menu.Colors.Bulletracer[1], V, ESP, "Visuals_Bullet_Tracer_Color[G]");
	SetupValue(Menu.Colors.Bulletracer[2], V, ESP, "Visuals_Bullet_Tracer_Color[B]");

	//Grenade Tracer
	SetupValue(Menu.Visuals.GrenadePrediction, F, ESP, XorStr("Visuals_Projectile"));

	SetupValue(Menu.Colors.GrenadePrediction[0], V, ESP, "Visuals_Projectile_Color[R]");
	SetupValue(Menu.Colors.GrenadePrediction[1], V, ESP, "Visuals_Projectile_Color[G]");
	SetupValue(Menu.Colors.GrenadePrediction[2], V, ESP, "Visuals_Projectile_Color[B]");

	//Spread Crosshair [Fade]
	SetupValue(Menu.Visuals.SpreadCrosshair, F, ESP, XorStr("Visuals_Inaccuracy_Overlay"));

	SetupValue(Menu.Colors.SpreadCrosshair[0], V, ESP, "Visuals_Inaccuracy_Overlay_Color[R]");
	SetupValue(Menu.Colors.SpreadCrosshair[1], V, ESP, "Visuals_Inaccuracy_Overlay_Color[G]");
	SetupValue(Menu.Colors.SpreadCrosshair[2], V, ESP, "Visuals_Inaccuracy_Overlay_Color[B]");
	SetupValue(Menu.Colors.SpreadCrosshair[3], V, ESP, "Visuals_Inaccuracy_Overlay_Color[A]");
	/*---------------------------THIRDPERSON----------------------------------*/
	SetupValue(Menu.Misc.TPKey, V, ESP, XorStr("Visuals_Thirdperson_Key"));
	SetupValue(Menu.Visuals.thirdperson_dist, 100, ESP, XorStr("Visuals_Thirdperson_Distance"));
	SetupValue(Menu.Misc.TPangles, 100, ESP, XorStr("Visuals_Thirdperson_Angle"));

	/*------------------------------CHAMS-------------------------------------*/
	SetupValue(Menu.Visuals.ChamsEnable, F, ESP, XorStr("Visuals_Chams_Enable"));
	SetupValue(Menu.Visuals.ChamsStyle, V, ESP, XorStr("Visuals_Chams_Type"));
	SetupValue(Menu.Visuals.blendonscope, F, ESP, XorStr("Visuals_Chams_Local_Blend_If_Scoped"));
	SetupValue(Menu.Visuals.lagModel, F, ESP, XorStr("Visuals_Chams_Local_Lag_Model_With_Fakelag"));
	SetupValue(Menu.Visuals.ChamsPlayer, F, ESP, XorStr("Visuals_Chams_Enemy"));
	SetupValue(Menu.Colors.PlayerChams[0], V, ESP, "Visuals_Chams_Enemy_Color[R]");
	SetupValue(Menu.Colors.PlayerChams[1], V, ESP, "Visuals_Chams_Enemy_Color[G]");
	SetupValue(Menu.Colors.PlayerChams[2], V, ESP, "Visuals_Chams_Enemy_Color[B]");
	SetupValue(Menu.Colors.PlayerChams[3], V, ESP, "Visuals_Chams_Enemy_Color[A]");

	SetupValue(Menu.Colors.PlayerChamsRf[0], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsRf[R]");
	SetupValue(Menu.Colors.PlayerChamsRf[1], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsRf[G]");
	SetupValue(Menu.Colors.PlayerChamsRf[2], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsRf[B]");
	SetupValue(Menu.Colors.PlayerChamsRf[3], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsRf[A]");

	SetupValue(Menu.Colors.PlayerChamsWallRf[0], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsWallRf[R]");
	SetupValue(Menu.Colors.PlayerChamsWallRf[1], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsWallRf[G]");
	SetupValue(Menu.Colors.PlayerChamsWallRf[2], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsWallRf[B]");
	SetupValue(Menu.Colors.PlayerChamsWallRf[3], V, ESP, "Visuals_Chams_Enemy_Color_PlayerChamsWallRf[A]");

	SetupValue(Menu.Visuals.ChamsPlayerWall, F, ESP, "Visuals_Chams_EnemyXQZ");
	SetupValue(Menu.Colors.PlayerChamsWall[0], V, ESP, "Visuals_Chams_EnemyXQZ_Color[R]");
	SetupValue(Menu.Colors.PlayerChamsWall[1], V, ESP, "Visuals_Chams_EnemyXQZ_Color[G]");
	SetupValue(Menu.Colors.PlayerChamsWall[2], V, ESP, "Visuals_Chams_EnemyXQZ_Color[B]");
	SetupValue(Menu.Colors.PlayerChamsWall[3], V, ESP, "Visuals_Chams_EnemyXQZ_Color[A]");

	SetupValue(Menu.Visuals.ShowBacktrack, F, ESP, "Visuals_Chams_History");
	SetupValue(Menu.Colors.PlayerChamsWall[0], V, ESP, "Visuals_Chams_History_Color[R]");
	SetupValue(Menu.Colors.PlayerChamsWall[1], V, ESP, "Visuals_Chams_History_Color[G]");
	SetupValue(Menu.Colors.PlayerChamsWall[2], V, ESP, "Visuals_Chams_History_Color[B]");
	SetupValue(Menu.Colors.PlayerChamsWall[3], V, ESP, "Visuals_Chams_History_Color[A]");

	SetupValue(Menu.Visuals.ChamsL, F, ESP, "Visuals_Chams_Local");
	SetupValue(Menu.Colors.PlayerChamsl[0], V, ESP, "Visuals_Chams_Local_Color[R]");
	SetupValue(Menu.Colors.PlayerChamsl[1], V, ESP, "Visuals_Chams_Local_Color[G]");
	SetupValue(Menu.Colors.PlayerChamsl[2], V, ESP, "Visuals_Chams_Local_Color[B]");
	SetupValue(Menu.Colors.PlayerChamsl[3], V, ESP, "Visuals_Chams_Local_Color[A]");

	SetupValue(Menu.Misc.WireHand, F, ESP, XorStr("Visuals_Chams_Wireframe_Hands"));
	SetupValue(Menu.Colors.styleshands[0], V, ESP, "Visuals_Chams_Wireframe_Hands_Color[R]");
	SetupValue(Menu.Colors.styleshands[1], V, ESP, "Visuals_Chams_Wireframe_Hands_Color[G]");
	SetupValue(Menu.Colors.styleshands[2], V, ESP, "Visuals_Chams_Wireframe_Hands_Color[B]");
	/*-----------------------------VISUALS------------------------------------*/
	SetupValue(Menu.Visuals.Name, F, ESP, XorStr("Visuals_Name"));
	SetupValue(Menu.Visuals.VisualsSize, F, ESP, XorStr("Visuals_VisualsSize"));
	SetupValue(Menu.Visuals.Health, F, ESP, XorStr("Visuals_Health"));
	SetupValue(Menu.Visuals.Weapon, F, ESP, XorStr("Visuals_Weapon"));
	SetupValue(Menu.Visuals.Flags, F, ESP, XorStr("Visuals_Flags_Enable"));
	SetupValue(Menu.Visuals.Armor, F, ESP, XorStr("Visuals_Flags_Armor"));
	SetupValue(Menu.Visuals.Scoped, F, ESP, XorStr("Visuals_Flags_Scoped"));
	SetupValue(Menu.Visuals.Fake, F, ESP, XorStr("Visuals_Flags_Fake"));
	SetupValue(Menu.Visuals.Hitmarker, F, ESP, XorStr("Visuals_Hitmarker"));
	SetupValue(Menu.Visuals.htSound, V, ESP, XorStr("Visuals_Hitmarker_Sound"));
	SetupValue(Menu.Visuals.nightmode, F, ESP, XorStr("Visuals_Nightmode"));
	SetupValue(Menu.Colors.Props, F, ESP, XorStr("Visuals_Asus_Props"));
	SetupValue(Menu.Visuals.Nosmoke, F, ESP, XorStr("Visuals_Remove_Smoke"));
	SetupValue(Menu.Visuals.NoFlash, F, ESP, XorStr("Visuals_Remove_Flash"));
	SetupValue(Menu.Visuals.Noscope, F, ESP, XorStr("Visuals_Remove_Scope"));
	SetupValue(Menu.Misc.static_scope, F, ESP, XorStr("Visuals_Remove_Zoom"));
	SetupValue(Menu.Visuals.RemoveParticles, F, ESP, XorStr("Visuals_Remove_Post_Processing"));
	SetupValue(Menu.Visuals.shaokpidork, F, ESP, XorStr("Visuals_Penetration_Check"));
	SetupValue(Menu.Antiaim.Indicator, F, ESP, XorStr("Visuals_LBY_Indicator"));
	SetupValue(Menu.Antiaim.Lines, F, ESP, XorStr("Visuals_Antiaim_Lines"));
	SetupValue(Menu.Visuals.Skybox, V, ESP, XorStr("Visuals_Skybox"));
	SetupValue(Menu.Misc.PlayerFOV, V, ESP, XorStr("Visuals_FOV"));
	SetupValue(Menu.Misc.PlayerViewmodel, V, ESP, XorStr("Visuals_Viewmodel_FOV"));
	/*-------------------------------MISC-------------------------------------*/
	SetupValue(Menu.Misc.AntiUT, true, MISC, XorStr("Misc_AntiUntrusted"));
	SetupValue(Menu.Misc.AutoJump, F, MISC, XorStr("Misc_BunnyHop"));
	SetupValue(Menu.Misc.AutoStrafe, F, MISC, XorStr("Misc_AutoStrafe"));
	SetupValue(Menu.Misc.Clantag, F, MISC, XorStr("Misc_Clantag"));
	SetupValue(Menu.LegitBot.bEnable, 0, LEGIT, std::string("Legit_Enable").c_str());
	for (int i = 0; i < 33; i++)
	{
		SetupValue(Menu.LegitBot.flFov[i], 0, LEGIT, std::string(std::string("Legit_Fov_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.flRecoil[i], 0, LEGIT, std::string(std::string("Legit_RCS_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.flPrediction[i], 0, LEGIT, std::string(std::string("Legit_Prediction_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.flSmooth[i], 0, LEGIT, std::string(std::string("Legit_Smooth_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.flSwitchTargetDelay[i], 0, LEGIT, std::string(std::string("Legit_SwitchTargetDelay_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iAimType[i], 0, LEGIT, std::string(std::string("Legit_AimType_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iBackTrackType[i], 0, LEGIT, std::string(std::string("Legit_BackTrackType_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iDelay[i], 0, LEGIT, std::string(std::string("Legit_Delay_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iSmoothType[i], 0, LEGIT, std::string(std::string("Legit_SmoothType_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iFovType[i], 0, LEGIT, std::string(std::string("Legit_FovType_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.iSpecificAimType[i], 0, LEGIT, std::string(std::string("Legit_SpecificAimType_") + std::string(GameUtils::GetWeaponName(i))).c_str());
		SetupValue(Menu.LegitBot.bUseSpecificAim[i], 0, LEGIT, std::string(std::string("Legit_UseSpecificAim_") + std::string(GameUtils::GetWeaponName(i))).c_str());
	}
}

void Confic_ayala::SetupValue(int &value, int def, std::string category, std::string name)
{
	value = def;
	ints.push_back(new ConfigValue<int>(category, name, &value));
}

void Confic_ayala::SetupValue(float &value, float def, std::string category, std::string name)
{
	value = def;
	floats.push_back(new ConfigValue<float>(category, name, &value));
}

void Confic_ayala::SetupValue(bool &value, bool def, std::string category, std::string name)
{
	value = def;
	bools.push_back(new ConfigValue<bool>(category, name, &value));
}

void Confic_ayala::Save()
{
	static char path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		char szCmd[256];
		sprintf(szCmd, "\\pware\\%s.ini", Menu.Misc.configname);

		folder = std::string(path) + ("\\pware\\");
		file = std::string(path) + szCmd;
	}

	CreateDirectoryA(folder.c_str(), NULL);

	for (auto value : ints)
		WritePrivateProfileStringA(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : floats)
		WritePrivateProfileStringA(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : bools)
		WritePrivateProfileStringA(value->category.c_str(), value->name.c_str(), *value->value ? "true" : "false", file.c_str());
}

void Confic_ayala::Load()
{
	static char path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		char szCmd[256];
		sprintf(szCmd, "\\pware\\%s.ini", Menu.Misc.configname);

		folder = std::string(path) + ("\\pware\\");
		file = std::string(path) + szCmd;
	}

	CreateDirectoryA(folder.c_str(), NULL);

	char value_l[32] = { '\0' };

	for (auto value : ints)
	{
		GetPrivateProfileStringA(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atoi(value_l);
	}

	for (auto value : floats)
	{
		GetPrivateProfileStringA(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atof(value_l);
	}

	for (auto value : bools)
	{
		GetPrivateProfileStringA(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = !strcmp(value_l, "true");
	}
}

Confic_ayala* Config = new Confic_ayala();