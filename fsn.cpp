﻿#include "hooks.h"
#include "Menu.h"
#include "fake_latency.hpp"
#include "SpoofedConvar.h"
#include "GameUtils.h"
#include "c_track-manager.hpp"
#include "resolve.h"
#include <chrono>
#include "Skin.h"
#include "Autowall.h"
#include "animation_sync.h"

#define INVALID_EHANDLE_INDEX 0xFFFFFFFF

#define TICK_INTERVAL (g_globals->interval_per_tick)
#define TIME_TO_TICKS(dt) ((int)(0.5f + (float)(dt) / TICK_INTERVAL))
#define TICKS_TO_TIME(t) (TICK_INTERVAL * (t))
ConVar* mp_facefronttime;
ConVar* r_DrawSpecificStaticProp;
void __fastcall Hooks::HookedTraceRay(void* thisptr, void*, const Ray_t& ray, unsigned int fMask, ITraceFilter* pTraceFilter, trace_t* pTrace)
{
	TraceVMT->GetOriginalMethod<TraceRayFn>(5)(thisptr, ray, fMask, pTraceFilter, pTrace);
	/*if (Autowall::Get().IsAutowalling())*/
	pTrace->surface.flags |= SURF_SKY;
}

void __stdcall Hooks::LockCursor()
{
	if (c_ayala::Opened)
		g_pSurface->UnlockCursor();
	else
		SurfaceVMT->GetOriginalMethod<UnlockCursor>(67)(g_pSurface);
}
float desync_yaw_calculation(float resolved_yaw, C_BaseEntity* e_ntity)
{

	return 0;
}
static double GetNumberOfTicksChoked(C_BaseEntity* pEntity){	double flSimulationTime = pEntity->GetSimulationTime();double flSimDiff = ((double)c_ayala::l_player->GetTickBase() * g_globals->interval_per_tick) - flSimulationTime;return TIME_TO_TICKS(max(0.0f, flSimDiff));}
float GetDesyncDelta(C_BaseEntity* e_ntity)
{
	auto animstate = uintptr_t(e_ntity->GetBasePlayerAnimState());
	float duckammount = *(float *)(animstate + 0xA4);
	float speedfraction = max(0, min(*reinterpret_cast<float*>(animstate + 0xF8), 1));
	float speedfactor = max(0, min(1, *reinterpret_cast<float*> (animstate + 0xFC)));
	float unk1 = ((*reinterpret_cast<float*> (animstate + 0x11C) * -0.30000001) - 0.19999999) * speedfraction;
	float unk2 = unk1 + 1.f;
	float unk3;

	if (duckammount > 0)
	{
		unk2 += ((duckammount * speedfactor) * (0.5f - unk2));
	}
	unk3 = *(float *)(animstate + 0x334) * unk2;
	return unk3;
}
void __stdcall Hooks::FrameStageNotify(ClientFrameStage_t curStage)
{
	if (Menu.Misc.FPSAbuse)
		g_globals->absoluteframetime = 1.f / (INT_MIN + 2);
	if (!g_pEngine->IsConnected() || !g_pEngine->IsInGame())
	{
		return;
	}
	switch (curStage)
	{
	case FRAME_START:
	{

	}break;

	case FRAME_NET_UPDATE_START:
	{
		animation_sync::Get().disable_interoplation();
	}break;

	case FRAME_NET_UPDATE_POSTDATAUPDATE_START:
	{
	}break;

	case FRAME_NET_UPDATE_POSTDATAUPDATE_END:
	{
	}break;

	case FRAME_RENDER_START:
	{
		animation_sync::Get().third_person();
	}break;

	case FRAME_NET_UPDATE_END:
	{
		animation_sync::Get().call_anim_sync();
	}break;


	case FRAME_RENDER_END:
	{

	}break;

	}
	clientVMT->GetOriginalMethod<FrameStageNotifyFn>(37)(curStage);
}

