#include "sdk.h"
#include "hooks.h"
#include "Menu.h"
#include "global.h"
#include "Hitmarker.h"
#include "imgui\imconfig.h"
#include "ImGUI\imgui.h"
#include "ImGUI\imgui_internal.h"
#include "ImGUI\stb_rect_pack.h"
#include "ImGUI\stb_textedit.h"
#include "ImGUI\stb_truetype.h"
#include "ImGUI\imgui_impl_dx9.h"
#include "Items.h"
#include "event_log.h"
#include "Config.h"
#include "imgui\ColorVar.h"
#include "GameUtils.h"
#include "fake_latency.hpp"
#include <Shlobj.h>
#include "imvisuals.h"
#include "Font.h"
#include "DynSkin.h"
#include "Math.h"
#include "animation_sync.h"

std::vector<std::string> configs;

void GetConfigMassive()
{
	//get all files on folder

	configs.clear();

	static char path[MAX_PATH];
	std::string szPath1;

	if (!SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, path)))
		return;

	szPath1 = std::string(path) + XorStr("\\pware\\*");


	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	configs.push_back(XorStr("default.ini"));

	hf = FindFirstFile(szPath1.c_str(), &FindFileData);
	if (hf != INVALID_HANDLE_VALUE) {
		do {
			std::string filename = FindFileData.cFileName;

			if (filename == XorStr("."))
				continue;

			if (filename == XorStr(".."))
				continue;

			if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				if (filename.find(XorStr(".ini")) != std::string::npos)
				{
					configs.push_back(std::string(filename));
				}
			}
		} while (FindNextFile(hf, &FindFileData) != 0);
		FindClose(hf);
	}
}

const char* KeyStrings[] = {
	"",
	"Mouse 1",
	"Mouse 2",
	"Cancel",
	"Middle Mouse",
	"Mouse 4",
	"Mouse 5",
	"",
	"Backspace",
	"Tab",
	"",
	"",
	"Clear",
	"Enter",
	"",
	"",
	"Shift",
	"Control",
	"Alt",
	"Pause",
	"Caps",
	"",
	"",
	"",
	"",
	"",
	"",
	"Escape",
	"",
	"",
	"",
	"",
	"Space",
	"Page Up",
	"Page Down",
	"End",
	"Home",
	"Left",
	"Up",
	"Right",
	"Down",
	"",
	"",
	"",
	"Print",
	"Insert",
	"Delete",
	"",
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z",
	"",
	"",
	"",
	"",
	"",
	"Numpad 0",
	"Numpad 1",
	"Numpad 2",
	"Numpad 3",
	"Numpad 4",
	"Numpad 5",
	"Numpad 6",
	"Numpad 7",
	"Numpad 8",
	"Numpad 9",
	"Multiply",
	"Add",
	"",
	"Subtract",
	"Decimal",
	"Divide",
	"F1",
	"F2",
	"F3",
	"F4",
	"F5",
	"F6",
	"F7",
	"F8",
	"F9",
	"F10",
	"F11",
	"F12",

};



PresentFn oPresent;

tReset oResetScene;

ImFont* def = nullptr;
ImFont* EspFont = nullptr;

void GUI_Init() // Setup for imgui
{
	ImGui::CreateContext();
	ImGui_ImplDX9_Init(FindWindow(XorStr("Valve001"), 0), g_pDevice);
	ImGui::StyleColorsLight();

	ImVec4 *colors = ImGui::GetStyle().Colors;

	ImGuiStyle& style = ImGui::GetStyle();

	style.Alpha = 1.f;
	style.WindowPadding = ImVec2(4, 4);
	style.WindowMinSize = ImVec2(32, 32);
	style.WindowRounding = 0.0f;
	style.WindowTitleAlign = ImVec2(0.5f, 0.5f);
	style.ChildRounding = 0.0f;
	style.FramePadding = ImVec2(2, 1.5);
	style.FrameRounding = 0.0f;
	style.ItemSpacing = ImVec2(4, 2);
	style.ItemInnerSpacing = ImVec2(2, 2);
	style.TouchExtraPadding = ImVec2(1, 1);
	style.IndentSpacing = 21.0f;
	style.ColumnsMinSpacing = 3.0f;
	style.ScrollbarSize = 12.0f;
	style.ScrollbarRounding = 0.0f;
	style.GrabMinSize = 5.0f;
	style.GrabRounding = 0.0f;
	style.FrameBorderSize = 0.0f;
	style.ChildBorderSize = 0.0f;
	style.ButtonTextAlign = ImVec2(0.5f, 0.5f);
	style.DisplayWindowPadding = ImVec2(22, 22);
	style.DisplaySafeAreaPadding = ImVec2(4, 4);
	style.AntiAliasedLines = true;
	style.AntiAliasedFill = true;
	style.CurveTessellationTol = 1.25f;

	ColorVar mainColor = ImColor(255, 170, 100, 255);
	ColorVar bodyColor = ImColor(72, 77, 86, 255);
	ColorVar childColor = ImColor(61, 65, 72, 255);

	ImVec4 mainColorHovered = ImVec4(mainColor.Color().Value.x + 0.1f, mainColor.Color().Value.y + 0.1f, mainColor.Color().Value.z + 0.1f, mainColor.Color().Value.w);
	ImVec4 mainColorActive = ImVec4(mainColor.Color().Value.x + 0.2f, mainColor.Color().Value.y + 0.2f, mainColor.Color().Value.z + 0.2f, mainColor.Color().Value.w);
	ImVec4 MenubarColor = ImVec4(bodyColor.Color().Value.x, bodyColor.Color().Value.y, bodyColor.Color().Value.z, bodyColor.Color().Value.w - 0.8f);
	ImVec4 frameBgColor = ImVec4(bodyColor.Color().Value.x, bodyColor.Color().Value.y, bodyColor.Color().Value.z, bodyColor.Color().Value.w + .1f);
	ImVec4 tooltipBgColor = ImVec4(bodyColor.Color().Value.x, bodyColor.Color().Value.y, bodyColor.Color().Value.z, bodyColor.Color().Value.w + .05f);
	

	style.Colors[ImGuiCol_Text] = ImVec4(255, 255, 255, 255);
	style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style.Colors[ImGuiCol_WindowBg] = bodyColor.Color();
	style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(childColor.color);
	style.Colors[ImGuiCol_PopupBg] = tooltipBgColor;
	style.Colors[ImGuiCol_Border] = ImColor(0, 0, 0, 255);
	style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.0f, 0.0f, 0.0f, 0.00f);
	style.Colors[ImGuiCol_FrameBg] = ImVec4(frameBgColor);
	style.Colors[ImGuiCol_FrameBgHovered] = mainColorHovered;
	style.Colors[ImGuiCol_FrameBgActive] = mainColorActive;
	style.Colors[ImGuiCol_TitleBg] = mainColor.Color();
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
	style.Colors[ImGuiCol_TitleBgActive] = mainColor.Color();
	style.Colors[ImGuiCol_MenuBarBg] = MenubarColor;
	style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(frameBgColor.x + .05f, frameBgColor.y + .05f, frameBgColor.z + .05f, frameBgColor.w);
	style.Colors[ImGuiCol_ScrollbarGrab] = mainColor.Color();
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = mainColor.Color();
	style.Colors[ImGuiCol_ScrollbarGrabActive] = mainColor.Color();
	style.Colors[ImGuiCol_CheckMark] = mainColor.Color();
	style.Colors[ImGuiCol_SliderGrab] = mainColor.Color();
	style.Colors[ImGuiCol_SliderGrabActive] = mainColorActive;
	style.Colors[ImGuiCol_Button] = bodyColor.Color();
	style.Colors[ImGuiCol_ButtonHovered] = mainColorHovered;
	style.Colors[ImGuiCol_ButtonActive] = mainColorActive;
	style.Colors[ImGuiCol_Header] = mainColor.Color();
	style.Colors[ImGuiCol_HeaderHovered] = mainColorHovered;
	style.Colors[ImGuiCol_HeaderActive] = mainColorActive;
	style.Colors[ImGuiCol_Column] = mainColor.Color();
	style.Colors[ImGuiCol_ColumnHovered] = mainColorHovered;
	style.Colors[ImGuiCol_ColumnActive] = mainColorActive;
	style.Colors[ImGuiCol_ResizeGrip] = ImColor(0, 0, 0, 0);
	style.Colors[ImGuiCol_ResizeGripHovered] = mainColorHovered;
	style.Colors[ImGuiCol_ResizeGripActive] = mainColorActive;
	style.Colors[ImGuiCol_PlotLines] = mainColor.Color();
	style.Colors[ImGuiCol_PlotLinesHovered] = mainColorHovered;
	style.Colors[ImGuiCol_PlotHistogram] = mainColor.Color();
	style.Colors[ImGuiCol_PlotHistogramHovered] = mainColorHovered;
	style.Colors[ImGuiCol_TextSelectedBg] = mainColorHovered;
	style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.0f, 0.0f, 0.0f, 0.75f);
	style.Colors[ImGuiCol_WindowBorder] = ImColor(0, 0, 0, 0);
	c_ayala::Init = true;
	ImGuiIO io = ImGui::GetIO();
	EspFont = io.Fonts->AddFontFromMemoryCompressedTTF(esp_font_compressed_data, esp_font_compressed_size, 13);
}
void morphBtnNormalTab();
void morphBtnActiveTab();
void morphBtnActiveTab()
{
	auto& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Button] = ImColor(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_ButtonHovered] = ImColor(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_Text] = ImVec4(0.76f, 0.10f, 0.24f, 1.00f);
}

void morphBtnNormalTab()
{
	auto& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Button] = ImVec4(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.13f, 0.13f, 0.13f, 1.f);
	style.Colors[ImGuiCol_Text] = ImVec4(90, 90, 90, 255);
}

namespace gui
{
	static const char* Setting4[] = 
	{
		"None",
		"Low",
		"Medium",
		"High",
		"Very high"
	};
	static const char* Setting3[] =
	{
		"None",
		"Low",
		"Medium",
		"High"
	};
	static const char* WeaponArray[] = 
	{
		"GLOCK",
		"CZ75",
		"P250",
		"FIVESEVEN",
		"DEAGLE",
		"DUALS",
		"TEC9",
		"P2000",
		"USPS",
		"REVOLVER",
		"MAC10",
		"MP9",
		"MP7",
		"UMP45",
		"BIZON",
		"P90",
		"GALIL",
		"FAMAS",
		"AK47",
		"M4A4",
		"M4A1S",
		"SG553",
		"AUG",
		"SSG08",
		"AWP",
		"G3SG1",
		"SCAR20",
		"NOVA",
		"XM1014",
		"SAWEDOFF",
		"MAG7",
		"M249",
		"NEGEV"
	};
	void begin() {
		/*if (g_pEngine->IsInGame() && g_pEngine->IsConnected())
			ImGui::GetIO().MouseDrawCursor = c_ayala::Opened;
		else
			ImGui::GetIO().MouseDrawCursor = true;*/
		
		ImGui_ImplDX9_NewFrame();
	}
	void RenderTabs()
	{

	}
	void render() {
		if (c_ayala::Opened)
			c_ayala::ShowMenu = true;
		else
			c_ayala::ShowMenu = false;

		if (c_ayala::ShowMenu) {
			ImGui::SetNextWindowSize(ImVec2(600, 400), ImGuiCond_FirstUseEver);
			ImGui::Begin(XorStr("sourcecode"), &c_ayala::Opened, ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar); {
				static const char* select[] = {
					"ragebot",
					"antiaims",
					"legitbot",
					"visuals",
					"cfg"
				};
				ImGui::PushItemWidth(588);
				ImGui::Combo("##tab_select", &Menu.Gui.tabs, select, ARRAYSIZE(select));
				ImGui::BeginChild("##tab_child", ImVec2(0, 0), true, ImGuiWindowFlags_NoScrollbar);
				switch (Menu.Gui.tabs)
				{
				case 0:
				{
					ImGui::BeginChild("##rage_subtab_child", ImVec2(0, 35), true, ImGuiWindowFlags_NoScrollbar);

					if (ImGui::Button("Main", ImVec2(90, 35))) Menu.Gui.ragesubtabs = 0;
					ImGui::SameLine();
					if (ImGui::Button("Targetting", ImVec2(90, 35))) Menu.Gui.ragesubtabs = 1;

					ImGui::EndChild();
					switch (Menu.Gui.ragesubtabs)
					{
					case 0:
						ImGui::BeginChild("##asdff", ImVec2(330, 268), true, ImGuiWindowFlags_NoScrollbar);
						ImGui::Checkbox("Enable###_aimbot", &Menu.Ragebot.EnableAimbot);
						ImGui::Checkbox("NoRecoil", &Menu.Ragebot.NoRecoil);
						if (!Menu.Misc.AntiUT) ImGui::Checkbox("NoSpread", &Menu.Ragebot.NoSpread);
						ImGui::Checkbox("Auto Revolver", &Menu.Ragebot.AutomaticRevolver);
						ImGui::Checkbox("Deceleration Revolver", &Menu.Ragebot.NewAutomaticRevolver);
						ImGui::Checkbox("Auto Fire", &Menu.Ragebot.AutomaticFire);
						ImGui::Checkbox("Auto Scope", &Menu.Ragebot.AutomaticScope);

						ImGui::Checkbox("Accuracy Boost", &Menu.Ragebot.PositionAdjustment);
						ImGui::Checkbox("Velocity Reduce", &Menu.Ragebot.VelocityReduce);
						ImGui::Combo("##accuracy boost type", &Menu.Ragebot.iPositionAdjustment, Setting3, ARRAYSIZE(Setting3));
						ImGui::Checkbox("Resolver", &Menu.Ragebot.AutomaticResolver);
						ImGui::Checkbox("Fake Ping", &Menu.Ragebot.FakeLatency); if (Menu.Ragebot.FakeLatency) { ImGui::SameLine(); if (ImGui::SmallButton("reset")) g_BacktrackHelper->ClearIncomingSequences(); }
						ImGui::PushItemWidth(180);
						ImGui::PopItemWidth();
						ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##asdfff", ImVec2(370, 300), true, ImGuiWindowFlags_NoScrollbar);

						ImGui::PushItemWidth(190);
						if (Menu.Ragebot.NewAutomaticRevolver)
							ImGui::SliderInt("Deceleration", &Menu.Ragebot.NewAutomaticRevolverFactor, 5, 20, "%.0f");
						ImGui::SliderFloat("Fake Ping", &Menu.Ragebot.FakeLatencyAmount, 0, 1, "%.2f");
						ImGui::SliderInt("Hitchance", &Menu.Ragebot.Minhitchance, 0, 100, "%.0f%%");
						ImGui::SliderInt("Min damage", &Menu.Ragebot.Mindamage, 0, 100, "%.0f%%");

						ImGui::EndChild();
						
					break;
					case 1:
						ImGui::BeginChild("##asddsff", ImVec2(330, 268), true, ImGuiWindowFlags_NoScrollbar);
						for (int i = 0; i < 7; i++)
						{
							std::string Name;
							switch (i)
							{
							case 0: Name = "Head"; break;
							case 1: Name = "Neck"; break;
							case 2: Name = "Chest"; break;
							case 3: Name = "Stomach"; break;
							case 4: Name = "Pelvis"; break;
							case 5: Name = "Arms"; break;
							case 6: Name = "Legs"; break;
							}
							ImGui::Text(Name.c_str());
							ImGui::Checkbox(std::string(std::string(XorStr("Enable")) + std::string(std::string("##") + Name)).c_str(), &Menu.Ragebot.HitboxSetting.Enable[i]);
							ImGui::Combo(std::string(std::string(XorStr("MultiPoint Count")) + std::string(std::string("##") + Name)).c_str(), &Menu.Ragebot.HitboxSetting.MPSize[i], Setting4, ARRAYSIZE(Setting4));
							ImGui::Combo(std::string(std::string(XorStr("Priority")) + std::string(std::string("##") + Name)).c_str(), &Menu.Ragebot.HitboxSetting.Priority[i], Setting4, ARRAYSIZE(Setting4));
							ImGui::SliderFloat(std::string(std::string(XorStr("PointScale")) + std::string(std::string("##") + Name)).c_str(), &Menu.Ragebot.HitboxSetting.PointScale[i], 0, 1);
						}
						ImGui::EndChild();
						break;
					}
				} break;
				case 1:
				{
					ImGui::BeginChild("##asdffff", ImVec2(745, 200), true, ImGuiWindowFlags_NoScrollbar);
					ImGui::PushItemWidth(250);
					ImGui::Checkbox("Enable###_anti_aim", &Menu.Antiaim.AntiaimEnable);
					ImGui::Combo("Pitch", &Menu.Antiaim.Pitch, AntiaimbotPitch, ARRAYSIZE(AntiaimbotPitch));
					ImGui::Combo("Yaw", &Menu.Antiaim.AntiaimMode, YawMod, ARRAYSIZE(YawMod));
					if (Menu.Antiaim.AntiaimMode == 1)
						ImGui::SliderInt("Jitter Range", &Menu.Antiaim.Jitterrange, -180, 180);
					if (Menu.Antiaim.AntiaimMode == 2)
						ImGui::SliderInt("Spin Speed", &Menu.Antiaim.spinspeed, 0, 10);
					ImGui::SliderInt("LBY Delta", &Menu.Antiaim.FreestandingDelta, -180, 180);
					ImGui::SliderInt("Auto Direction Distance", &Menu.Antiaim.AutoDirectionDistance, 1, 10);
					ImGui::Combo("FakeWalk", &Menu.Antiaim.FakeWalkKey, KeyStrings, ARRAYSIZE(KeyStrings));
					ImGui::Checkbox("Enable Fakelag", &Menu.Misc.FakelagEnable);
					ImGui::SliderInt("Factor ##amount_fakelag", &Menu.Misc.FakelagAmount, 0, 15);
					ImGui::Combo("Mode###fake_lag_mode", &Menu.Misc.FakelagMode, FakelagMode, ARRAYSIZE(FakelagMode));
					ImGui::PopItemWidth();
					ImGui::EndChild();
				}break;
				case 2:
				{
					static const char* SettingType[] = {
						"Weapon in hand",
						"Weapon list"
					};
					static const char* AimbotType[] = {
						"Linear",
						"Curve"
					};
					static const char* BacktrackType[] = {
						"Off",
						"Support",
						"Full"
					};
					static const char* SmoothType[] = {
						"Auto",
						"Factor"
					};
					static const char* DelayType[] = {
						"Off",
						"Auto",
						"Auto + shot"
					};

					int WeaponID = -1;
					ImGui::PushItemWidth(250);
					ImGui::Checkbox("Enable LegitBot", &Menu.LegitBot.bEnable);
					if (Menu.Ragebot.EnableAimbot)
					{
						ImGui::Text("Turn off ragebot before you turn it on.");
						Menu.LegitBot.bEnable = false;
					}
					else
					{
						ImGui::Combo("Weapon", &Menu.LegitBot.iSettingType, SettingType, ARRAYSIZE(SettingType));
						ImGui::PopItemWidth();
						ImGui::PushItemWidth(250);
						if (c_ayala::c_weapon && !(c_ayala::c_weapon->GetWeaponType() == WEPCLASS_KNIFE || c_ayala::c_weapon->GetWeaponType() == WEPCLASS_INVALID) && Menu.LegitBot.iSettingType == 0)
							WeaponID = c_ayala::c_weapon->GetWeaponNum();

						else if (Menu.LegitBot.iSettingType == 1)
						{
							ImGui::Combo("Select Weapon", &Menu.LegitBot.iCustomIndex, WeaponArray, ARRAYSIZE(WeaponArray));
							WeaponID = Menu.LegitBot.iCustomIndex;
						}
						ImGui::PopItemWidth();
						ImGui::BeginChild("##WeaponSetting", ImVec2(0, 0), true, ImGuiWindowFlags_NoScrollbar);
						ImGui::PushItemWidth(250);
						ImGui::Combo("AimBot style", &Menu.LegitBot.iAimType[WeaponID], AimbotType, ARRAYSIZE(AimbotType));
						ImGui::Combo("Backtrack style", &Menu.LegitBot.iBackTrackType[WeaponID], BacktrackType, ARRAYSIZE(BacktrackType));
						ImGui::Combo("Smooth style", &Menu.LegitBot.iSmoothType[WeaponID], SmoothType, ARRAYSIZE(SmoothType));
						ImGui::Checkbox("Use specific aim", &Menu.LegitBot.bUseSpecificAim[WeaponID]);
						if (Menu.LegitBot.iSmoothType[WeaponID] == 1)
							ImGui::SliderFloat("Aim time", &Menu.LegitBot.flSmooth[WeaponID], 0, 1, "%.2fms");
						ImGui::SliderFloat("Fov", &Menu.LegitBot.flFov[WeaponID], 0, 50, "%.1f�");
						ImGui::SliderFloat("Recoil Control", &Menu.LegitBot.flRecoil[WeaponID], 0, 100, "%.1f%%");
						ImGui::SliderFloat("Prediction", &Menu.LegitBot.flPrediction[WeaponID], 0, 100, "%.1f%%");
						ImGui::SliderFloat("Target switch delay", &Menu.LegitBot.flSwitchTargetDelay[WeaponID], 0, 1, "%.2fms");
						ImGui::Combo("Delay style", &Menu.LegitBot.iDelay[WeaponID], DelayType, ARRAYSIZE(DelayType));
						ImGui::PopItemWidth();
						ImGui::EndChild();
					}
				}
				break;
				case 3:
				{
					ImGui::Checkbox("Enable", &Menu.Visuals.EspEnable);
					ImGui::SliderFloat("Visuals scale", &Menu.Visuals.VisualsSize, 1, 30);
					ImGui::Checkbox("Health", &Menu.Visuals.Health);
					ImGui::Checkbox("Name", &Menu.Visuals.Name);
					ImGui::Checkbox("Ammo", &Menu.Visuals.Ammo);
					ImGui::Checkbox("Scoped", &Menu.Visuals.Scoped);
					ImGui::Checkbox("Flags", &Menu.Visuals.Flags);
					ImGui::Checkbox("Money", &Menu.Visuals.Monitor);
					ImGui::Checkbox("Arrows", &Menu.Visuals.OutOfPOVArrows);
					ImGui::Checkbox("Glow", &Menu.Visuals.Glow); ImGui::SameLine();	ImGui::MyColorEdit4("##GlowCol", Menu.Colors.Glow, 1 << 7 | 1 << 0);

					ImGui::Checkbox("Chams", &Menu.Visuals.ChamsEnable);
					ImGui::PushItemWidth(250);
					ImGui::Combo("Type", &Menu.Visuals.ChamsStyle, ModelsMode, ARRAYSIZE(ModelsMode));
					ImGui::Checkbox("Player", &Menu.Visuals.ChamsPlayer); ImGui::SameLine();	ImGui::MyColorEdit4("##PlayerCol", Menu.Colors.PlayerChams, 1 << 7 | 1 << 0); ImGui::SameLine();	ImGui::MyColorEdit4("##PlayerColRf", Menu.Colors.PlayerChamsRf, 1 << 7 | 1 << 0);
					ImGui::Checkbox("Player XQZ", &Menu.Visuals.ChamsPlayerWall); ImGui::SameLine();	ImGui::MyColorEdit4("##PlayerXQZCol", Menu.Colors.PlayerChamsWall, 1 << 7 | 1 << 0); ImGui::SameLine();	ImGui::MyColorEdit4("##PlayerXQZColRf", Menu.Colors.PlayerChamsWallRf, 1 << 7 | 1 << 0);
					ImGui::Checkbox("Local Player##ofodofod", &Menu.Visuals.ChamsL); ImGui::SameLine();	ImGui::MyColorEdit4("##l_playerCol", Menu.Colors.PlayerChamsl, 1 << 7 | 1 << 0);
					ImGui::Checkbox("Show Backtrack", &Menu.Visuals.ShowBacktrack); ImGui::SameLine();	ImGui::MyColorEdit4("##BacktrackCol", Menu.Colors.ChamsHistory, 1 << 7 | 1 << 0);
					ImGui::Combo("Show Backtrack Types", &Menu.Visuals.ShowBacktrackMethod, TypeOfShowBacktrackingRecord, ARRAYSIZE(TypeOfShowBacktrackingRecord));
					ImGui::Checkbox("Hands", &Menu.Misc.WireHand);
					ImGui::Checkbox("Blend model on scope", &Menu.Visuals.blendonscope);
					/*ImGui::SliderInt("l_player Alpha", &Menu.Visuals.playeralpha, 0, 255, "%.1f%%");*/
					ImGui::Checkbox("Bullet Tracers", &Menu.Visuals.BulletTracers);
					ImGui::Checkbox("Grenade trajectory", &Menu.Visuals.GrenadePrediction);
					ImGui::Checkbox("Spread Crosshair", &Menu.Visuals.SpreadCrosshair);
					ImGui::PushItemWidth(250);
					ImGui::Combo("ThirdPerson", &Menu.Misc.TPKey, KeyStrings, ARRAYSIZE(KeyStrings));
					ImGui::PopItemWidth();

					ImGui::Checkbox("Hitmarker", &Menu.Visuals.Hitmarker);
					if (Menu.Visuals.Hitmarker) {
						static const char* hitsound[] =
						{
							"aimware",
							"bameware",
							"penguware",
							"porn",
							"pew",
							"one",
							"bubble",
							"gamesense"
						};
						ImGui::Combo("Sound", &Menu.Visuals.htSound, hitsound, ARRAYSIZE(hitsound));
					}

					ImGui::Checkbox("Nightmode", &Menu.Visuals.nightmode);
					ImGui::Checkbox("Asus props", &Menu.Colors.Props);
					ImGui::Checkbox("Remove smoke", &Menu.Visuals.Nosmoke);
					ImGui::Checkbox("Remove scope", &Menu.Visuals.Noscope);
					ImGui::Checkbox("Remove zoom", &Menu.Misc.static_scope);
					ImGui::Checkbox("Remove post processing", &Menu.Visuals.RemoveParticles);
					ImGui::SliderInt("Player Viewmodel", &Menu.Misc.PlayerViewmodel, 0, 180, "%.0f%%");
					ImGui::SliderInt("Player FOV", &Menu.Misc.PlayerFOV, -50, 50, "%.0f%%");
					ImGui::PopItemWidth();

				}
				break;
				case 4:
				{
					ImGui::Checkbox("Anti untrusted", &Menu.Misc.AntiUT);
					ImGui::Checkbox("Bunny hop", &Menu.Misc.AutoJump);
					ImGui::Checkbox("Auto strafe", &Menu.Misc.AutoStrafe);
					ImGui::Checkbox("FPS abuse", &Menu.Misc.FPSAbuse);
					ImGui::SliderInt("##Fps", &Menu.Misc.FPSValue, 0, 10000);
					ImGui::BeginChild("##cfg_child", ImVec2(284, 175), true, ImGuiWindowFlags_NoScrollbar);
					GetConfigMassive();
					ImGui::Text(XorStr("Config Name"));
					//configs
					static int selectedcfg = 0;
					static std::string cfgname = "default";
					if (ImGui::Combo(XorStr("Parsed Cfgs"), &selectedcfg, [](void* data, int idx, const char** out_text)
					{
						*out_text = configs[idx].c_str();
						return true;
					}, nullptr, configs.size(), 10))
					{
						cfgname = configs[selectedcfg];
						cfgname.erase(cfgname.length() - 4, 4);
						strcpy(Menu.Misc.configname, cfgname.c_str());
					}

					ImGui::InputText(XorStr("Current Cfg"), Menu.Misc.configname, 128);
					if (ImGui::Button(XorStr("Save Config"), ImVec2(93.f, 20.f))) Config->Save();
					ImGui::SameLine();
					if (ImGui::Button(XorStr("Load Config"), ImVec2(93.f, 20.f))) {
						Config->Load();

					}
					ImGui::EndChild();
				}
				break;
				case 5:
				{
					ImGui::Checkbox("Anti untrusted", &Menu.Misc.AntiUT);
					ImGui::Checkbox("Bunny hop", &Menu.Misc.AutoJump);
					ImGui::Checkbox("Auto strafe", &Menu.Misc.AutoStrafe);
					ImGui::Checkbox("FPS abuse", &Menu.Misc.FPSAbuse);
					ImGui::SliderInt("##Fps", &Menu.Misc.FPSValue, 0, 10000);
					ImGui::BeginChild("##cfg_child", ImVec2(284, 175), true, ImGuiWindowFlags_NoScrollbar);
					GetConfigMassive();
					ImGui::Text(XorStr("Config Name"));
					//configs
					static int selectedcfg = 0;
					static std::string cfgname = "default";
					if (ImGui::Combo(XorStr("Parsed Cfgs"), &selectedcfg, [](void* data, int idx, const char** out_text)
					{
						*out_text = configs[idx].c_str();
						return true;
					}, nullptr, configs.size(), 10))
					{
						cfgname = configs[selectedcfg];
						cfgname.erase(cfgname.length() - 4, 4);
						strcpy(Menu.Misc.configname, cfgname.c_str());
					}

					ImGui::InputText(XorStr("Current Cfg"), Menu.Misc.configname, 128);
					if (ImGui::Button(XorStr("Save Config"), ImVec2(93.f, 20.f))) Config->Save();
					ImGui::SameLine();
					if (ImGui::Button(XorStr("Load Config"), ImVec2(93.f, 20.f))) {
						Config->Load();

					}
					ImGui::EndChild();
				}
				break;
		}
		ImGui::EndChild();
		ImGui::PopItemWidth();
	}
	ImGui::End();
}
		
		if (g_pEngine->IsConnected() && g_pEngine->IsInGame());
		{
			int iWidth, iHeight;
			g_pEngine->GetScreenSize(iWidth, iHeight);
			ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_Always);
			ImGui::SetNextWindowSize(ImVec2(iWidth, iHeight), ImGuiSetCond_Always);
			ImGui::Begin("RenderSelfGui", (bool*)true, ImVec2(iWidth, iHeight), 0.f, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoInputs);
			ImGui::PushFont(EspFont);
			ImDrawList* pDraw = ImGui::GetWindowDrawList();
			INetChannelInfo* nci = g_pEngine->GetNetChannelInfo();
			DWORD ClientState = *(DWORD*)FakeLatency::ClientState;
			if (nci) {
				//pDraw->AddText(ImVec2(5, 30), ImColor(255, 255, 255), std::string("Ping: " + std::to_string((int)((nci->GetAvgLatency(FLOW_OUTGOING) + nci->GetAvgLatency(FLOW_INCOMING)) * 1000))).c_str());
			}
			if (c_ayala::l_player)
			{
				pDraw->AddText(ImVec2(5, 50), ImColor(255, 255, 255), std::string("setupvelocity_rebuild: " + std::to_string(animation_sync::Get().setupvelocity_rebuild(c_ayala::l_player))).c_str());
				pDraw->AddText(ImVec2(5, 70), ImColor(255, 255, 255), std::string("curupdatedelta: " + std::to_string(c_ayala::l_player->GetBasePlayerAnimState()->curupdatedelta())).c_str());
				pDraw->AddText(ImVec2(5, 90), ImColor(255, 255, 255), std::string("m_flCurrentFeetYaw: " + std::to_string(c_ayala::l_player->GetBasePlayerAnimState()->m_flCurrentFeetYaw)).c_str());
				/*pDraw->AddText(ImVec2(5, 50), ImColor(255, 255, 255), std::string("m_flCycle: " + std::to_string(c_ayala::l_player->GetAnimOverlay(3)->m_flCycle)).c_str());
				pDraw->AddText(ImVec2(5, 70), ImColor(255, 255, 255), std::string("m_flPlaybackRate: " + std::to_string(c_ayala::l_player->GetAnimOverlay(3)->m_flPlaybackRate)).c_str());
				pDraw->AddText(ImVec2(5, 90), ImColor(255, 255, 255), std::string("m_flPrevCycle: " + std::to_string(c_ayala::l_player->GetAnimOverlay(3)->m_flPrevCycle)).c_str());
				pDraw->AddText(ImVec2(5, 130), ImColor(255, 255, 255), std::string("m_flWeight: " + std::to_string(c_ayala::l_player->GetAnimOverlay(3)->m_flWeight)).c_str());*/
			}

		/*	pDraw->AddText(ImVec2(5, 110), ImColor(255, 255, 255), std::string("unknown: " + std::to_string(c_ayala::l_player->GetBasePlayerAnimState()->unknown())).c_str());
			pDraw->AddText(ImVec2(5, 140), ImColor(255, 255, 255), std::string("unknown2: " + std::to_string(c_ayala::l_player->GetBasePlayerAnimState()->unknown2())).c_str());
			pDraw->AddText(ImVec2(5, 170), ImColor(255, 255, 255), std::string("unknown3: " + std::to_string(c_ayala::l_player->GetBasePlayerAnimState()->unknown3())).c_str());
			pDraw->AddText(ImVec2(5, 190), ImColor(255, 255, 255), std::string("PredictedTime: " + std::to_string(c_ayala::PredictedTime)).c_str());*/
			CImVisuals::Get().Run();
			CImVisuals::Get().DrawCustomUI();
			ImGui::PopFont();
			ImGui::End();
		}
	}

	void end() {
		ImGui::Render();
		ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
	}

	void shutdown() {
		ImGui_ImplDX9_Shutdown();
	}

	void create_objects() {
		ImGui_ImplDX9_CreateDeviceObjects();
	}

	void destroy_objects() {
		ImGui_ImplDX9_InvalidateDeviceObjects();
	}
}

HRESULT __stdcall Hooks::D3D9_EndScene(IDirect3DDevice9* pDevice)
{

	HRESULT result = d3d9VMT->GetOriginalMethod<EndSceneFn>(42)(pDevice);
	
//	g_pRender->Reset();
	DWORD colorwrite, srgbwrite;
	pDevice->GetRenderState(D3DRS_COLORWRITEENABLE, &colorwrite);
	pDevice->GetRenderState(D3DRS_SRGBWRITEENABLE, &srgbwrite);
	pDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0xffffffff);
	pDevice->SetRenderState(D3DRS_SRGBWRITEENABLE, false);
//	Visuals::Run();
//	g_ESP->Crosshair(pDevice);
	//gui::begin();
	//gui::render();
	//gui::end();
	pDevice->SetRenderState(D3DRS_COLORWRITEENABLE, colorwrite);
	pDevice->SetRenderState(D3DRS_SRGBWRITEENABLE, srgbwrite);
	return result;
}


HRESULT __stdcall Hooks::hkdReset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresParam)
{
	if (!c_ayala::Init)
		return d3d9VMT->GetOriginalMethod<tReset>(16)(pDevice, pPresParam);

	gui::destroy_objects();
//	g_pRender->LostDevice();
	auto hr = d3d9VMT->GetOriginalMethod<tReset>(16)(pDevice, pPresParam);
	if (hr >= 0) {
//		g_pRender->ResetDevice();
		gui::create_objects();
	}
	return hr;
}