#pragma once
#include <DirectXMath.h>
#define M_HPI			DirectX::XM_PIDIV2	// half pi
#define M_QPI			DirectX::XM_PIDIV4	// quarter pi
#define M_PI			DirectX::XM_PI		// 3.1415926535897f	// pi const
#define M_2PI			DirectX::XM_2PI		// 6.2831853071794f	// pi double
#define M_GPI			1.6180339887498f	// golden ratio
#define M_RADPI			57.295779513082f	// pi in radians
namespace Math
{
	extern float NormalizeYaw(float value);
	float * NormalizeYawPtr(float * value);
	extern void NormalizeAngle(QAngle & value);
	extern QAngle NormalizeAngle2(QAngle value);
	extern void VectorAngles(const Vector&vecForward, Vector&vecAngles);
	float GRD_TO_BOG(float GRD);
	extern void AngleVectors(const Vector angles, Vector& forward, Vector& right, Vector& up);
	extern void VectorMA(const Vector & start, float scale, const Vector & direction, Vector & dest);
	extern void NormalizeVector(Vector & vecIn);
	extern void VectorTransform(const Vector& in1, const matrix3x4_t& in2, Vector& out);
	extern	void VectorTransform(const float * in1, const matrix3x4_t & in2, float * out);
	extern void VectorTransform2(const float *in1, const matrix3x4_t& in2, float *out);
	extern void VectorSubtract(const Vector & a, const Vector & b, Vector & c);
	extern void NormalizeNum(Vector & vIn, Vector & vOut);
	extern void AngleVectors(const QAngle &angles, Vector* forward);
	extern void AngleVectors(const QAngle & angles, Vector & forward);
	extern void ClampAngles(QAngle& angles);
	extern float RandomFloat(float min, float max);
	extern float RandomFloat2(float min, float max);
    extern void AngleVectors(const Vector &angles, Vector *forward, Vector *right, Vector *up);
    extern void vector_subtract(const Vector& a, const Vector& b, Vector& c);
	extern void vector_transform(const Vector in1, float in2[3][4], Vector &out);
	extern Vector VectorTransform(const Vector in1, float in2[3][4]);
	extern void VectorTransform(Vector& in1, matrix3x4_t& in2, Vector &out);
	/*extern Vector VectorTransform(Vector& in1, VMatrix& in2);*/
	extern bool DotInVector(Vector vector, Vector dot, float limit);
}

extern bool enabledtp;