﻿#include "hooks.h"
#include <intrin.h>
#include "Utilities.h"
#include "c_track-manager.hpp"
bool __stdcall Hooks::hooked_timedemo() {
	static auto original_fn = engineVMT->GetOriginalMethod<IsPlayingTimeDemo_t>(84);

	static auto interpolated_server_entities = reinterpret_cast< void * >(Utilities::Memory::FindPatternIDA("client_panorama.dll", "55 8B EC 83 EC 1C 8B 0D ? ? ? ? 53"));
	static auto pRet = reinterpret_cast<void *>(Utilities::Memory::FindPatternIDA("client_panorama.dll", "E8 ? ? ? ? A1 ? ? ? ? FF 05 ? ? ? ?") + 0xD1);
	
	if (abs((int)((int*)_ReturnAddress()) - (int)interpolated_server_entities) < 216 && c_ayala::l_player->isAlive() && Menu.Ragebot.PositionAdjustment)
		//if (pRet == _ReturnAddress())
	{
		
		return true;
	}

	return original_fn(g_pEngine);
}

//
//bool __fastcall Hooks::isplayingtimedemo(void* thisptr, void*)
//{
//	static void* ret = nullptr;
//	if (!ret)
//		ret = (void*)Utilities::Memory::FindPatternIDA("client", "84 C0 75 14 8B 0D ? ? ? ? 8B 01 8B 80 ? ? ? ? FF D0 84 C0 74 07");
//
//	//ida paste to make sure people think I know what im doing
//	//.text:101E2957		mov     s_bInterpolate, bl
//	//.text:101E295D		mov     eax, [ecx]
//	//.text:101E295F		mov     eax, [eax+150h] ; IsPlayingTimeDemo (engine, 84)
//	//.text:101E2965		call    eax
//	//.text:101E2967		test    al, al
//
//	if (_ReturnAddress() == ret)
//		return true; // wow! disabled interpolation!!
//
//	return org_isplayingtimedemo(thisptr);
//}