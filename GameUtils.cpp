#include "hooks.h"
#include "GameUtils.h"
#include "global.h"
#include "fake_latency.hpp"
#include "Menu.h"
#include "Autowall.h"
#include "Math.h"
#define POINTERCHK( pointer ) ( pointer  && pointer !=0  && HIWORD( pointer ) )
bool CGameTrace::DidHitWorld() const
{
	return m_pEnt == g_pEntitylist->GetClientEntity(0);
}
bool CGameTrace::DidHit() const
{
	return fraction < 1.0f || allsolid || startsolid;
}

// Returns true if we hit something and it wasn't the world.
bool CGameTrace::DidHitNonWorldC_BaseEntity() const
{
	return m_pEnt != NULL && !DidHitWorld();
}

bool GameUtils::WorldToScreen(const Vector& in, Vector& position)
{
	const matrix3x4& worldToScreen = *(matrix3x4*)(*(PDWORD)(offys.WMatrix) + 0x3DC);
	if (!POINTERCHK(worldToScreen))
		return false;


	int ScrW, ScrH;

	g_pEngine->GetScreenSize(ScrW, ScrH);

	float w = worldToScreen[3][0] * in[0] + worldToScreen[3][1] * in[1] + worldToScreen[3][2] * in[2] + worldToScreen[3][3];
	position.z = 0;
	if (w > 0.01)
	{
		float inverseWidth = 1 / w;
		position.x = (ScrW / 2) + (0.5 * ((worldToScreen[0][0] * in[0] + worldToScreen[0][1] * in[1] + worldToScreen[0][2] * in[2] + worldToScreen[0][3]) * inverseWidth) * ScrW + 0.5);
		position.y = (ScrH / 2) - (0.5 * ((worldToScreen[1][0] * in[0] + worldToScreen[1][1] * in[1] + worldToScreen[1][2] * in[2] + worldToScreen[1][3]) * inverseWidth) * ScrH + 0.5);
		return true;
	}
	return false;
}

std::vector<Vector> GameUtils::GetMultiplePointsForHitbox(C_BaseEntity* pBaseEntity, int iHitbox, VMatrix BoneMatrix[128])

{
	auto VectorTransform_Wrapper = [](const Vector& in1, const VMatrix &in2, Vector &out)
	{
		auto VectorTransform = [](const float *in1, const VMatrix& in2, float *out)
		{
			auto DotProducts = [](const float *v1, const float *v2)
			{
				return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
			};
			out[0] = DotProducts(in1, in2[0]) + in2[0][3];
			out[1] = DotProducts(in1, in2[1]) + in2[1][3];
			out[2] = DotProducts(in1, in2[2]) + in2[2][3];
		};
		VectorTransform(&in1.x, in2, &out.x);
	};

	studiohdr_t* pStudioModel = g_pModelInfo->GetStudioModel(pBaseEntity->GetModel());
	mstudiohitboxset_t* set = pStudioModel->pHitboxSet(0);
	mstudiobbox_t *hitbox = set->pHitbox(iHitbox);

	std::vector<Vector> vecArray;

	Vector max;
	Vector min;
	VectorTransform_Wrapper(hitbox->bbmax, BoneMatrix[hitbox->bone], max);
	VectorTransform_Wrapper(hitbox->bbmin, BoneMatrix[hitbox->bone], min);

	auto center = (min + max) * 0.5f;

	QAngle CurrentAngles = GameUtils::CalculateAngle(center, c_ayala::l_player->GetEyePosition());

	Vector Forward;
	Math::AngleVectors(CurrentAngles, &Forward);

	Vector Right = Forward.Cross(Vector(0, 0, 1));
	Vector Left = Vector(-Right.x, -Right.y, Right.z);

	Vector Top = Vector(0, 0, 1);
	Vector Bot = Vector(0, 0, -1);
	Vector v1 = Vector(Right.x / 2, Right.y, 0.6);
	Vector v2 = Vector(Right.x, Right.y / 2, 0.6);
	Vector v3 = Vector(Right.x / 2, Right.y / 2, 0.6);
	Vector v4 = Vector(-Right.x / 2, -Right.y / 2, 0.6);
	const float psh = Menu.Ragebot.Headscale / 100;
	const float psb = Menu.Ragebot.Bodyscale / 100;
	switch (iHitbox) {
	case (int)CSGOHitboxID::HEAD:
		for (auto i = 0; i < 8; ++i)
		{
			vecArray.emplace_back(center);
		}
		vecArray.at(1) += Top * (hitbox->radius * psh);
		vecArray.at(2) += Right * (hitbox->radius * psh);
		vecArray.at(3) += Left * (hitbox->radius * psh);
		vecArray.at(4) += v1 * (hitbox->radius * psh);
		vecArray.at(5) += v2 * (hitbox->radius * psh);
		vecArray.at(6) += v3 * (hitbox->radius * psh);
		vecArray.at(7) += v4 * (hitbox->radius * psh);
		break;

	default:

		for (auto i = 0; i < 3; ++i)
		{
			vecArray.emplace_back(center);
		}
		vecArray[1] += Right * (hitbox->radius * psb);
		vecArray[2] += Left * (hitbox->radius * psb);
		break;
	}
	return vecArray;
}

/*{
	auto VectorTransform_Wrapper = []( const Vector& in1, const VMatrix &in2, Vector &out )
	{
		auto VectorTransform = []( const float *in1, const VMatrix& in2, float *out )
		{
			auto DotProducts = []( const float *v1, const float *v2 )
			{
				return v1[ 0 ] * v2[ 0 ] + v1[ 1 ] * v2[ 1 ] + v1[ 2 ] * v2[ 2 ];
			};
			out[ 0 ] = DotProducts( in1, in2[ 0 ] ) + in2[ 0 ][ 3 ];
			out[ 1 ] = DotProducts( in1, in2[ 1 ] ) + in2[ 1 ][ 3 ];
			out[ 2 ] = DotProducts( in1, in2[ 2 ] ) + in2[ 2 ][ 3 ];
		};
		VectorTransform( &in1.x, in2, &out.x );
	};

	studiohdr_t* pStudioModel = g_pModelInfo->GetStudioModel(pBaseEntity->GetModel());
	mstudiohitboxset_t* set = pStudioModel->pHitboxSet(0);
	mstudiobbox_t *hitbox = set->pHitbox(iHitbox);

	std::vector<Vector> vecArray;

	Vector max;
	Vector min;
	VectorTransform_Wrapper(hitbox->bbmax, BoneMatrix[hitbox->bone], max);
	VectorTransform_Wrapper(hitbox->bbmin, BoneMatrix[hitbox->bone], min);

	auto center = (min + max) * 0.5f;

	QAngle CurrentAngles = GameUtils::CalculateAngle(center, c_ayala::l_player->GetEyePosition());

	Vector Forward;
	Math::AngleVectors(CurrentAngles, &Forward);

	Vector Right = Forward.Cross(Vector(0, 0, 1));
	Vector Left = Vector(-Right.x, -Right.y, Right.z);

	Vector Top = Vector(0, 0, 1);
	Vector Bot = Vector(0, 0, -1);

	switch (iHitbox) {
	case (int)CSGOHitboxID::Head:
		for (auto i = 0; i < 4; ++i)
		{
			vecArray.emplace_back(center);
		}
		vecArray[1] += Top * (hitbox->radius * Menu.Ragebot.Headscale);
		vecArray[2] += Right * (hitbox->radius * Menu.Ragebot.Headscale);
		vecArray[3] += Left * (hitbox->radius * Menu.Ragebot.Headscale);
		break;

	default:

		for (auto i = 0; i < 3; ++i)
		{
			vecArray.emplace_back(center);
		}
		vecArray[1] += Right * (hitbox->radius * Menu.Ragebot.Bodyscale);
		vecArray[2] += Left * (hitbox->radius * Menu.Ragebot.Bodyscale);
		break;
	}
	return vecArray;
}*/

Vector GameUtils::GetBonePosition(C_BaseEntity* pPlayer, int Bone, VMatrix* MatrixArray/*[ 128 ]*/)
{
	return  Vector(MatrixArray[Bone][0][3], MatrixArray[Bone][1][3], MatrixArray[Bone][2][3]);
}
void GameUtils::TraceLine(Vector& vecAbsStart, Vector& vecAbsEnd, unsigned int mask, C_BaseEntity* ignore, trace_t* ptr)
{
	Ray_t ray;
	ray.Init(vecAbsStart, vecAbsEnd);
	CTraceFilter filter;
	filter.pSkip1 = ignore;

	g_pEngineTrace->TraceRay(ray, mask, &filter, ptr);
}

bool GameUtils::IsVisible_Fix(Vector vecOrigin, Vector vecOther, unsigned int mask, C_BaseEntity* pC_BaseEntity, C_BaseEntity* pIgnore, int& hitgroup)
{
	auto AngleVectors = [](const Vector &angles, Vector *forward)
	{
		Assert(s_bMathlibInitialized);
		Assert(forward);

		float	sp, sy, cp, cy;

		sy = sin(DEG2RAD(angles[1]));
		cy = cos(DEG2RAD(angles[1]));

		sp = sin(DEG2RAD(angles[0]));
		cp = cos(DEG2RAD(angles[0]));

		forward->x = cp * cy;
		forward->y = cp * sy;
		forward->z = -sp;
	};

	Ray_t ray;
	trace_t tr;
	ray.Init(vecOrigin, vecOther);
	CTraceFilter filter;
	filter.pSkip1 = pIgnore;

	g_pEngineTrace->TraceRay(ray, mask, &filter, &tr);

	//not correctly done tho

	//Vector direction;

	//Vector angles = GameUtils::CalculateAngle(vecOrigin, vecOther);
	//AngleVectors(angles, &direction);
	//VectorNormalize(direction);

	//UTIL_ClipTraceToPlayers(vecOrigin, vecOrigin + direction*40.f, 0x4600400B, &filter, &tr);

	hitgroup = tr.hitgroup;

	return (tr.m_pEnt == pC_BaseEntity || tr.fraction >= 1.0f);
}

static float GameUtils::GetFov(const Vector& viewAngle, const Vector& aimAngle)
{
	Vector ang, aim;

	Math::AngleVectors(viewAngle, aim);
	Math::AngleVectors(aimAngle, ang);

	return RAD2DEG(acos(aim.Dot(ang) / aim.LengthSqr()));
}


static bool GameUtils::isVisible(C_BaseEntity* lul, int bone)
{
	Ray_t ray;
	trace_t tr;

	ray.Init(c_ayala::l_player->GetEyePosition(), lul->GetBonePos(bone));

	CTraceFilter filter;
	filter.pSkip1 = c_ayala::l_player;

	g_pEngineTrace->TraceRay(ray, (0x1 | 0x4000 | 0x2000000 | 0x2 | 0x4000000 | 0x40000000), &filter, &tr);

	if (tr.m_pEnt == lul)
	{
		return true;
	}

	return false;
}


float GameUtils::GetFoV(QAngle qAngles, Vector vecSource, Vector vecDestination, bool bDistanceBased)
{
	auto MakeVector = [](QAngle qAngles)
	{
		auto ret = Vector();
		auto pitch = float(qAngles[0] * M_PI / 180.f);
		auto yaw = float(qAngles[1] * M_PI / 180.f);
		auto tmp = float(cos(pitch));
		ret.x = float(-tmp * -cos(yaw));
		ret.y = float(sin(yaw)*tmp);
		ret.z = float(-sin(pitch));
		return ret;
	};

	Vector ang, aim;
	double fov;

	ang = CalculateAngle(vecSource, vecDestination);
	aim = MakeVector(qAngles);
	ang = MakeVector(ang);

	auto mag_s = sqrt((aim[0] * aim[0]) + (aim[1] * aim[1]) + (aim[2] * aim[2]));
	auto mag_d = sqrt((aim[0] * aim[0]) + (aim[1] * aim[1]) + (aim[2] * aim[2]));
	auto u_dot_v = aim[0] * ang[0] + aim[1] * ang[1] + aim[2] * ang[2];

	fov = acos(u_dot_v / (mag_s*mag_d)) * (180.f / M_PI);

	if (bDistanceBased) {
		fov *= 1.4;
		float xDist = abs(vecSource[0] - vecDestination[0]);
		float yDist = abs(vecSource[1] - vecDestination[1]);
		float Distance = sqrt((xDist * xDist) + (yDist * yDist));

		Distance /= 650.f;

		if (Distance < 0.7f)
			Distance = 0.7f;

		if (Distance > 6.5)
			Distance = 6.5;

		fov *= Distance;
	}

	return (float)fov;
}

QAngle GameUtils::CalculateAngle(Vector vecOrigin, Vector vecOther)
{
	auto ret = Vector();
	Vector delta = vecOrigin - vecOther;
	double hyp = delta.Length2D();
	ret.y = (atan(delta.y / delta.x) * 57.295779513082f);
	ret.x = (atan(delta.z / hyp) * 57.295779513082f);
	ret.z = 0.f;

	if (delta.x >= 0.f)
		ret.y += 180.f;
	return ret;
}

float GameUtils::GetCurTime() {
	if (!c_ayala::l_player)
		return 0;
	int g_tick = 0;
	c_user_cmd* g_pLastCmd = nullptr;
	if (!g_pLastCmd || g_pLastCmd->hasbeenpredicted) {
		g_tick = c_ayala::l_player->GetTickBase();
	}
	else {
		++g_tick;
	}
	g_pLastCmd = c_ayala::user_cmd;
	float curtime = (g_tick - 1) * g_globals->interval_per_tick + max<float>(g_globals->interval_per_tick, g_globals->frametime);
	return curtime;
}
void GameUtils::calculate_angle(Vector src, Vector dst, Vector &angles)
{
	Vector delta = src - dst;
	double hyp = delta.Length2D();
	angles.y = (atan(delta.y / delta.x) * 57.295779513082f);
	angles.x = (atan(delta.z / hyp) * 57.295779513082f);
	angles[2] = 0.0f;
	if (delta.x >= 0.0) angles.y += 180.0f;
}
char * GameUtils::GetWeaponName(int confignum)
{
	switch (confignum)
	{
	case WEAPON_GLOCK:
		return "Glock";
	case WEAPON_CZ75A:
		return "CZ75";
	case WEAPON_P250:
		return "P250";
	case WEAPON_FIVESEVEN:
		return "FiveSeven";
	case WEAPON_DEAGLE:
		return "Deagle";
	case WEAPON_ELITE:
		return "Duals";
	case WEAPON_TEC9:
		return "Tec9";
	case WEAPON_HKP2000:
		return "P2000";
	case WEAPON_USP_SILENCER:
		return "USPS";
	case WEAPON_REVOLVER:
		return "R8";
	case WEAPON_MAC10:
		return "Mac10";
	case WEAPON_MP9:
		return "MP9";
	case WEAPON_MP7:
		return "MP7";
	case WEAPON_UMP45:
		return "UMP45";
	case WEAPON_BIZON:
		return "Bizon";
	case WEAPON_P90:
		return "P90";
	case WEAPON_GALILAR:
		return "Galil";
	case WEAPON_FAMAS:
		return "Famas";
	case WEAPON_AK47:
		return "AK47";
	case WEAPON_M4A1:
		return "M4A1";
	case WEAPON_M4A1_SILENCER:
		return "M4A1S";
	case WEAPON_SG553:
		return "SG553";
	case WEAPON_AUG:
		return "Aug";
	case WEAPON_SSG08:
		return "Scout";
	case WEAPON_AWP:
		return "AWP";
	case WEAPON_G3SG1:
		return "G3SG1";
	case WEAPON_SCAR20:
		return "Scar20";
	case WEAPON_NOVA:
		return "Nova";
	case WEAPON_XM1014:
		return "XM1014";
	case WEAPON_SAWEDOFF:
		return "SawedOff";
	case WEAPON_MAG7:
		return "Mag7";
	case WEAPON_M249:
		return "M249";
	case WEAPON_NEGEV:
		return "Negev";
	default:
		return "None";
	}
	return "None";
}

bool GameUtils::IsAbleToShoot()
{
	DWORD ClientState = *(DWORD*)FakeLatency::ClientState;

	if (!g_globals)
		return false;

	if (!c_ayala::l_player || !c_ayala::c_weapon)
		return false;

	auto flServerTime = GameUtils::GetCurTime();
	//auto flServerTime = (float)c_ayala::l_player->GetTickBase() * g_globals->interval_per_tick;

	auto flNextPrimaryAttack = c_ayala::c_weapon->NextPrimaryAttack();

	return(!(flNextPrimaryAttack > flServerTime));
}

bool GameUtils::IsBreakableEntity(C_BaseEntity* pBaseEntity)
{
	if (!pBaseEntity)
		return false;

	if (pBaseEntity->GetCollisionGroup() != COLLISION_GROUP_PUSHAWAY && pBaseEntity->GetCollisionGroup() != COLLISION_GROUP_BREAKABLE_GLASS && pBaseEntity->GetCollisionGroup() != COLLISION_GROUP_NONE)
		return false;

	if (pBaseEntity->GetHealth() > 200)
		return false;

	IMultiplayerPhysics* pPhysicsInterface = (IMultiplayerPhysics*)pBaseEntity;
	if (pPhysicsInterface) {
		if (pPhysicsInterface->GetMultiplayerPhysicsMode() != PHYSICS_MULTIPLAYER_SOLID)
			return false;
	}
	else {
		ClientClass * pClass = (ClientClass*)pBaseEntity->GetClientClass();
		if (!string(pClass->m_pNetworkName).compare(XorStr("func_breakable")) || !string(pClass->m_pNetworkName).compare(XorStr("func_breakable_surf"))) {
			if (!string(pClass->m_pNetworkName).compare(XorStr("func_breakable_surf")))
				if (pBaseEntity->IsBroken())
					return false;
		}
		else if (pBaseEntity->PhysicsSolidMaskForEntity() & CONTENTS_PLAYERCLIP)
			return false;
	}

	IBreakableWithPropData* pBreakableInterface = (IBreakableWithPropData*)pBaseEntity;
	if (pBreakableInterface)
		if (pBreakableInterface->GetDmgModBullet() <= 0.0f)
			return false;

	return true;
}
void GameUtils::UTIL_TraceLine_simpleWay(Vector &vecAbsStart, Vector &vecAbsEnd, unsigned int mask, C_BaseEntity *ign, trace_t *tr)
{
	Ray_t ray;

	CTraceFilter filter;
	filter.pSkip1 = ign;

	ray.Init(vecAbsStart, vecAbsEnd);

	g_pEngineTrace->TraceRay(ray, mask, &filter, tr);
}
void GameUtils::UTIL_TraceLine(const Vector & vecAbsStart, const Vector & vecAbsEnd, unsigned int mask, C_BaseEntity * ignore, int collisionGroup, trace_t * ptr)
{
	Ray_t ray;
	CTraceFilter filter;
	filter.pSkip1 = ignore;

	ray.Init(vecAbsStart, vecAbsEnd);
	g_pEngineTrace->TraceRay(ray, mask, &filter, ptr);
}

void GameUtils::UTIL_ClipTraceToPlayers(const Vector & vecAbsStart, const Vector & vecAbsEnd, unsigned int mask, ITraceFilter * filter, trace_t * tr)
{
	Ray_t ray;
	ray.Init(vecAbsStart, vecAbsEnd);

	trace_t trace;
	float flTempFraction = tr->fraction;

	for (int i = 0; i <= g_globals->max_�lients; i++)
	{
		C_BaseEntity* pEntity = g_pEntitylist->GetClientEntity(i);

		if (pEntity == nullptr || !pEntity->isAlive())
			return;

		if (filter != nullptr && !filter->ShouldHitEntity(pEntity, mask))
			return;

		g_pEngineTrace->ClipRayToC_BaseEntity(ray, mask | CONTENTS_HITBOX, pEntity, &trace);

		if (trace.fraction < flTempFraction)
		{
			*tr = trace;
			flTempFraction = trace.fraction;
		}
	}
}
