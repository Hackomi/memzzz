#pragma once

class CFixMove
{
public:
	void Start(c_user_cmd* cmd);
	void End(c_user_cmd* cmd);
private:
	float forward, right, up;
	Vector viewforward, viewright, viewup, aimforward, aimright, aimup, vForwardNorm, vRightNorm, vUpNorm;
	QAngle oldAngle;
}; extern CFixMove* g_FixMove;
