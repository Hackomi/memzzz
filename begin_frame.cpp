#include "hooks.h"
#include "sdk.h"
#include "bullet_log.h"
#include "global.h"
#include "c_track-manager.hpp"
#include "MaterialHelper.h"
#include "imvisuals.h"
#include "GameUtils.h"

void __fastcall Hooks::g_hkBeginFrame(void* thisptr)
{
	if (!c_ayala::l_player) {
		beginFrame->GetOriginalMethod<BeginFrameFn>(9)(thisptr);
		return;
	}
	
	beginFrame->GetOriginalMethod<BeginFrameFn>(9)(thisptr);
		for (size_t i = 0; i < logs.size(); i++)
		{
			auto current = logs.at(i);
			g_pDebugOverlay->AddBoxOverlay(current.dst, Vector(-2, -2, -2), Vector(2, 2, 2), Vector(0, 0, 0), 0, 0, 205, 120, -1.f);
			if (fabs(g_globals->curtime - current.time) > 3.f)
				logs.erase(logs.begin() + i);
		}
}
enum
{
	STUDIORENDER_DRAW_ENTIRE_MODEL = 0,
	STUDIORENDER_DRAW_OPAQUE_ONLY = 0x01,
	STUDIORENDER_DRAW_TRANSLUCENT_ONLY = 0x02,
	STUDIORENDER_DRAW_GROUP_MASK = 0x03,

	STUDIORENDER_DRAW_NO_FLEXES = 0x04,
	STUDIORENDER_DRAW_STATIC_LIGHTING = 0x08,

	STUDIORENDER_DRAW_ACCURATETIME = 0x10,		// Use accurate timing when drawing the model.
	STUDIORENDER_DRAW_NO_SHADOWS = 0x20,
	STUDIORENDER_DRAW_GET_PERF_STATS = 0x40,

	STUDIORENDER_DRAW_WIREFRAME = 0x80,

	STUDIORENDER_DRAW_ITEM_BLINK = 0x100,

	STUDIORENDER_SHADOWDEPTHTEXTURE = 0x200
};
void __fastcall Hooks::DrawModel(void* thisptr, void* edx, DrawModelResults_t* pResults, const DrawModelInfo_t& info, VMatrix *pBoneToWorld, float *pFlexWeights, float *pFlexDelayedWeights, const Vector &modelOrigin, int flags)
{
	beginFrame->GetOriginalMethod<DrawModelFn>(29)(thisptr, pResults, info, pBoneToWorld, pFlexWeights, pFlexDelayedWeights, modelOrigin, flags);

	/*static IMaterial* CoveredFlat = g_MaterialHelper->CreateGlowMaterial(true, false, 1.f, Menu.Colors.ChamsHistory[0], Menu.Colors.ChamsHistory[1], Menu.Colors.ChamsHistory[2]);
	float Green[3] = { 0, 1, 0 };
	C_BaseEntity* pEnt = (C_BaseEntity*)((DWORD)info.m_pClientEntity - 4);
	studiohdr_t* hdr = info.m_pStudioHdr;
	if ((int)pEnt == 0xfffffffc && hdr)
	{
		g_pStudioRender->SetColorModulation(Green);
		g_pModelRender->ForcedMaterialOverride(CoveredFlat);
		beginFrame->GetOriginalMethod<DrawModelFn>(29)(thisptr, pResults, info, pBoneToWorld, pFlexWeights, pFlexDelayedWeights, modelOrigin, flags);
		return;
	}*/
	//if (c_ayala::c_weapon)
	//{
	//	model_t* LocalWeaponModel = c_ayala::c_weapon->GetModel();
	//	if (LocalWeaponModel && hdr && hdr == g_pModelInfo->GetStudioModel(LocalWeaponModel))
	//	{
	//		for (int i = 0; i < g_globals->max_�lients; i++)
	//		{
	//			C_BaseEntity* pBackTrackEnt = g_pEntitylist->GetClientEntity(i);
	//			if (!pBackTrackEnt
	//				|| pBackTrackEnt == c_ayala::l_player
	//				|| !c_ayala::l_player
	//				|| !pBackTrackEnt->IsPlayer()
	//				|| !pBackTrackEnt->isAlive()
	//				|| !pBackTrackEnt->IsEnemy())
	//				continue;

	//			auto Ticks = CBackTrackManager::Get().GetScanTicks(pBackTrackEnt);
	//			for (auto Tick : Ticks)
	//			{
	//				//g_pStudioRender->SetColorModulation(Green);
	//				//g_pModelRender->ForcedMaterialOverride(CoveredFlat);
	//				DrawModelInfo_t Info = info;
	//				Info.m_pClientEntity = pBackTrackEnt + 0x4;
	//				const model_t* EntModel = pBackTrackEnt->GetModel();
	//				if (!EntModel)
	//					continue;
	//				//Info.m_pStudioHdr = g_pModelInfo->GetStudioModel(EntModel);
	//				beginFrame->GetOriginalMethod<DrawModelFn>(29)(thisptr, pResults, Info, Tick.boneMatrix, pFlexWeights, pFlexDelayedWeights, modelOrigin, flags);
	//			}
	//		}
	//	}
	//}
}
	//g_pStudioRender->SetColorModulation(Green);
	